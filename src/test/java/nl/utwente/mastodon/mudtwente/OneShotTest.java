package nl.utwente.mastodon.mudtwente;

import com.google.common.collect.Lists;
import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.Quest;
import nl.utwente.mastodon.mudtwente.entities.itemsystem.Inventory;
import nl.utwente.mastodon.mudtwente.entities.itemsystem.ShopItem;
import nl.utwente.mastodon.mudtwente.entities.npcsystem.NPC;
import nl.utwente.mastodon.mudtwente.entities.npcsystem.NPCText;
import nl.utwente.mastodon.mudtwente.entities.songsystem.PlayerSong;
import nl.utwente.mastodon.mudtwente.repositories.MUDCharacterRepository;
import nl.utwente.mastodon.mudtwente.repositories.QuestRepository;
import nl.utwente.mastodon.mudtwente.repositories.RepositoryWrapper;
import nl.utwente.mastodon.mudtwente.repositories.fightsystem.EnemyInstanceRepository;
import nl.utwente.mastodon.mudtwente.repositories.fightsystem.EnemyTypeRepository;
import nl.utwente.mastodon.mudtwente.repositories.fightsystem.FightSystemRepositoryWrapper;
import nl.utwente.mastodon.mudtwente.repositories.itemsystem.InventoryRepository;
import nl.utwente.mastodon.mudtwente.repositories.itemsystem.ItemRepository;
import nl.utwente.mastodon.mudtwente.repositories.itemsystem.ItemSystemRepositoryWrapper;
import nl.utwente.mastodon.mudtwente.repositories.itemsystem.ShopitemRepository;
import nl.utwente.mastodon.mudtwente.repositories.locationsystem.LocationRepository;
import nl.utwente.mastodon.mudtwente.repositories.locationsystem.LocationSystemRepositoryWrapper;
import nl.utwente.mastodon.mudtwente.repositories.locationsystem.ShopRepository;
import nl.utwente.mastodon.mudtwente.repositories.npcsystem.NPCRepository;
import nl.utwente.mastodon.mudtwente.repositories.npcsystem.NPCSystemRepositoryWrapper;
import nl.utwente.mastodon.mudtwente.repositories.npcsystem.NPCTextRepository;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.PuzzleInstanceRepository;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.PuzzleSystemRepositoryWrapper;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.ScaleRepository;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.puzzle_instances.BinaryPuzzleInstanceRepository;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.puzzle_instances.LightsPuzzleInstanceRepository;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.puzzle_instances.PuzzleInstancesRepositoryWrapper;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.puzzle_instances.RiddlePuzzleInstanceRepository;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.puzzle_instances.balancepuzzle_instance.BalancePuzzleInstanceRepository;
import nl.utwente.mastodon.mudtwente.repositories.songsystem.PlayerSongRepository;
import nl.utwente.mastodon.mudtwente.repositories.songsystem.SongRepository;
import nl.utwente.mastodon.mudtwente.repositories.songsystem.SongSystemRepositoryWrapper;
import nl.utwente.mastodon.mudtwente.services.MUDCharacterService;
import nl.utwente.mastodon.mudtwente.services.fightsystem.EnemyInstanceService;
import nl.utwente.mastodon.mudtwente.services.itemsystem.InventoryService;
import nl.utwente.mastodon.mudtwente.services.locationsystem.LocationService;
import nl.utwente.mastodon.mudtwente.services.locationsystem.ShopService;
import nl.utwente.mastodon.mudtwente.services.npcsystem.NPCService;
import nl.utwente.mastodon.mudtwente.services.npcsystem.NPCTextService;
import nl.utwente.mastodon.mudtwente.services.puzzlesystem.SafeService;
import nl.utwente.mastodon.mudtwente.services.songsystem.PlayerSongService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static nl.utwente.mastodon.mudtwente.utils.Constants.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OneShotTest {
    private RepositoryWrapper repositoryWrapper;
    private List<String> wrongAnswers;

    @Mock private MUDCharacterRepository mudCharacterRepositoryMock;
    @Mock private LocationRepository locationRepositoryMock;
    @Mock private InventoryRepository inventoryRepositoryMock;
    @Mock private ShopRepository shopRepositoryMock;
    @Mock private QuestRepository questRepositoryMock;
    @Mock private ItemRepository itemRepositoryMock;
    @Mock private ShopitemRepository shopitemRepositoryMock;
    @Mock private NPCRepository npcRepositoryMock;
    @Mock private EnemyTypeRepository enemyTypeRepositoryMock;
    @Mock private EnemyInstanceRepository enemyInstanceRepositoryMock;
    @Mock private NPCTextRepository npcTextRepositoryMock;
    @Mock private SongRepository songRepositoryMock;
    @Mock private PlayerSongRepository playerSongRepositoryMock;
    @Mock private PuzzleInstanceRepository puzzleInstanceRepositoryMock;
    @Mock private RiddlePuzzleInstanceRepository riddlePuzzleInstanceRepositoryMock;
    @Mock private BalancePuzzleInstanceRepository balancePuzzleInstanceRepositoryMock;
    @Mock private BinaryPuzzleInstanceRepository binaryPuzzleInstanceRepositoryMock;
    @Mock private LightsPuzzleInstanceRepository lightsPuzzleInstanceRepositoryMock;
    @Mock private ScaleRepository scaleRepositoryMock;

    @InjectMocks private MUDCharacterService mudCharacterService;
    @InjectMocks private InventoryService inventoryService;
    @InjectMocks private ShopService shopService;
    @InjectMocks private NPCService npcService;
    @InjectMocks private NPCTextService npcTextService;
    @InjectMocks private EnemyInstanceService enemyInstanceService;
    @InjectMocks private PlayerSongService playerSongService;
    @InjectMocks private LocationService locationService;
    @InjectMocks private SafeService safeService;

    @Before
    public void executeBefore() {
        NPCSystemRepositoryWrapper npcSystemRepositoryWrapper = new NPCSystemRepositoryWrapper(npcRepositoryMock, npcTextRepositoryMock);
        ItemSystemRepositoryWrapper itemSystemRepositoryWrapper = new ItemSystemRepositoryWrapper(inventoryRepositoryMock, itemRepositoryMock, shopitemRepositoryMock);
        FightSystemRepositoryWrapper fightSystemRepositoryWrapper = new FightSystemRepositoryWrapper(enemyInstanceRepositoryMock, enemyTypeRepositoryMock);
        SongSystemRepositoryWrapper songSystemRepositoryWrapper = new SongSystemRepositoryWrapper(songRepositoryMock, playerSongRepositoryMock);
        LocationSystemRepositoryWrapper locationSystemRepositoryWrapper = new LocationSystemRepositoryWrapper(locationRepositoryMock, shopRepositoryMock);
        PuzzleInstancesRepositoryWrapper puzzleInstancesRepositoryWrapper = new PuzzleInstancesRepositoryWrapper(riddlePuzzleInstanceRepositoryMock, balancePuzzleInstanceRepositoryMock, binaryPuzzleInstanceRepositoryMock, lightsPuzzleInstanceRepositoryMock, scaleRepositoryMock);
        PuzzleSystemRepositoryWrapper puzzleSystemRepositoryWrapper = new PuzzleSystemRepositoryWrapper(puzzleInstanceRepositoryMock, puzzleInstancesRepositoryWrapper);

        repositoryWrapper = new RepositoryWrapper(locationSystemRepositoryWrapper,
                npcSystemRepositoryWrapper,
                itemSystemRepositoryWrapper,
                questRepositoryMock,
                fightSystemRepositoryWrapper,
                songSystemRepositoryWrapper,
                puzzleSystemRepositoryWrapper);

        wrongAnswers = Lists.newArrayList(" To whom do you want to talk?", " Unknown NPC!",
                " It is currently not possible to talk with this NPC!", " You do not got access yet to this npc/quest!",
                " There is no enemy at this location!");

        MUDCharacter mudCharacter = new MUDCharacter();
        mudCharacter.setId(1);
        mudCharacter.setName("Jantje");
        mudCharacter.setStoryLevel(1);
        mudCharacter.setHealth(100000);
        mudCharacter.setUsername("@kahraman11");
        mudCharacter.setLastMessageAt(LocalDateTime.now());
        List<MUDCharacter> jantjeList = new ArrayList<>();
        List<ShopItem> shopItemList = new ArrayList<>();
        HashMap<Integer, List<NPCText>> npcTexts = new HashMap<>();
        jantjeList.add(mudCharacter);
        when(mudCharacterRepositoryMock.findByName("Jantje")).thenReturn(jantjeList);

        when(npcRepositoryMock.findByName(BIOLOGY_STUDENT.getName())).thenReturn(Optional.of(BIOLOGY_STUDENT));
        when(npcRepositoryMock.findByName(EXAMINATION_BOARD.getName())).thenReturn(Optional.of(EXAMINATION_BOARD));
        when(npcRepositoryMock.findByName(UNCLE_DUO.getName())).thenReturn(Optional.of(UNCLE_DUO));
        when(npcRepositoryMock.findByName(NANOLAB_STUDENT.getName())).thenReturn(Optional.of(NANOLAB_STUDENT));
        when(npcRepositoryMock.findByName(CREATIVE_TECHNOLOGY_STUDENT.getName())).thenReturn(Optional.of(CREATIVE_TECHNOLOGY_STUDENT));
        when(npcRepositoryMock.findByName(RUGBY_PLAYER.getName())).thenReturn(Optional.of(RUGBY_PLAYER));
        when(npcRepositoryMock.findByName(DREAM.getName())).thenReturn(Optional.of(DREAM));
        when(npcRepositoryMock.findByName(INFO_NPC.getName())).thenReturn(Optional.of(INFO_NPC));
        when(npcRepositoryMock.findByName(COMMANDS_NPC.getName())).thenReturn(Optional.of(COMMANDS_NPC));
        when(npcRepositoryMock.findByName(DRUNK_MATE.getName())).thenReturn(Optional.of(DRUNK_MATE));
        when(npcRepositoryMock.findByName(DOCTOR_NPC.getName())).thenReturn(Optional.of(DOCTOR_NPC));
        when(npcRepositoryMock.findByName(CULTURE_NPC.getName())).thenReturn(Optional.of(CULTURE_NPC));
        when(npcRepositoryMock.findByName(BARTENDER.getName())).thenReturn(Optional.of(BARTENDER));
        when(npcRepositoryMock.findByName(BARTENDERS_BOSS.getName())).thenReturn(Optional.of(BARTENDERS_BOSS));
        when(npcRepositoryMock.findByName(EXIT_NPC.getName())).thenReturn(Optional.of(EXIT_NPC));
        when(npcRepositoryMock.findByName(HOUSE_MATE_NPC.getName())).thenReturn(Optional.of(HOUSE_MATE_NPC));
        when(npcRepositoryMock.findByName(ITALIAN_MAFIA_MEMBER.getName())).thenReturn(Optional.of(ITALIAN_MAFIA_MEMBER));
        when(npcRepositoryMock.findByName(SMART_STUDENT.getName())).thenReturn(Optional.of(SMART_STUDENT));
        when(npcRepositoryMock.findByName(BLAKE.getName())).thenReturn(Optional.of(BLAKE));
        when(npcRepositoryMock.findByName(ICTS_NPC.getName())).thenReturn(Optional.of(ICTS_NPC));
        when(npcRepositoryMock.findByName(JAIR.getName())).thenReturn(Optional.of(JAIR));
        when(npcRepositoryMock.findByName(ACCOUNTANT.getName())).thenReturn(Optional.of(ACCOUNTANT));
        when(npcRepositoryMock.findByName(NOBODY.getName())).thenReturn(Optional.of(NOBODY));
        when(npcRepositoryMock.findByName(DJMADPROFESSOR.getName())).thenReturn(Optional.of(DJMADPROFESSOR));

        when(locationRepositoryMock.findById(40)).thenReturn(Optional.of(CUBICUS1));
        when(locationRepositoryMock.findById(41)).thenReturn(Optional.of(CUBICUS2));
        when(locationRepositoryMock.findById(42)).thenReturn(Optional.of(CUBICUS3));
        when(locationRepositoryMock.findById(43)).thenReturn(Optional.of(CUBICUS4));
        when(locationRepositoryMock.findById(44)).thenReturn(Optional.of(CUBICUS5));
        when(locationRepositoryMock.findById(46)).thenReturn(Optional.of(CUBICUS7));
        when(locationRepositoryMock.findById(47)).thenReturn(Optional.of(CUBICUS8));
        when(locationRepositoryMock.findById(48)).thenReturn(Optional.of(CUBICUS9));

        npcTexts.put(1, Lists.newArrayList(BIOLOGY_STUDENT_TEXT_1, BIOLOGY_STUDENT_TEXT_2));
        npcTexts.put(2, Lists.newArrayList(EXAMINATION_BOARD_TEXT_1,
                EXAMINATION_BOARD_TEXT_2,
                EXAMINATION_BOARD_TEXT_3,
                EXAMINATION_BOARD_TEXT_4,
                EXAMINATION_BOARD_TEXT_5,
                EXAMINATION_BOARD_TEXT_6,
                EXAMINATION_BOARD_TEXT_7));
        npcTexts.put(3, Lists.newArrayList(UNCLE_DUO_TEXT_1));
        npcTexts.put(4, Lists.newArrayList(NANOLAB_STUDENT_TEXT_1, NANOLAB_STUDENT_TEXT_2));
        npcTexts.put(5, Lists.newArrayList(CREATE_STUDENT_TEXT_1, CREATE_STUDENT_TEXT_2));
        npcTexts.put(6, Lists.newArrayList(RUGBY_PLAYER_TEXT_1, RUGBY_PLAYER_TEXT_2, RUGBY_PLAYER_TEXT_3));
        npcTexts.put(7, Lists.newArrayList(DREAM_TEXT_1));
        npcTexts.put(8, Lists.newArrayList(TUTORIAL_INFO_TEXT_1));
        npcTexts.put(9, Lists.newArrayList(TUTORIAL_COMMANDS_TEXT_1));
        npcTexts.put(10, Lists.newArrayList(TUTORIAL_DRUNK_MATE_TEXT_1));
        npcTexts.put(11, Lists.newArrayList(TUTORIAL_DOCTOR_TEXT_1));
        npcTexts.put(12, Lists.newArrayList(TUTORIAL_CULTURE_TEXT_1));
        npcTexts.put(13, Lists.newArrayList(TUTORIAL_BARTENDER_TEXT_1));
        npcTexts.put(14, Lists.newArrayList(TUTORIAL_BOSS_TEXT_1));
        npcTexts.put(15, Lists.newArrayList(TUTORIAL_EXIT_TEXT_1));
        npcTexts.put(16, Lists.newArrayList(HOUSE_MATE_TEXT_1));
        npcTexts.put(17, Lists.newArrayList(ITALIAN_MAFIA_MEMBER_TEXT_1));
        npcTexts.put(18, Lists.newArrayList(SMART_STUDENT_TEXT_1, SMART_STUDENT_TEXT_2));
        npcTexts.put(19, Lists.newArrayList(BLAKE_TEXT_1));
        npcTexts.put(20, Lists.newArrayList(ICTS_TEXT_1, ICTS_TEXT_2, ICTS_TEXT_3));
        npcTexts.put(21, Lists.newArrayList(JAIR_TEXT_1, JAIR_TEXT_2));
        npcTexts.put(22, Lists.newArrayList(ACCOUNTANT_TEXT_1));
        npcTexts.put(23, Lists.newArrayList(NOBODY_TEXT_1));
        npcTexts.put(24, Lists.newArrayList(DJMADPROFESSOR_TEXT_1));
        when(npcTextService.findByNpc(BIOLOGY_STUDENT)).thenReturn(npcTexts.get(1));
        when(npcTextService.findByNpc(EXAMINATION_BOARD)).thenReturn(npcTexts.get(2));
        when(npcTextService.findByNpc(UNCLE_DUO)).thenReturn(npcTexts.get(3));
        when(npcTextService.findByNpc(NANOLAB_STUDENT)).thenReturn(npcTexts.get(4));
        when(npcTextService.findByNpc(CREATIVE_TECHNOLOGY_STUDENT)).thenReturn(npcTexts.get(5));
        when(npcTextService.findByNpc(RUGBY_PLAYER)).thenReturn(npcTexts.get(6));
        when(npcTextService.findByNpc(DREAM)).thenReturn(npcTexts.get(7));
        when(npcTextService.findByNpc(INFO_NPC)).thenReturn(npcTexts.get(8));
        when(npcTextService.findByNpc(COMMANDS_NPC)).thenReturn(npcTexts.get(9));
        when(npcTextService.findByNpc(DRUNK_MATE)).thenReturn(npcTexts.get(10));
        when(npcTextService.findByNpc(DOCTOR_NPC)).thenReturn(npcTexts.get(11));
        when(npcTextService.findByNpc(CULTURE_NPC)).thenReturn(npcTexts.get(12));
        when(npcTextService.findByNpc(BARTENDER)).thenReturn(npcTexts.get(13));
        when(npcTextService.findByNpc(BARTENDERS_BOSS)).thenReturn(npcTexts.get(14));
        when(npcTextService.findByNpc(EXIT_NPC)).thenReturn(npcTexts.get(15));
        when(npcTextService.findByNpc(HOUSE_MATE_NPC)).thenReturn(npcTexts.get(16));
        when(npcTextService.findByNpc(ITALIAN_MAFIA_MEMBER)).thenReturn(npcTexts.get(17));
        when(npcTextService.findByNpc(SMART_STUDENT)).thenReturn(npcTexts.get(18));
        when(npcTextService.findByNpc(BLAKE)).thenReturn(npcTexts.get(19));
        when(npcTextService.findByNpc(ICTS_NPC)).thenReturn(npcTexts.get(20));
        when(npcTextService.findByNpc(JAIR)).thenReturn(npcTexts.get(21));
        when(npcTextService.findByNpc(ACCOUNTANT)).thenReturn(npcTexts.get(22));
        when(npcTextService.findByNpc(NOBODY)).thenReturn(npcTexts.get(23));
        when(npcTextService.findByNpc(DJMADPROFESSOR)).thenReturn(npcTexts.get(24));

        when(itemRepositoryMock.findByName(PIE.getName())).thenReturn(Optional.of(PIE));

        shopItemList.add(SHOP_ITEM_PIE_COOP);
        when(shopitemRepositoryMock.findByShop(COOP_SHOP)).thenReturn(shopItemList);

        when(enemyTypeRepositoryMock.findByLocationAndLevel(ENEMY_BIOLOGY.getLocation(), ENEMY_BIOLOGY.getLevel())).thenReturn(Optional.of(ENEMY_BIOLOGY));
        when(enemyTypeRepositoryMock.findByLocationAndLevel(ENEMY_ROM_LANGERAK.getLocation(), ENEMY_ROM_LANGERAK.getLevel())).thenReturn(Optional.of(ENEMY_ROM_LANGERAK));
        when(enemyTypeRepositoryMock.findByLocationAndLevel(ENEMY_KLAAS_SIKKEL.getLocation(), ENEMY_KLAAS_SIKKEL.getLevel())).thenReturn(Optional.of(ENEMY_KLAAS_SIKKEL));
        when(enemyTypeRepositoryMock.findByLocationAndLevel(ENEMY_DRUNK_MATE.getLocation(), ENEMY_DRUNK_MATE.getLevel())).thenReturn(Optional.of(ENEMY_DRUNK_MATE));
        when(enemyTypeRepositoryMock.findByLocationAndLevel(ENEMY_BARTENDER.getLocation(), ENEMY_BARTENDER.getLevel())).thenReturn(Optional.of(ENEMY_BARTENDER));
        when(enemyTypeRepositoryMock.findByLocationAndLevel(ENEMY_IN_HOUSE.getLocation(), ENEMY_IN_HOUSE.getLevel())).thenReturn(Optional.of(ENEMY_IN_HOUSE));
        when(enemyTypeRepositoryMock.findByLocationAndLevel(ENEMY_ITALIAN_MAFIA.getLocation(), ENEMY_ITALIAN_MAFIA.getLevel())).thenReturn(Optional.of(ENEMY_ITALIAN_MAFIA));
        when(enemyTypeRepositoryMock.findByLocationAndLevel(ENEMY_BUS.getLocation(), ENEMY_BUS.getLevel())).thenReturn(Optional.of(ENEMY_BUS));
        when(enemyTypeRepositoryMock.findByLocationAndLevel(ENEMY_DJMADPROFESSOR.getLocation(), ENEMY_DJMADPROFESSOR.getLevel())).thenReturn(Optional.of(ENEMY_DJMADPROFESSOR));

        when(shopRepositoryMock.findByLocation(COOP_SHOP.getLocation())).thenReturn(Optional.of(COOP_SHOP));

        List<Inventory> inventoryList = new ArrayList<>();
        inventoryList.add(new Inventory(1, mudCharacter, HAIR, 0));
        inventoryList.add(new Inventory(2, mudCharacter, PIE, 0));
        inventoryList.add(new Inventory(3, mudCharacter, WEAPON_DESIGN_1, 0));
        inventoryList.add(new Inventory(4, mudCharacter, PLASTIC, 0));
        inventoryList.add(new Inventory(5, mudCharacter, PEN, 0));
        inventoryList.add(new Inventory(6, mudCharacter, RUGBY_CLOTHES, 0));
        inventoryList.add(new Inventory(7, mudCharacter, POTION, 0));
        inventoryList.add(new Inventory(8, mudCharacter, SUPER_POTION, 0));
        inventoryList.add(new Inventory(9, mudCharacter, KLAAS_SIKKEL, 0));
        inventoryList.add(new Inventory(10, mudCharacter, COIN, 0));
        inventoryList.add(new Inventory(11, mudCharacter, ROM_LANGERAK_ITEM, 0));
        inventoryList.add(new Inventory(12, mudCharacter, DRUNK_MATE_ITEM, 0));
        inventoryList.add(new Inventory(13, mudCharacter, BARTENDER_ITEM, 0));
        inventoryList.add(new Inventory(14, mudCharacter, MICROPHONE, 0));
        inventoryList.add(new Inventory(15, mudCharacter, THOMAS_CALCULUS, 0));
        inventoryList.add(new Inventory(16, mudCharacter, ENEMY_HOUSE, 0));
        inventoryList.add(new Inventory(17, mudCharacter, ITALIAN_MAFIA_ITEM, 0));
        inventoryList.add(new Inventory(18, mudCharacter, CHINESE_TEXT, 0));
        inventoryList.add(new Inventory(19, mudCharacter, ROSE, 0));
        inventoryList.add(new Inventory(20, mudCharacter, RAM, 0));
        inventoryList.add(new Inventory(21, mudCharacter, CUP, 0));
        when(inventoryRepositoryMock.findByMudCharacterAndItem(mudCharacter, HAIR)).thenReturn(Optional.of(inventoryList.get(0)));
        when(inventoryRepositoryMock.findByMudCharacterAndItem(mudCharacter, PIE)).thenReturn(Optional.of(inventoryList.get(1)));
        when(inventoryRepositoryMock.findByMudCharacterAndItem(mudCharacter, WEAPON_DESIGN_1)).thenReturn(Optional.of(inventoryList.get(2)));
        when(inventoryRepositoryMock.findByMudCharacterAndItem(mudCharacter, PLASTIC)).thenReturn(Optional.of(inventoryList.get(3)));
        when(inventoryRepositoryMock.findByMudCharacterAndItem(mudCharacter, PEN)).thenReturn(Optional.of(inventoryList.get(4)));
        when(inventoryRepositoryMock.findByMudCharacterAndItem(mudCharacter, RUGBY_CLOTHES)).thenReturn(Optional.of(inventoryList.get(5)));
        when(inventoryRepositoryMock.findByMudCharacterAndItem(mudCharacter, POTION)).thenReturn(Optional.of(inventoryList.get(6)));
        when(inventoryRepositoryMock.findByMudCharacterAndItem(mudCharacter, KLAAS_SIKKEL)).thenReturn(Optional.of(inventoryList.get(7)));
        when(inventoryRepositoryMock.findByMudCharacterAndItem(mudCharacter, COIN)).thenReturn(Optional.of(inventoryList.get(8)));
        when(inventoryRepositoryMock.findByMudCharacterAndItem(mudCharacter, ROM_LANGERAK_ITEM)).thenReturn(Optional.of(inventoryList.get(9)));
        when(inventoryRepositoryMock.findByMudCharacterAndItem(mudCharacter, DRUNK_MATE_ITEM)).thenReturn(Optional.of(inventoryList.get(10)));
        when(inventoryRepositoryMock.findByMudCharacterAndItem(mudCharacter, BARTENDER_ITEM)).thenReturn(Optional.of(inventoryList.get(11)));
        when(inventoryRepositoryMock.findByMudCharacterAndItem(mudCharacter, MICROPHONE)).thenReturn(Optional.of(inventoryList.get(12)));
        when(inventoryRepositoryMock.findByMudCharacterAndItem(mudCharacter, THOMAS_CALCULUS)).thenReturn(Optional.of(inventoryList.get(13)));
        when(inventoryRepositoryMock.findByMudCharacterAndItem(mudCharacter, ENEMY_HOUSE)).thenReturn(Optional.of(inventoryList.get(14)));
        when(inventoryRepositoryMock.findByMudCharacterAndItem(mudCharacter, ITALIAN_MAFIA_ITEM)).thenReturn(Optional.of(inventoryList.get(15)));
        when(inventoryRepositoryMock.findByMudCharacterAndItem(mudCharacter, CHINESE_TEXT)).thenReturn(Optional.of(inventoryList.get(16)));
        when(inventoryRepositoryMock.findByMudCharacterAndItem(mudCharacter, ROSE)).thenReturn(Optional.of(inventoryList.get(17)));
        when(inventoryRepositoryMock.findByMudCharacterAndItem(mudCharacter, RAM)).thenReturn(Optional.of(inventoryList.get(18)));
        when(inventoryRepositoryMock.findByMudCharacterAndItem(mudCharacter, CUP)).thenReturn(Optional.of(inventoryList.get(19)));

        PlayerSong sinterklaas = new PlayerSong(1, mudCharacter, SINTERKLAAS);
        when(playerSongRepositoryMock.findByMudCharacter(mudCharacter)).thenReturn(Optional.of(sinterklaas));

        List<Quest> questList = new ArrayList<>();
        questList.add(new Quest(1, mudCharacter, BIOLOGY_STUDENT, 1));
        questList.add(new Quest(1, mudCharacter, EXAMINATION_BOARD, 1));
        questList.add(new Quest(1, mudCharacter, UNCLE_DUO, 1));
        questList.add(new Quest(1, mudCharacter, NANOLAB_STUDENT, 1));
        questList.add(new Quest(1, mudCharacter, CREATIVE_TECHNOLOGY_STUDENT, 1));
        questList.add(new Quest(1, mudCharacter, RUGBY_PLAYER, 1));
        questList.add(new Quest(1, mudCharacter, DREAM, 1));
        questList.add(new Quest(1, mudCharacter, HOUSE_MATE_NPC, 1));
        questList.add(new Quest(1, mudCharacter, ITALIAN_MAFIA_MEMBER, 1));
        questList.add(new Quest(1, mudCharacter, SMART_STUDENT, 1));
        questList.add(new Quest(1, mudCharacter, BLAKE, 1));
        questList.add(new Quest(1, mudCharacter, ICTS_NPC, 1));
        questList.add(new Quest(1, mudCharacter, JAIR, 1));
        questList.add(new Quest(1, mudCharacter, ACCOUNTANT, 1));
        questList.add(new Quest(1, mudCharacter, NOBODY, 1));
        questList.add(new Quest(1, mudCharacter, DJMADPROFESSOR, 1));
        when(questRepositoryMock.findByMudCharacterAndNpc(mudCharacter, BIOLOGY_STUDENT)).thenReturn(Optional.of(questList.get(0)));
        when(questRepositoryMock.findByMudCharacterAndNpc(mudCharacter, EXAMINATION_BOARD)).thenReturn(Optional.of(questList.get(1)));
        when(questRepositoryMock.findByMudCharacterAndNpc(mudCharacter, UNCLE_DUO)).thenReturn(Optional.of(questList.get(2)));
        when(questRepositoryMock.findByMudCharacterAndNpc(mudCharacter, NANOLAB_STUDENT)).thenReturn(Optional.of(questList.get(3)));
        when(questRepositoryMock.findByMudCharacterAndNpc(mudCharacter, CREATIVE_TECHNOLOGY_STUDENT)).thenReturn(Optional.of(questList.get(4)));
        when(questRepositoryMock.findByMudCharacterAndNpc(mudCharacter, RUGBY_PLAYER)).thenReturn(Optional.of(questList.get(5)));
        when(questRepositoryMock.findByMudCharacterAndNpc(mudCharacter, DREAM)).thenReturn(Optional.of(questList.get(6)));
        when(questRepositoryMock.findByMudCharacterAndNpc(mudCharacter, HOUSE_MATE_NPC)).thenReturn(Optional.of(questList.get(7)));
        when(questRepositoryMock.findByMudCharacterAndNpc(mudCharacter, ITALIAN_MAFIA_MEMBER)).thenReturn(Optional.of(questList.get(8)));
        when(questRepositoryMock.findByMudCharacterAndNpc(mudCharacter, SMART_STUDENT)).thenReturn(Optional.of(questList.get(9)));
        when(questRepositoryMock.findByMudCharacterAndNpc(mudCharacter, BLAKE)).thenReturn(Optional.of(questList.get(10)));
        when(questRepositoryMock.findByMudCharacterAndNpc(mudCharacter, ICTS_NPC)).thenReturn(Optional.of(questList.get(11)));
        when(questRepositoryMock.findByMudCharacterAndNpc(mudCharacter, JAIR)).thenReturn(Optional.of(questList.get(12)));
        when(questRepositoryMock.findByMudCharacterAndNpc(mudCharacter, ACCOUNTANT)).thenReturn(Optional.of(questList.get(13)));
        when(questRepositoryMock.findByMudCharacterAndNpc(mudCharacter, NOBODY)).thenReturn(Optional.of(questList.get(14)));
        when(questRepositoryMock.findByMudCharacterAndNpc(mudCharacter, DJMADPROFESSOR)).thenReturn(Optional.of(questList.get(15)));
    }

    @Test
    public void testIsCharacterRegistered() {
        Assert.assertEquals(1, mudCharacterService.findByName("Jantje").size());
        Assert.assertEquals("Jantje", mudCharacterService.findByName("Jantje").get(0).getName());
        Optional<NPC> biologyStudent = npcService.findByName("biology student");
        assertTrue(biologyStudent.isPresent());
    }

    @Test
    public void testTutorial() {
        List<MUDCharacter> mudCharacters = mudCharacterService.findByName("Jantje");
        assertEquals(1, mudCharacters.size());
        MUDCharacter mudCharacter = mudCharacters.get(0);
        mudCharacter.setLocation(REGISTRATION_TENT);

        List<String> words = new ArrayList<>();
        words.add("info");

        String result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));
        mudCharacter.setLocation(CARILLON_TUTORIAL);

        words.clear();
        words.add("commands");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));
        mudCharacter.setLocation(VESTINGBAR_TUTORIAL);

        words.clear();
        words.add("drunk mate");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));
        result = enemyInstanceService.handleAttackCommand(mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));

        Optional<Inventory> inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, ENEMY_DRUNK_MATE.getReward());
        if (inventoryOptional.isPresent() && inventoryOptional.get().getAmount() == 0) {
            inventoryService.addItemToInventory(mudCharacter, ENEMY_DRUNK_MATE.getReward(), 1);
        }

        assertNotNull(ENEMY_DRUNK_MATE.getReward());

        assertTrue("1 drunk mate", inventoryService.findByMudCharacterAndItem(mudCharacter, DRUNK_MATE_ITEM).isPresent());
        assertEquals("1 drunk mate", 1,
                inventoryService.findByMudCharacterAndItem(mudCharacter, DRUNK_MATE_ITEM).get().getAmount());
        mudCharacter.setLocation(MEDICAL_TENT);

        words.clear();
        words.add("doctor");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));
        assertEquals("0 drunk mate", 0,
                inventoryService.findByMudCharacterAndItem(mudCharacter, DRUNK_MATE_ITEM).get().getAmount());
        mudCharacter.setLocation(CULTURE);

        words.clear();
        words.add("culture");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));
        mudCharacter.setLocation(CAMP);

        words.clear();
        words.add("bartender");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));
        result = enemyInstanceService.handleAttackCommand(mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));
        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, BARTENDER_ITEM);
        if (inventoryOptional.isPresent() && inventoryOptional.get().getAmount() == 0) {
            inventoryService.addItemToInventory(mudCharacter,  BARTENDER_ITEM, 1);
        }

        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, BARTENDER_ITEM);
        assertTrue(inventoryOptional.isPresent());

        assertEquals("1 bartender", 1,
                inventoryOptional.get().getAmount());
        mudCharacter.setLocation(PUB);

        words.clear();
        words.add("boss");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));

        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, BARTENDER_ITEM);
        assertTrue(inventoryOptional.isPresent());

        assertEquals("0 bartender", 0,
                inventoryOptional.get().getAmount());
        words.clear();
        words.add("newsong");
        playerSongService.handleNewSongCommand(repositoryWrapper, mudCharacter);
        words.clear();
        words.add("haal");
        words.add("je");
        words.add("pc");
        words.add("er");
        words.add("maar");
        words.add("bij");
        playerSongService.handleSongCommand(words, repositoryWrapper, mudCharacter);
        words.clear();
        words.add("dank");
        words.add("je");
        words.add("sinterklaasje!");
        playerSongService.handleSongCommand(words, repositoryWrapper, mudCharacter);
        words.clear();
        words.add("and");
        words.add("it");
        words.add("feels");
        words.add("like");
        words.add("home");
        playerSongService.handleSongCommand(words, repositoryWrapper, mudCharacter);
        words.clear();
        words.add("with");
        words.add("somebody");
        words.add("who");
        words.add("loves");
        words.add("me");
        playerSongService.handleSongCommand(words, repositoryWrapper, mudCharacter);

        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, MICROPHONE);
        assertTrue(inventoryOptional.isPresent());

        assertEquals("1 microphone", 1,
                inventoryOptional.get().getAmount());
        mudCharacter.setLocation(EXIT);

        words.clear();
        words.add("exit");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));

        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, MICROPHONE);
        assertTrue(inventoryOptional.isPresent());

        assertEquals("0 microphone", 0,
                inventoryOptional.get().getAmount());
        mudCharacter.setLocation(MAIN_ENTRANCE);
        assertEquals("Correct start location", MAIN_ENTRANCE, mudCharacter.getLocation());
    }

    @Test
    public void testMainStoryLine() {
        List<MUDCharacter> mudCharacters = mudCharacterService.findByName("Jantje");
        assertEquals(1, mudCharacters.size());
        MUDCharacter mudCharacter = mudCharacters.get(0);
        mudCharacter.setLocation(MAIN_ENTRANCE);
        mudCharacter.setLocation(NANOLAB);

        assertEquals("Nanolab", NANOLAB, mudCharacter.getLocation());
        List<String> words = new ArrayList<>();
        words.add("biology student");
        String result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));
        result = enemyInstanceService.handleAttackCommand(mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));

        Optional<Inventory> inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, HAIR);
        if (inventoryOptional.isPresent() && inventoryOptional.get().getAmount() == 0) {
            inventoryService.addItemToInventory(mudCharacter, HAIR, 1);
        }

        assertTrue("1 hair", inventoryService.findByMudCharacterAndItem(mudCharacter, HAIR).isPresent());
        words.clear();
        words.add("biology student");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));
        assertEquals("0 hair", 0,
                inventoryService.findByMudCharacterAndItem(mudCharacter, HAIR).get().getAmount());

        mudCharacter.setLocation(ZILVERLING);
        assertEquals("Zilverling", ZILVERLING, mudCharacter.getLocation());

        words.clear();
        words.add("examination board");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));

        mudCharacter.setLocation(VRIJHOF);
        assertEquals("Vrijhof", VRIJHOF, mudCharacter.getLocation());

        words.clear();
        words.add("uncle duo");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));

        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, COIN);
        assertTrue(inventoryOptional.isPresent());

        assertEquals("15 coins", 15, inventoryOptional.get().getAmount());

        mudCharacter.setLocation(COOP);
        assertEquals("Coop", COOP, mudCharacter.getLocation());

        words.clear();
        words.add("pie");
        shopService.handleBuyCommand(repositoryWrapper, mudCharacter, words);
        shopService.handleBuyCommand(repositoryWrapper, mudCharacter, words);
        shopService.handleBuyCommand(repositoryWrapper, mudCharacter, words);

        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, PIE);
        assertTrue(inventoryOptional.isPresent());

        assertEquals("3 pies", 3, inventoryOptional.get().getAmount());

        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, COIN);
        assertTrue(inventoryOptional.isPresent());

        assertEquals("0 coins", 0, inventoryOptional.get().getAmount());

        mudCharacter.setLocation(ZILVERLING);
        assertEquals("Zilverling", ZILVERLING, mudCharacter.getLocation());

        words.clear();
        words.add("examination board");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));

        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, PIE);
        assertTrue(inventoryOptional.isPresent());

        assertEquals("0 pies", 0, inventoryOptional.get().getAmount());

        mudCharacter.setLocation(NANOLAB);
        assertEquals("Nanolab", NANOLAB, mudCharacter.getLocation());

        words.clear();
        words.add("nanolab student");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));

        mudCharacter.setLocation(DESIGN_LAB);
        assertEquals("Design lab", DESIGN_LAB, mudCharacter.getLocation());

        words.clear();
        words.add("creative technology student");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));

        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, PLASTIC);
        assertTrue(inventoryOptional.isPresent());

        mudCharacter.setLocation(ZUL_NORTH);
        assertEquals("ZUL_NORTH", ZUL_NORTH, mudCharacter.getLocation());

        words.clear();
        words.add("east");
        result = locationService.handleGoCommand(words, mudCharacterService, mudCharacter);
        assertEquals("Going East!", "Lets go east!", result);
        assertEquals(CUBICUS5, mudCharacter.getLocation());

        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, PLASTIC);
        assertTrue(inventoryOptional.isPresent());
        assertEquals("0 plastic", 0, inventoryOptional.get().getAmount());

        //RiddlePuzzle
        mudCharacter.setLocation(CUBICUS6);
        result = locationService.handleLocationCommand(mudCharacter);
        assertEquals("Your current location is: Cubicus\n" +
                "North of you is: Cubicus\n" +
                "West of you is: Cubicus\n" +
                "South of you is: Cubicus\n" +
                "East of you is: Cubicus\n" +
                "\n" +
                "There is a puzzle at this location: There is a statue in this room. It says: \"You get three riddles.\n" +
                "If you answer them all correct, you will receive your next hint.\"\n" +
                "Use 'puzzlestatus' to get the current riddle to solve. Use 'answer'\n" +
                "<answer> to answer the riddle. Answers only consist of one word!\n" +
                "\n" +
                "Use the 'checkpuzzle' command to see the information of the puzzle again!", result);

        //LightsPuzzle
        mudCharacter.setLocation(CUBICUS7);
        result = locationService.handleLocationCommand(mudCharacter);
        assertEquals("Your current location is: Cubicus\n" +
                "North of you is: Cubicus\n" +
                "West of you is: Cubicus\n" +
                "South of you is: Cubicus\n" +
                "East of you is: Cubicus\n" +
                "\n" +
                "There is a puzzle at this location: You see six lights and six levers. A note on the wall reads:\n" +
                "\"Your goal is to switch all six lights on. Each lever changes the lights in its own way.\n" +
                "Use 'pull' <leverNumber> in order to pull a lever.\n" +
                "Use 'puzzlestatus' to check the status of this puzzle.\"\n" +
                "Use the 'checkpuzzle' command to see the information of the puzzle again!", result);

        //BinaryPuzzle
        mudCharacter.setLocation(CUBICUS2);
        result = locationService.handleLocationCommand(mudCharacter);
        assertEquals("Your current location is: Cubicus\n" +
                "North of you is: Cubicus\n" +
                "West of you is: Cubicus\n" +
                "South of you is: Cubicus\n" +
                "East of you is: Cubicus\n" +
                "\n" +
                "There is a puzzle at this location: You see five lights and five levers. On the one side of each lever you see \"ON\" and on the \n" +
                "other side \"OFF\". Use the 'pull' <number> command to use them. The lights form your answer \n" +
                "to the riddle. Tip: how do you represent a bigger number with only lights?\n" +
                "Use the 'puzzlestatus' command to get the current riddle to solve!\n" +
                "Use the 'checkpuzzle' command to see the information of the puzzle again!", result);

        //BalancePuzzle
        mudCharacter.setLocation(CUBICUS1);
        result = locationService.handleLocationCommand(mudCharacter);
        assertEquals("Your current location is: Cubicus\n" +
                "North of you is: Cubicus\n" +
                "West of you is: Cubicus\n" +
                "South of you is: Cubicus\n" +
                "East of you is: Cubicus\n" +
                "\n" +
                "There is a puzzle at this location: In this room, you see a balance scale, and 9 weights on the table labeled 0 through 8. \n" +
                "A magical voice whispers in your ear: \"I need you to find the heaviest weight.\n" +
                "Use the scales to identify the heavier weight. Be warned! You only get to weigh the\n" +
                "scales 3 times before the puzzle is reset! If you think you have identified the\n" +
                "heaviest weight, pick it.\" There are six command you can use: \n" +
                "\n" +
                "- placeleft <weightNumber>: places the weight with the given number on the\n" +
                "left side of the scale.\n" +
                "- placeright <weightNumber>: places the weight with the given number on the\n" +
                "right side of the scale.\n" +
                "- placedown <weightNumber>: places the weight with the given number\n" +
                "down from the scale.\n" +
                "- pick: <weightNumber>: used to identify the weight after you have used the\n" +
                "weigh command for 3 times. If incorrect, all weights are magically placed down from\n" +
                "the scale and you have to start over again. \n" +
                "- weigh: used to weigh the scale. Only shows if the scale is in balance or if one ofthe sides is heavier.\n" +
                "- puzzlestatus: shows the current status of this puzzle.\n" +
                "Use the 'checkpuzzle' command to see the information of the puzzle again!", result);

        //TODO insert puzzle testing

        mudCharacter.setLocation(CUBICUS5);
        words.clear();
        words.add("2741");
        assertFalse(wrongAnswers.contains(result));
        safeService.handleTryCodeCommand(mudCharacter, words, inventoryService);

        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, PLASTIC);
        assertTrue(inventoryOptional.isPresent());
        assertEquals("1 plastic", 1, inventoryOptional.get().getAmount());

        mudCharacter.setLocation(DESIGN_LAB);
        words.clear();
        words.add("creative technology student");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));
        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, WEAPON_DESIGN_1);
        assertTrue(inventoryOptional.isPresent());

        assertEquals("1 design", 1, inventoryOptional.get().getAmount());

        mudCharacter.setLocation(NANOLAB);
        assertEquals("Nano lab", NANOLAB, mudCharacter.getLocation());

        words.clear();
        words.add("nanolab student");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));

        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, WEAPON_DESIGN_1);
        assertTrue(inventoryOptional.isPresent());

        assertEquals("0 design", 0, inventoryOptional.get().getAmount());

        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, PEN);
        assertTrue(inventoryOptional.isPresent());

        assertEquals("1 pen", 1, inventoryOptional.get().getAmount());
        result = enemyInstanceService.handleAttackCommand(mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));
        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, ROM_LANGERAK_ITEM);
        if (inventoryOptional.isPresent() && inventoryOptional.get().getAmount() == 0) {
            inventoryService.addItemToInventory(mudCharacter, ROM_LANGERAK_ITEM, 1);
        }

        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, ROM_LANGERAK_ITEM);
        assertTrue(inventoryOptional.isPresent());

        assertEquals("1 rom langerak", 1, inventoryOptional.get().getAmount());

        mudCharacter.setLocation(ZILVERLING);
        assertEquals("Zilverling", ZILVERLING, mudCharacter.getLocation());

        words.clear();
        words.add("examination board");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));

        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, ROM_LANGERAK_ITEM);
        assertTrue(inventoryOptional.isPresent());

        assertEquals("0 rom langerak", 0, inventoryOptional.get().getAmount());

        mudCharacter.setLocation(STUDENT_HOUSING);
        assertEquals("Student housing", STUDENT_HOUSING, mudCharacter.getLocation());

        words.clear();
        words.add("dream");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));

        mudCharacter.setLocation(ATHLETICS_FIELD);
        assertEquals("Athletics field", ATHLETICS_FIELD, mudCharacter.getLocation());

        words.clear();
        words.add("rugby player");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));

        mudCharacter.setLocation(ZILVERLING);
        assertEquals("Zilverling", ZILVERLING, mudCharacter.getLocation());
        result = enemyInstanceService.handleAttackCommand(mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));
        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, KLAAS_SIKKEL);
        if (inventoryOptional.isPresent() && inventoryOptional.get().getAmount() == 0) {
            inventoryService.addItemToInventory(mudCharacter, KLAAS_SIKKEL, 1);
        }

        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, KLAAS_SIKKEL);
        assertTrue(inventoryOptional.isPresent());

        assertEquals("1 Klaas Sikkel", 1, inventoryOptional.get().getAmount());

        mudCharacter.setLocation(ATHLETICS_FIELD);
        assertEquals("Atletics field", ATHLETICS_FIELD, mudCharacter.getLocation());

        words.clear();
        words.add("rugby player");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));

        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, KLAAS_SIKKEL);
        assertTrue(inventoryOptional.isPresent());

        assertEquals("0 Klaas Sikkel", 0, inventoryOptional.get().getAmount());

        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, RUGBY_CLOTHES);
        assertTrue(inventoryOptional.isPresent());

        assertEquals("1 rugby clothes", 1, inventoryOptional.get().getAmount());

        //New part
        words = new ArrayList<>();
        words.add("rugby player");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));

        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, POTION);
        assertTrue(inventoryOptional.isPresent());
        assertEquals("1 potion", 1, inventoryOptional.get().getAmount());

        mudCharacter.setLocation(STUDENT_HOUSING);
        assertEquals("Student housing", STUDENT_HOUSING, mudCharacter.getLocation());

        words = new ArrayList<>();
        words.add("house mate");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));

        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, THOMAS_CALCULUS);
        assertTrue(inventoryOptional.isPresent());
        assertEquals("1 thomas calculus", 1, inventoryOptional.get().getAmount());

        result = enemyInstanceService.handleAttackCommand(mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));
        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, ENEMY_HOUSE);
        if (inventoryOptional.isPresent() && inventoryOptional.get().getAmount() == 0) {
            inventoryService.addItemToInventory(mudCharacter, ENEMY_HOUSE, 1);
        }
        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, ENEMY_HOUSE);
        assertTrue(inventoryOptional.isPresent());
        assertEquals("1 enemy house", 1, inventoryOptional.get().getAmount());

        mudCharacter.setLocation(ZILVERLING);
        assertEquals("Zilverling", ZILVERLING, mudCharacter.getLocation());

        words = new ArrayList<>();
        words.add("examination board");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));
        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, ENEMY_HOUSE);
        assertTrue(inventoryOptional.isPresent());
        assertEquals("0 enemy house", 0, inventoryOptional.get().getAmount());

        mudCharacter.setLocation(GANZENVELD);
        assertEquals("Ganzenveld", GANZENVELD, mudCharacter.getLocation());

        result = enemyInstanceService.handleAttackCommand(mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));
        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, ENEMY_HOUSE);
        if (inventoryOptional.isPresent() && inventoryOptional.get().getAmount() == 0) {
            inventoryService.addItemToInventory(mudCharacter, ITALIAN_MAFIA_ITEM, 1);
        }
        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, ITALIAN_MAFIA_ITEM);
        assertTrue(inventoryOptional.isPresent());
        assertEquals("1 italian maffia item", 1, inventoryOptional.get().getAmount());

        words = new ArrayList<>();
        words.add("italian mafia member");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));
        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, ITALIAN_MAFIA_ITEM);
        assertTrue(inventoryOptional.isPresent());
        assertEquals("0 italian maffia item", 0, inventoryOptional.get().getAmount());
        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, CHINESE_TEXT);
        assertTrue(inventoryOptional.isPresent());
        assertEquals("1 chinese text", 1, inventoryOptional.get().getAmount());

        mudCharacter.setLocation(VRIJHOF);
        assertEquals("Vrijhof", VRIJHOF, mudCharacter.getLocation());

        words = new ArrayList<>();
        words.add("smart student");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));
        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, CHINESE_TEXT);
        assertTrue(inventoryOptional.isPresent());
        assertEquals("0 chinese text", 0, inventoryOptional.get().getAmount());
        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, ROSE);
        assertTrue(inventoryOptional.isPresent());
        assertEquals("10 roses", 10, inventoryOptional.get().getAmount());

        mudCharacter.setLocation(SPORT_CENTER);
        assertEquals("Sport center", SPORT_CENTER, mudCharacter.getLocation());

        words = new ArrayList<>();
        words.add("blake");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));
        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, ROSE);
        assertTrue(inventoryOptional.isPresent());
        assertEquals("0 roses", 0, inventoryOptional.get().getAmount());

        mudCharacter.setLocation(VRIJHOF);
        assertEquals("Vrijhof", VRIJHOF, mudCharacter.getLocation());

        words = new ArrayList<>();
        words.add("smart student");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));

        mudCharacter.setLocation(ZILVERLING);
        assertEquals("Zilverling", ZILVERLING, mudCharacter.getLocation());

        words = new ArrayList<>();
        words.add("examination board");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));

        mudCharacter.setLocation(OO_PLAZA);
        assertEquals("o&o", OO_PLAZA, mudCharacter.getLocation());

        words = new ArrayList<>();
        words.add("icts");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));

        mudCharacter.setLocation(HALLENWEG_WEST);
        assertEquals("Hallenweg West", HALLENWEG_WEST, mudCharacter.getLocation());

        result = enemyInstanceService.handleAttackCommand(mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));
        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, RAM);
        if (inventoryOptional.isPresent() && inventoryOptional.get().getAmount() == 0) {
            inventoryService.addItemToInventory(mudCharacter, RAM, 1);
        }
        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, RAM);
        assertTrue(inventoryOptional.isPresent());
        assertEquals("1 ram", 1, inventoryOptional.get().getAmount());

        mudCharacter.setLocation(OO_PLAZA);
        assertEquals("o&o", OO_PLAZA, mudCharacter.getLocation());

        words = new ArrayList<>();
        words.add("icts");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));
        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, RAM);
        assertTrue(inventoryOptional.isPresent());
        assertEquals("0 ram", 0, inventoryOptional.get().getAmount());

        mudCharacter.setLocation(ZILVERLING);
        assertEquals("Zilverling", ZILVERLING, mudCharacter.getLocation());

        words = new ArrayList<>();
        words.add("jair");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));

        words = new ArrayList<>();
        words.add("examination board");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));

        mudCharacter.setLocation(SPIEGEL);
        assertEquals("Spiegel", SPIEGEL, mudCharacter.getLocation());

        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, RAM);
        assertTrue(inventoryOptional.isPresent());
        int amountCoinsBefore = inventoryOptional.get().getAmount();
        words = new ArrayList<>();
        words.add("accountant");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));
        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, COIN);
        assertTrue(inventoryOptional.isPresent());
        assertEquals("x coins", amountCoinsBefore+20, inventoryOptional.get().getAmount());

        mudCharacter.setLocation(ZILVERLING);
        assertEquals("Zilverling", ZILVERLING, mudCharacter.getLocation());

        amountCoinsBefore = inventoryOptional.get().getAmount();
        words = new ArrayList<>();
        words.add("jair");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));
        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, COIN);
        assertTrue(inventoryOptional.isPresent());
        assertEquals("x coins", amountCoinsBefore-20, inventoryOptional.get().getAmount());

        mudCharacter.setLocation(OO_PLAZA);
        assertEquals("o&o", OO_PLAZA, mudCharacter.getLocation());

        words = new ArrayList<>();
        words.add("icts");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));

        mudCharacter.setLocation(ZILVERLING);
        assertEquals("Zilverling", ZILVERLING, mudCharacter.getLocation());

        words = new ArrayList<>();
        words.add("nobody");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));

        words = new ArrayList<>();
        words.add("examination board");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));

        mudCharacter.setLocation(HORSTTOREN);
        assertEquals("Horsttower", HORSTTOREN, mudCharacter.getLocation());

        words = new ArrayList<>();
        words.add("djmadprofessor");
        result = npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));

        result = enemyInstanceService.handleAttackCommand(mudCharacter, repositoryWrapper, mudCharacterRepositoryMock);
        assertFalse(wrongAnswers.contains(result));
        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, CUP);
        if (inventoryOptional.isPresent() && inventoryOptional.get().getAmount() == 0) {
            inventoryService.addItemToInventory(mudCharacter, CUP, 1);
        }
        inventoryOptional = inventoryService.findByMudCharacterAndItem(mudCharacter, CUP);
        assertTrue(inventoryOptional.isPresent());
        assertEquals("1 cup", 1, inventoryOptional.get().getAmount());
    }
}

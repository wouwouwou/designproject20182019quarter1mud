package nl.utwente.mastodon.mudtwente.services.puzzlesystem;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.locationsystem.Location;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.RiddlePuzzle;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

/**
 * Test for the PuzzleService class.
 */
@RunWith(MockitoJUnitRunner.class)
public class PuzzleServiceTest {

    @InjectMocks private PuzzleService puzzleService;

    private Location location;

    @Before
    public void setUp() {
        RiddlePuzzle riddlePuzzle = new RiddlePuzzle();
        location = new Location();
        location.setId(3);
        riddlePuzzle.setHintAfterCompletion("1234");
        riddlePuzzle.setDescription("etsdfasd");
        riddlePuzzle.setId(3);
        location.setPuzzle(riddlePuzzle);
    }

    /**
     * Tests handleCheckPuzzleCommand method.
     */
    @Test
    public void handleCheckPuzzleCommand() {
        MUDCharacter mudCharacter = new MUDCharacter();
        mudCharacter.setLocation(new Location());
        assertEquals("There is no puzzle at this location to give more information about!",
                puzzleService.handleCheckPuzzleCommand(mudCharacter));
        mudCharacter.setLocation(location);
        assertEquals("More information about the puzzle at this location:\netsdfasd",
                puzzleService.handleCheckPuzzleCommand(mudCharacter));
    }
}

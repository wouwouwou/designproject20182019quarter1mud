package nl.utwente.mastodon.mudtwente.services;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.repositories.MUDCharacterRepository;
import nl.utwente.mastodon.mudtwente.repositories.itemsystem.InventoryRepository;
import nl.utwente.mastodon.mudtwente.utils.Constants;
import org.hamcrest.beans.HasPropertyWithValue;
import org.hamcrest.core.Every;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for the MUDCharacterService class.
 */
@RunWith(MockitoJUnitRunner.class)
public class MUDCharacterServiceTest {

    @Mock private MUDCharacterRepository mudCharacterRepositoryMock;
    @Mock private InventoryRepository inventoryRepositoryMock;

    @InjectMocks private MUDCharacterService mudCharacterService;

    @Before
    public void setUp() {
        MUDCharacter jantje1 = new MUDCharacter();
        MUDCharacter jantje2 = new MUDCharacter();
        MUDCharacter kees1 = new MUDCharacter();
        MUDCharacter kees2 = new MUDCharacter();

        jantje1.setId(33);

        jantje1.setName("Jantje");
        jantje1.setUsername("X");
        kees1.setName("Kees");
        kees2.setName("Kees");

        jantje1.setLocation(Constants.CARILLON_TUTORIAL);
        jantje2.setLocation(Constants.CARILLON_TUTORIAL);
        kees1.setLocation(Constants.DRIENERLOLAAN_NORTH);
        kees2.setLocation(Constants.DRIENERLOLAAN_NORTH);

        LocalDateTime localDateTime = LocalDateTime.now();
        jantje1.setLastMessageAt(localDateTime);
        jantje2.setLastMessageAt(localDateTime);
        kees1.setLastMessageAt(localDateTime);
        kees2.setLastMessageAt(localDateTime);

        List<MUDCharacter> jantjeList = new ArrayList<>();
        List<MUDCharacter> keesList = new ArrayList<>();
        jantjeList.add(jantje1);
        keesList.add(kees1);
        keesList.add(kees2);

        when(mudCharacterRepositoryMock.findById(33)).thenReturn(Optional.of(jantje1));
        when(mudCharacterRepositoryMock.findById(11)).thenReturn(Optional.empty());

        when(mudCharacterRepositoryMock.findByName("Henk")).thenReturn(new ArrayList<>());
        when(mudCharacterRepositoryMock.findByName("Jantje")).thenReturn(jantjeList);
        when(mudCharacterRepositoryMock.findByName("Kees")).thenReturn(keesList);

        when(mudCharacterRepositoryMock.findMUDCharacterByUsername("X")).thenReturn(Optional.of(jantje1));

        when(mudCharacterRepositoryMock
                .findMUDCharacterByUsernameAndMastodonAccountId("A", 1))
                .thenReturn(Optional.of(jantje1));
        when(mudCharacterRepositoryMock
                .findMUDCharacterByUsernameAndMastodonAccountId("B", 2))
                .thenReturn(Optional.empty());
    }

    /**
     * Test 2 cases: one with a mudCharacter with a valid id and one without a valid id.
     */
    @Test
    public void findById() {
        Optional<MUDCharacter> mudCharacterOptional1 = mudCharacterService.findById(33);
        Optional<MUDCharacter> mudCharacterOptional2 = mudCharacterService.findById(11);

        assertTrue(mudCharacterOptional1.isPresent());
        assertFalse(mudCharacterOptional2.isPresent());

        assertEquals(33, mudCharacterOptional1.get().getId());
    }

    /**
     * Tests 3 cases: name does not exist, name exists 1 time, name exists multiple times.
     */
    @Test
    public void findByName() {
        assertEquals(0, mudCharacterService.findByName("Henk").size());
        assertEquals(1, mudCharacterService.findByName("Jantje").size());
        assertTrue(mudCharacterService.findByName("Kees").size() > 1);
        assertEquals("Jantje", mudCharacterService.findByName("Jantje").get(0).getName());
        assertThat(mudCharacterService.findByName("Kees"),
                Every.everyItem(HasPropertyWithValue.hasProperty("name", Is.is("Kees"))));
    }

    /**
     * Tests 2 lists: one with all MUDCharacter's online set to true, one with all MUDCharacter's online false.
     */
    @Test
    public void findMUDCharactersByOnline() {
        assertThat(mudCharacterService.getAllOnlinePlayers(),
                Every.everyItem(HasPropertyWithValue.hasProperty("online", Is.is(true))));
        assertThat(mudCharacterService.getAllOnlinePlayers(),
                Every.everyItem(HasPropertyWithValue.hasProperty("online", Is.is(false))));

    }

    /**
     * Tests 4 cases for each combination of location and online.
     */
    @Test
    public void findMUDCharactersByLocationAndOnline() {
        List<MUDCharacter> mudCharacterList1 = mudCharacterService.getAllOnlinePlayersInLocation(Constants.CARILLON_TUTORIAL);
        List<MUDCharacter> mudCharacterList3 = mudCharacterService.getAllOnlinePlayersInLocation(Constants.DRIENERLOLAAN_NORTH);
        assertThat(mudCharacterList1,
                Every.everyItem(HasPropertyWithValue.hasProperty("online", Is.is(true))));
        assertThat(mudCharacterList1,
                Every.everyItem(HasPropertyWithValue.hasProperty("location", Is.is(Constants.CARILLON_TUTORIAL))));
        assertThat(mudCharacterList3,
                Every.everyItem(HasPropertyWithValue.hasProperty("online", Is.is(true))));
        assertThat(mudCharacterList3,
                Every.everyItem(HasPropertyWithValue.hasProperty("location", Is.is(Constants.DRIENERLOLAAN_NORTH))));

    }

    /**
     * Tests save method.
     */
    @Test
    public void save() {
        MUDCharacter mudCharacter = new MUDCharacter();
        mudCharacterService.save(mudCharacter);

        verify(mudCharacterRepositoryMock).save(mudCharacter);
    }

    /**
     * Tests findMUDCharacterByUsernameAndMastodonAccountId method.
     */
    @Test
    public void findMUDCharacterByUsernameAndMastodonAccountId() {
        Optional<MUDCharacter> mudCharacterOptional = mudCharacterService
                .findMUDCharacterByUsernameAndMastodonAccountId("B", 2);
        assertFalse(mudCharacterOptional.isPresent());
        mudCharacterOptional = mudCharacterService
                .findMUDCharacterByUsernameAndMastodonAccountId("A", 1);
        assertTrue(mudCharacterOptional.isPresent());
        assertEquals("Jantje", mudCharacterOptional.get().getName());
    }

    /**
     * Tests handleOnlineCommand method.
     */
    @Test
    public void handleOnlineCommand() {
        assertEquals("The users that are online: ",
                mudCharacterService.handleOnlineCommand());
    }

    /**
     * Tests handleHelpCommand method.
     */
    @Test
    public void handleHelpCommand() {
        assertEquals("go north/west/south/east - Go the given direction\n" +
                        "whichnpcs - Shows the NPCs\n" +
                        "talk {npcname} - Talks to the NPC\n" +
                        "location - Shows current and neighbour locations\n" +
                        "locationinfo - Shows info about the current location\n" +
                        "whattobuy - Shows what is for sale\n" +
                        "buy {itemname} - Buys the item\n" +
                        "whoishere - Shows which players are at the current location\n" +
                        "online - Shows who is online\n" +
                        "toallatlocation {message} - Sends a message to all players at this location.\n" +
                        "sleep - Go sleep for a while to recover health. This might cost you money!\n" +
                        "inventory - Shows what and how many items are in your inventory\n" +
                        "usepotion {potionname} - Uses the potion to recover your health.\n" +
                        "attack - Attacks the current enemy.\n" +
                        "newsong - Gets you a new song if you do not have one already.\n" +
                        "sing {answer} - Try an answer to your current song.\n" +
                        "checksafe - Checks if there is a safe at this location.\n" +
                        "checkpuzzle - Checks if there is a puzzle at this location.\n" +
                        "puzzlestatus - Shows the status of the puzzle at this location.\n",
                mudCharacterService.handleHelpCommand());
    }

    /**
     * Tests handleRegisterCommand method.
     */
    @Test
    public void handleRegisterCommand() {
        //No character name given
        assertEquals("You did not enter 1 character name. Please enter only one name!",
                mudCharacterService.handleRegisterCommand(new ArrayList<>(),
                        1,
                        "D",
                        inventoryRepositoryMock));

        //One name given
        assertEquals("You have been succesfully registered. It is now possible to play the game!\n" +
                        "Use the {location} command to get your current location and use {go north/west/south/east} to " +
                        "walk into a direction.\n" +
                        "Your first quest is to talk to the NPC Info via using the {talk Info} command.\n" +
                        "You start location is the Registration tent at the start of the Kick-In!",
                mudCharacterService.handleRegisterCommand(
                        new ArrayList<>(Collections.singletonList("ASDF")),
                        1,
                        "D",
                        inventoryRepositoryMock));

        //Name already exists
        assertEquals("That username is not available anymore.",
                mudCharacterService.handleRegisterCommand(
                        new ArrayList<>(Collections.singletonList("Jantje")),
                        1,
                        "D",
                        inventoryRepositoryMock));

        //Username already has account
        assertEquals("You already got an account. Use the {help} command to get instructions.",
                mudCharacterService.handleRegisterCommand(
                        new ArrayList<>(Collections.singletonList("Henkie")),
                        1,
                        "X",
                        inventoryRepositoryMock));
    }

    /**
     * Tests handleUsePotionCommand method.
     */
    @Test
    public void handleUsePotionCommand() {
        //TODO implement this test
    }

    /**
     * Tests handleSleepCommand method.
     */
    @Test
    public void handleSleepCommand() {
        //TODO implement this test
    }

    /**
     * Tests handleToAllAtLocationCommand method.
     */
    @Test
    public void handleToAllAtLocationCommand() {
        //TODO implement this test
    }

    /**
     * Tests walkTo method.
     */
    @Test
    public void walkTo() {
        //TODO implement this test
    }

    /**
     * Tests getAllOnlinePlayers method.
     */
    @Test
    public void getAllOnlinePlayers() {
        //TODO implement this test
    }

    /**
     * Tests getAllOnlinePlayersInLocation method.
     */
    @Test
    public void getAllOnlinePlayersInLocation() {
        //TODO implement this test
    }

    /**
     * Tests handleBanCommand method.
     */
    @Test
    public void handleBanCommand() {
        //TODO implement this test
    }

    /**
     * Tests handleUnbanCommand method.
     */
    @Test
    public void handleUnbanCommand() {
        //TODO implement this test
    }

    /**
     * Tests banAndUnbanPlayer method.
     */
    @Test
    public void banAndUnbanPlayer() {
        //TODO implement this test
    }
}

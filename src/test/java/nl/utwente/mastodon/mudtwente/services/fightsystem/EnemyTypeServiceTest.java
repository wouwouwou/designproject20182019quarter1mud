package nl.utwente.mastodon.mudtwente.services.fightsystem;

import nl.utwente.mastodon.mudtwente.entities.fightsystem.EnemyType;
import nl.utwente.mastodon.mudtwente.entities.locationsystem.Location;
import nl.utwente.mastodon.mudtwente.repositories.fightsystem.EnemyTypeRepository;
import nl.utwente.mastodon.mudtwente.utils.Constants;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static nl.utwente.mastodon.mudtwente.utils.Constants.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for the MonsterService class.
 */
@RunWith(MockitoJUnitRunner.class)
public class EnemyTypeServiceTest {

    @Mock
    private EnemyTypeRepository enemyTypeRepositoryMock;

    @InjectMocks
    private EnemyTypeService enemyTypeService;

    @Before
    public void setUp() throws Exception {
        EnemyType enemyType1 = new EnemyType();

        enemyType1.setLocation(Constants.CARILLON_TUTORIAL);
        enemyType1.setLevel(33);

        when(enemyTypeRepositoryMock.findByLocationAndLevel(Constants.CARILLON_TUTORIAL, 33)).thenReturn(Optional.of(enemyType1));
        when(enemyTypeRepositoryMock.findByLocationAndLevel(Constants.CARILLON_TUTORIAL, 11)).thenReturn(Optional.empty());
        when(enemyTypeRepositoryMock.findByLocationAndLevel(new Location(), 33)).thenReturn(Optional.empty());
        when(enemyTypeRepositoryMock.findByLocationAndLevel(new Location(), 11)).thenReturn(Optional.empty());
    }

    /**
     * Test 4 cases: one with a EnemyType and three where it does not return a EnemyType.
     */
    @Test
    public void findByLocationAndLevel() {
        Optional<EnemyType> monsterOptional1 = enemyTypeService.findByLocationAndLevel(Constants.CARILLON_TUTORIAL, 33);
        Optional<EnemyType> monsterOptional2 = enemyTypeService.findByLocationAndLevel(Constants.CARILLON_TUTORIAL, 11);
        Optional<EnemyType> monsterOptional3 = enemyTypeService.findByLocationAndLevel(new Location(), 33);
        Optional<EnemyType> monsterOptional4 = enemyTypeService.findByLocationAndLevel(new Location(), 11);

        assertTrue(monsterOptional1.isPresent());
        assertFalse(monsterOptional2.isPresent());
        assertFalse(monsterOptional3.isPresent());
        assertFalse(monsterOptional4.isPresent());

        assertEquals(Constants.CARILLON_TUTORIAL, monsterOptional1.get().getLocation());
        assertEquals(33, monsterOptional1.get().getLevel());
    }

    @Test
    public void addToDatabase() {
        enemyTypeService.addToDatabase();

        verify(enemyTypeRepositoryMock).save(ENEMY_BIOLOGY);
        verify(enemyTypeRepositoryMock).save(ENEMY_ROM_LANGERAK);
        verify(enemyTypeRepositoryMock).save(ENEMY_KLAAS_SIKKEL);
        verify(enemyTypeRepositoryMock).save(ENEMY_DRUNK_MATE);
        verify(enemyTypeRepositoryMock).save(ENEMY_BARTENDER);
    }
}

package nl.utwente.mastodon.mudtwente.services.songsystem;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.songsystem.PlayerSong;
import nl.utwente.mastodon.mudtwente.entities.songsystem.Song;
import nl.utwente.mastodon.mudtwente.repositories.songsystem.PlayerSongRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Test for the PlayerSongService class.
 */
@RunWith(MockitoJUnitRunner.class)
public class PlayerSongServiceTest {

    @Mock
    private PlayerSongRepository playerSongRepositoryMock;

    @InjectMocks
    private PlayerSongService playerSongService;

    private MUDCharacter mudCharacter;

    private MUDCharacter wrongCharacter;

    private Song song;

    @Before
    public void setUp() throws Exception {
        mudCharacter = new MUDCharacter();
        mudCharacter.setId(32);

        song = new Song();
        song.setAnswer("Somethingwithananswer");

        PlayerSong playerSong1 = new PlayerSong();

        playerSong1.setMudCharacter(mudCharacter);
        playerSong1.setSong(song);

        wrongCharacter = new MUDCharacter();

        when(playerSongRepositoryMock.findByMudCharacter(mudCharacter)).thenReturn(Optional.of(playerSong1));
        when(playerSongRepositoryMock.findByMudCharacter(wrongCharacter)).thenReturn(Optional.empty()); }

    /**
     * Test 2 cases: one with a PlayerSong with the right characterId and one without.
     */
    @Test
    public void getByCharacterId() {
        Optional<PlayerSong> playerSongOptional1 = playerSongService.findByMudCharacter(mudCharacter);
        Optional<PlayerSong> playerSongOptional2 = playerSongService.findByMudCharacter(wrongCharacter);

        assertTrue(playerSongOptional1.isPresent());
        assertFalse(playerSongOptional2.isPresent());

        assertEquals(mudCharacter, playerSongOptional1.get().getMudCharacter());
    }
}

package nl.utwente.mastodon.mudtwente.services.puzzlesystem;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.itemsystem.Item;
import nl.utwente.mastodon.mudtwente.entities.locationsystem.Location;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.Safe;
import nl.utwente.mastodon.mudtwente.services.itemsystem.InventoryService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;

/**
 * Test for the SafeService class.
 */
@RunWith(MockitoJUnitRunner.class)
public class SafeServiceTest {

    @Mock private InventoryService inventoryServiceMock;

    @InjectMocks private SafeService safeService;

    @Before
    public void setUp() throws Exception {

    }

    /**
     * Tests the handleCheckSafe method. Verifies every
     * code branch and line.
     */
    @Test
    public void handleCheckSafeCommand() {
        Location location = new Location();
        location.setId(22);

        MUDCharacter mudCharacter = new MUDCharacter();
        mudCharacter.setLocation(location);

        assertEquals("There is no safe at this location to give more information about!",
                safeService.handleCheckSafeCommand(mudCharacter));

        Safe safe = new Safe();
        safe.setInfo("Hallo daar!");
        location.setSafe(safe);

        assertEquals("More information about the safe at this location:\nHallo daar!",
                safeService.handleCheckSafeCommand(mudCharacter));
    }

    /**
     * Tests the handleTryCodeCommand method. Verifies every
     * code branch and line.
     */
    @Test
    public void handleTryCodeCommand() {
        List<String> words = new ArrayList<>();

        Location location = new Location();
        location.setId(22);

        MUDCharacter mudCharacter = new MUDCharacter();
        mudCharacter.setLocation(location);

        assertEquals("Please follow the 'trycode' command with the code you want to try on the safe. You have not given exactly one code to try.",
                safeService.handleTryCodeCommand(mudCharacter, words, inventoryServiceMock));

        words.add("ASDF");
        words.add("ASDF");
        assertEquals("Please follow the 'trycode' command with the code you want to try on the safe. You have not given exactly one code to try.",
                safeService.handleTryCodeCommand(mudCharacter, words, inventoryServiceMock));

        words.remove(1);
        assertEquals("There is no safe to try your code on at this location!",
                safeService.handleTryCodeCommand(mudCharacter, words, inventoryServiceMock));

        Safe safe = new Safe();
        location.setSafe(safe);

        assertEquals("Please use a numeric code on the safe!",
                safeService.handleTryCodeCommand(mudCharacter, words, inventoryServiceMock));

        words.clear();
        words.add("1234");
        assertEquals("You input the code... and the safe stays locked! That was the wrong code!",
                safeService.handleTryCodeCommand(mudCharacter, words, inventoryServiceMock));
    }

    /**
     * Tests the tryOpen method. Verifies every
     * code branch and line.
     */
    @Test
    public void tryOpen() {
        Location location1 = new Location();
        location1.setId(33);

        Item item1 = new Item();
        item1.setName("Plastic");
        item1.setId(4);

        Safe safe = new Safe();
        safe.setInfo("Hallo daar!");
        safe.setPassCode(2345);
        safe.setItem(item1);

        Item item = new Item();
        item.setName("Plastic");
        item.setId(4);

        Location location2 = new Location();
        location2.setId(22);

        MUDCharacter mudCharacter = new MUDCharacter();
        mudCharacter.setId(33);

        int answer = 1234;
        assertEquals("You input the code... and the safe stays locked! That was the wrong code!",
                safeService.tryOpen(mudCharacter, safe, answer, inventoryServiceMock));

        answer = 2345;
        assertEquals("The safe opens! Inside, you find Plastic. You add it to your inventory.",
                safeService.tryOpen(mudCharacter, safe, answer, inventoryServiceMock));
        verify(inventoryServiceMock).addItemToInventory(mudCharacter, item, 1);
    }
}

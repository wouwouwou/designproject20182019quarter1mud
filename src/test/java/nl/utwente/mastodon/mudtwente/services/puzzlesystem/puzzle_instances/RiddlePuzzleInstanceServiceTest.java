package nl.utwente.mastodon.mudtwente.services.puzzlesystem.puzzle_instances;

import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.BinaryPuzzleInstance;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.RiddlePuzzleInstance;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.RiddlePuzzle;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.puzzle_instances.RiddlePuzzleInstanceRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Test for RiddlePuzzleInstanceService class.
 */
@RunWith(MockitoJUnitRunner.class)
public class RiddlePuzzleInstanceServiceTest {

    @Mock private RiddlePuzzleInstanceRepository riddlePuzzleInstanceRepositoryMock;

    @InjectMocks private RiddlePuzzleInstanceService riddlePuzzleInstanceService;

    private RiddlePuzzleInstance riddlePuzzleInstance;

    @Before
    public void setUp() throws Exception {
        List<String> questions = new ArrayList<>(Arrays.asList("A", "B"));
        List<String> answers = new ArrayList<>(Arrays.asList("1", "0"));
        RiddlePuzzle riddlePuzzle = new RiddlePuzzle();
        riddlePuzzle.setQuestions(questions);
        riddlePuzzle.setAnswers(answers);
        riddlePuzzleInstance = new RiddlePuzzleInstance();
        riddlePuzzleInstance.setPuzzle(riddlePuzzle);
    }

    /**
     * Tests tryAnswer method
     */
    @Test
    public void tryAnswer() {
        //Wrong command usage
        assertEquals("Please use 'answer' <numericAnswer> in order to answer the riddle!",
                riddlePuzzleInstanceService
                        .tryAnswer(new ArrayList<>(),
                                riddlePuzzleInstance));

        //Wrong answer
        assertEquals("You have not given the right answer to the riddle!",
                riddlePuzzleInstanceService
                        .tryAnswer(new ArrayList<>(Collections.singletonList("6")),
                                riddlePuzzleInstance));

        //First time usage
        assertEquals("That answer is correct!\n" +
                        "The next riddle of this puzzle is:\n" +
                        "B",
                riddlePuzzleInstanceService
                        .tryAnswer(new ArrayList<>(Collections.singletonList("1")),
                                riddlePuzzleInstance));

        //Second time usage
        assertEquals("That answer is correct!\n" +
                        "You have solved all the riddles of this puzzle! The statue says: \"\"",
                riddlePuzzleInstanceService
                        .tryAnswer(new ArrayList<>(Collections.singletonList("0")),
                                riddlePuzzleInstance));
    }
}

package nl.utwente.mastodon.mudtwente.services.itemsystem;

import nl.utwente.mastodon.mudtwente.entities.itemsystem.Item;
import nl.utwente.mastodon.mudtwente.repositories.itemsystem.ItemRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static nl.utwente.mastodon.mudtwente.utils.Constants.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for the ItemService class.
 */
@RunWith(MockitoJUnitRunner.class)
public class ItemServiceTest {

    @Mock
    private ItemRepository itemRepositoryMock;

    @InjectMocks
    private ItemService itemService;

    @Before
    public void setUp() throws Exception {
        Item item1 = new Item();

        item1.setName("Healthpotion");

        when(itemRepositoryMock.findByName("Healthpotion")).thenReturn(Optional.of(item1));
        when(itemRepositoryMock.findByName("Green")).thenReturn(Optional.empty());
    }

    /**
     * Test 2 cases: one with an Item with the right name and one without.
     */
    @Test
    public void findByName() {
        Optional<Item> itemOptional1 = itemService.findByName("Healthpotion");
        Optional<Item> itemOptional2 = itemService.findByName("Green");

        assertTrue(itemOptional1.isPresent());
        assertFalse(itemOptional2.isPresent());

        assertEquals("Healthpotion", itemOptional1.get().getName());
    }

    @Test
    public void findById() {
        //TODO: Implement this
    }

    /**
     * Tests addToDatabase method.
     */
    @Test
    public void addToDatabase() {
        itemService.addToDatabase();

        verify(itemRepositoryMock).save(HAIR);
        verify(itemRepositoryMock).save(PIE);
        verify(itemRepositoryMock).save(WEAPON_DESIGN_1);
        verify(itemRepositoryMock).save(PLASTIC);
        verify(itemRepositoryMock).save(PEN);
        verify(itemRepositoryMock).save(RUGBY_CLOTHES);
        verify(itemRepositoryMock).save(POTION);
        verify(itemRepositoryMock).save(SUPER_POTION);
        verify(itemRepositoryMock).save(KLAAS_SIKKEL);
        verify(itemRepositoryMock).save(COIN);
        verify(itemRepositoryMock).save(ROM_LANGERAK_ITEM);
        verify(itemRepositoryMock).save(DRUNK_MATE_ITEM);
        verify(itemRepositoryMock).save(BARTENDER_ITEM);
        verify(itemRepositoryMock).save(MICROPHONE);
    }
}

package nl.utwente.mastodon.mudtwente.services.itemsystem;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.itemsystem.Inventory;
import nl.utwente.mastodon.mudtwente.entities.itemsystem.Item;
import nl.utwente.mastodon.mudtwente.repositories.itemsystem.InventoryRepository;
import org.hamcrest.beans.HasPropertyWithValue;
import org.hamcrest.core.Every;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Test for the InventoryService class.
 */
@RunWith(MockitoJUnitRunner.class)
public class InventoryServiceTest {

    @Mock
    private InventoryRepository inventoryRepositoryMock;

    @InjectMocks
    private InventoryService inventoryService;

    private MUDCharacter character1;

    private MUDCharacter character2;

    private Item item;

    @Before
    public void setUp() throws Exception {
        Inventory inventory1 = new Inventory();
        Inventory inventory2 = new Inventory();
        Inventory inventory3 = new Inventory();
        character1 = new MUDCharacter();
        character1.setId(22);
        character2 = new MUDCharacter();
        character2.setId(33);

        item = new Item();
        item.setName("Koekjes");

        inventory1.setMudCharacter(character1);
        inventory2.setMudCharacter(character2);
        inventory3.setMudCharacter(character2);

        inventory2.setItem(item);

        inventory2.setAmount(3);

        List<Inventory> inventoryList1 = new ArrayList<>();
        List<Inventory> inventoryList2 = new ArrayList<>();

        inventoryList1.add(inventory1);
        inventoryList2.add(inventory2);
        inventoryList2.add(inventory3);

        when(inventoryRepositoryMock.findByMudCharacter(character1)).thenReturn(inventoryList1);
        when(inventoryRepositoryMock.findByMudCharacter(character2)).thenReturn(inventoryList2);

        when(inventoryRepositoryMock.findByMudCharacterAndItem(character2, item)).thenReturn(Optional.of(inventory2));
        when(inventoryRepositoryMock.findByMudCharacterAndItem(character2, new Item())).thenReturn(Optional.empty());
    }

    /**
     * Tests adding an rewardAmount of an item to the inventory. Also tests retracting more than the available rewardAmount and adding
     * to a non-existing inventory
     */
    @Test
    public void addItemToInventory() {
        Optional<Inventory> inventoryOptional1 = inventoryService.findByMudCharacterAndItem(character2, item);

        assertTrue(inventoryOptional1.isPresent());

        Inventory inventory = inventoryOptional1.get();

        assertEquals(character2, inventory.getMudCharacter());
        assertEquals(item, inventory.getItem());
        assertEquals(3, inventory.getAmount());

        inventory = inventoryService.addItemToInventory(character2, item, 5);

        assertEquals(8, inventory.getAmount());

        inventory = inventoryService.addItemToInventory(character2, item, -10);

        assertEquals(0, inventory.getAmount());

        MUDCharacter mudCharacter = new MUDCharacter();

        inventory = inventoryService.addItemToInventory(mudCharacter, new Item(), 2);

        assertEquals(mudCharacter, inventory.getMudCharacter());
        assertEquals(new Item(), inventory.getItem());
        assertEquals(2, inventory.getAmount());
    }


    /**
     * Test 4 cases: one with an Inventory and three where it does not return a Inventory.
     */
    @Test
    public void findByMudCharacterIdAndItemId() {
        Optional<Inventory> inventoryOptional1 = inventoryService.findByMudCharacterAndItem(character2, item);
        Optional<Inventory> inventoryOptional2 = inventoryService.findByMudCharacterAndItem(character2, new Item());
        Optional<Inventory> inventoryOptional3 = inventoryService.findByMudCharacterAndItem(new MUDCharacter(), item);
        Optional<Inventory> inventoryOptional4 = inventoryService.findByMudCharacterAndItem(new MUDCharacter(), new Item());

        assertTrue(inventoryOptional1.isPresent());
        assertFalse(inventoryOptional2.isPresent());
        assertFalse(inventoryOptional3.isPresent());
        assertFalse(inventoryOptional4.isPresent());

        assertEquals(character2, inventoryOptional1.get().getMudCharacter());
        assertEquals(item, inventoryOptional1.get().getItem());
    }

    /**
     * Tests 3 cases: mudCharacterId does not exist, mudCharacterId exists 1 time, mudCharacterId exists multiple times.
     */
    @Test
    public void findByMudCharacterId() {
        assertEquals(0, inventoryService.findByMudCharacter(new MUDCharacter()).size());
        assertEquals(1, inventoryService.findByMudCharacter(character1).size());
        assertTrue(inventoryService.findByMudCharacter(character2).size() > 1);
        assertEquals(character1, inventoryService.findByMudCharacter(character1).get(0).getMudCharacter());
        assertThat(inventoryService.findByMudCharacter(character2),
                Every.everyItem(HasPropertyWithValue.hasProperty("mudCharacter", Is.is(character2))));
    }
}

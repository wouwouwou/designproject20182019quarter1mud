package nl.utwente.mastodon.mudtwente.services.puzzlesystem;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.locationsystem.Location;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.Puzzle;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.PuzzleInstance;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.BinaryPuzzleInstance;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.LightsPuzzleInstance;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.RiddlePuzzleInstance;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.balancepuzzle_instance.BalancePuzzleInstance;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.BalancePuzzle;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.RiddlePuzzle;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.lightspuzzle.LightsPuzzle;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.lightspuzzle.LightswitchPattern;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.PuzzleInstanceRepository;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.ScaleRepository;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.puzzle_instances.BinaryPuzzleInstanceRepository;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.puzzle_instances.LightsPuzzleInstanceRepository;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.puzzle_instances.RiddlePuzzleInstanceRepository;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.puzzle_instances.balancepuzzle_instance.BalancePuzzleInstanceRepository;
import nl.utwente.mastodon.mudtwente.services.puzzlesystem.puzzle_instances.BinaryPuzzleInstanceService;
import nl.utwente.mastodon.mudtwente.services.puzzlesystem.puzzle_instances.LightsPuzzleInstanceService;
import nl.utwente.mastodon.mudtwente.services.puzzlesystem.puzzle_instances.RiddlePuzzleInstanceService;
import nl.utwente.mastodon.mudtwente.services.puzzlesystem.puzzle_instances.balancepuzzle_instance.BalancePuzzleInstanceService;
import nl.utwente.mastodon.mudtwente.services.puzzlesystem.puzzle_instances.balancepuzzle_instance.ScaleService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for the PuzzleInstanceService class.
 */
@RunWith(MockitoJUnitRunner.class)
public class PuzzleInstanceServiceTest {

    @Mock private BalancePuzzleInstanceRepository balancePuzzleInstanceRepositoryMock;
    @Mock private RiddlePuzzleInstanceRepository riddlePuzzleInstanceRepositoryMock;
    @Mock private BinaryPuzzleInstanceRepository binaryPuzzleInstanceRepositoryMock;
    @Mock private LightsPuzzleInstanceRepository lightsPuzzleInstanceRepositoryMock;
    @Mock private PuzzleInstanceRepository puzzleInstanceRepositoryMock;
    @Mock private ScaleRepository scaleRepositoryMock;

    @InjectMocks private BalancePuzzleInstanceService balancePuzzleInstanceService;
    @InjectMocks private RiddlePuzzleInstanceService riddlePuzzleInstanceService;
    @InjectMocks private BinaryPuzzleInstanceService binaryPuzzleInstanceService;
    @InjectMocks private LightsPuzzleInstanceService lightsPuzzleInstanceService;
    @InjectMocks private PuzzleInstanceService puzzleInstanceService;
    @InjectMocks private ScaleService scaleService;

    private MUDCharacter emptyCharacter;
    private Puzzle emptyPuzzle;

    private MUDCharacter riddleCharacter;
    private MUDCharacter emptyRiddleCharacter;
    private RiddlePuzzle riddlePuzzle;

    private MUDCharacter balanceCharacter;
    private MUDCharacter emptyBalanceCharacter;
    private Location emptyBalanceLocation;
    private BalancePuzzle emptyBalancePuzzle;

    private MUDCharacter lightsCharacter;
    private MUDCharacter emptyLightsCharacter;

    private MUDCharacter binaryCharacter;
    private MUDCharacter emptyBinaryCharacter;

    @Before
    public void setUp() throws Exception {
        // Q&A initialisation
        List<String> questions1 = new ArrayList<>(Collections.singletonList("A"));
        List<String> answers1 = new ArrayList<>(Collections.singletonList("B"));
        List<String> questions2 = new ArrayList<>(Collections.singletonList("D"));
        List<String> answers2 = new ArrayList<>(Collections.singletonList("3"));

        // NoPuzzle initialisation
        emptyCharacter = new MUDCharacter();
        Location emptyLocation = new Location();
        emptyPuzzle = new RiddlePuzzle();
        emptyCharacter.setLocation(emptyLocation);
        when(puzzleInstanceRepositoryMock
                .findByMudCharacterAndPuzzle(emptyCharacter, emptyPuzzle))
                .thenReturn(Optional.empty());

        // RiddlePuzzle initialisation, one returning a PuzzleInstance, one without.
        riddleCharacter = new MUDCharacter();
        Location riddleLocation = new Location();
        riddlePuzzle = new RiddlePuzzle();
        riddleCharacter.setId(1);
        riddleLocation.setId(1);
        riddlePuzzle.setId(1);
        riddlePuzzle.setQuestions(questions1);
        riddlePuzzle.setAnswers(answers1);
        riddleCharacter.setLocation(riddleLocation);
        riddleLocation.setPuzzle(riddlePuzzle);
        PuzzleInstance riddlePuzzleInstance = new RiddlePuzzleInstance();
        riddlePuzzleInstance.setPuzzle(riddlePuzzle);
        emptyRiddleCharacter = new MUDCharacter();
        Location emptyRiddleLocation = new Location();
        RiddlePuzzle emptyRiddlePuzzle = new RiddlePuzzle();
        emptyRiddlePuzzle.setQuestions(questions1);
        emptyRiddlePuzzle.setAnswers(answers1);
        emptyRiddleCharacter.setLocation(emptyRiddleLocation);
        emptyRiddleLocation.setPuzzle(emptyRiddlePuzzle);
        when(puzzleInstanceRepositoryMock
                .findByMudCharacterAndPuzzle(riddleCharacter, riddlePuzzle))
                .thenReturn(Optional.of(riddlePuzzleInstance));
        when(puzzleInstanceRepositoryMock
                .findByMudCharacterAndPuzzle(emptyRiddleCharacter, emptyRiddlePuzzle))
                .thenReturn(Optional.empty());

        // BalancePuzzle initialisation, one returning a PuzzleInstance, one without.
        balanceCharacter = new MUDCharacter();
        Location balanceLocation = new Location();
        BalancePuzzle balancePuzzle = new BalancePuzzle();
        balanceCharacter.setId(1);
        balanceLocation.setId(1);
        balancePuzzle.setId(1);
        balancePuzzle.setHeaviestWeightId(0);
        balancePuzzle.setMaxWeighings(0);
        balancePuzzle.setAmountOfWeights(0);
        balanceCharacter.setLocation(balanceLocation);
        balanceLocation.setPuzzle(balancePuzzle);
        BalancePuzzleInstance balancePuzzleInstance = new BalancePuzzleInstance();
        balancePuzzleInstance.setPuzzle(balancePuzzle);
        emptyBalanceCharacter = new MUDCharacter();
        emptyBalanceLocation = new Location();
        emptyBalancePuzzle = new BalancePuzzle();
        emptyBalanceCharacter.setLocation(emptyBalanceLocation);
        emptyBalanceLocation.setPuzzle(emptyBalancePuzzle);
        when(puzzleInstanceRepositoryMock
                .findByMudCharacterAndPuzzle(balanceCharacter, balancePuzzle))
                .thenReturn(Optional.of(balancePuzzleInstance));
        when(puzzleInstanceRepositoryMock
                .findByMudCharacterAndPuzzle(emptyBalanceCharacter, emptyBalancePuzzle))
                .thenReturn(Optional.empty());

        // LightsPuzzle initialisation, one returning a PuzzleInstance, one without.
        lightsCharacter = new MUDCharacter();
        Location lightsLocation = new Location();
        LightsPuzzle lightsPuzzle = new LightsPuzzle();
        lightsCharacter.setId(1);
        lightsLocation.setId(1);
        lightsPuzzle.setId(1);
        lightsPuzzle.setLightswitchPatterns(
                new ArrayList<>(
                        Collections.singletonList(
                                new LightswitchPattern(
                                        new ArrayList<>(
                                                Arrays.asList(false, false, false, false, false, false))))));
        lightsCharacter.setLocation(lightsLocation);
        lightsLocation.setPuzzle(lightsPuzzle);
        LightsPuzzleInstance lightsPuzzleInstance = new LightsPuzzleInstance();
        lightsPuzzleInstance.setPuzzle(lightsPuzzle);
        emptyLightsCharacter = new MUDCharacter();
        Location emptyLightsLocation = new Location();
        LightsPuzzle emptyLightsPuzzle = new LightsPuzzle();
        emptyLightsPuzzle.setLightswitchPatterns(
                new ArrayList<>(
                        Collections.singletonList(
                                new LightswitchPattern(
                                        new ArrayList<>(
                                                Arrays.asList(false, false, false, false, false, false))))));
        emptyLightsCharacter.setLocation(emptyLightsLocation);
        emptyLightsLocation.setPuzzle(emptyLightsPuzzle);
        when(puzzleInstanceRepositoryMock
                .findByMudCharacterAndPuzzle(lightsCharacter, lightsPuzzle))
                .thenReturn(Optional.of(lightsPuzzleInstance));
        when(puzzleInstanceRepositoryMock
                .findByMudCharacterAndPuzzle(emptyLightsCharacter, emptyLightsPuzzle))
                .thenReturn(Optional.empty());

        // BinaryPuzzle initialisation, one returning a PuzzleInstance, one without.
        binaryCharacter = new MUDCharacter();
        Location binaryLocation = new Location();
        RiddlePuzzle binaryPuzzle = new RiddlePuzzle();
        binaryCharacter.setId(1);
        binaryLocation.setId(1);
        binaryPuzzle.setId(1);
        binaryPuzzle.setQuestions(questions2);
        binaryPuzzle.setAnswers(answers2);
        binaryCharacter.setLocation(binaryLocation);
        binaryLocation.setPuzzle(binaryPuzzle);
        BinaryPuzzleInstance binaryPuzzleInstance = new BinaryPuzzleInstance();
        binaryPuzzleInstance.setPuzzle(binaryPuzzle);
        emptyBinaryCharacter = new MUDCharacter();
        Location emptyBinaryLocation = new Location();
        RiddlePuzzle emptyBinaryPuzzle = new RiddlePuzzle();
        emptyBinaryPuzzle.setQuestions(questions2);
        emptyBinaryPuzzle.setAnswers(answers2);
        emptyBinaryPuzzle.setBinaryPuzzle(true);
        emptyBinaryCharacter.setLocation(emptyBinaryLocation);
        emptyBinaryLocation.setPuzzle(emptyBinaryPuzzle);
        when(puzzleInstanceRepositoryMock
                .findByMudCharacterAndPuzzle(binaryCharacter, binaryPuzzle))
                .thenReturn(Optional.of(binaryPuzzleInstance));
        when(puzzleInstanceRepositoryMock
                .findByMudCharacterAndPuzzle(emptyBinaryCharacter, emptyBinaryPuzzle))
                .thenReturn(Optional.empty());
    }

    /**
     * Tests findByMudCharacterAndPuzzle method. 2 result cases: result
     * Optional including a PuzzleInstance and one without.
     */
    @Test
    public void findByMudCharacterAndPuzzle() {
        //No PuzzleInstance
        Optional<PuzzleInstance> puzzleInstanceOptional = puzzleInstanceService
                .findByMudCharacterAndPuzzle(emptyCharacter, emptyPuzzle);
        assertFalse(puzzleInstanceOptional.isPresent());

        //PuzzleInstance exists
        MUDCharacter mudCharacter1 = new MUDCharacter();
        Puzzle puzzle1 = new RiddlePuzzle();
        mudCharacter1.setId(33);
        puzzle1.setId(33);

        puzzleInstanceOptional = puzzleInstanceService.findByMudCharacterAndPuzzle(riddleCharacter, riddlePuzzle);

        assertTrue(puzzleInstanceOptional.isPresent());
    }

    /**
     * Tests save method
     */
    @Test
    public void save() {
        PuzzleInstance puzzleInstance = new RiddlePuzzleInstance();
        puzzleInstanceService.save(puzzleInstance);

        verify(puzzleInstanceRepositoryMock).save(puzzleInstance);
    }

    /**
     * Tests handlePuzzleStatusCommand method.
     */
    @Test
    public void handlePuzzleStatusCommand() {
        //NoPuzzle
        assertEquals("There is no puzzle at this location to check the status of!",
                puzzleInstanceService.handlePuzzleStatusCommand(emptyCharacter));

        //RiddlePuzzle, puzzleInstance does not exist.
        assertEquals("You have answered 0 out of 1 riddles. Your next riddle is: \n" +
                        "A\n" +
                        "\n" +
                        "Use the 'answer' command to try and solve this riddle.",
                puzzleInstanceService.handlePuzzleStatusCommand(emptyRiddleCharacter));

        //RiddlePuzzle, does not exist.
        assertEquals("You have answered 0 out of 1 riddles. Your next riddle is: \n" +
                        "A\n" +
                        "\n" +
                        "Use the 'answer' command to try and solve this riddle.",
                puzzleInstanceService.handlePuzzleStatusCommand(riddleCharacter));

        //LightsPuzzle, does not exist.
        assertEquals("You see six lights: \n" +
                        "\n" +
                        "-Light 1 is OFF.\n" +
                        "-Light 2 is OFF.\n" +
                        "-Light 3 is OFF.\n" +
                        "-Light 4 is OFF.\n" +
                        "-Light 5 is OFF.\n" +
                        "-Light 6 is OFF.\n" +
                        "\n" +
                        "Use the 'pull' <number> command to change the status of the lights.\n" +
                        "There are 6 levers you can pull here. Each of them changes the lights in a different way.\n" +
                        "Tip: look for a pattern!",
                puzzleInstanceService.handlePuzzleStatusCommand(emptyLightsCharacter));

        //BalancePuzzle, does not exist.
        assertEquals("You have weighed 0 out of 0 attempts.\n" +
                        "The goal is to find the one weight out of 0 which is heavier than the others. You see a scale which you can use. \n" +
                        "\n" +
                        "On the left side of the scale are weights: \n" +
                        "On the right side of the scale are weights: \n" +
                        "On the table are weights: \n" +
                        "\n" +
                        "You can use 'placeleft' to place weights on the left side of the scale.\n" +
                        "'placeright' does the same thing for the right side.\n" +
                        "'placedown' places weights off the scale.",
                puzzleInstanceService.handlePuzzleStatusCommand(emptyBalanceCharacter));

        //RiddlePuzzle with Binary set to true, does not exist.
        assertEquals("You have answered 0 out of 1 riddles.\n" +
                        "Your next riddle is: \n" +
                        "D\n" +
                        "You also see five lights: \n" +
                        "\n" +
                        "-Light 1 is OFF.\n" +
                        "-Light 2 is OFF.\n" +
                        "-Light 3 is OFF.\n" +
                        "-Light 4 is OFF.\n" +
                        "-Light 5 is OFF.\n" +
                        "\n" +
                        "There are five levers with which you can turn the lights on or off. Use the 'pull' <number>\n" +
                        "command to use them. The lights form your answer to the riddle. Tip: how do you represent a bigger\n" +
                        "number with only lights?",
                puzzleInstanceService.handlePuzzleStatusCommand(emptyBinaryCharacter));


    }

    /**
     * Tests handlePlaceLeftCommand method.
     */
    @Test
    public void handlePlaceLeftCommand() {
        //NoPuzzle
        assertEquals("There is no puzzle at this location. Therefore you cannot use this command here!",
                puzzleInstanceService.handlePlaceLeftCommand(emptyCharacter,
                        new ArrayList<>(Collections.singletonList("KJ")),
                        scaleService));

        //Wrong usage command, PuzzleInstance Exists
        assertEquals("Please use 'placeleft' <weightNumber> in order to put a weight " +
                        "on the left side of the scale!",
                puzzleInstanceService.handlePlaceLeftCommand(balanceCharacter,
                        new ArrayList<>(Collections.singletonList("KJ")),
                        scaleService));

        //Right usage command, PuzzleInstance Exists
        assertEquals("This weight does not exist!\n" +
                        "\n" +
                        "You have weighed 0 out of 0 attempts.\n" +
                        "The goal is to find the one weight out of 0 which is heavier than the others. You see a scale which you can use. \n" +
                        "\n" +
                        "On the left side of the scale are weights: \n" +
                        "On the right side of the scale are weights: \n" +
                        "On the table are weights: \n" +
                        "\n" +
                        "You can use 'placeleft' to place weights on the left side of the scale.\n" +
                        "'placeright' does the same thing for the right side.\n" +
                        "'placedown' places weights off the scale.",
                puzzleInstanceService.handlePlaceLeftCommand(balanceCharacter,
                        new ArrayList<>(Collections.singletonList("1")),
                        scaleService));

        //Right usage command, PuzzleInstance does not exist
        emptyBalanceLocation.setPuzzle(emptyBalancePuzzle);
        assertEquals("This weight does not exist!\n" +
                        "\n" +
                        "You have weighed 0 out of 0 attempts.\n" +
                        "The goal is to find the one weight out of 0 which is heavier than the others. You see a scale which you can use. \n" +
                        "\n" +
                        "On the left side of the scale are weights: \n" +
                        "On the right side of the scale are weights: \n" +
                        "On the table are weights: \n" +
                        "\n" +
                        "You can use 'placeleft' to place weights on the left side of the scale.\n" +
                        "'placeright' does the same thing for the right side.\n" +
                        "'placedown' places weights off the scale.",
                puzzleInstanceService.handlePlaceLeftCommand(emptyBalanceCharacter,
                        new ArrayList<>(Collections.singletonList("1")),
                        scaleService));
    }

    /**
     * Tests handlePlaceRightCommand method.
     */
    @Test
    public void handlePlaceRightCommand() {
        //NoPuzzle
        assertEquals("There is no puzzle at this location. Therefore you cannot use this command here!",
            puzzleInstanceService.handlePlaceRightCommand(emptyCharacter,
                    new ArrayList<>(Collections.singletonList("KJ")),
                    scaleService));

        //Wrong usage command, PuzzleInstance Exists
        assertEquals("Please use 'placeright' <weightNumber> in order to put a weight on the right side of the scale!",
                puzzleInstanceService.handlePlaceRightCommand(balanceCharacter,
                        new ArrayList<>(Collections.singletonList("KJ")),
                        scaleService));

        //Right usage command, PuzzleInstance Exists
        assertEquals("This weight does not exist!\n" +
                        "\n" +
                        "You have weighed 0 out of 0 attempts.\n" +
                        "The goal is to find the one weight out of 0 which is heavier than the others. You see a scale which you can use. \n" +
                        "\n" +
                        "On the left side of the scale are weights: \n" +
                        "On the right side of the scale are weights: \n" +
                        "On the table are weights: \n" +
                        "\n" +
                        "You can use 'placeleft' to place weights on the left side of the scale.\n" +
                        "'placeright' does the same thing for the right side.\n" +
                        "'placedown' places weights off the scale.",
                puzzleInstanceService.handlePlaceRightCommand(balanceCharacter,
                        new ArrayList<>(Collections.singletonList("1")),
                        scaleService));

        //Right usage command, PuzzleInstance does not exist
        emptyBalanceLocation.setPuzzle(emptyBalancePuzzle);
        assertEquals("This weight does not exist!\n" +
                        "\n" +
                        "You have weighed 0 out of 0 attempts.\n" +
                        "The goal is to find the one weight out of 0 which is heavier than the others. You see a scale which you can use. \n" +
                        "\n" +
                        "On the left side of the scale are weights: \n" +
                        "On the right side of the scale are weights: \n" +
                        "On the table are weights: \n" +
                        "\n" +
                        "You can use 'placeleft' to place weights on the left side of the scale.\n" +
                        "'placeright' does the same thing for the right side.\n" +
                        "'placedown' places weights off the scale.",
                puzzleInstanceService.handlePlaceRightCommand(emptyBalanceCharacter,
                        new ArrayList<>(Collections.singletonList("1")),
                        scaleService));
    }

    /**
     * Tests handlePlaceDownCommand method.
     */
    @Test
    public void handlePlaceDownCommand() {
        //NoPuzzle
        assertEquals("There is no puzzle at this location. Therefore you cannot use this command here!",
                puzzleInstanceService.handlePlaceDownCommand(emptyCharacter,
                        new ArrayList<>(Collections.singletonList("KJ")),
                        scaleService));

        //Wrong usage command, PuzzleInstance Exists
        assertEquals("Please use 'placedown' <weightNumber> in order to put a weight down from the scale!",
                puzzleInstanceService.handlePlaceDownCommand(balanceCharacter,
                        new ArrayList<>(Collections.singletonList("KJ")),
                        scaleService));

        //Right usage command, PuzzleInstance Exists
        assertEquals("This weight does not exist!\n" +
                        "\n" +
                        "You have weighed 0 out of 0 attempts.\n" +
                        "The goal is to find the one weight out of 0 which is heavier than the others. You see a scale which you can use. \n" +
                        "\n" +
                        "On the left side of the scale are weights: \n" +
                        "On the right side of the scale are weights: \n" +
                        "On the table are weights: \n" +
                        "\n" +
                        "You can use 'placeleft' to place weights on the left side of the scale.\n" +
                        "'placeright' does the same thing for the right side.\n" +
                        "'placedown' places weights off the scale.",
                puzzleInstanceService.handlePlaceDownCommand(balanceCharacter,
                        new ArrayList<>(Collections.singletonList("1")),
                        scaleService));

        //Right usage command, PuzzleInstance does not exist
        emptyBalanceLocation.setPuzzle(emptyBalancePuzzle);
        assertEquals("This weight does not exist!\n" +
                        "\n" +
                        "You have weighed 0 out of 0 attempts.\n" +
                        "The goal is to find the one weight out of 0 which is heavier than the others. You see a scale which you can use. \n" +
                        "\n" +
                        "On the left side of the scale are weights: \n" +
                        "On the right side of the scale are weights: \n" +
                        "On the table are weights: \n" +
                        "\n" +
                        "You can use 'placeleft' to place weights on the left side of the scale.\n" +
                        "'placeright' does the same thing for the right side.\n" +
                        "'placedown' places weights off the scale.",
                puzzleInstanceService.handlePlaceDownCommand(emptyBalanceCharacter,
                        new ArrayList<>(Collections.singletonList("1")),
                        scaleService));
    }

    /**
     * Tests handleAnswerCommand method.
     */
    @Test
    public void handleAnswerCommand() {
        //NoPuzzle
        assertEquals("There is no puzzle at this location. Therefore you cannot use this command here!", puzzleInstanceService
                .handleAnswerCommand(emptyCharacter,
                        new ArrayList<>(Collections.singletonList("KJ")),
                        riddlePuzzleInstanceService));

        //RiddlePuzzle, wrong answer, Instance exists.
        assertEquals("You have not given the right answer to the riddle!", puzzleInstanceService
                .handleAnswerCommand(riddleCharacter,
                        new ArrayList<>(Collections.singletonList("KJ")),
                        riddlePuzzleInstanceService));

        //RiddlePuzzle, wrong answer, Instance does not exist.
        assertEquals("You have not given the right answer to the riddle!", puzzleInstanceService
                .handleAnswerCommand(emptyRiddleCharacter,
                        new ArrayList<>(Collections.singletonList("KJ")),
                        riddlePuzzleInstanceService));

        //RiddlePuzzle, right answer, Instance exists.
        assertEquals("That answer is correct!\n" +
                "You have solved all the riddles of this puzzle! The statue says: \"\"", puzzleInstanceService
                .handleAnswerCommand(riddleCharacter,
                        new ArrayList<>(Collections.singletonList("B")),
                        riddlePuzzleInstanceService));
    }

    /**
     * Tests handlePullCommand method.
     */
    @Test
    public void handlePullCommand() {
        //NoPuzzle
        assertEquals("There is no puzzle at this location. Therefore you cannot use this command here!", puzzleInstanceService
                .handlePullCommand(emptyCharacter,
                        new ArrayList<>(Collections.singletonList("KJ")),
                        lightsPuzzleInstanceService,
                        binaryPuzzleInstanceService));

        //Wrong LightsPuzzle usage
        assertEquals("Please use 'pull' <leverNumber> in order to pull a lever!", puzzleInstanceService
                .handlePullCommand(lightsCharacter,
                        new ArrayList<>(Collections.singletonList("KJ")),
                        lightsPuzzleInstanceService,
                        binaryPuzzleInstanceService));

        //Right LightsPuzzle usage, Instance exists
        assertEquals("You have pulled lever 1.\n" +
                "\n" +
                "You see six lights: \n" +
                "\n" +
                "-Light 1 is OFF.\n" +
                "-Light 2 is OFF.\n" +
                "-Light 3 is OFF.\n" +
                "-Light 4 is OFF.\n" +
                "-Light 5 is OFF.\n" +
                "-Light 6 is OFF.\n" +
                "\n" +
                "Use the 'pull' <number> command to change the status of the lights.\n" +
                "There are 6 levers you can pull here. Each of them changes the lights in a different way.\n" +
                "Tip: look for a pattern!", puzzleInstanceService
                .handlePullCommand(lightsCharacter,
                        new ArrayList<>(Collections.singletonList("1")),
                        lightsPuzzleInstanceService,
                        binaryPuzzleInstanceService));

        //Right LightsPuzzle usage, Instance does not exist
        assertEquals("You have pulled lever 1.\n" +
                "\n" +
                "You see six lights: \n" +
                "\n" +
                "-Light 1 is OFF.\n" +
                "-Light 2 is OFF.\n" +
                "-Light 3 is OFF.\n" +
                "-Light 4 is OFF.\n" +
                "-Light 5 is OFF.\n" +
                "-Light 6 is OFF.\n" +
                "\n" +
                "Use the 'pull' <number> command to change the status of the lights.\n" +
                "There are 6 levers you can pull here. Each of them changes the lights in a different way.\n" +
                "Tip: look for a pattern!", puzzleInstanceService
                .handlePullCommand(emptyLightsCharacter,
                        new ArrayList<>(Collections.singletonList("1")),
                        lightsPuzzleInstanceService,
                        binaryPuzzleInstanceService));

        //Wrong BinaryPuzzle usage
        assertEquals("Please use 'pull' <leverNumber> in order to pull a lever!", puzzleInstanceService
                .handlePullCommand(binaryCharacter,
                        new ArrayList<>(Collections.singletonList("KJ")),
                        lightsPuzzleInstanceService,
                        binaryPuzzleInstanceService));

        //Right BinaryPuzzle usage, Instance exists
        assertEquals("You have pulled lever 0.\n" +
                "\n" +
                "You have answered 0 out of 1 riddles.\n" +
                "Your next riddle is: \n" +
                "D\n" +
                "You also see five lights: \n" +
                "\n" +
                "-Light 1 is ON.\n" +
                "-Light 2 is OFF.\n" +
                "-Light 3 is OFF.\n" +
                "-Light 4 is OFF.\n" +
                "-Light 5 is OFF.\n" +
                "\n" +
                "There are five levers with which you can turn the lights on or off. Use the 'pull' <number>\n" +
                "command to use them. The lights form your answer to the riddle. Tip: how do you represent a bigger\n" +
                "number with only lights?", puzzleInstanceService
                .handlePullCommand(binaryCharacter,
                        new ArrayList<>(Collections.singletonList("1")),
                        lightsPuzzleInstanceService,
                        binaryPuzzleInstanceService));

        //Right BinaryPuzzle usage, Instance does not exist
        assertEquals("You have pulled lever 0.\n" +
                "\n" +
                "You have answered 0 out of 1 riddles.\n" +
                "Your next riddle is: \n" +
                "D\n" +
                "You also see five lights: \n" +
                "\n" +
                "-Light 1 is ON.\n" +
                "-Light 2 is OFF.\n" +
                "-Light 3 is OFF.\n" +
                "-Light 4 is OFF.\n" +
                "-Light 5 is OFF.\n" +
                "\n" +
                "There are five levers with which you can turn the lights on or off. Use the 'pull' <number>\n" +
                "command to use them. The lights form your answer to the riddle. Tip: how do you represent a bigger\n" +
                "number with only lights?", puzzleInstanceService
                .handlePullCommand(emptyBinaryCharacter,
                        new ArrayList<>(Collections.singletonList("1")),
                        lightsPuzzleInstanceService,
                        binaryPuzzleInstanceService));
    }

    /**
     * Tests handleWeighCommand method.
     */
    @Test
    public void handleWeighCommand() {
        //NoPuzzle
        assertEquals("There is no puzzle at this location. Therefore you cannot use this command here!", puzzleInstanceService
                .handleWeighCommand(emptyCharacter,
                        balancePuzzleInstanceService));

        //Puzzle exists, PuzzleInstance exists
        assertEquals("You have reached the maximum amount of weighing attempts! Please pick a weight of the scale!", puzzleInstanceService
                .handleWeighCommand(balanceCharacter,
                        balancePuzzleInstanceService));

        //Puzzle exists, PuzzleInstance doest not exist
        assertEquals("You have not moved any weights yet! \n" +
                "\n" +
                "You have weighed 0 out of 0 attempts.\n" +
                "The goal is to find the one weight out of 0 which is heavier than the others. You see a scale which you can use. \n" +
                "\n" +
                "On the left side of the scale are weights: \n" +
                "On the right side of the scale are weights: \n" +
                "On the table are weights: \n" +
                "\n" +
                "You can use 'placeleft' to place weights on the left side of the scale.\n" +
                "'placeright' does the same thing for the right side.\n" +
                "'placedown' places weights off the scale.", puzzleInstanceService
                .handleWeighCommand(emptyBalanceCharacter,
                        balancePuzzleInstanceService));
    }

    /**
     * Tests handlePickCommand method.
     */
    @Test
    public void handlePickCommand() {
        //NoPuzzle
        assertEquals("There is no puzzle at this location. Therefore you cannot use this command here!", puzzleInstanceService
                .handlePickCommand(emptyCharacter,
                        new ArrayList<>(Collections.singletonList("KJ")),
                        balancePuzzleInstanceService));

        //Puzzle exists, wrong command usage, PuzzleInstance exists
        assertEquals("Please use 'pick' <weightNumber> in order to pick out a weight " +
                "from the scale you think is heaviest!", puzzleInstanceService
                .handlePickCommand(balanceCharacter,
                        new ArrayList<>(Collections.singletonList("KJ")),
                        balancePuzzleInstanceService));

        //Puzzle exists, right command usage, PuzzleInstance exists
        assertEquals("You have picked the wrong weight! The puzzle has been reset!\n" +
                "\n" +
                "You have weighed 0 out of 0 attempts.\n" +
                "The goal is to find the one weight out of 0 which is heavier than the others. You see a scale which you can use. \n" +
                "\n" +
                "On the left side of the scale are weights: \n" +
                "On the right side of the scale are weights: \n" +
                "On the table are weights: \n" +
                "\n" +
                "You can use 'placeleft' to place weights on the left side of the scale.\n" +
                "'placeright' does the same thing for the right side.\n" +
                "'placedown' places weights off the scale.", puzzleInstanceService
                .handlePickCommand(balanceCharacter,
                        new ArrayList<>(Collections.singletonList("1")),
                        balancePuzzleInstanceService));

        //Puzzle exists, right command usage, PuzzleInstance does not exist
        assertEquals("You have not moved any weights yet! \n" +
                "\n" +
                "You have weighed 0 out of 0 attempts.\n" +
                "The goal is to find the one weight out of 0 which is heavier than the others. You see a scale which you can use. \n" +
                "\n" +
                "On the left side of the scale are weights: \n" +
                "On the right side of the scale are weights: \n" +
                "On the table are weights: \n" +
                "\n" +
                "You can use 'placeleft' to place weights on the left side of the scale.\n" +
                "'placeright' does the same thing for the right side.\n" +
                "'placedown' places weights off the scale.", puzzleInstanceService
                .handlePickCommand(emptyBalanceCharacter,
                        new ArrayList<>(Collections.singletonList("KJ")),
                        balancePuzzleInstanceService));
    }

//    /**
//     * Tests createPuzzleInstance method.
//     */
//    @Test
//    public void createPuzzleInstance() {
//
//    }
//
//    /**
//     * Tests createPuzzleInstance1 method.
//     */
//    @Test
//    public void createPuzzleInstance1() {
//
//    }
//
//    /**
//     * Tests createLightsPuzzleInstance method.
//     */
//    @Test
//    public void createLightsPuzzleInstance() {
//
//    }
//
//    /**
//     * Tests createBalancePuzzleInstance method.
//     */
//    @Test
//    public void createBalancePuzzleInstance() {
//
//    }
//
//    /**
//     * Tests createRiddlePuzzleInstance method.
//     */
//    @Test
//    public void createRiddlePuzzleInstance() {
//
//    }
//
//    /**
//     * Tests createBinaryPuzzleInstance method.
//     */
//    @Test
//    public void createBinaryPuzzleInstance() {
//
//    }
}

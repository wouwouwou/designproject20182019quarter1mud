package nl.utwente.mastodon.mudtwente.services.fightsystem;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.fightsystem.EnemyInstance;
import nl.utwente.mastodon.mudtwente.entities.fightsystem.EnemyType;
import nl.utwente.mastodon.mudtwente.repositories.fightsystem.EnemyInstanceRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Test for the MonsterAttackService class.
 */
@RunWith(MockitoJUnitRunner.class)
public class EnemyInstanceServiceTest {

    @Mock
    private EnemyInstanceRepository enemyInstanceRepositoryMock;

    @InjectMocks
    private EnemyInstanceService enemyInstanceService;

    private MUDCharacter mudCharacter;

    private MUDCharacter wrongCharacter;

    private EnemyType enemyType;

    private EnemyType wrongType;


    @Before
    public void setUp() throws Exception {
        EnemyInstance enemyInstance1 = new EnemyInstance();
        mudCharacter = new MUDCharacter();
        mudCharacter.setName("Henk");
        enemyType = new EnemyType();
        enemyType.setName("Piet");
        wrongCharacter = new MUDCharacter();
        wrongType = new EnemyType();

        enemyInstance1.setMudCharacter(mudCharacter);
        enemyInstance1.setEnemyType(enemyType);

        when(enemyInstanceRepositoryMock.findByMudCharacterAndEnemyType(mudCharacter, enemyType)).thenReturn(Optional.of(enemyInstance1));
        when(enemyInstanceRepositoryMock.findByMudCharacterAndEnemyType(mudCharacter, wrongType)).thenReturn(Optional.empty());
        when(enemyInstanceRepositoryMock.findByMudCharacterAndEnemyType(wrongCharacter, enemyType)).thenReturn(Optional.empty());
        when(enemyInstanceRepositoryMock.findByMudCharacterAndEnemyType(wrongCharacter, wrongType)).thenReturn(Optional.empty());
    }

    /**
     * Test 4 cases: one with a EnemyInstance and three where it does not return a EnemyInstance.
     */
    @Test
    public void findByCharacterIdAndLocation() {
        Optional<EnemyInstance> monsterAttackOptional1 = enemyInstanceService.findByMudCharacterAndEnemyType(mudCharacter, enemyType);
        Optional<EnemyInstance> monsterAttackOptional2 = enemyInstanceService.findByMudCharacterAndEnemyType(mudCharacter, wrongType);
        Optional<EnemyInstance> monsterAttackOptional3 = enemyInstanceService.findByMudCharacterAndEnemyType(wrongCharacter, enemyType);
        Optional<EnemyInstance> monsterAttackOptional4 = enemyInstanceService.findByMudCharacterAndEnemyType(wrongCharacter, wrongType);

        assertTrue(monsterAttackOptional1.isPresent());
        assertFalse(monsterAttackOptional2.isPresent());
        assertFalse(monsterAttackOptional3.isPresent());
        assertFalse(monsterAttackOptional4.isPresent());

        assertEquals(mudCharacter, monsterAttackOptional1.get().getMudCharacter());
        assertEquals(enemyType, monsterAttackOptional1.get().getEnemyType());
    }

    //TODO add missing tests
}

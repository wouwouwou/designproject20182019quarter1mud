package nl.utwente.mastodon.mudtwente.services.locationsystem;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.locationsystem.Location;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.Safe;
import nl.utwente.mastodon.mudtwente.repositories.MUDCharacterRepository;
import nl.utwente.mastodon.mudtwente.repositories.locationsystem.LocationRepository;
import nl.utwente.mastodon.mudtwente.services.MUDCharacterService;
import org.hamcrest.beans.HasPropertyWithValue;
import org.hamcrest.core.Every;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

import static nl.utwente.mastodon.mudtwente.utils.Constants.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LocationServiceTest {

    @Mock private LocationRepository locationRepositoryMock;
    @Mock private MUDCharacterRepository mudCharacterRepositoryMock;

    @InjectMocks private LocationService locationService;
    @InjectMocks private MUDCharacterService mudCharacterService;

    private MUDCharacter mudCharacter;

    @Before
    public void setUp() throws Exception {
        Location location = new Location();
        location.setId(2);
        location.setName("One");
        location.setNorth(3);
        location.setEast(4);
        location.setSafe(new Safe());

        Location location1 = new Location();
        location1.setId(3);
        location1.setSouth(2);
        location1.setName("Multiple");
        location1.setInfo("blabla");
        Location location2 = new Location();
        location2.setId(4);
        location2.setWest(2);
        location2.setName("Multiple");

        mudCharacter = new MUDCharacter();
        mudCharacter.setLocation(location);

        List<Location> locations = new ArrayList<>(Collections.singletonList(location));
        List<Location> locations1 = new ArrayList<>(Arrays.asList(location1, location2));

        when(locationRepositoryMock.findById(1)).thenReturn(Optional.empty());
        when(locationRepositoryMock.findById(2)).thenReturn(Optional.of(location));
        when(locationRepositoryMock.findById(3)).thenReturn(Optional.of(location1));
        when(locationRepositoryMock.findById(4)).thenReturn(Optional.of(location2));

        when(locationRepositoryMock.findByName("None")).thenReturn(new ArrayList<>());
        when(locationRepositoryMock.findByName("One")).thenReturn(locations);
        when(locationRepositoryMock.findByName("Multiple")).thenReturn(locations1);
    }

    /**
     * Tests findById method.
     */
    @Test
    public void findById() {
        Optional<Location> locationOptional = locationService.findById(1);
        assertFalse(locationOptional.isPresent());
        locationOptional = locationService.findById(2);
        assertTrue(locationOptional.isPresent());
        assertEquals(2, locationOptional.get().getId());
    }

    /**
     * Tests findByName method.
     */
    @Test
    public void findByName() {
        List<Location> locationList = locationRepositoryMock.findByName("None");
        assertEquals(0, locationList.size());
        locationList = locationRepositoryMock.findByName("One");
        assertEquals(1, locationList.size());
        assertEquals("One", locationList.get(0).getName());
        locationList = locationRepositoryMock.findByName("Multiple");
        assertEquals(2, locationList.size());
        assertThat(locationService.findByName("Multiple"),
                Every.everyItem(HasPropertyWithValue.hasProperty("name", Is.is("Multiple"))));
    }

    /**
     * Tests handleGoCommand method.
     */
    @Test
    public void handleGoCommand() {
        //No words after command
        assertEquals("Which direction do you want to go?",
                locationService.handleGoCommand(new ArrayList<>(),
                        mudCharacterService,
                        mudCharacter));

        //More than 1 word after command
        assertEquals("You have incorrectly used the 'go' <direction> command.\n" +
                        "Possible directions are: north, west, south and east.",
                locationService.handleGoCommand(new ArrayList<>(Arrays.asList("asdf", "asdf")),
                        mudCharacterService,
                        mudCharacter));

        //Unvalid direction
        assertEquals("That direction is not valid!",
                locationService.handleGoCommand(new ArrayList<>(Collections.singletonList("asdf")),
                        mudCharacterService,
                        mudCharacter));

        //North
        assertEquals("Lets go north!",
                locationService.handleGoCommand(new ArrayList<>(Collections.singletonList("north")),
                        mudCharacterService,
                        mudCharacter));

        //South
        assertEquals("Lets go south!",
                locationService.handleGoCommand(new ArrayList<>(Collections.singletonList("south")),
                        mudCharacterService,
                        mudCharacter));

        //East
        assertEquals("Lets go east!",
                locationService.handleGoCommand(new ArrayList<>(Collections.singletonList("east")),
                        mudCharacterService,
                        mudCharacter));

        //West
        assertEquals("Lets go west!",
                locationService.handleGoCommand(new ArrayList<>(Collections.singletonList("west")),
                        mudCharacterService,
                        mudCharacter));
    }

    /**
     * Tests handleLocationCommand method.
     */
    @Test
    public void handleLocationCommand() {
        //Check Safe
        assertEquals("Your current location is: One\n" +
                        "North of you is: Multiple\n" +
                        "West of you is: Nothing\n" +
                        "South of you is: Nothing\n" +
                        "East of you is: Multiple\n" +
                        "\n" +
                        "There is a safe at this location: \n" +
                        "\n" +
                        "Use the 'checkSafe' command to see the information of the safe again!",
                locationService.handleLocationCommand(mudCharacter));
    }

    /**
     * Tests handleTeleportCommand method.
     */
    @Test
    public void handleTeleportCommand() {
        //Empty words list after command
        assertEquals("You have incorrectly used the 'teleport' <locationId> command.",
                locationService.handleTeleportCommand(new ArrayList<>(),
                        mudCharacter,
                        mudCharacterRepositoryMock));

        //Word as Location ID
        assertEquals("Please put in a number for the 'teleport' <locationId>  command!",
                locationService.handleTeleportCommand(new ArrayList<>(Collections.singletonList("asdf")),
                        mudCharacter,
                        mudCharacterRepositoryMock));

        //Too low Location ID
        assertEquals("Please put in a location ID bigger than 0!",
                locationService.handleTeleportCommand(new ArrayList<>(Collections.singletonList("-1")),
                        mudCharacter,
                        mudCharacterRepositoryMock));

        //Valid Teleport
        assertEquals("You teleported correctly to: ",
                locationService.handleTeleportCommand(new ArrayList<>(Collections.singletonList("4")),
                        mudCharacter,
                        mudCharacterRepositoryMock));

        //Location does not exist
        assertEquals("A location with this ID does not exist!",
                locationService.handleTeleportCommand(new ArrayList<>(Collections.singletonList("9")),
                        mudCharacter,
                        mudCharacterRepositoryMock));
    }

    /**
     * Tests handleLocationInfoCommand method.
     */
    @Test
    public void handleLocationInfoCommand() {
        //Without info
        assertEquals("This map has no further information.",
                locationService.handleLocationInfoCommand(mudCharacter));

        //With info
        mudCharacter.getLocation().setInfo("blabla");
        assertEquals("blabla",
                locationService.handleLocationInfoCommand(mudCharacter));
    }

    /**
     * Tests handleWhoIsHereCommand method.
     */
    @Test
    public void handleWhoIsHereCommand() {
        //With info
        assertEquals("The users that are online and in your location: ",
                locationService.handleWhoIsHereCommand(mudCharacterService, mudCharacter));
    }

    /**
     * Tests addToDatabase method.
     */
    @Test
    public void addToDatabase() {
        locationService.addToDatabase();

        verify(locationRepositoryMock).save(ATHLETICS_FIELD);
        verify(locationRepositoryMock).save(SPORT_CENTER);
        verify(locationRepositoryMock).save(PROMENADE_WEST);
        verify(locationRepositoryMock).save(COOP);
        verify(locationRepositoryMock).save(PROMENADE_MIDDLE_WEST);
        verify(locationRepositoryMock).save(BASTILLE);
        verify(locationRepositoryMock).save(VESTINGBAR);
        verify(locationRepositoryMock).save(PROMENADE_MIDDLE_EAST);
        verify(locationRepositoryMock).save(PROMENADE_EAST);
        verify(locationRepositoryMock).save(VRIJHOF);
        verify(locationRepositoryMock).save(DRIENERLOLAAN_NORTH);
        verify(locationRepositoryMock).save(DRIENERLOLAAN_SOUTH);
        verify(locationRepositoryMock).save(CARILLONVELD);
        verify(locationRepositoryMock).save(STUDENT_HOUSING);
        verify(locationRepositoryMock).save(DRIENERBEEKLAAN);
        verify(locationRepositoryMock).save(SPIEGEL);
        verify(locationRepositoryMock).save(MAIN_ENTRANCE);
        verify(locationRepositoryMock).save(HALLENWEG_WEST);
        verify(locationRepositoryMock).save(HALLENWEG_EAST);
        verify(locationRepositoryMock).save(GANZENVELD);
        verify(locationRepositoryMock).save(ZUL_NORTH);
        verify(locationRepositoryMock).save(BOERDERIJLAAN_WEST);
        verify(locationRepositoryMock).save(BOERDERIJLAAN_EAST);
        verify(locationRepositoryMock).save(ZUL_SOUTH);
        verify(locationRepositoryMock).save(OO_PLAZA);
        verify(locationRepositoryMock).save(ZILVERLING);
        verify(locationRepositoryMock).save(DESIGN_LAB);
        verify(locationRepositoryMock).save(WAAIER);
        verify(locationRepositoryMock).save(HORSTTOREN);
        verify(locationRepositoryMock).save(NANOLAB);


        verify(locationRepositoryMock).save(REGISTRATION_TENT);
        verify(locationRepositoryMock).save(CARILLON_TUTORIAL);
        verify(locationRepositoryMock).save(VESTINGBAR_TUTORIAL);
        verify(locationRepositoryMock).save(MEDICAL_TENT);
        verify(locationRepositoryMock).save(CULTURE);
        verify(locationRepositoryMock).save(CAMP);
        verify(locationRepositoryMock).save(PUB);
        verify(locationRepositoryMock).save(EXIT);
    }
}

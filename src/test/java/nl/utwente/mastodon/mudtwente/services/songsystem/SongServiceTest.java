package nl.utwente.mastodon.mudtwente.services.songsystem;

import nl.utwente.mastodon.mudtwente.entities.songsystem.Song;
import nl.utwente.mastodon.mudtwente.repositories.songsystem.SongRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static nl.utwente.mastodon.mudtwente.utils.Constants.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for the SongService class.
 */
@RunWith(MockitoJUnitRunner.class)
public class SongServiceTest {

    @Mock
    private SongRepository songRepositoryMock;

    @InjectMocks
    private SongService songService;

    @Before
    public void setUp() throws Exception {
        Song song1 = new Song();

        song1.setId(33);

        when(songRepositoryMock.findById(33)).thenReturn(Optional.of(song1));
        when(songRepositoryMock.findById(11)).thenReturn(Optional.empty());
    }

    /**
     * Test 2 cases: one with a Song with the right Id and one without.
     */
    @Test
    public void findById() {
        Optional<Song> song1 = songService.findById(33);
        Optional<Song> song2 = songService.findById(11);

        assertTrue(song1.isPresent());
        assertFalse(song2.isPresent());

        assertEquals(33, song1.get().getId());
    }

    /**
     * Tests addToDatabase method.
     */
    @Test
    public void addToDatabase() {
        songService.addToDatabase();

        verify(songRepositoryMock).save(INTER_ACTIEF);
        verify(songRepositoryMock).save(SINTERKLAAS);
        verify(songRepositoryMock).save(MADONNA);
        verify(songRepositoryMock).save(HOUSTON);
    }
}

package nl.utwente.mastodon.mudtwente.services.puzzlesystem.puzzle_instances;

import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.LightsPuzzleInstance;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.lightspuzzle.LightsPuzzle;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.lightspuzzle.LightswitchPattern;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.puzzle_instances.LightsPuzzleInstanceRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Test for LightsPuzzleInstanceService class.
 */
@RunWith(MockitoJUnitRunner.class)
public class LightsPuzzleInstanceServiceTest {

    @Mock private LightsPuzzleInstanceRepository lightsPuzzleInstanceRepositoryMock;

    @InjectMocks private LightsPuzzleInstanceService lightsPuzzleInstanceService;

    private LightsPuzzleInstance lightsPuzzleInstance;


    @Before
    public void setUp() {
        List<LightswitchPattern> lightswitchPatterns = new ArrayList<>(
                Arrays.asList(
                        new LightswitchPattern(
                                new ArrayList<>(
                                        Arrays.asList(false, false, false, false, false, false))),
                        new LightswitchPattern(
                                new ArrayList<>(
                                        Arrays.asList(true, true, true, true, true, true))),
                        new LightswitchPattern(
                                new ArrayList<>(
                                        Arrays.asList(false, false, false, false, false, false))),
                        new LightswitchPattern(
                                new ArrayList<>(
                                        Arrays.asList(false, false, false, false, false, false))),
                        new LightswitchPattern(
                                new ArrayList<>(
                                        Arrays.asList(false, false, false, false, false, false))),
                        new LightswitchPattern(
                                new ArrayList<>(
                                        Arrays.asList(false, false, false, false, false, false)))

                )
        );
        LightsPuzzle lightsPuzzle = new LightsPuzzle();
        lightsPuzzle.setLightswitchPatterns(lightswitchPatterns);
        lightsPuzzleInstance = new LightsPuzzleInstance();
        lightsPuzzleInstance.setPuzzle(lightsPuzzle);;
    }

    /**
     * Tests pull method
     */
    @Test
    public void pull() {
        //Wrong command usage
        assertEquals("Please use 'pull' <leverNumber> in order to pull a lever!",
                lightsPuzzleInstanceService
                        .pull(lightsPuzzleInstance,
                                new ArrayList<>()));

        //Lever number too high
        assertEquals("There are 6 levers labeled 1 to 6. Please pick an existing lever to pull!",
                lightsPuzzleInstanceService
                        .pull(lightsPuzzleInstance,
                                new ArrayList<>(Collections.singletonList("7"))));

        //Lever number too low
        assertEquals("There are 6 levers labeled 1 to 6. Please pick an existing lever to pull!",
                lightsPuzzleInstanceService
                        .pull(lightsPuzzleInstance,
                                new ArrayList<>(Collections.singletonList("-1"))));

        //First time usage
        assertEquals("You have pulled lever 1.\n" +
                        "\n" +
                        "You see six lights: \n" +
                        "\n" +
                        "-Light 1 is OFF.\n" +
                        "-Light 2 is OFF.\n" +
                        "-Light 3 is OFF.\n" +
                        "-Light 4 is OFF.\n" +
                        "-Light 5 is OFF.\n" +
                        "-Light 6 is OFF.\n" +
                        "\n" +
                        "Use the 'pull' <number> command to change the status of the lights.\n" +
                        "There are 6 levers you can pull here. Each of them changes the lights in a different way.\n" +
                        "Tip: look for a pattern!",
                lightsPuzzleInstanceService
                        .pull(lightsPuzzleInstance,
                                new ArrayList<>(Collections.singletonList("1"))));

        //Second time usage
        assertEquals("The lights are all on! Good job! A message magically appears: \"\"",
                lightsPuzzleInstanceService
                        .pull(lightsPuzzleInstance,
                                new ArrayList<>(Collections.singletonList("2"))));
    }
}

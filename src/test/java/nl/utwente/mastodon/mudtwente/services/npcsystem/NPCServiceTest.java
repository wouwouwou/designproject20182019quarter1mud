package nl.utwente.mastodon.mudtwente.services.npcsystem;

import nl.utwente.mastodon.mudtwente.entities.locationsystem.Location;
import nl.utwente.mastodon.mudtwente.entities.npcsystem.NPC;
import nl.utwente.mastodon.mudtwente.repositories.npcsystem.NPCRepository;
import nl.utwente.mastodon.mudtwente.utils.Constants;
import org.hamcrest.beans.HasPropertyWithValue;
import org.hamcrest.core.Every;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static nl.utwente.mastodon.mudtwente.utils.Constants.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for the NPCService class.
 */
@RunWith(MockitoJUnitRunner.class)
public class NPCServiceTest {

    @Mock
    private NPCRepository npcRepositoryMock;

    @InjectMocks
    private NPCService npcService;

    @Before
    public void setUp() {
        NPC npc1 = new NPC();
        NPC npc2 = new NPC();
        NPC npc3 = new NPC();

        npc1.setName("Kees");

        npc1.setLocation(Constants.BOERDERIJLAAN_WEST);
        npc2.setLocation(Constants.CARILLON_TUTORIAL);
        npc3.setLocation(Constants.CARILLON_TUTORIAL);

        List<NPC> ttList = new ArrayList<>();
        List<NPC> ddList = new ArrayList<>();

        ttList.add(npc1);
        ddList.add(npc2);
        ddList.add(npc3);

        when(npcRepositoryMock.findByName("Kees")).thenReturn(Optional.of(npc1));
        when(npcRepositoryMock.findByName("Henk")).thenReturn(Optional.empty());

        when(npcRepositoryMock.findByLocation(Constants.BOERDERIJLAAN_WEST)).thenReturn(ttList);
        when(npcRepositoryMock.findByLocation(Constants.CARILLON_TUTORIAL)).thenReturn(ddList);
    }

    /**
     * Test 2 cases: one with an NPC with the right name and one without.
     */
    @Test
    public void findByName() {
        Optional<NPC> npcOptional1 = npcService.findByName("Kees");
        Optional<NPC> npcOptional2 = npcService.findByName("Henk");

        assertTrue(npcOptional1.isPresent());
        assertFalse(npcOptional2.isPresent());

        assertEquals("Kees", npcOptional1.get().getName());
    }

    /**
     * Tests 3 cases: location does not exist, location exists 1 time, location exists multiple times.
     */
    @Test
    public void findByLocation() {
        assertEquals(0, npcService.findByLocation(new Location()).size());
        assertEquals(1, npcService.findByLocation(Constants.BOERDERIJLAAN_WEST).size());
        assertTrue(npcService.findByLocation(Constants.CARILLON_TUTORIAL).size() > 1);
        assertEquals(Constants.BOERDERIJLAAN_WEST, npcService.findByLocation(BOERDERIJLAAN_WEST).get(0).getLocation());
        assertThat(npcService.findByLocation(Constants.CARILLON_TUTORIAL),
                Every.everyItem(HasPropertyWithValue.hasProperty("location", Is.is(CARILLON_TUTORIAL))));
    }

    @Test
    public void handleTalkCommand() {
        //TODO: implement this
    }

    @Test
    public void handleWhichNPCsCommand() {
        //TODO: implement this
    }

    /**
     * Tests addToDatabase method.
     */
    @Test
    public void addToDatabase() {
        npcService.addToDatabase();

        verify(npcRepositoryMock).save(BIOLOGY_STUDENT);
        verify(npcRepositoryMock).save(EXAMINATION_BOARD);
        verify(npcRepositoryMock).save(UNCLE_DUO);
        verify(npcRepositoryMock).save(NANOLAB_STUDENT);
        verify(npcRepositoryMock).save(CREATIVE_TECHNOLOGY_STUDENT);
        verify(npcRepositoryMock).save(RUGBY_PLAYER);
        verify(npcRepositoryMock).save(DREAM);

        verify(npcRepositoryMock).save(INFO_NPC);
        verify(npcRepositoryMock).save(COMMANDS_NPC);
        verify(npcRepositoryMock).save(DRUNK_MATE);
        verify(npcRepositoryMock).save(DOCTOR_NPC);
        verify(npcRepositoryMock).save(CULTURE_NPC);
        verify(npcRepositoryMock).save(BARTENDER);
        verify(npcRepositoryMock).save(BARTENDERS_BOSS);
        verify(npcRepositoryMock).save(EXIT_NPC);
    }
}

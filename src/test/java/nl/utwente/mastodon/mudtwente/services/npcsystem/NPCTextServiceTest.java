package nl.utwente.mastodon.mudtwente.services.npcsystem;

import nl.utwente.mastodon.mudtwente.entities.npcsystem.NPCText;
import nl.utwente.mastodon.mudtwente.repositories.npcsystem.NPCTextRepository;
import nl.utwente.mastodon.mudtwente.utils.Constants;
import org.hamcrest.beans.HasPropertyWithValue;
import org.hamcrest.core.Every;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static nl.utwente.mastodon.mudtwente.utils.Constants.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for the NPCTextService class.
 */
@RunWith(MockitoJUnitRunner.class)
public class NPCTextServiceTest {

    @Mock
    private NPCTextRepository npcTextRepositoryMock;

    @InjectMocks
    private NPCTextService npcTextService;

    @Before
    public void setUp(){
        NPCText npcText1 = new NPCText();
        NPCText npcText2 = new NPCText();
        NPCText npcText3 = new NPCText();

        npcText1.setNpc(EXIT_NPC);
        npcText2.setNpc(BARTENDERS_BOSS);
        npcText3.setNpc(BARTENDERS_BOSS);

        List<NPCText> npcTextList1 = new ArrayList<>();
        List<NPCText> npcTextList2 = new ArrayList<>();

        npcTextList1.add(npcText1);
        npcTextList2.add(npcText2);
        npcTextList2.add(npcText3);

        when(npcTextRepositoryMock.findByNpc(BIOLOGY_STUDENT)).thenReturn(new ArrayList<>());
        when(npcTextRepositoryMock.findByNpc(EXIT_NPC)).thenReturn(npcTextList1);
        when(npcTextRepositoryMock.findByNpc(BARTENDERS_BOSS)).thenReturn(npcTextList2);
    }

    /**
     * Tests 3 cases: npcId does not exist, npcId exists 1 time, npcId exists multiple times.
     */
    @Test
    public void findByNpcId() {
        assertEquals(0, npcTextService.findByNpc(BIOLOGY_STUDENT).size());
        assertEquals(1, npcTextService.findByNpc(EXIT_NPC).size());
        assertTrue(npcTextService.findByNpc(BARTENDERS_BOSS).size() > 1);
        assertEquals(EXIT_NPC, npcTextService.findByNpc(EXIT_NPC).get(0).getNpc());
        assertThat(npcTextService.findByNpc(BARTENDERS_BOSS),
                Every.everyItem(HasPropertyWithValue.hasProperty("npc", Is.is(BARTENDERS_BOSS))));
    }

    /**
     * Tests addToDatabase method.
     */
    @Test
    public void addToDatabase() {
        npcTextService.addToDatabase();

        verify(npcTextRepositoryMock).save(BIOLOGY_STUDENT_TEXT_1);
        verify(npcTextRepositoryMock).save(BIOLOGY_STUDENT_TEXT_2);
        verify(npcTextRepositoryMock).save(EXAMINATION_BOARD_TEXT_1);
        verify(npcTextRepositoryMock).save(UNCLE_DUO_TEXT_1);
        verify(npcTextRepositoryMock).save(EXAMINATION_BOARD_TEXT_2);
        verify(npcTextRepositoryMock).save(NANOLAB_STUDENT_TEXT_1);
        verify(npcTextRepositoryMock).save(CREATE_STUDENT_TEXT_1);
        verify(npcTextRepositoryMock).save(CREATE_STUDENT_TEXT_2);
        verify(npcTextRepositoryMock).save(NANOLAB_STUDENT_TEXT_2);
        verify(npcTextRepositoryMock).save(EXAMINATION_BOARD_TEXT_3);
        verify(npcTextRepositoryMock).save(RUGBY_PLAYER_TEXT_1);
        verify(npcTextRepositoryMock).save(RUGBY_PLAYER_TEXT_2);
        verify(npcTextRepositoryMock).save(DREAM_TEXT_1);
        verify(npcTextRepositoryMock).save(TUTORIAL_INFO_TEXT_1);
        verify(npcTextRepositoryMock).save(TUTORIAL_COMMANDS_TEXT_1);
        verify(npcTextRepositoryMock).save(TUTORIAL_DRUNK_MATE_TEXT_1);
        verify(npcTextRepositoryMock).save(TUTORIAL_DOCTOR_TEXT_1);
        verify(npcTextRepositoryMock).save(TUTORIAL_CULTURE_TEXT_1);
        verify(npcTextRepositoryMock).save(TUTORIAL_BARTENDER_TEXT_1);
        verify(npcTextRepositoryMock).save(TUTORIAL_BOSS_TEXT_1);
        verify(npcTextRepositoryMock).save(TUTORIAL_EXIT_TEXT_1);
    }
}

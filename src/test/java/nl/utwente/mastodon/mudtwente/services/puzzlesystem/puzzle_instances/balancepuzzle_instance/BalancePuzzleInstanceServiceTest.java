package nl.utwente.mastodon.mudtwente.services.puzzlesystem.puzzle_instances.balancepuzzle_instance;

import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.balancepuzzle_instance.BalancePuzzleInstance;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.BalancePuzzle;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.puzzle_instances.balancepuzzle_instance.BalancePuzzleInstanceRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

/**
 * Test class for BalancePuzzleInstanceService class.
 */
@RunWith(MockitoJUnitRunner.class)
public class BalancePuzzleInstanceServiceTest {

    @Mock private BalancePuzzleInstanceRepository balancePuzzleInstanceRepositoryMock;

    @InjectMocks private BalancePuzzleInstanceService balancePuzzleInstanceService;

    private BalancePuzzleInstance balancePuzzleInstance;

    @Before
    public void setUp() {
        BalancePuzzle balancePuzzle = new BalancePuzzle();
        balancePuzzleInstance = new BalancePuzzleInstance();
        balancePuzzleInstance.setPuzzle(balancePuzzle);
    }

    /**
     * Tests attemptWeighing method
     */
    @Test
    public void attemptWeighing() {
        balancePuzzleInstance.getBalancePuzzle().setMaxWeighings(2);
        //Normal attempt
        assertEquals("The scales are perfectly balanced!\n" +
                        "\n" +
                        "You have now weighted this puzzle 1 out of 2 times.",
                balancePuzzleInstanceService
                        .attemptWeighing(balancePuzzleInstance));

        //Last attempt
        assertEquals("The scales are perfectly balanced!\n" +
                        "\n" +
                        "You have reached the maximum amount of weighing attempts! Please pick a weight of the scale!",
                balancePuzzleInstanceService
                        .attemptWeighing(balancePuzzleInstance));

        //Already at max weighings
        assertEquals("You have reached the maximum amount of weighing attempts! " +
                        "Please pick a weight of the scale!",
                balancePuzzleInstanceService
                        .attemptWeighing(balancePuzzleInstance));

    }

    /**
     * Tests pick method
     */
    @Test
    public void pick() {
        balancePuzzleInstance.getBalancePuzzle().setMaxWeighings(2);
        //Wrong command usage
        assertEquals("Please use 'pick' <weightNumber> in order to pick out a weight from " +
                        "the scale you think is heaviest!",
                balancePuzzleInstanceService
                        .pick(balancePuzzleInstance,
                                new ArrayList<>()));

        //Pick before weighing
        assertEquals("You must first weigh a total of 2 times in order to pick out a weight from the scale you think is heaviest.\n" +
                        "\n" +
                        "You have weighed 0 out of 2 attempts.\n" +
                        "The goal is to find the one weight out of 0 which is heavier than the others. You see a scale which you can use. \n" +
                        "\n" +
                        "On the left side of the scale are weights: \n" +
                        "On the right side of the scale are weights: \n" +
                        "On the table are weights: \n" +
                        "\n" +
                        "You can use 'placeleft' to place weights on the left side of the scale.\n" +
                        "'placeright' does the same thing for the right side.\n" +
                        "'placedown' places weights off the scale.",
                balancePuzzleInstanceService
                        .pick(balancePuzzleInstance,
                                new ArrayList<>(
                                        Collections.singletonList("D")
                                )));

        //Wrong pick
        balancePuzzleInstance.getBalancePuzzle().setMaxWeighings(0);
        assertEquals("You have picked the wrong weight! The puzzle has been reset!\n" +
                        "\n" +
                        "You have weighed 0 out of 0 attempts.\n" +
                        "The goal is to find the one weight out of 0 which is heavier than the others. You see a scale which you can use. \n" +
                        "\n" +
                        "On the left side of the scale are weights: \n" +
                        "On the right side of the scale are weights: \n" +
                        "On the table are weights: \n" +
                        "\n" +
                        "You can use 'placeleft' to place weights on the left side of the scale.\n" +
                        "'placeright' does the same thing for the right side.\n" +
                        "'placedown' places weights off the scale.",
                balancePuzzleInstanceService
                        .pick(balancePuzzleInstance,
                                new ArrayList<>(
                                        Collections.singletonList("1")
                                )));

        //Right Pick
        balancePuzzleInstance.getBalancePuzzle().setHeaviestWeightId(1);
        assertEquals("You have picked the correct weight! It says: \"\"",
                balancePuzzleInstanceService
                        .pick(balancePuzzleInstance,
                                new ArrayList<>(
                                        Collections.singletonList("1")
                                )));
    }
}

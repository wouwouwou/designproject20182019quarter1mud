package nl.utwente.mastodon.mudtwente.services.puzzlesystem.puzzle_instances.balancepuzzle_instance;

import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.balancepuzzle_instance.BalancePuzzleInstance;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.balancepuzzle_instance.Scale;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.BalancePuzzle;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.ScaleRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.Assert.*;

/**
 * Test class for ScaleService class.
 */
@RunWith(MockitoJUnitRunner.class)
public class ScaleServiceTest {

    @Mock private ScaleRepository scaleRepositoryMock;

    @InjectMocks private ScaleService scaleService;

    private BalancePuzzleInstance balancePuzzleInstance;

    @Before
    public void setUp() throws Exception {
        balancePuzzleInstance = new BalancePuzzleInstance();
        Scale scale = new Scale();
        BalancePuzzle balancePuzzle = new BalancePuzzle();
        balancePuzzleInstance.setScale(scale);
        balancePuzzleInstance.setPuzzle(balancePuzzle);
    }

    /**
     * Tests attemptPlaceLeft method
     */
    @Test
    public void attemptPlaceLeft() {
        //Wrong command usage nothing with command
        assertEquals("Please use 'placeleft' <weightNumber> in order to put a weight " +
                        "on the left side of the scale!",
                scaleService
                        .attemptPlaceLeft(balancePuzzleInstance,
                                new ArrayList<>()));

        //Wrong command usage not giving a number
        assertEquals("Please use 'placeleft' <weightNumber> in order to put a weight " +
                        "on the left side of the scale!",
                scaleService
                        .attemptPlaceLeft(balancePuzzleInstance,
                                new ArrayList<>(
                                        Collections.singletonList("KJ")
                                )));

        //Weight does not exist
        assertEquals("This weight does not exist!\n" +
                        "\n" +
                        "You have weighed 0 out of 0 attempts.\n" +
                        "The goal is to find the one weight out of 0 which is heavier than the others. You see a scale which you can use. \n" +
                        "\n" +
                        "On the left side of the scale are weights: \n" +
                        "On the right side of the scale are weights: \n" +
                        "On the table are weights: \n" +
                        "\n" +
                        "You can use 'placeleft' to place weights on the left side of the scale.\n" +
                        "'placeright' does the same thing for the right side.\n" +
                        "'placedown' places weights off the scale.",
                scaleService
                        .attemptPlaceLeft(balancePuzzleInstance,
                                new ArrayList<>(
                                        Collections.singletonList("9")
                                )));

        //Weight placeable from right side
        balancePuzzleInstance.getScale().getRightSide().add(9);
        assertEquals("Succesfully placed weight 9 on the left side of the scale!\n" +
                        "\n" +
                        "You have weighed 0 out of 0 attempts.\n" +
                        "The goal is to find the one weight out of 0 which is heavier than the others. You see a scale which you can use. \n" +
                        "\n" +
                        "On the left side of the scale are weights: 9\n" +
                        "On the right side of the scale are weights: \n" +
                        "On the table are weights: \n" +
                        "\n" +
                        "You can use 'placeleft' to place weights on the left side of the scale.\n" +
                        "'placeright' does the same thing for the right side.\n" +
                        "'placedown' places weights off the scale.",
                scaleService
                        .attemptPlaceLeft(balancePuzzleInstance,
                                new ArrayList<>(
                                        Collections.singletonList("9")
                                )));

        //Weight placeable from NotOnScale
        balancePuzzleInstance.getScale().getNotOnScale().add(8);
        assertEquals("Succesfully placed weight 8 on the left side of the scale!\n" +
                        "\n" +
                        "You have weighed 0 out of 0 attempts.\n" +
                        "The goal is to find the one weight out of 0 which is heavier than the others. You see a scale which you can use. \n" +
                        "\n" +
                        "On the left side of the scale are weights: 8, 9\n" +
                        "On the right side of the scale are weights: \n" +
                        "On the table are weights: \n" +
                        "\n" +
                        "You can use 'placeleft' to place weights on the left side of the scale.\n" +
                        "'placeright' does the same thing for the right side.\n" +
                        "'placedown' places weights off the scale.",
                scaleService
                        .attemptPlaceLeft(balancePuzzleInstance,
                                new ArrayList<>(
                                        Collections.singletonList("8")
                                )));

        //Weight already at left side
        assertEquals("This weight is already on the left side!\n" +
                        "\n" +
                        "You have weighed 0 out of 0 attempts.\n" +
                        "The goal is to find the one weight out of 0 which is heavier than the others. You see a scale which you can use. \n" +
                        "\n" +
                        "On the left side of the scale are weights: 8, 9\n" +
                        "On the right side of the scale are weights: \n" +
                        "On the table are weights: \n" +
                        "\n" +
                        "You can use 'placeleft' to place weights on the left side of the scale.\n" +
                        "'placeright' does the same thing for the right side.\n" +
                        "'placedown' places weights off the scale.",
                scaleService
                        .attemptPlaceLeft(balancePuzzleInstance,
                                new ArrayList<>(
                                        Collections.singletonList("9")
                                )));
    }

    /**
     * Tests attemptPlaceRight method
     */
    @Test
    public void attemptPlaceRight() {
        //Wrong command usage nothing with command
        assertEquals("Please use 'placeright' <weightNumber> in order to put a weight " +
                        "on the right side of the scale!",
                scaleService
                        .attemptPlaceRight(balancePuzzleInstance,
                                new ArrayList<>()));

        //Wrong command usage not giving a number
        assertEquals("Please use 'placeright' <weightNumber> in order to put a weight " +
                        "on the right side of the scale!",
                scaleService
                        .attemptPlaceRight(balancePuzzleInstance,
                                new ArrayList<>(
                                        Collections.singletonList("KJ")
                                )));

        //Weight does not exist
        assertEquals("This weight does not exist!\n" +
                        "\n" +
                        "You have weighed 0 out of 0 attempts.\n" +
                        "The goal is to find the one weight out of 0 which is heavier than the others. You see a scale which you can use. \n" +
                        "\n" +
                        "On the left side of the scale are weights: \n" +
                        "On the right side of the scale are weights: \n" +
                        "On the table are weights: \n" +
                        "\n" +
                        "You can use 'placeleft' to place weights on the left side of the scale.\n" +
                        "'placeright' does the same thing for the right side.\n" +
                        "'placedown' places weights off the scale.",
                scaleService
                        .attemptPlaceRight(balancePuzzleInstance,
                                new ArrayList<>(
                                        Collections.singletonList("9")
                                )));

        //Weight placeable from left side
        balancePuzzleInstance.getScale().getLeftSide().add(9);
        assertEquals("Succesfully placed weight 9 on the right side of the scale!\n" +
                        "\n" +
                        "You have weighed 0 out of 0 attempts.\n" +
                        "The goal is to find the one weight out of 0 which is heavier than the others. You see a scale which you can use. \n" +
                        "\n" +
                        "On the left side of the scale are weights: \n" +
                        "On the right side of the scale are weights: 9\n" +
                        "On the table are weights: \n" +
                        "\n" +
                        "You can use 'placeleft' to place weights on the left side of the scale.\n" +
                        "'placeright' does the same thing for the right side.\n" +
                        "'placedown' places weights off the scale.",
                scaleService
                        .attemptPlaceRight(balancePuzzleInstance,
                                new ArrayList<>(
                                        Collections.singletonList("9")
                                )));

        //Weight placeable from NotOnScale
        balancePuzzleInstance.getScale().getNotOnScale().add(8);
        assertEquals("Succesfully placed weight 8 on the right side of the scale!\n" +
                        "\n" +
                        "You have weighed 0 out of 0 attempts.\n" +
                        "The goal is to find the one weight out of 0 which is heavier than the others. You see a scale which you can use. \n" +
                        "\n" +
                        "On the left side of the scale are weights: \n" +
                        "On the right side of the scale are weights: 8, 9\n" +
                        "On the table are weights: \n" +
                        "\n" +
                        "You can use 'placeleft' to place weights on the left side of the scale.\n" +
                        "'placeright' does the same thing for the right side.\n" +
                        "'placedown' places weights off the scale.",
                scaleService
                        .attemptPlaceRight(balancePuzzleInstance,
                                new ArrayList<>(
                                        Collections.singletonList("8")
                                )));

        //Weight already at right side
        assertEquals("This weight is already on the right side!\n" +
                        "\n" +
                        "You have weighed 0 out of 0 attempts.\n" +
                        "The goal is to find the one weight out of 0 which is heavier than the others. You see a scale which you can use. \n" +
                        "\n" +
                        "On the left side of the scale are weights: \n" +
                        "On the right side of the scale are weights: 8, 9\n" +
                        "On the table are weights: \n" +
                        "\n" +
                        "You can use 'placeleft' to place weights on the left side of the scale.\n" +
                        "'placeright' does the same thing for the right side.\n" +
                        "'placedown' places weights off the scale.",
                scaleService
                        .attemptPlaceRight(balancePuzzleInstance,
                                new ArrayList<>(
                                        Collections.singletonList("9")
                                )));

    }

    /**
     * Tests attemptPlaceDown method
     */
    @Test
    public void attemptPlaceDown() {
        //Wrong command usage nothing with command
        assertEquals("Please use 'placedown' <weightNumber> in order to put a weight down from the scale!",
                scaleService
                        .attemptPlaceDown(balancePuzzleInstance,
                                new ArrayList<>()));

        //Wrong command usage not giving a number
        assertEquals("Please use 'placedown' <weightNumber> in order to put a weight down from the scale!",
                scaleService
                        .attemptPlaceDown(balancePuzzleInstance,
                                new ArrayList<>(
                                        Collections.singletonList("KJ")
                                )));

        //Weight does not exist
        assertEquals("This weight does not exist!\n" +
                        "\n" +
                        "You have weighed 0 out of 0 attempts.\n" +
                        "The goal is to find the one weight out of 0 which is heavier than the others. You see a scale which you can use. \n" +
                        "\n" +
                        "On the left side of the scale are weights: \n" +
                        "On the right side of the scale are weights: \n" +
                        "On the table are weights: \n" +
                        "\n" +
                        "You can use 'placeleft' to place weights on the left side of the scale.\n" +
                        "'placeright' does the same thing for the right side.\n" +
                        "'placedown' places weights off the scale.",
                scaleService
                        .attemptPlaceDown(balancePuzzleInstance,
                                new ArrayList<>(
                                        Collections.singletonList("9")
                                )));

        //Weight placeable from right side
        balancePuzzleInstance.getScale().getRightSide().add(9);
        assertEquals("Succesfully placed weight 9 down from the scale!\n" +
                        "\n" +
                        "You have weighed 0 out of 0 attempts.\n" +
                        "The goal is to find the one weight out of 0 which is heavier than the others. You see a scale which you can use. \n" +
                        "\n" +
                        "On the left side of the scale are weights: \n" +
                        "On the right side of the scale are weights: \n" +
                        "On the table are weights: 9\n" +
                        "\n" +
                        "You can use 'placeleft' to place weights on the left side of the scale.\n" +
                        "'placeright' does the same thing for the right side.\n" +
                        "'placedown' places weights off the scale.",
                scaleService
                        .attemptPlaceDown(balancePuzzleInstance,
                                new ArrayList<>(
                                        Collections.singletonList("9")
                                )));

        //Weight placeable from left side
        balancePuzzleInstance.getScale().getLeftSide().add(8);
        assertEquals("Succesfully placed weight 8 down from the scale!\n" +
                        "\n" +
                        "You have weighed 0 out of 0 attempts.\n" +
                        "The goal is to find the one weight out of 0 which is heavier than the others. You see a scale which you can use. \n" +
                        "\n" +
                        "On the left side of the scale are weights: \n" +
                        "On the right side of the scale are weights: \n" +
                        "On the table are weights: 8, 9\n" +
                        "\n" +
                        "You can use 'placeleft' to place weights on the left side of the scale.\n" +
                        "'placeright' does the same thing for the right side.\n" +
                        "'placedown' places weights off the scale.",
                scaleService
                        .attemptPlaceDown(balancePuzzleInstance,
                                new ArrayList<>(
                                        Collections.singletonList("8")
                                )));

        //Weight already down
        assertEquals("This weight is already down from the scale!\n" +
                        "\n" +
                        "You have weighed 0 out of 0 attempts.\n" +
                        "The goal is to find the one weight out of 0 which is heavier than the others. You see a scale which you can use. \n" +
                        "\n" +
                        "On the left side of the scale are weights: \n" +
                        "On the right side of the scale are weights: \n" +
                        "On the table are weights: 8, 9\n" +
                        "\n" +
                        "You can use 'placeleft' to place weights on the left side of the scale.\n" +
                        "'placeright' does the same thing for the right side.\n" +
                        "'placedown' places weights off the scale.",
                scaleService
                        .attemptPlaceDown(balancePuzzleInstance,
                                new ArrayList<>(
                                        Collections.singletonList("9")
                                )));

    }
}

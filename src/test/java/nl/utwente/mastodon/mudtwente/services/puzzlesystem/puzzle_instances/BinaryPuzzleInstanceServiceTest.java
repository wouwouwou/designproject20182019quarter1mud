package nl.utwente.mastodon.mudtwente.services.puzzlesystem.puzzle_instances;

import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.BinaryPuzzleInstance;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.RiddlePuzzle;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.puzzle_instances.BinaryPuzzleInstanceRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Test for BinaryPuzzleInstanceService class.
 */
@RunWith(MockitoJUnitRunner.class)
public class BinaryPuzzleInstanceServiceTest {

    @Mock private BinaryPuzzleInstanceRepository binaryPuzzleInstanceRepositoryMock;

    @InjectMocks private BinaryPuzzleInstanceService binaryPuzzleInstanceService;

    private BinaryPuzzleInstance binaryPuzzleInstance;

    @Before
    public void setUp() throws Exception {
        List<String> questions = new ArrayList<>(Arrays.asList("A", "B"));
        List<String> answers = new ArrayList<>(Arrays.asList("1", "0"));
        RiddlePuzzle riddlePuzzle = new RiddlePuzzle();
        riddlePuzzle.setQuestions(questions);
        riddlePuzzle.setAnswers(answers);
        binaryPuzzleInstance = new BinaryPuzzleInstance();
        binaryPuzzleInstance.setPuzzle(riddlePuzzle);
    }

    /**
     * Tests pull method
     */
    @Test
    public void pull() {
        //Wrong command usage
        assertEquals("Please use 'pull' <leverNumber> in order to pull a lever!",
                binaryPuzzleInstanceService
                        .pull(binaryPuzzleInstance,
                                new ArrayList<>()));

        //Lever number too high
        assertEquals("There are no more than 5 levers. Please pick an existing lever to pull!",
                binaryPuzzleInstanceService
                        .pull(binaryPuzzleInstance,
                                new ArrayList<>(Collections.singletonList("6"))));

        //Lever number too low
        assertEquals("There are no more than 5 levers. Please pick an existing lever to pull!",
                binaryPuzzleInstanceService
                        .pull(binaryPuzzleInstance,
                                new ArrayList<>(Collections.singletonList("-1"))));

        //First time usage
        assertEquals("The lights show the correct answer!\n" +
                        "You have answered 1 out of 2 riddles.\n" +
                        "Your next riddle is: \n" +
                        "B\n" +
                        "You also see five lights: \n" +
                        "\n" +
                        "-Light 1 is ON.\n" +
                        "-Light 2 is OFF.\n" +
                        "-Light 3 is OFF.\n" +
                        "-Light 4 is OFF.\n" +
                        "-Light 5 is OFF.\n" +
                        "\n" +
                        "There are five levers with which you can turn the lights on or off. Use the 'pull' <number>\n" +
                        "command to use them. The lights form your answer to the riddle. Tip: how do you represent a bigger\n" +
                        "number with only lights?\n" +
                        "The next riddle of this puzzle is: B",
                binaryPuzzleInstanceService
                        .pull(binaryPuzzleInstance,
                                new ArrayList<>(Collections.singletonList("1"))));

        //Second time usage
        assertEquals("The lights show the correct answer!\n" +
                        "You have solved all the riddles of this puzzle! " +
                        "The most right light throws out a paper that reads: \"\"",
                binaryPuzzleInstanceService
                        .pull(binaryPuzzleInstance,
                                new ArrayList<>(Collections.singletonList("1"))));
    }
}

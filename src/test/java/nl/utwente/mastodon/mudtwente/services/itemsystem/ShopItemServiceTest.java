package nl.utwente.mastodon.mudtwente.services.itemsystem;

import nl.utwente.mastodon.mudtwente.entities.itemsystem.ShopItem;
import nl.utwente.mastodon.mudtwente.repositories.itemsystem.ShopitemRepository;
import org.hamcrest.beans.HasPropertyWithValue;
import org.hamcrest.core.Every;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static nl.utwente.mastodon.mudtwente.utils.Constants.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for the ShopitemService class.
 */
@RunWith(MockitoJUnitRunner.class)
public class ShopItemServiceTest {

    @Mock
    private ShopitemRepository shopitemRepositoryMock;

    @InjectMocks
    private ShopitemService shopitemService;

    @Before
    public void setUp() throws Exception {
        ShopItem shopItem1 = new ShopItem();
        ShopItem shopItem2 = new ShopItem();
        ShopItem shopItem3 = new ShopItem();

        shopItem1.setShop(COOP_SHOP);
        shopItem2.setShop(VRIJHOF_SHOP);
        shopItem3.setShop(VRIJHOF_SHOP);

        List<ShopItem> shopItemList1 = new ArrayList<>();
        List<ShopItem> shopItemList2 = new ArrayList<>();

        shopItemList1.add(shopItem1);
        shopItemList2.add(shopItem2);
        shopItemList2.add(shopItem3);

        when(shopitemRepositoryMock.findByShop(SPIEGEL_SHOP)).thenReturn(new ArrayList<>());
        when(shopitemRepositoryMock.findByShop(COOP_SHOP)).thenReturn(shopItemList1);
        when(shopitemRepositoryMock.findByShop(VRIJHOF_SHOP)).thenReturn(shopItemList2);
    }

    /**
     * Tests 3 cases: shopId does not exist, shopId exists 1 time, shopId exists multiple times.
     */
    @Test
    public void findByShopId() {
        assertEquals(0, shopitemService.findByShop(SPIEGEL_SHOP).size());
        assertEquals(1, shopitemService.findByShop(COOP_SHOP).size());
        assertTrue(shopitemService.findByShop(VRIJHOF_SHOP).size() > 1);
        assertEquals(COOP_SHOP, shopitemService.findByShop(COOP_SHOP).get(0).getShop());
        assertThat(shopitemService.findByShop(VRIJHOF_SHOP),
                Every.everyItem(HasPropertyWithValue.hasProperty("shop", Is.is(VRIJHOF_SHOP))));
    }

    /**
     * Tests addToDatabase command.
     */
    @Test
    public void addToDatabase() {
        shopitemService.addToDatabase();

        verify(shopitemRepositoryMock).save(SHOP_ITEM_PIE_COOP);
        verify(shopitemRepositoryMock).save(SHOP_ITEM_POTION_COOP);
        verify(shopitemRepositoryMock).save(SHOP_ITEM_POTION_VESTINGBAR);
        verify(shopitemRepositoryMock).save(SHOP_ITEM_POTION_VRIJHOF);
        verify(shopitemRepositoryMock).save(SHOP_ITEM_POTION_SPIEGEL);
        verify(shopitemRepositoryMock).save(SHOP_ITEM_POTION_ZUL);
        verify(shopitemRepositoryMock).save(SHOP_ITEM_POTION_WAAIER);
        verify(shopitemRepositoryMock).save(SHOP_ITEM_SUPER_PUTION_COOP);
        verify(shopitemRepositoryMock).save(SHOP_ITEM_SUPER_POTION_VESTINGBAR);
        verify(shopitemRepositoryMock).save(SHOP_ITEM_SUPER_POTION_VRIJHOF);
        verify(shopitemRepositoryMock).save(SHOP_ITEM_SUPER_POTION_SPIEGEL);
        verify(shopitemRepositoryMock).save(SHOP_ITEM_SUPER_POTION_ZUL);
        verify(shopitemRepositoryMock).save(SHOP_ITEM_SUPER_POTION_WAAIER);
    }
}

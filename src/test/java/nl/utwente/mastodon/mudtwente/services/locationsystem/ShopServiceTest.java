package nl.utwente.mastodon.mudtwente.services.locationsystem;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.itemsystem.Item;
import nl.utwente.mastodon.mudtwente.entities.itemsystem.ShopItem;
import nl.utwente.mastodon.mudtwente.entities.locationsystem.Location;
import nl.utwente.mastodon.mudtwente.entities.locationsystem.Shop;
import nl.utwente.mastodon.mudtwente.repositories.RepositoryWrapper;
import nl.utwente.mastodon.mudtwente.repositories.itemsystem.ShopitemRepository;
import nl.utwente.mastodon.mudtwente.repositories.locationsystem.ShopRepository;
import nl.utwente.mastodon.mudtwente.utils.Constants;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import static nl.utwente.mastodon.mudtwente.utils.Constants.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for the ShopService class.
 */
@RunWith(MockitoJUnitRunner.class)
public class ShopServiceTest {

    @Mock private ShopRepository shopRepositoryMock;
    @Mock private ShopitemRepository shopitemRepositoryMock;

    @InjectMocks private ShopService shopService;

    @Before
    public void setUp() {
        Shop shop1 = new Shop();

        shop1.setLocation(CARILLON_TUTORIAL);

        when(shopRepositoryMock.findByLocation(CARILLON_TUTORIAL)).thenReturn(Optional.of(shop1));
        when(shopRepositoryMock.findByLocation(VRIJHOF)).thenReturn(Optional.empty());

        when(shopRepositoryMock.save(COOP_SHOP)).thenReturn(new Shop());
        when(shopRepositoryMock.save(VESTINGBAR_SHOP)).thenReturn(new Shop());
        when(shopRepositoryMock.save(VRIJHOF_SHOP)).thenReturn(new Shop());
        when(shopRepositoryMock.save(SPIEGEL_SHOP)).thenReturn(new Shop());
        when(shopRepositoryMock.save(ZUL_SHOP)).thenReturn(new Shop());
        when(shopRepositoryMock.save(WAAIER_SHOP)).thenReturn(new Shop());

        ShopItem shopItem = new ShopItem();
        ShopItem shopItem1 = new ShopItem();
        Item item = new Item();
        Item item1 = new Item();
        shopItem.setShop(shop1);
        shopItem1.setShop(shop1);
        shopItem.setItem(item);
        shopItem1.setItem(item1);

        when(shopitemRepositoryMock.findByShop(shop1)).thenReturn(new ArrayList<>(
                Arrays.asList(
                        shopItem,
                        shopItem1)
        ));
    }

    /**
     * Test 2 cases: one with a shop at one location and one without a shop at another location.
     */
    @Test
    public void findByLocation() {
        Optional<Shop> shopOptional1 = shopService.findByLocation(CARILLON_TUTORIAL);
        Optional<Shop> shopOptional2 = shopService.findByLocation(VRIJHOF);

        assertTrue(shopOptional1.isPresent());
        assertFalse(shopOptional2.isPresent());

        assertEquals(CARILLON_TUTORIAL, shopOptional1.get().getLocation());
    }

    /**
     * Tests all five possible strings which can be returned.
     */
    @Test
    public void handleBuyCommand() {
        //TODO implement this after command overhaul
    }

    /**
     * Tests both possible strings wich can be returned.
     */
    @Test
    public void handleWhatToBuyCommand() {
        //No shop at location
        Location location = CARILLON_TUTORIAL;
        MUDCharacter mudCharacter = new MUDCharacter();
        mudCharacter.setLocation(location);
        assertEquals("This location does not have a shop!",
                shopService.handleWhatToBuyCommand(mudCharacter, shopitemRepositoryMock));

        //Shop at location
        location.setShop(true);
        assertEquals("This shop sells: \n" +
                        ":  - 0 coins.\n" +
                        ":  - 0 coins.",
                shopService.handleWhatToBuyCommand(mudCharacter, shopitemRepositoryMock));
    }

    /**
     * Tests addToDatabase method.
     */
    @Test
    public void addToDatabase() {
        shopService.addToDatabase();

        verify(shopRepositoryMock).save(COOP_SHOP);
        verify(shopRepositoryMock).save(VESTINGBAR_SHOP);
        verify(shopRepositoryMock).save(VRIJHOF_SHOP);
        verify(shopRepositoryMock).save(SPIEGEL_SHOP);
        verify(shopRepositoryMock).save(ZUL_SHOP);
        verify(shopRepositoryMock).save(WAAIER_SHOP);
    }
}

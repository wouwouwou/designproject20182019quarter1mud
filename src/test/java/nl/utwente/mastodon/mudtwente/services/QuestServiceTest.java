package nl.utwente.mastodon.mudtwente.services;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.Quest;
import nl.utwente.mastodon.mudtwente.entities.npcsystem.NPC;
import nl.utwente.mastodon.mudtwente.repositories.QuestRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Test for the QuestService class.
 */
@RunWith(MockitoJUnitRunner.class)
public class QuestServiceTest {

    @Mock
    private QuestRepository questRepositoryMock;

    @InjectMocks
    private QuestService questService;

    private MUDCharacter mudCharacter;

    private NPC npc;

    @Before
    public void setUp() throws Exception {
        mudCharacter = new MUDCharacter();
        mudCharacter.setId(21);

        npc = new NPC();
        npc.setName("Henk");

        Quest quest1 = new Quest();

        quest1.setMudCharacter(mudCharacter);
        quest1.setNpc(npc);

        when(questRepositoryMock.findByMudCharacterAndNpc(mudCharacter, npc)).thenReturn(Optional.of(quest1));
        when(questRepositoryMock.findByMudCharacterAndNpc(mudCharacter, new NPC())).thenReturn(Optional.empty());
    }

    /**
     * Test 4 cases: one with a quest and three where it does not return a quest.
     */
    @Test
    public void findByMudCharacterIdAndNpcId() {
        Optional<Quest> questOptional1 = questService.findByMudCharacterAndNpc(mudCharacter, npc);
        Optional<Quest> questOptional2 = questService.findByMudCharacterAndNpc(mudCharacter, new NPC());
        Optional<Quest> questOptional3 = questService.findByMudCharacterAndNpc(new MUDCharacter(), npc);
        Optional<Quest> questOptional4 = questService.findByMudCharacterAndNpc(new MUDCharacter(), new NPC());

        assertTrue(questOptional1.isPresent());
        assertFalse(questOptional2.isPresent());
        assertFalse(questOptional3.isPresent());
        assertFalse(questOptional4.isPresent());

        assertEquals(mudCharacter, questOptional1.get().getMudCharacter());
        assertEquals(npc, questOptional1.get().getNpc());
    }
}

package nl.utwente.mastodon.mudtwente.utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class StringUtilsTest {

    @Test
    public void testValid() {
        assertEquals("2 words", "Drunk mate", StringUtils.setFirstLetterUpperCase(Constants.ENEMY_DRUNK_MATE.getName()));
        assertEquals("1 word", "Bartender", StringUtils.setFirstLetterUpperCase(Constants.BARTENDER.getName()));
        assertEquals("2 words", "Athletics field", StringUtils.setFirstLetterUpperCase(Constants.ATHLETICS_FIELD.getName()));
        assertEquals("@@@@", "@@@@@", StringUtils.setFirstLetterUpperCase("@@@@@"));
        assertEquals("", "", StringUtils.setFirstLetterUpperCase(""));
    }

    @Test
    public void testInvalid() {
        assertNotEquals("2 words", "Drunk Mate", StringUtils.setFirstLetterUpperCase(Constants.ENEMY_DRUNK_MATE.getName()));
        assertNotEquals("1 word", "BarTender", StringUtils.setFirstLetterUpperCase(Constants.BARTENDER.getName()));
        assertNotEquals("2 words", "Athletics Field", StringUtils.setFirstLetterUpperCase(Constants.ATHLETICS_FIELD.getName()));
        assertNotEquals("@@@@", "!@@@@", StringUtils.setFirstLetterUpperCase("@@@@@"));
    }
}

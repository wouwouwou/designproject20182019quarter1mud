package nl.utwente.mastodon.mudtwente.entities.puzzlesystem;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.LightsPuzzleInstance;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.lightspuzzle.LightsPuzzle;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Test for PuzzleInstance Entity
 */
public class PuzzleInstanceTest {

    private PuzzleInstance puzzleInstance;

    @Before
    public void setUp() throws Exception {
        puzzleInstance = new LightsPuzzleInstance();
    }

    /**
     * Tests any constructors which are not default.
     */
    @Test
    public void testConstructors() {
        MUDCharacter mudCharacter = new MUDCharacter();
        LightsPuzzle puzzle = new LightsPuzzle();
        PuzzleInstance puzzleInstance1 = new LightsPuzzleInstance(mudCharacter, puzzle, new ArrayList<>());
        puzzleInstance1.setPuzzle(puzzle);
        puzzleInstance1 = new LightsPuzzleInstance(1, mudCharacter, puzzle, LocalDateTime.now(), new ArrayList<>());
        puzzleInstance1.setPuzzle(puzzle);
    }

    /**
     * Tests getId method.
     */
    @Test
    public void getId() {
        assertEquals(0, puzzleInstance.getId());
    }

    /**
     * Tests getMudCharacter method.
     */
    @Test
    public void getMudCharacter() {
        assertNull(puzzleInstance.getMudCharacter());
    }

    /**
     * Tests getPuzzle method.
     */
    @Test
    public void getPuzzle() {
        assertNull(puzzleInstance.getPuzzle());
    }

    /**
     * Tests getDeletedAt method.
     */
    @Test
    public void getDeletedAt() {
        assertNull(puzzleInstance.getDeletedAt());
    }

    /**
     * Tests setId method.
     */
    @Test
    public void setId() {
        puzzleInstance.setId(1);
        assertEquals(1, puzzleInstance.getId());
    }

    /**
     * Tests setMudCharacter method.
     */
    @Test
    public void setMudCharacter() {
        MUDCharacter mudCharacter = new MUDCharacter();
        puzzleInstance.setMudCharacter(mudCharacter);
        assertEquals(mudCharacter, puzzleInstance.getMudCharacter());
    }

    /**
     * Tests setPuzzle method.
     */
    @Test
    public void setPuzzle() {
        Puzzle puzzle = new LightsPuzzle();
        puzzleInstance.setPuzzle(puzzle);
        assertEquals(puzzle, puzzleInstance.getPuzzle());
    }

    /**
     * Tests setDeletedAt method.
     */
    @Test
    public void setDeletedAt() {
        LocalDateTime deletedAt = LocalDateTime.now();
        puzzleInstance.setDeletedAt(deletedAt);
        assertEquals(deletedAt, puzzleInstance.getDeletedAt());
    }

    @Test
    public void isSolved() {
        assertFalse(puzzleInstance.isSolved());
        puzzleInstance.setDeletedAt(LocalDateTime.now());
        assertTrue(puzzleInstance.isSolved());
    }

    @Test
    public void alreadySolved() {
        Puzzle puzzle = new LightsPuzzle();
        puzzle.setHintAfterCompletion("asdf");
        puzzleInstance.setPuzzle(puzzle);
        assertEquals("You have already solved this! The hint that you got was: asdf",
                puzzleInstance.alreadySolved());
    }
}

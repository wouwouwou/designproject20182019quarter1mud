package nl.utwente.mastodon.mudtwente.entities.puzzlesystem;

import nl.utwente.mastodon.mudtwente.entities.itemsystem.Item;
import nl.utwente.mastodon.mudtwente.entities.locationsystem.Location;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test for Safe entity.
 */
public class SafeTest {

    private Safe safe;

    @Before
    public void setUp() throws Exception {
        safe = new Safe();
    }

    /**
     * Tests any constructors which are not default.
     */
    @Test
    public void testConstructors() {
        Safe safe2 = new Safe(1, new Item(), 1234, "info");
        safe2.setId(3);
    }

    /**
     * Tests getId method.
     */
    @Test
    public void getId() {
        assertEquals(0, safe.getId());
    }

    /**
     * Tests getItem method.
     */
    @Test
    public void getItem() {
        assertEquals(new Item(), safe.getItem());
    }

    /**
     * Tests getPasscode method.
     */
    @Test
    public void getPassCode() {
        assertEquals(0, safe.getPassCode());
    }

    /**
     * Tests getInfo method.
     */
    @Test
    public void getInfo() {
        assertEquals("", safe.getInfo());
    }

    /**
     * Tests setId method.
     */
    @Test
    public void setId() {
        safe.setId(3);
        assertEquals(3, safe.getId());
    }

    /**
     * Tests setItem method.
     */
    @Test
    public void setItem() {
        Item item = new Item();
        item.setId(4);
        safe.setItem(item);
        assertEquals(item, safe.getItem());
    }

    /**
     * Tests setPassCode method.
     */
    @Test
    public void setPassCode() {
        safe.setPassCode(2345);
        assertEquals(2345, safe.getPassCode());
    }

    /**
     * Tests setInfo method.
     */
    @Test
    public void setInfo() {
        safe.setInfo("Heeelp");
        assertEquals("Heeelp", safe.getInfo());
    }
}

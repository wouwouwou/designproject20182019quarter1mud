package nl.utwente.mastodon.mudtwente.entities.npcsystem;

import nl.utwente.mastodon.mudtwente.entities.locationsystem.Location;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test for NPC Entity
 */
public class NPCTest {

    private NPC npc1;

    @Before
    public void setUp() throws Exception {
        npc1 = new NPC();
    }

    /**
     * Tests getId method.
     */
    @Test
    public void getId() {
        assertEquals(0, npc1.getId());
    }

    /**
     * Tests getName method.
     */
    @Test
    public void getName() {
        assertEquals("", npc1.getName());
    }

    /**
     * Tests getLocation method.
     */
    @Test
    public void getLocation() {
        assertNull(npc1.getLocation());
    }

    /**
     * Tests getMinimalStoryLevel method.
     */
    @Test
    public void getMinimalStoryLevel() {
        assertEquals(0, npc1.getMinimalStoryLevel());
    }

    /**
     * Tests isStoryLine method.
     */
    @Test
    public void isStoryLine() {
        assertFalse(npc1.isStoryLine());
    }

    /**
     * Tests setId method.
     */
    @Test
    public void setId() {
        npc1.setId(1);
        assertEquals(1, npc1.getId());
    }

    /**
     * Tests setName method.
     */
    @Test
    public void setName() {
        npc1.setName("slfdoiuhcxzv");
        assertEquals("slfdoiuhcxzv", npc1.getName());
    }

    /**
     * Tests setLocation method.
     */
    @Test
    public void setLocation() {
        Location location = new Location();
        npc1.setLocation(location);
        assertEquals(location, npc1.getLocation());
    }

    /**
     * Tests setMinimalStoryLevel method.
     */
    @Test
    public void setMinimalStoryLevel() {
        npc1.setMinimalStoryLevel(1);
        assertEquals(1, npc1.getMinimalStoryLevel());
    }

    /**
     * Tests setStoryLine method.
     */
    @Test
    public void setStoryLine() {
        npc1.setStoryLine(true);
        assertTrue(npc1.isStoryLine());
    }
}

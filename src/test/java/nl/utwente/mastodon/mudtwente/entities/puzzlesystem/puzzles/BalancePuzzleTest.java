package nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles;

import nl.utwente.mastodon.mudtwente.entities.locationsystem.Location;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.BalancePuzzle;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test for BalancePuzzle Entity
 */
public class BalancePuzzleTest {

    private BalancePuzzle balancePuzzle;

    @Before
    public void setUp() throws Exception {
        balancePuzzle = new BalancePuzzle();
    }

    /**
     * Tests any constructors which are not default.
     */
    @Test
    public void testConstructors() {
        Location location = new Location();
        int amountOfWeights = 3;
        int heaviestWeightId = 2;
        int maxWeighings = 3;
        BalancePuzzle puzzle1 = new BalancePuzzle(amountOfWeights, heaviestWeightId, maxWeighings);
        puzzle1 = new BalancePuzzle(1, "", "", amountOfWeights, heaviestWeightId, maxWeighings);
    }

    /**
     * Tests getAmountOfWeights method.
     */
    @Test
    public void getAmountOfWeights() {
        assertEquals(0, balancePuzzle.getAmountOfWeights());
    }

    /**
     * Tests getHeaviestWeightId method.
     */
    @Test
    public void getHeaviestWeightId() {
        assertEquals(0, balancePuzzle.getHeaviestWeightId());
    }

    /**
     * Tests getMaxWeighings method.
     */
    @Test
    public void getMaxWeighings() {
        assertEquals(0, balancePuzzle.getMaxWeighings());
    }

    /**
     * Tests setAmountOfWeights method.
     */
    @Test
    public void setAmountOfWeights() {
        balancePuzzle.setAmountOfWeights(3);
        assertEquals(3, balancePuzzle.getAmountOfWeights());
    }

    /**
     * Tests setHeaviestWeightId method.
     */
    @Test
    public void setHeaviestWeightId() {
        balancePuzzle.setHeaviestWeightId(3);
        assertEquals(3, balancePuzzle.getHeaviestWeightId());
    }

    /**
     * Tests setMaxWeighings method.
     */
    @Test
    public void setMaxWeighings() {
        balancePuzzle.setMaxWeighings(3);
        assertEquals(3, balancePuzzle.getMaxWeighings());
    }

    /**
     * Tests equals method.
     */
    @Test
    public void equalsTest() {
        BalancePuzzle balancePuzzle1 = new BalancePuzzle();
        balancePuzzle1.setMaxWeighings(3);
        balancePuzzle.setMaxWeighings(3);
        assertEquals(balancePuzzle1, balancePuzzle1);
    }
}

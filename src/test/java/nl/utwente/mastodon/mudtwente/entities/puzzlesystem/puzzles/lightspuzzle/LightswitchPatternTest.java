package nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.lightspuzzle;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Test for LightswitchPattern Entity
 */
public class LightswitchPatternTest {

    private LightswitchPattern lightSwitchPattern;

    @Before
    public void setUp() throws Exception {
        lightSwitchPattern = new LightswitchPattern();
    }

    /**
     * Tests any constructors which are not default.
     */
    @Test
    public void testConstructors() {
        LightswitchPattern lightswitchPattern1 = new LightswitchPattern(new ArrayList<>());
        lightswitchPattern1.setId(0);
        lightswitchPattern1 = new LightswitchPattern(1, new ArrayList<>());
        lightswitchPattern1.setId(1);
    }

    /**
     * Tests getId method.
     */
    @Test
    public void getId() {
        assertEquals(0, lightSwitchPattern.getId());
    }

    /**
     * Tests getSwitchList method.
     */
    @Test
    public void getSwitchList() {
        assertNull(lightSwitchPattern.getSwitchList());
    }

    /**
     * Tests setId method.
     */
    @Test
    public void setId() {
        lightSwitchPattern.setId(1);
        assertEquals(1, lightSwitchPattern.getId());
    }

    /**
     * Tests setSwitchList method.
     */
    @Test
    public void setSwitchList() {
        lightSwitchPattern.setSwitchList(new ArrayList<>());
        assertEquals(0, lightSwitchPattern.getSwitchList().size());
    }
}

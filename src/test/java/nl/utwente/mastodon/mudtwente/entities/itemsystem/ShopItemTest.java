package nl.utwente.mastodon.mudtwente.entities.itemsystem;

import nl.utwente.mastodon.mudtwente.entities.locationsystem.Shop;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test for Shop Entity
 */
public class ShopItemTest {

    private ShopItem shopItem1;

    @Before
    public void setUp() throws Exception {
        shopItem1 = new ShopItem();
    }

    /**
     * Tests getId method.
     */
    @Test
    public void getId() {
        assertEquals(0, shopItem1.getId());
    }

    /**
     * Tests getShopId method.
     */
    @Test
    public void getShopId() {
        assertNull(shopItem1.getShop());
    }

    /**
     * Tests getItemId method.
     */
    @Test
    public void getItemId() {
        assertNull(shopItem1.getItem());
    }

    /**
     * Tests getCost method.
     */
    @Test
    public void getCost() {
        assertEquals(0, shopItem1.getCost());
    }

    /**
     * Tests setId method.
     */
    @Test
    public void setId() {
        shopItem1.setId(1);
        assertEquals(1, shopItem1.getId());
    }

    /**
     * Tests setShopId method.
     */
    @Test
    public void setShopId() {
        Shop shop = new Shop();
        shopItem1.setShop(shop);
        assertEquals(shop, shopItem1.getShop());
    }

    /**
     * Tests setItemId method.
     */
    @Test
    public void setItemId() {
        Item item = new Item();
        shopItem1.setItem(item);
        assertEquals(item, shopItem1.getItem());
    }

    /**
     * Tests setCost method.
     */
    @Test
    public void setCost() {
        shopItem1.setCost(1);
        assertEquals(1, shopItem1.getCost());
    }
}

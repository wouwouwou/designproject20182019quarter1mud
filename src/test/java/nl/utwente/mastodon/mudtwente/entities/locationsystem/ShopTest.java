package nl.utwente.mastodon.mudtwente.entities.locationsystem;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test for Shop Entity
 */
public class ShopTest {

    private Shop shop1;

    @Before
    public void setUp() throws Exception {
        shop1 = new Shop();
    }

    /**
     * Tests getId method.
     */
    @Test
    public void getId() {
        assertEquals(0, shop1.getId());
    }

    /**
     * Tests getName method.
     */
    @Test
    public void getName() {
        assertEquals("", shop1.getName());
    }

    /**
     * Tests getLocation method.
     */
    @Test
    public void getLocation() {
        assertNull(shop1.getLocation());
    }

    /**
     * Tests setId method.
     */
    @Test
    public void setId() {
        shop1.setId(1);
        assertEquals(1, shop1.getId());
    }

    /**
     * Tests setName method.
     */
    @Test
    public void setName() {
        shop1.setName("shop");
        assertEquals("shop", shop1.getName());
    }

    /**
     * Tests setLocation method.
     */
    @Test
    public void setLocation() {
        Location location = new Location();
        shop1.setLocation(location);
        assertEquals(location, shop1.getLocation());
    }
}

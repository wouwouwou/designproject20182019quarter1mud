package nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.RiddlePuzzle;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class BinaryPuzzleInstanceTest {

    private BinaryPuzzleInstance binaryPuzzleInstance;

    @Before
    public void setUp() throws Exception {
        List<String> questions = new ArrayList<>(Arrays.asList("1", "2", "3"));
        List<String> answers = new ArrayList<>(Arrays.asList("4", "5", "6"));

        RiddlePuzzle riddlePuzzle = new RiddlePuzzle();
        riddlePuzzle.setQuestions(questions);
        riddlePuzzle.setAnswers(answers);

        binaryPuzzleInstance = new BinaryPuzzleInstance();
        binaryPuzzleInstance.setPuzzle(riddlePuzzle);
    }

    /**
     * Tests any constructors which are not default.
     */
    @Test
    public void testConstructors() {
        MUDCharacter mudCharacter = new MUDCharacter();
        RiddlePuzzle puzzle = new RiddlePuzzle();
        List<Boolean> lights = new ArrayList<>();
        BinaryPuzzleInstance puzzle1 = new BinaryPuzzleInstance(mudCharacter, puzzle, lights);
        puzzle1.setMudCharacter(mudCharacter);
        puzzle1 = new BinaryPuzzleInstance(1, mudCharacter, puzzle, LocalDateTime.now(), lights);
        puzzle1.setMudCharacter(mudCharacter);
    }

    /**
     * Tests statusOfLights method.
     */
    @Test
    public void statusOfLights() {
        List<Boolean> lights = new ArrayList<>();
        lights.add(true);
        binaryPuzzleInstance.setLights(lights);
        assertEquals("-Light 1 is ON.",
                binaryPuzzleInstance.statusOfLights());
        lights.add(true);
        assertEquals("-Light 1 is ON.\n-Light 2 is ON.",
                binaryPuzzleInstance.statusOfLights());
    }

    /**
     * Tests statusOfLight method.
     */
    @Test
    public void statusOfLight() {
        List<Boolean> lights = new ArrayList<>();
        lights.add(false);
        binaryPuzzleInstance.setLights(lights);
        assertEquals("-Light 1 is OFF.",
                binaryPuzzleInstance.statusOfLight(0));
        lights.add(false);
        assertEquals("-Light 2 is OFF.",
                binaryPuzzleInstance.statusOfLight(1));
    }

    /**
     * Tests calculateDecimal method.
     */
    @Test
    public void calculateDecimal() {
        List<Boolean> lights = new ArrayList<>();
        lights.add(false);
        lights.add(false);
        lights.add(false);
        lights.add(false);
        lights.add(false);
        binaryPuzzleInstance.setLights(lights);
        assertEquals(0, binaryPuzzleInstance.calculateDecimal());
        lights.set(0, true);
        assertEquals(1, binaryPuzzleInstance.calculateDecimal());
        lights.set(1, true);
        assertEquals(3, binaryPuzzleInstance.calculateDecimal());
        lights.set(2, true);
        assertEquals(7, binaryPuzzleInstance.calculateDecimal());
        lights.set(3, true);
        assertEquals(15, binaryPuzzleInstance.calculateDecimal());
        lights.set(4, true);
        assertEquals(31, binaryPuzzleInstance.calculateDecimal());
    }

    /**
     * Tests getStatus method.
     */
    @Test
    public void getStatus() {
        assertEquals("You have answered 0 out of 3 riddles.\n" +
                "Your next riddle is: \n" +
                "1\n" +
                "You also see five lights: \n" +
                "\n" +
                "-Light 1 is OFF.\n" +
                "-Light 2 is OFF.\n" +
                "-Light 3 is OFF.\n" +
                "-Light 4 is OFF.\n" +
                "-Light 5 is OFF.\n" +
                "\n" +
                "There are five levers with which you can turn the lights on or off. Use the 'pull' <number>\n" +
                "command to use them. The lights form your answer to the riddle. Tip: how do you represent a bigger\n" +
                "number with only lights?", binaryPuzzleInstance.getStatus());
        binaryPuzzleInstance.setDeletedAt(LocalDateTime.now());
        assertEquals("You have already solved this! The hint that you got was: ",
                binaryPuzzleInstance.getStatus());
    }

    /**
     * Tests getInstructions method.
     */
    @Test
    public void getInstructions() {
        assertEquals("There are five levers with which you can turn the lights on or off. Use the 'pull' <number>\n" +
                "command to use them. The lights form your answer to the riddle. Tip: how do you represent a bigger\n" +
                "number with only lights?", binaryPuzzleInstance.getInstructions());
    }

    /**
     * Tests getRiddlePuzzle method.
     */
    @Test
    public void getRiddlePuzzle() {
        assertNotNull(binaryPuzzleInstance.getRiddlePuzzle());
    }

    /**
     * Tests correctLightStatus method.
     */
    @Test
    public void correctLightStatus() {
        assertEquals(0, binaryPuzzleInstance.getProgress());
        assertNull(binaryPuzzleInstance.getDeletedAt());
        binaryPuzzleInstance.correctLightStatus();
        assertEquals(1, binaryPuzzleInstance.getProgress());
        assertNull(binaryPuzzleInstance.getDeletedAt());
        binaryPuzzleInstance.correctLightStatus();
        binaryPuzzleInstance.correctLightStatus();
        assertEquals(3, binaryPuzzleInstance.getProgress());
        assertNotNull(binaryPuzzleInstance.getDeletedAt());
    }

    /**
     * Tests isCorrectLightStatus method.
     */
    @Test
    public void isCorrectLightStatus() {
        assertFalse(binaryPuzzleInstance.isCorrectLightStatus());
        binaryPuzzleInstance.getLights().set(2, true);
        assertTrue(binaryPuzzleInstance.isCorrectLightStatus());
    }

    /**
     * Tests getProgress method.
     */
    @Test
    public void getProgress() {
        assertEquals(0, binaryPuzzleInstance.getProgress());
    }

    /**
     * Tests setProgress method.
     */
    @Test
    public void setProgress() {
        binaryPuzzleInstance.setProgress(1);
        assertEquals(1, binaryPuzzleInstance.getProgress());
    }

    /**
     * Tests getLights method.
     */
    @Test
    public void getLights() {
        assertEquals(new ArrayList<>(Arrays.asList(false, false, false, false, false)),
                binaryPuzzleInstance.getLights());
    }

    /**
     * Tests setLights method.
     */
    @Test
    public void setLights() {
        binaryPuzzleInstance.setLights(new ArrayList<>());
        assertEquals(new ArrayList<>(), binaryPuzzleInstance.getLights());
        assertEquals(0, binaryPuzzleInstance.getLights().size());
    }

    /**
     * Tests equals method.
     */
    @Test
    public void equalsTest() {
        MUDCharacter mudCharacter = new MUDCharacter();
        BinaryPuzzleInstance binaryPuzzleInstance1 = new BinaryPuzzleInstance();
        binaryPuzzleInstance1.setMudCharacter(mudCharacter);
        BinaryPuzzleInstance binaryPuzzleInstance2 = new BinaryPuzzleInstance();
        binaryPuzzleInstance2.setMudCharacter(mudCharacter);
        assertEquals(binaryPuzzleInstance2, binaryPuzzleInstance1);
    }
}

package nl.utwente.mastodon.mudtwente.entities.puzzlesystem;

import nl.utwente.mastodon.mudtwente.entities.locationsystem.Location;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.lightspuzzle.LightswitchPattern;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.lightspuzzle.LightsPuzzle;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Test for Puzzle Entity
 */
public class PuzzleTest {

    private Puzzle puzzle;

    @Before
    public void setUp() throws Exception {
        puzzle = new LightsPuzzle();
    }

    /**
     * Tests any constructors which are not default.
     */
    @Test
    public void testConstructors() {
        Location location = new Location();
        List<LightswitchPattern> lists = new ArrayList<>();
        Puzzle puzzle1 = new LightsPuzzle(lists);
        puzzle1 = new LightsPuzzle(1, "", "", lists);
    }

    /**
     * Tests getId method.
     */
    @Test
    public void getId() {
        assertEquals(0, puzzle.getId());
    }

    /**
     * Tests getDescription method.
     */
    @Test
    public void getDescription() {
        assertEquals("", puzzle.getDescription());
    }

    /**
     * Tests getHintAfterCompletion method.
     */
    @Test
    public void getHintAfterCompletion() {
        assertEquals("", puzzle.getHintAfterCompletion());
    }

    /**
     * Tests setId method.
     */
    @Test
    public void setId() {
        puzzle.setId(1);
        assertEquals(1, puzzle.getId());
    }

    /**
     * Tests setDescription method.
     */
    @Test
    public void setDescription() {
        puzzle.setDescription("description");
        assertEquals("description", puzzle.getDescription());
    }

    /**
     * Tests setHintAfterCompletion method.
     */
    @Test
    public void setHintAfterCompletion() {
        puzzle.setHintAfterCompletion("hint");
        assertEquals("hint", puzzle.getHintAfterCompletion());
    }
}

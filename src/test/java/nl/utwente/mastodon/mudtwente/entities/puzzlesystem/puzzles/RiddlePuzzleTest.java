package nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles;

import nl.utwente.mastodon.mudtwente.entities.locationsystem.Location;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Test for RiddlePuzzle Entity.
 */
public class RiddlePuzzleTest {

    private RiddlePuzzle riddlePuzzle;

    @Before
    public void setUp() throws Exception {
        riddlePuzzle = new RiddlePuzzle();
    }

    /**
     * Tests any constructors which are not default.
     */
    @Test
    public void testConstructors() {
        List<String> questions = new ArrayList<>();
        List<String> answers = new ArrayList<>();
        RiddlePuzzle puzzle1 = new RiddlePuzzle(questions, answers, false);
        puzzle1.setQuestions(questions);
        puzzle1 = new RiddlePuzzle(1, "", "", questions, answers, false);
        puzzle1.setQuestions(questions);
    }

    /**
     * Tests getQuestions method.
     */
    @Test
    public void getQuestions() {
        assertEquals(new ArrayList<>(), riddlePuzzle.getQuestions());
    }

    /**
     * Tests getAnswers method.
     */
    @Test
    public void getAnswers() {
        assertEquals(new ArrayList<>(), riddlePuzzle.getAnswers());
    }

    /**
     * Tests setQuestions method.
     */
    @Test
    public void setQuestions() {
        riddlePuzzle.setQuestions(new ArrayList<>());
        assertEquals(new ArrayList<>(), riddlePuzzle.getQuestions());
        assertEquals(0, riddlePuzzle.getQuestions().size());
    }

    /**
     * Tests setAnswers method.
     */
    @Test
    public void setAnswers() {
        riddlePuzzle.setAnswers(new ArrayList<>());
        assertEquals(new ArrayList<>(), riddlePuzzle.getAnswers());
        assertEquals(0, riddlePuzzle.getAnswers().size());
    }

    /**
     * Tests equals method.
     */
    @Test
    public void equalsTest() {
        RiddlePuzzle riddlePuzzle1 = new RiddlePuzzle();
        riddlePuzzle1.setQuestions(new ArrayList<>());
        riddlePuzzle.setQuestions(new ArrayList<>());
        assertEquals(riddlePuzzle1, riddlePuzzle);
    }
}

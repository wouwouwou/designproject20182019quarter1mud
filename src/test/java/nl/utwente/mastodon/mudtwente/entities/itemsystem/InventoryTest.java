package nl.utwente.mastodon.mudtwente.entities.itemsystem;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test for Inventory Entity
 */
public class InventoryTest {

    private Inventory inventory1;

    @Before
    public void setUp() throws Exception {
        inventory1 = new Inventory();
    }

    /**
     * Tests getId method.
     */
    @Test
    public void getId() {
        assertEquals(0, inventory1.getId());
    }

    /**
     * Tests getMudCharacterId method.
     */
    @Test
    public void getMudCharacterId() {
        assertNull(inventory1.getMudCharacter());
    }

    /**
     * Tests getItemId method.
     */
    @Test
    public void getItemId() {
        assertNull(inventory1.getItem());
    }

    /**
     * Tests getRewardAmount method.
     */
    @Test
    public void getAmount() {
        assertEquals(0, inventory1.getAmount());
    }

    /**
     * Tests setId method.
     */
    @Test
    public void setId() {
        inventory1.setId(1);
        assertEquals(1, inventory1.getId());
    }

    /**
     * Tests setMudCharacterId method.
     */
    @Test
    public void setMudCharacterId() {
        MUDCharacter mudCharacter = new MUDCharacter();
        inventory1.setMudCharacter(mudCharacter);
        assertEquals(mudCharacter, inventory1.getMudCharacter());
    }

    /**
     * Tests setItemId method.
     */
    @Test
    public void setItemId() {
        Item item = new Item();
        inventory1.setItem(item);
        assertEquals(item, inventory1.getItem());
    }

    /**
     * Tests setRewardAmount method.
     */
    @Test
    public void setAmount() {
        inventory1.setAmount(1);
        assertEquals(1, inventory1.getAmount());
    }
}

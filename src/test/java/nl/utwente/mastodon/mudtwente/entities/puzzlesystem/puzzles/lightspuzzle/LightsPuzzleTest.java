package nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.lightspuzzle;

import nl.utwente.mastodon.mudtwente.entities.locationsystem.Location;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Test for LightsPuzzle Entity
 */
public class LightsPuzzleTest {

    private LightsPuzzle lightsPuzzle;

    @Before
    public void setUp() throws Exception {
        lightsPuzzle = new LightsPuzzle();
    }

    /**
     * Tests any constructors which are not default.
     */
    @Test
    public void testConstructors() {
        Location location = new Location();
        List<LightswitchPattern> lists = new ArrayList<>();
        LightsPuzzle puzzle1 = new LightsPuzzle(lists);
        puzzle1 = new LightsPuzzle(1, "", "", lists);
    }

    /**
     * Tests getSwitchesLists method.
     */
    @Test
    public void getSwitchesLists() {
        assertNull(lightsPuzzle.getLightswitchPatterns());
    }

    /**
     * Tests setSwitchesLists method.
     */
    @Test
    public void setSwitchesLists() {
        lightsPuzzle.setLightswitchPatterns(new ArrayList<>());
        assertEquals(new ArrayList<>(), lightsPuzzle.getLightswitchPatterns());
        assertEquals(0, lightsPuzzle.getLightswitchPatterns().size());
    }

    /**
     * Tests equals method.
     */
    @Test
    public void equalsTest() {
        LightsPuzzle lightsPuzzle1 = new LightsPuzzle();
        lightsPuzzle1.setLightswitchPatterns(new ArrayList<>());
        lightsPuzzle.setLightswitchPatterns(new ArrayList<>());
        assertEquals(lightsPuzzle1, lightsPuzzle1);
    }
}

package nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.balancepuzzle_instance;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.balancepuzzle_instance.BalancePuzzleInstance;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.BalancePuzzle;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.balancepuzzle_instance.Scale;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * Test for BalancePuzzleInstance Entity
 */
public class BalancePuzzleInstanceTest {

    private BalancePuzzleInstance balancePuzzleInstance;

    @Before
    public void setUp() throws Exception {
        balancePuzzleInstance = new BalancePuzzleInstance();
        balancePuzzleInstance.setPuzzle(new BalancePuzzle());
    }

    /**
     * Tests any constructors which are not default.
     */
    @Test
    public void testConstructors() {
        MUDCharacter mudCharacter = new MUDCharacter();
        BalancePuzzle puzzle = new BalancePuzzle();
        Scale scale = new Scale();
        int pastWeighings = 0;
        BalancePuzzleInstance puzzle1 = new BalancePuzzleInstance(mudCharacter, puzzle, scale);
        puzzle1.setMudCharacter(mudCharacter);
        puzzle1 = new BalancePuzzleInstance(1, mudCharacter, puzzle, LocalDateTime.now(), scale, pastWeighings);
        puzzle1.setMudCharacter(mudCharacter);
    }

    /**
     * Tests getStatus method.
     */
    @Test
    public void getStatus() {
        assertEquals("You have weighed 0 out of 0 attempts.\n" +
                "The goal is to find the one weight out of 0 which is heavier than the others. " +
                "You see a scale which you can use. \n" +
                "\n" +
                "On the left side of the scale are weights: \n" +
                "On the right side of the scale are weights: \n" +
                "On the table are weights: \n" +
                "\n" +
                "You can use 'placeleft' to place weights on the left side of the scale.\n" +
                "'placeright' does the same thing for the right side.\n" +
                "'placedown' places weights off the scale.", balancePuzzleInstance.getStatus());
        balancePuzzleInstance.setDeletedAt(LocalDateTime.now());
        assertEquals("You have already solved this! The hint that you got was: ",
                balancePuzzleInstance.getStatus());
    }

    /**
     * Tests getInstructions method.
     */
    @Test
    public void getInstructions() {
        assertEquals("You can use 'placeleft' to place weights on the left side of the scale.\n" +
                "'placeright' does the same thing for the right side.\n" +
                "'placedown' places weights off the scale.", balancePuzzleInstance.getInstructions());
    }

    /**
     * Tests getBalancePuzzle method.
     */
    @Test
    public void getBalancePuzzle() {
        assertNotNull(balancePuzzleInstance.getBalancePuzzle());
    }

    /**
     * Tests increasePastWeighings method.
     */
    @Test
    public void increasePastWeighings() {
        assertEquals(0, balancePuzzleInstance.getPastWeighings());
        balancePuzzleInstance.increasePastWeighings();
        assertEquals(1, balancePuzzleInstance.getPastWeighings());
    }

    /**
     * Tests getScale method.
     */
    @Test
    public void getScale() {
        assertEquals(new Scale(), balancePuzzleInstance.getScale());
    }

    /**
     * Tests getPastWeighings method.
     */
    @Test
    public void getPastWeighings() {
        assertEquals(0, balancePuzzleInstance.getPastWeighings());
    }

    /**
     * Tests setScale method.
     */
    @Test
    public void setScale() {
        Scale scale = new Scale();
        balancePuzzleInstance.setScale(scale);
        assertEquals(scale, balancePuzzleInstance.getScale());
    }

    /**
     * Tests setPastWeighings method.
     */
    @Test
    public void setPastWeighings() {
        balancePuzzleInstance.setPastWeighings(3);
        assertEquals(3, balancePuzzleInstance.getPastWeighings());
    }

    /**
     * Tests equals method.
     */
    @Test
    public void equalsTest() {
        MUDCharacter mudCharacter = new MUDCharacter();
        BalancePuzzleInstance balancePuzzleInstance1 = new BalancePuzzleInstance();
        balancePuzzleInstance1.setMudCharacter(mudCharacter);
        BalancePuzzleInstance balancePuzzleInstance2 = new BalancePuzzleInstance();
        balancePuzzleInstance2.setMudCharacter(mudCharacter);
        assertEquals(balancePuzzleInstance2, balancePuzzleInstance1);
    }
}

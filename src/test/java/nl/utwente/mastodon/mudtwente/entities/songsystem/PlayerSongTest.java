package nl.utwente.mastodon.mudtwente.entities.songsystem;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test for PlayerSong Entity
 */
public class PlayerSongTest {

    private PlayerSong playerSong1 = new PlayerSong();

    @Before
    public void setUp() throws Exception {
        playerSong1 = new PlayerSong();
    }

    /**
     * Tests getId method.
     */
    @Test
    public void getId() {
        assertEquals(0, playerSong1.getId());
    }

    /**
     * Tests getSong method.
     */
    @Test
    public void getSong() {
        assertNull(playerSong1.getSong());
    }

    /**
     * Tests getMudCharacter method.
     */
    @Test
    public void testGetMudCharacter() {
        assertNull(playerSong1.getMudCharacter());
    }

    /**
     * Tests setId method.
     */
    @Test
    public void setId() {
        playerSong1.setId(1);
        assertEquals(1, playerSong1.getId());
    }

    /**
     * Tests setSong method.
     */
    @Test
    public void setSong() {
        Song song = new Song();
        playerSong1.setSong(song);
        assertEquals(song, playerSong1.getSong());
    }

    /**
     * Tests setMudCharacter method.
     */
    @Test
    public void setMudCharacter() {
        MUDCharacter mudCharacter = new MUDCharacter();
        playerSong1.setMudCharacter(mudCharacter);
        assertEquals(mudCharacter, playerSong1.getMudCharacter());
    }

}

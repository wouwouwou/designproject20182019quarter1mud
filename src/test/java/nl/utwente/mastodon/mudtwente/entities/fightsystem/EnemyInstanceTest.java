package nl.utwente.mastodon.mudtwente.entities.fightsystem;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import org.hibernate.validator.constraints.br.TituloEleitoral;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test for EnemyInstance Entity
 */
public class EnemyInstanceTest {

    private EnemyInstance enemyInstance1;

    @Before
    public void setUp() throws Exception {
        enemyInstance1 = new EnemyInstance();
    }

    /**
     * Tests getId method.
     */
    @Test
    public void getId() {
        assertEquals(0, enemyInstance1.getId());
    }

    /**
     * Tests getMudCharacter method.
     */
    @Test
    public void getMudCharacterId() {
        assertNull(enemyInstance1.getMudCharacter());
    }

    /**
     * Tests getEnemyType method.
     */
    @Test
    public void getEnemyType() {
        assertNull(enemyInstance1.getEnemyType());
    }
    /**
     * Tests getHP method.
     */
    @Test
    public void getHp() {
        assertEquals(0, enemyInstance1.getHp());
    }

    /**
     * Tests setId method.
     */
    @Test
    public void setId() {
        enemyInstance1.setId(1);
        assertEquals(1, enemyInstance1.getId());
    }

    /**
     * Tests setCharacterId method.
     */
    @Test
    public void setCharacterId() {
        MUDCharacter mudCharacter = new MUDCharacter();
        enemyInstance1.setMudCharacter(mudCharacter);
        assertEquals(mudCharacter, enemyInstance1.getMudCharacter());
    }

    /**
     * Tests setEnemyType method.
     */
    @Test
    public void setEnemyType() {
        EnemyType enemyType = new EnemyType();
        enemyInstance1.setEnemyType(enemyType);
        assertEquals(enemyType, enemyInstance1.getEnemyType());
    }

    /**
     * Tests setMaxHP method.
     */
    @Test
    public void setHp() {
        enemyInstance1.setHp(1);
        assertEquals(1, enemyInstance1.getHp());
    }
}

package nl.utwente.mastodon.mudtwente.entities;

import nl.utwente.mastodon.mudtwente.entities.npcsystem.NPC;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test for Quest Entity
 */
public class QuestTest {

    private Quest quest1;

    @Before
    public void setUp() throws Exception {
        quest1 = new Quest();
    }

    /**
     * Tests getId method.
     */
    @Test
    public void getId() {
        assertEquals(0, quest1.getId());
    }

    /**
     * Tests getMudCharacterId method.
     */
    @Test
    public void getMudCharacterId() {
        assertNull(quest1.getMudCharacter());
    }

    /**
     * Tests getNpcId method.
     */
    @Test
    public void getNpcId() {
        assertNull(quest1.getNpc());
    }

    /**
     * Tests getLevel method.
     */
    @Test
    public void getLevel() {
        assertEquals(0, quest1.getLevel());
    }

    /**
     * Tests setId method.
     */
    @Test
    public void setId() {
        quest1.setId(1);
        assertEquals(1, quest1.getId());
    }

    /**
     * Tests setMudCharacterId method.
     */
    @Test
    public void setMudCharacterId() {
        MUDCharacter mudCharacter = new MUDCharacter();
        quest1.setMudCharacter(mudCharacter);
        assertEquals(mudCharacter, quest1.getMudCharacter());
    }

    /**
     * Tests setNpcId method.
     */
    @Test
    public void setNpcId() {
        NPC npc = new NPC();
        quest1.setNpc(npc);
        assertEquals(npc, quest1.getNpc());
    }

    /**
     * Tests setLevel method.
     */
    @Test
    public void setLevel() {
        quest1.setLevel(1);
        assertEquals(1, quest1.getLevel());
    }
}

package nl.utwente.mastodon.mudtwente.entities.npcsystem;

import nl.utwente.mastodon.mudtwente.entities.itemsystem.Item;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test for NPCText Entity
 */
public class NPCTextTest {

    private NPCText npcText1;

    @Before
    public void setUp() throws Exception {
        npcText1 = new NPCText();
    }

    /**
     * Tests getId method.
     */
    @Test
    public void getId() {
        assertEquals(0, npcText1.getId());
    }

    /**
     * Tests getNpcId method.
     */
    @Test
    public void getNpcId() {
        assertNull(npcText1.getNpc());
    }

    /**
     * Tests getText method.
     */
    @Test
    public void getText() {
        assertEquals("", npcText1.getText());
    }

    /**
     * Tests getLevel method.
     */
    @Test
    public void getLevel() {
        assertEquals(0, npcText1.getLevel());
    }

    /**
     * Tests getRequiredItem method.
     */
    @Test
    public void getRequiredItem() {
        assertNull(npcText1.getRequiredItem());
    }

    /**
     * Tests getRequiredAmount method.
     */
    @Test
    public void getRequiredAmount() {
        assertEquals(0, npcText1.getRequiredAmount());
    }

    /**
     * Tests getRewardItem method.
     */
    @Test
    public void getRewardItem() {
        assertNull(npcText1.getRewardItem());
    }

    /**
     * Tests getRewardAmount method.
     */
    @Test
    public void getRewardAmount() {
        assertEquals(0, npcText1.getRewardAmount());
    }

    /**
     * Tests isIncreaseStoryLevel method.
     */
    @Test
    public void isIncreaseStoryLevel() {
        assertFalse(npcText1.isIncreaseStoryLevel());
    }

    /**
     * Tests getMinimalStoryLevel method.
     */
    @Test
    public void getMinimalStoryLevel() {
        assertEquals(0, npcText1.getMinimalStoryLevel());
    }

    /**
     * Tests setId method.
     */
    @Test
    public void setId() {
        npcText1.setId(1);
        assertEquals(1, npcText1.getId());
    }

    /**
     * Tests setNpcId method.
     */
    @Test
    public void setNpcId() {
        NPC npc = new NPC();
        npcText1.setNpc(npc);
        assertEquals(npc, npcText1.getNpc());
    }

    /**
     * Tests setText method.
     */
    @Test
    public void setText() {
        npcText1.setText("jhg");
        assertEquals("jhg", npcText1.getText());
    }

    /**
     * Tests setLevel method.
     */
    @Test
    public void setLevel() {
        npcText1.setLevel(1);
        assertEquals(1, npcText1.getLevel());
    }

    /**
     * Tests setRequiredItem method.
     */
    @Test
    public void setRequiredItem() {
        Item item = new Item();
        npcText1.setRequiredItem(item);
        assertEquals(item, npcText1.getRequiredItem());
    }

    /**
     * Tests setRequiredAmount method.
     */
    @Test
    public void setRequiredAmount() {
        npcText1.setRequiredAmount(1);
        assertEquals(1, npcText1.getRequiredAmount());
    }

    /**
     * Tests setRewardItem method.
     */
    @Test
    public void setRewardItem() {
        Item item = new Item();
        npcText1.setRewardItem(item);
        assertEquals(item, npcText1.getRewardItem());
    }

    /**
     * Tests setRewardAmount method.
     */
    @Test
    public void setRewardAmount() {
        npcText1.setRewardAmount(1);
        assertEquals(1, npcText1.getRewardAmount());
    }

    /**
     * Tests setIncreaseStoryLevel method.
     */
    @Test
    public void setIncreaseStoryLevel() {
        npcText1.setIncreaseStoryLevel(true);
        assertTrue(npcText1.isIncreaseStoryLevel());
    }

    /**
     * Tests setMinimalStoryLevel method.
     */
    @Test
    public void setMinimalStoryLevel() {
        npcText1.setMinimalStoryLevel(1);
        assertEquals(1, npcText1.getMinimalStoryLevel());
    }
}

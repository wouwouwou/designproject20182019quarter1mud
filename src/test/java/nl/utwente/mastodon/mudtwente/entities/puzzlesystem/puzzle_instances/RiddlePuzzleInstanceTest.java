package nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.BalancePuzzle;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.RiddlePuzzle;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class RiddlePuzzleInstanceTest {

    private RiddlePuzzleInstance riddlePuzzleInstance;

    @Before
    public void setUp() throws Exception {
        List<String> questions = new ArrayList<>(Arrays.asList("A", "B", "C"));
        List<String> answers = new ArrayList<>(Arrays.asList("D", "E", "F"));

        RiddlePuzzle riddlePuzzle = new RiddlePuzzle();
        riddlePuzzle.setQuestions(questions);
        riddlePuzzle.setAnswers(answers);
        riddlePuzzleInstance = new RiddlePuzzleInstance();
        riddlePuzzleInstance.setPuzzle(riddlePuzzle);
    }

    /**
     * Tests getStatus method.
     */
    @Test
    public void getStatus() {
        assertEquals("You have answered 0 out of 3 riddles. Your next riddle is: \nA\n\n" +
                "Use the 'answer' command to try and solve this riddle.", riddlePuzzleInstance.getStatus());
        riddlePuzzleInstance.setDeletedAt(LocalDateTime.now());
        assertEquals("You have already solved this! The hint that you got was: ",
                riddlePuzzleInstance.getStatus());
    }

    /**
     * Tests isCorrectAnswer method.
     */
    @Test
    public void isCorrectAnswer() {
        assertFalse(riddlePuzzleInstance.isCorrectAnswer("A"));
        assertTrue(riddlePuzzleInstance.isCorrectAnswer("D"));
    }

    /**
     * Tests correctAnswerGiven method.
     */
    @Test
    public void correctAnswerGiven() {
        assertEquals(0, riddlePuzzleInstance.getProgress());
        riddlePuzzleInstance.correctAnswerGiven();
        assertEquals(1, riddlePuzzleInstance.getProgress());
        assertNull(riddlePuzzleInstance.getDeletedAt());
        riddlePuzzleInstance.correctAnswerGiven();
        riddlePuzzleInstance.correctAnswerGiven();
        assertEquals(3, riddlePuzzleInstance.getProgress());
        assertNotNull(riddlePuzzleInstance.getDeletedAt());
    }

    /**
     * Tests getRiddlePuzzle method.
     */
    @Test
    public void getRiddlePuzzle() {
        assertNotNull(riddlePuzzleInstance.getPuzzle());
        assertNotNull(riddlePuzzleInstance.getRiddlePuzzle());
    }

    /**
     * Tests getProgress method.
     */
    @Test
    public void getProgress() {
        assertEquals(0, riddlePuzzleInstance.getProgress());
    }

    /**
     * Tests setProgress method.
     */
    @Test
    public void setProgress() {
        riddlePuzzleInstance.setProgress(1);
        assertEquals(1, riddlePuzzleInstance.getProgress());
    }

    /**
     * Tests any constructors which are not default.
     */
    @Test
    public void testConstructors() {
        MUDCharacter mudCharacter = new MUDCharacter();
        RiddlePuzzle puzzle = new RiddlePuzzle();
        RiddlePuzzleInstance puzzle1 = new RiddlePuzzleInstance(mudCharacter, puzzle);
        puzzle1.setMudCharacter(mudCharacter);
        puzzle1 = new RiddlePuzzleInstance(1, mudCharacter, puzzle, LocalDateTime.now());
        puzzle1.setMudCharacter(mudCharacter);
    }

    /**
     * Tests equals method.
     */
    @Test
    public void equalsTest() {
        MUDCharacter mudCharacter = new MUDCharacter();
        RiddlePuzzleInstance riddlePuzzleInstance1 = new RiddlePuzzleInstance();
        riddlePuzzleInstance1.setMudCharacter(mudCharacter);
        RiddlePuzzleInstance riddlePuzzleInstance2 = new RiddlePuzzleInstance();
        riddlePuzzleInstance2.setMudCharacter(mudCharacter);
        assertEquals(riddlePuzzleInstance2, riddlePuzzleInstance1);
    }
}

package nl.utwente.mastodon.mudtwente.entities.itemsystem;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test for Shop Entity
 */
public class ItemTest {

    private Item item1;

    @Before
    public void setUp() throws Exception {
        item1 = new Item();
    }

    /**
     * Tests getId method.
     */
    @Test
    public void getId() {
        assertEquals(0, item1.getId());
    }

    /**
     * Tests getName method.
     */
    @Test
    public void getName() {
        assertEquals("", item1.getName());
    }

    /**
     * Tests getDescription method.
     */
    @Test
    public void getDescription() {
        assertEquals("", item1.getDescription());
    }

    /**
     * Tests getAddHp method.
     */
    @Test
    public void getAddHp() {
        assertEquals(0, item1.getAddHp());
    }

    /**
     * Tests setId method.
     */
    @Test
    public void setId() {
        item1.setId(1);
        assertEquals(1, item1.getId());
    }

    /**
     * Tests setName method.
     */
    @Test
    public void setName() {
        item1.setName("asdfcxzv");
        assertEquals("asdfcxzv", item1.getName());
    }

    /**
     * Tests setDescription method.
     */
    @Test
    public void setDescription() {
        item1.setDescription("oicuvzoxic");
        assertEquals("oicuvzoxic", item1.getDescription());
    }

    /**
     * Tests setAddHp method.
     */
    @Test
    public void setAddHp() {
        item1.setAddHp(1);
        assertEquals(1, item1.getAddHp());
    }
}

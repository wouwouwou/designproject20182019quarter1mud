package nl.utwente.mastodon.mudtwente.entities.fightsystem;

import nl.utwente.mastodon.mudtwente.entities.itemsystem.Item;
import nl.utwente.mastodon.mudtwente.entities.locationsystem.Location;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test for EnemyType Entity
 */
public class EnemyTypeTest {

    private EnemyType enemyType1;

    @Before
    public void setUp() throws Exception {
        enemyType1 = new EnemyType();
    }

    /**
     * Tests getId method.
     */
    @Test
    public void getId() {
        assertEquals(0, enemyType1.getId());
    }

    /**
     * Tests getName method.
     */
    @Test
    public void getName() {
        assertEquals("", enemyType1.getName());
    }

    /**
     * Tests getMaxHP method.
     */
    @Test
    public void getHp() {
        assertEquals(0, enemyType1.getMaxHP());
    }

    /**
     * Tests getLevel method.
     */
    @Test
    public void getLevel() {
        assertEquals(0, enemyType1.getLevel());
    }

    /**
     * Tests getLocation method.
     */
    @Test
    public void getLocation() {
        assertNull(enemyType1.getLocation());
    }

    /**
     * Tests getRewardItem method.
     */
    @Test
    public void getReward() {
        assertNull(enemyType1.getReward());
    }

    /**
     * Tests getRewardAmount method.
     */
    @Test
    public void getRewardAmount() {
        assertEquals(0, enemyType1.getRewardAmount());
    }

    /**
     * Tests getPower method.
     */
    @Test
    public void getPower() {
        assertEquals(0, enemyType1.getPower());
    }

    /**
     * Tests setId method.
     */
    @Test
    public void setId() {
        enemyType1.setId(1);
        assertEquals(1, enemyType1.getId());
    }

    /**
     * Tests setName method.
     */
    @Test
    public void setName() {
        enemyType1.setName("sadiuhvcxzoiuh");
        assertEquals("sadiuhvcxzoiuh", enemyType1.getName());
    }

    /**
     * Tests setMaxHP method.
     */
    @Test
    public void setHp() {
        enemyType1.setMaxHP(1);
        assertEquals(1, enemyType1.getMaxHP());
    }

    /**
     * Tests setLevel method.
     */
    @Test
    public void setLevel() {
        enemyType1.setLevel(1);
        assertEquals(1, enemyType1.getLevel());
    }

    /**
     * Tests setLocation method.
     */
    @Test
    public void setLocation() {
        Location location = new Location();
        enemyType1.setLocation(location);
        assertEquals(location, enemyType1.getLocation());
    }

    /**
     * Tests setRewardItem method.
     */
    @Test
    public void setReward() {
        Item item = new Item();
        enemyType1.setReward(item);
        assertEquals(item, enemyType1.getReward());
    }

    /**
     * Tests setRewardAmount method.
     */
    @Test
    public void setRewardAmount() {
        enemyType1.setRewardAmount(1);
        assertEquals(1, enemyType1.getRewardAmount());
    }

    /**
     * Tests setPower method.
     */
    @Test
    public void setPower() {
        enemyType1.setPower(1);
        assertEquals(1, enemyType1.getPower());
    }
}

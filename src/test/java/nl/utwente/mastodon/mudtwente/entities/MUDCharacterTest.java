package nl.utwente.mastodon.mudtwente.entities;

import nl.utwente.mastodon.mudtwente.entities.locationsystem.Location;
import nl.utwente.mastodon.mudtwente.utils.Constants;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.*;

/**
 * Test for MUDCharacter Entity
 */
public class MUDCharacterTest {

    private MUDCharacter mudCharacter1;
    private MUDCharacter mudCharacter2;
    private LocalDateTime localDateTime;

    @Before
    public void setUp() throws Exception {
        localDateTime = LocalDateTime.now();
        mudCharacter1 = new MUDCharacter();
        mudCharacter2 = new MUDCharacter(33, "alsdkjfc", Constants.CARILLON_TUTORIAL, "salfdkj", 33,
                33, LocalDateTime.now(), 33, localDateTime, true,  false);
    }

    /**
     * Tests getId method.
     */
    @Test
    public void getId() {
        assertEquals(0, mudCharacter1.getId());
        assertEquals(33, mudCharacter2.getId());
    }

    /**
     * Tests getName method.
     */
    @Test
    public void getName() {
        assertEquals("", mudCharacter1.getName());
        assertEquals("alsdkjfc", mudCharacter2.getName());
    }

    /**
     * Tests getLocation method.
     */
    @Test
    public void getLocation() {
        assertNull(mudCharacter1.getLocation());
        assertEquals(Constants.CARILLON_TUTORIAL, mudCharacter2.getLocation());
    }

    /**
     * Tests isOnline method.
     */
    @Test
    public void getLastMessage() {
        assertEquals(localDateTime, mudCharacter2.getLastMessageAt());
    }

    /**
     * Tests getUsername method.
     */
    @Test
    public void getUsername() {
        assertEquals("", mudCharacter1.getUsername());
        assertEquals("salfdkj", mudCharacter2.getUsername());
    }

    /**
     * Tests getHealth method.
     */
    @Test
    public void getHealth() {
        assertEquals(0, mudCharacter1.getHealth());
        assertEquals(33, mudCharacter2.getHealth());
    }

    /**
     * Tests getStoryLevel method.
     */
    @Test
    public void getStoryLevel() {
        assertEquals(0, mudCharacter1.getStoryLevel());
        assertEquals(33, mudCharacter2.getStoryLevel());
    }

    /**
     * Tests getMastodonAccountId method.
     */
    @Test
    public void getMastodonAccountId() {
        assertEquals(0, mudCharacter1.getMastodonAccountId());
        assertEquals(33, mudCharacter2.getMastodonAccountId());
    }

    /**
     * Tests setId method.
     */
    @Test
    public void setId() {
        mudCharacter1.setId(1);
        assertEquals(1, mudCharacter1.getId());
    }

    /**
     * Tests setName method.
     */
    @Test
    public void setName() {
        mudCharacter1.setName("mud");
        assertEquals("mud", mudCharacter1.getName());
    }

    /**
     * Tests setLocation method.
     */
    @Test
    public void setLocation() {
        Location location = new Location();
        mudCharacter1.setLocation(location);
        assertEquals(location, mudCharacter1.getLocation());
    }

    /**
     * Tests setOnline method.
     */
    @Test
    public void setLastMessage() {
        localDateTime = LocalDateTime.now();
        mudCharacter2.setLastMessageAt(localDateTime);
        assertEquals(localDateTime, mudCharacter2.getLastMessageAt());
    }

    /**
     * Tests setUsername method.
     */
    @Test
    public void setUsername() {
        mudCharacter1.setUsername("user");
        assertEquals("user", mudCharacter1.getUsername());
    }

    /**
     * Tests setHealth method.
     */
    @Test
    public void setHealth() {
        mudCharacter1.setHealth(1);
        assertEquals(1, mudCharacter1.getHealth());
    }

    /**
     * Tests setStoryLevel method.
     */
    @Test
    public void setStoryLevel() {
        mudCharacter1.setStoryLevel(1);
        assertEquals(1, mudCharacter1.getStoryLevel());
    }

    /**
     * Tests setMastodonAccountId method.
     */
    @Test
    public void setMastodonAccountId() {
        mudCharacter1.setMastodonAccountId(1);
        assertEquals(1, mudCharacter1.getMastodonAccountId());
    }
}

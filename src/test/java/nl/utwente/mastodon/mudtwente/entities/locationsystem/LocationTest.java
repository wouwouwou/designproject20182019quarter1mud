package nl.utwente.mastodon.mudtwente.entities.locationsystem;

import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.Safe;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test for Location Entity
 */
public class LocationTest {

    private Location location1;

    @Before
    public void setUp() throws Exception {
        location1 = new Location();
    }

    /**
     * Tests getId method.
     */
    @Test
    public void getId() {
        assertEquals(0, location1.getId());
    }

    /**
     * Tests getName method.
     */
    @Test
    public void getName() {
        assertEquals("", location1.getName());
    }

    /**
     * Tests getNorth method.
     */
    @Test
    public void getNorth() {
        assertEquals(0, location1.getNorth());
    }

    /**
     * Tests getWest method.
     */
    @Test
    public void getWest() {
        assertEquals(0, location1.getWest());
    }

    /**
     * Tests getSouth method.
     */
    @Test
    public void getSouth() {
        assertEquals(0, location1.getSouth());
    }

    /**
     * Tests getEast method.
     */
    @Test
    public void getEast() {
        assertEquals(0, location1.getEast());
    }

    /**
     * Tests getInfo method.
     */
    @Test
    public void getInfo() {
        assertEquals("", location1.getInfo());
    }

    /**
     * Tests isShop method.
     */
    @Test
    public void isShop() {
        assertFalse(location1.isShop());
    }

    /**
     * Tests getSafe method.
     */
    @Test
    public void getSafe() {
        assertNull(location1.getSafe());
    }

    /**
     * Tests setId method.
     */
    @Test
    public void setId() {
        location1.setId(1);
        assertEquals(1, location1.getId());
    }

    /**
     * Tests setName method.
     */
    @Test
    public void setName() {
        location1.setName("map");
        assertEquals("map", location1.getName());
    }

    /**
     * Tests setNorth method.
     */
    @Test
    public void setNorth() {
        location1.setNorth(1);
        assertEquals(1, location1.getNorth());
    }

    /**
     * Tests setWest method.
     */
    @Test
    public void setWest() {
        location1.setWest(1);
        assertEquals(1, location1.getWest());
    }

    /**
     * Tests setSouth method.
     */
    @Test
    public void setSouth() {
        location1.setSouth(1);
        assertEquals(1, location1.getSouth());
    }

    /**
     * Tests setEast method.
     */
    @Test
    public void setEast() {
        location1.setEast(1);
        assertEquals(1, location1.getEast());
    }

    /**
     * Tests setInfo method.
     */
    @Test
    public void setInfo() {
        location1.setInfo("info");
        assertEquals("info", location1.getInfo());
    }

    /**
     * Tests setShop method.
     */
    @Test
    public void setShop() {
        location1.setShop(true);
        assertTrue(location1.isShop());
    }

    /**
     * Tests setSafe method.
     */
    @Test
    public void setSafe() {
        location1.setSafe(new Safe());
        assertEquals(new Safe(), location1.getSafe());
    }
}

package nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.lightspuzzle.LightsPuzzle;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Array;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Test for LightsPuzzleInstance Entity
 */
public class LightsPuzzleInstanceTest {

    private LightsPuzzleInstance lightsPuzzleInstance;

    @Before
    public void setUp() throws Exception{
        lightsPuzzleInstance = new LightsPuzzleInstance();
        lightsPuzzleInstance.setPuzzle(new LightsPuzzle());
    }

    /**
     * Tests any constructors which are not default.
     */
    @Test
    public void testConstructors() {
        MUDCharacter mudCharacter = new MUDCharacter();
        LightsPuzzle puzzle = new LightsPuzzle();
        List<Boolean> lights = new ArrayList<>();
        LightsPuzzleInstance puzzle1 = new LightsPuzzleInstance(mudCharacter, puzzle, lights);
        puzzle1.setMudCharacter(mudCharacter);
        puzzle1 = new LightsPuzzleInstance(1, mudCharacter, puzzle, LocalDateTime.now(), lights);
        puzzle1.setMudCharacter(mudCharacter);
    }

    /**
     * Tests getLights method.
     */
    @Test
    public void getLights() {
        assertEquals(new ArrayList<>(Arrays.asList(false, false, false, false, false, false)),
                lightsPuzzleInstance.getLights());
    }

    /**
     * Tests setLights method.
     */
    @Test
    public void setLights() {
        lightsPuzzleInstance.setLights(new ArrayList<>());
        assertEquals(new ArrayList<>(), lightsPuzzleInstance.getLights());
        assertEquals(0, lightsPuzzleInstance.getLights().size());
    }

    /**
     * Tests getStatus method.
     */
    @Test
    public void getStatus() {
        assertEquals("You see six lights: \n" +
                "\n" +
                "-Light 1 is OFF.\n" +
                "-Light 2 is OFF.\n" +
                "-Light 3 is OFF.\n" +
                "-Light 4 is OFF.\n" +
                "-Light 5 is OFF.\n" +
                "-Light 6 is OFF.\n" +
                "\n" +
                "Use the 'pull' <number> command to change the status of the lights.\n" +
                "There are 6 levers you can pull here. Each of them changes the lights in a different way.\n" +
                "Tip: look for a pattern!", lightsPuzzleInstance.getStatus());
        lightsPuzzleInstance.setDeletedAt(LocalDateTime.now());
        assertEquals("You have already solved this! The hint that you got was: ",
                lightsPuzzleInstance.getStatus());
    }

    /**
     * Tests getInstructions method.
     */
    @Test
    public void getInstructions() {
        assertEquals("You see six lights: \n" +
                "\n" +
                "-Light 1 is OFF.\n" +
                "-Light 2 is OFF.\n" +
                "-Light 3 is OFF.\n" +
                "-Light 4 is OFF.\n" +
                "-Light 5 is OFF.\n" +
                "-Light 6 is OFF.\n" +
                "\n" +
                "Use the 'pull' <number> command to change the status of the lights.\n" +
                "There are 6 levers you can pull here. Each of them changes the lights in a different way.\n" +
                "Tip: look for a pattern!", lightsPuzzleInstance.getStatus());
    }

    /**
     * Tests getLightsPuzzle method.
     */
    @Test
    public void getLightsPuzzle() {
        assertNotNull(lightsPuzzleInstance.getLightsPuzzle());
    }

    /**
     * Tests switchLights method.
     */
    @Test
    public void switchLights() {
        assertEquals(new ArrayList<>(Arrays.asList(false, false, false, false, false, false)),
                lightsPuzzleInstance.getLights());
        lightsPuzzleInstance.switchLights(new ArrayList<>(Arrays.asList(false, false, false, false, false, false)));
        assertEquals(new ArrayList<>(Arrays.asList(false, false, false, false, false, false)),
                lightsPuzzleInstance.getLights());
        lightsPuzzleInstance.switchLights(new ArrayList<>(Arrays.asList(true, true, true, true, true, true)));
        assertEquals(new ArrayList<>(Arrays.asList(true, true, true, true, true, true)),
                lightsPuzzleInstance.getLights());
    }

    /**
     * Tests statusOfLights method.
     */
    @Test
    public void statusOfLights() {
        List<Boolean> lights = new ArrayList<>();
        lights.add(true);
        lightsPuzzleInstance.setLights(lights);
        assertEquals("-Light 1 is ON.",
                lightsPuzzleInstance.statusOfLights());
        lights.add(false);
        assertEquals("-Light 1 is ON.\n-Light 2 is OFF.",
                lightsPuzzleInstance.statusOfLights());
    }

    /**
     * Tests statusOfLight method.
     */
    @Test
    public void statusOfLight() {
        List<Boolean> lights = new ArrayList<>();
        lights.add(true);
        lightsPuzzleInstance.setLights(lights);
        assertEquals("-Light 1 is ON.",
                lightsPuzzleInstance.statusOfLight(0));
        lights.add(false);
        assertEquals("-Light 2 is OFF.",
                lightsPuzzleInstance.statusOfLight(1));
    }

    /**
     * Tests allLightsOn method. Tests a true and a false return.
     */
    @Test
    public void allLightsOn() {
        List<Boolean> lights = new ArrayList<>();
        lights.add(true);
        lights.add(false);
        lightsPuzzleInstance.setLights(lights);
        assertFalse(lightsPuzzleInstance.allLightsOn());
        lights.set(1, true);
        assertTrue(lightsPuzzleInstance.allLightsOn());
    }

    /**
     * Tests equals method.
     */
    @Test
    public void equalsTest() {
        MUDCharacter mudCharacter = new MUDCharacter();
        LightsPuzzleInstance lightsPuzzleInstance1 = new LightsPuzzleInstance();
        lightsPuzzleInstance1.setMudCharacter(mudCharacter);
        LightsPuzzleInstance lightsPuzzleInstance2 = new LightsPuzzleInstance();
        lightsPuzzleInstance2.setMudCharacter(mudCharacter);
        assertEquals(lightsPuzzleInstance2, lightsPuzzleInstance1);
    }
}

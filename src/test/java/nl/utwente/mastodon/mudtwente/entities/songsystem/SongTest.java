package nl.utwente.mastodon.mudtwente.entities.songsystem;

import nl.utwente.mastodon.mudtwente.entities.itemsystem.Item;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test for Song Entity
 */
public class SongTest {

    private Song song1;

    @Before
    public void setUp() throws Exception {
        song1 = new Song();
    }

    /**
     * Tests getId method.
     */
    @Test
    public void getId() {
        assertEquals(0, song1.getId());
    }

    /**
     * Tests getSongText method.
     */
    @Test
    public void getSongText() {
        assertEquals("", song1.getSongText());
    }

    /**
     * Tests getAnswer method.
     */
    @Test
    public void getAnswer() {
        assertEquals("", song1.getAnswer());
    }

    /**
     * Tests getRewardItem method.
     */
    @Test
    public void getReward() {
        assertNull(song1.getRewardItem());
    }

    /**
     * Tests getRewardAmount method.
     */
    @Test
    public void getAmount() {
        assertEquals(0, song1.getRewardAmount());
    }

    /**
     * Tests setId method.
     */
    @Test
    public void setId() {
        song1.setId(1);
        assertEquals(1, song1.getId());
    }

    /**
     * Tests setSongText method.
     */
    @Test
    public void setSongText() {
        song1.setSongText("songtext");
        assertEquals("songtext", song1.getSongText());
    }

    /**
     * Tests setAnswer method.
     */
    @Test
    public void setAnswer() {
        song1.setAnswer("yes");
        assertEquals("yes", song1.getAnswer());
    }

    /**
     * Tests setRewardItem method.
     */
    @Test
    public void setReward() {
        Item item = new Item();

        song1.setRewardItem(item);
        assertEquals(item, song1.getRewardItem());
    }

    /**
     * Tests setRewardAmount method.
     */
    @Test
    public void setAmount() {
        song1.setRewardAmount(1);
        assertEquals(1, song1.getRewardAmount());
    }
}

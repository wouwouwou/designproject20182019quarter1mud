package nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.balancepuzzle_instance;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * Test for Scale Entity
 */
public class ScaleTest {

    private Scale scale;

    @Before
    public void setUp() throws Exception {
        scale = new Scale();
    }

    /**
     * Tests any constructors which are not default.
     */
    @Test
    public void testConstructors() {
        Scale scale1 = new Scale();
        scale1.setId(0);
        scale1 = new Scale(1, new HashSet<>(), new HashSet<>(), new HashSet<>());
        scale1.setId(1);
    }

    /**
     * Tests getStatus method.
     */
    @Test
    public void scaleStatus() {
        assertEquals("On the left side of the scale are weights: \n" +
                "On the right side of the scale are weights: \n" +
                "On the table are weights: ", scale.getStatus());
    }

    /**
     * Tests isBalanced method.
     */
    @Test
    public void isBalanced() {
        assertTrue(scale.isBalanced());
        scale.getLeftSide().add(4);
        assertFalse(scale.isBalanced());
    }

    /**
     * Tests isLeftSideHeavier method.
     */
    @Test
    public void isLeftSideHeavier() {
        assertFalse(scale.isLeftSideHeavier());
        scale.getLeftSide().add(4);
        assertTrue(scale.isLeftSideHeavier());
    }

    /**
     * Tests isRightSideHeavier method.
     */
    @Test
    public void isRightSideHeavier() {
        assertFalse(scale.isRightSideHeavier());
        scale.getRightSide().add(4);
        assertTrue(scale.isRightSideHeavier());
    }

    /**
     * Tests placeLeft method.
     */
    @Test
    public void placeLeft() {
        scale.getNotOnScale().add(4);
        scale.getRightSide().add(5);
        assertEquals(new HashSet<>(Collections.singletonList(4)), scale.getNotOnScale());
        assertEquals(new HashSet<>(Collections.singletonList(5)), scale.getRightSide());
        assertEquals(new HashSet<>(), scale.getLeftSide());
        scale.placeLeft(4);
        assertEquals(new HashSet<>(), scale.getNotOnScale());
        assertEquals(new HashSet<>(Collections.singletonList(5)), scale.getRightSide());
        assertEquals(new HashSet<>(Collections.singletonList(4)), scale.getLeftSide());
        scale.placeLeft(5);
        assertEquals(new HashSet<>(), scale.getNotOnScale());
        assertEquals(new HashSet<>(), scale.getRightSide());
        assertEquals(new HashSet<>(Arrays.asList(4, 5)), scale.getLeftSide());
    }

    /**
     * Tests placeRight method.
     */
    @Test
    public void placeRight() {
        scale.getNotOnScale().add(4);
        scale.getLeftSide().add(5);
        assertEquals(new HashSet<>(Collections.singletonList(4)), scale.getNotOnScale());
        assertEquals(new HashSet<>(Collections.singletonList(5)), scale.getLeftSide());
        assertEquals(new HashSet<>(), scale.getRightSide());
        scale.placeRight(4);
        assertEquals(new HashSet<>(), scale.getNotOnScale());
        assertEquals(new HashSet<>(Collections.singletonList(4)), scale.getRightSide());
        assertEquals(new HashSet<>(Collections.singletonList(5)), scale.getLeftSide());
        scale.placeRight(5);
        assertEquals(new HashSet<>(), scale.getNotOnScale());
        assertEquals(new HashSet<>(), scale.getLeftSide());
        assertEquals(new HashSet<>(Arrays.asList(4, 5)), scale.getRightSide());
    }

    /**
     * Tests placeDown method.
     */
    @Test
    public void placeDown() {
        scale.getRightSide().add(4);
        scale.getLeftSide().add(5);
        assertEquals(new HashSet<>(Collections.singletonList(4)), scale.getRightSide());
        assertEquals(new HashSet<>(Collections.singletonList(5)), scale.getLeftSide());
        assertEquals(new HashSet<>(), scale.getNotOnScale());
        scale.placeDown(4);
        assertEquals(new HashSet<>(), scale.getRightSide());
        assertEquals(new HashSet<>(Collections.singletonList(4)), scale.getNotOnScale());
        assertEquals(new HashSet<>(Collections.singletonList(5)), scale.getLeftSide());
        scale.placeDown(5);
        assertEquals(new HashSet<>(), scale.getRightSide());
        assertEquals(new HashSet<>(), scale.getLeftSide());
        assertEquals(new HashSet<>(Arrays.asList(4, 5)), scale.getNotOnScale());
    }

    /**
     * Tests weigh method.
     */
    @Test
    public void weigh() {
        assertEquals("The scales are perfectly balanced!", scale.weigh(1));
        scale.getRightSide().add(3);
        assertEquals("The right side of the scale drops. It is heavier than the left side!", scale.weigh(1));
        scale.getLeftSide().add(1);
        assertEquals("The scales are perfectly balanced!", scale.weigh(2));
        assertEquals("The left side of the scale drops. It is heavier than the right side!", scale.weigh(1));
        assertEquals("The right side of the scale drops. It is heavier than the left side!", scale.weigh(3));
    }

    /**
     * Tests reset method.
     */
    @Test
    public void reset() {
        scale.getLeftSide().add(5);
        scale.getRightSide().add(4);
        assertEquals(new HashSet<>(), scale.getNotOnScale());
        assertEquals(new HashSet<>(Collections.singletonList(5)), scale.getLeftSide());
        assertEquals(new HashSet<>(Collections.singletonList(4)), scale.getRightSide());
        scale.reset();
        assertEquals(new HashSet<>(), scale.getRightSide());
        assertEquals(new HashSet<>(), scale.getLeftSide());
        assertEquals(new HashSet<>(Arrays.asList(4, 5)), scale.getNotOnScale());
    }

    /**
     * Tests getId method.
     */
    @Test
    public void getId() {
        assertEquals(0, scale.getId());
    }

    /**
     * Tests getLeftSide method.
     */
    @Test
    public void getLeftSide() {
        assertEquals(new HashSet<>(), scale.getLeftSide());
    }

    /**
     * Tests getRightSide method.
     */
    @Test
    public void getRightSide() {
        assertEquals(new HashSet<>(), scale.getRightSide());
    }

    /**
     * Tests getNotOnScale method.
     */
    @Test
    public void getNotOnScale() {
        assertEquals(new HashSet<>(), scale.getNotOnScale());
    }

    /**
     * Tests setId method.
     */
    @Test
    public void setId() {
        scale.setId(1);
        assertEquals(1, scale.getId());
    }

    /**
     * Tests setLeftSide method.
     */
    @Test
    public void setLeftSide() {
        scale.setLeftSide(new HashSet<>());
        assertEquals(0, scale.getLeftSide().size());
    }

    /**
     * Tests setRightSide method.
     */
    @Test
    public void setRightSide() {
        scale.setRightSide(new HashSet<>());
        assertEquals(0, scale.getRightSide().size());
    }

    /**
     * Tests setNotOnScale method.
     */
    @Test
    public void setNotOnScale() {
        scale.setNotOnScale(new HashSet<>());
        assertEquals(0, scale.getNotOnScale().size());
    }
}

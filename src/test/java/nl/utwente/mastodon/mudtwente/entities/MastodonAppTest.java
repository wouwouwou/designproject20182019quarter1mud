package nl.utwente.mastodon.mudtwente.entities;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.Assert.*;

/**
 * Test for MastodonApp Entity
 */
public class MastodonAppTest {

    private MastodonApp mastodonApp1;
    private LocalDateTime localDateTime;

    @Before
    public void setUp() throws Exception {
        localDateTime = LocalDateTime.now();
        mastodonApp1 = new MastodonApp(0, "", "", localDateTime);
    }

    /**
     * Tests getId method.
     */
    @Test
    public void getId() {
        assertEquals(0, mastodonApp1.getId());
    }

    /**
     * Tests getClientId method.
     */
    @Test
    public void getClientId() {
        assertEquals("", mastodonApp1.getClientId());
    }

    /**
     * Tests getClientSecret method.
     */
    @Test
    public void getClientSecret() {
        assertEquals("", mastodonApp1.getClientSecret());
    }

    /**
     * Tests getCreatedAt method.
     */
    @Test
    public void getCreatedAt() {
        assertEquals(localDateTime, mastodonApp1.getCreatedAt());
    }

    /**
     * Tests setId method.
     */
    @Test
    public void setId() {
        mastodonApp1.setId(1);
        assertEquals(1, mastodonApp1.getId());
    }

    /**
     * Tests setClientId method.
     */
    @Test
    public void setClientId() {
        mastodonApp1.setClientId("asdf");
        assertEquals("asdf", mastodonApp1.getClientId());
    }

    /**
     * Tests setClientSecret method.
     */
    @Test
    public void setClientSecret() {
        mastodonApp1.setClientSecret("fdsa");
        assertEquals("fdsa", mastodonApp1.getClientSecret());
    }

    /**
     * Tests setCreatedAt method.
     */
    @Test
    public void setLocalDateTime() {
        LocalDateTime testTime = LocalDateTime.now();
        mastodonApp1.setCreatedAt(testTime);
        assertEquals(testTime, mastodonApp1.getCreatedAt());
    }
}

CREATE SEQUENCE npcId;
CREATE TABLE npc (
  id integer DEFAULT NEXTVAL('npcId'),
  name varchar(255),
  location integer,
  minimal_story_level integer,
  is_story_line boolean,
  PRIMARY KEY (id)
);

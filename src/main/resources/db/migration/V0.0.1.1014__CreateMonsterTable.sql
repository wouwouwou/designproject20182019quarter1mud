CREATE SEQUENCE monsterId;
CREATE TABLE monster (
  id integer DEFAULT NEXTVAL('monsterId'),
  name text,
  hp integer,
  level integer,
  map integer,
  reward integer,
  reward_amount integer,
  PRIMARY KEY (id)
);

CREATE SEQUENCE attackmonsterId;
CREATE TABLE monster_attack (
  id integer DEFAULT NEXTVAL('attackmonsterId'),
  character_id integer,
  hp integer,
  level integer,
  map integer,
  reward integer,
  reward_amount integer,
  PRIMARY KEY (id)
);

UPDATE mudtwente.mudcharacter
SET mastodon_account_id = id
WHERE id IN (
            SELECT id
            FROM (
                 SELECT id, username, mastodon_account_id
                 FROM mudtwente.mudcharacter
                 WHERE mastodon_account_id IS NULL
                 ) wrongid
            );
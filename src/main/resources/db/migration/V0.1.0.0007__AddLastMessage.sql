ALTER TABLE mudcharacter
ADD COLUMN last_message_at timestamp;
ALTER TABLE mudtwente.mudcharacter ALTER COLUMN last_message_at SET DEFAULT now();
UPDATE mudtwente.mudcharacter
SET last_message_at = now();
ALTER TABLE mudtwente.mudcharacter DROP COLUMN online;
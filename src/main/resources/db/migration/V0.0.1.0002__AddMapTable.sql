CREATE TABLE map (
  id integer,
  name varchar(255),
  north integer,
  west integer,
  south integer,
  east integer
);

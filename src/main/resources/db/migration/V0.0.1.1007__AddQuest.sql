CREATE SEQUENCE questId;
CREATE TABLE quest (
  id integer DEFAULT NEXTVAL('questId'),
  mud_character_id integer,
  npc_id integer,
  level integer,
  PRIMARY KEY (id)
);

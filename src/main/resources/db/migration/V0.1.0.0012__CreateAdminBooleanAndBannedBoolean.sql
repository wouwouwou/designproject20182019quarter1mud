ALTER TABLE mudcharacter
ADD COLUMN admin BOOLEAN,
ADD COLUMN banned BOOLEAN;

ALTER TABLE mudcharacter ALTER COLUMN admin SET DEFAULT FALSE;
UPDATE mudcharacter SET admin = FALSE;

ALTER TABLE mudcharacter ALTER COLUMN banned SET DEFAULT FALSE;
UPDATE mudcharacter SET banned = FALSE;
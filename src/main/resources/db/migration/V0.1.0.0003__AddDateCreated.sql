ALTER TABLE mudtwente.mudcharacter
ADD created_at timestamp;
ALTER TABLE mudtwente.mudcharacter ALTER COLUMN created_at SET DEFAULT now();
UPDATE mudtwente.mudcharacter
SET created_at = now();

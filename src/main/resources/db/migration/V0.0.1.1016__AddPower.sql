ALTER TABLE monster
ADD COLUMN power integer;

ALTER TABLE monster_attack
ADD COLUMN power integer;
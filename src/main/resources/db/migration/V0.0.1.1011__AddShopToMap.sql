ALTER TABLE map ADD COLUMN shop boolean;

CREATE SEQUENCE shopId;
CREATE TABLE shop (
  id integer DEFAULT NEXTVAL('shopId'),
  name text,
  location integer,
  PRIMARY KEY (id)
);

CREATE SEQUENCE shopitemId;
CREATE TABLE shopitem (
  id integer DEFAULT NEXTVAL('shopitemId'),
  shop_id integer,
  item_id integer,
  cost integer,
  PRIMARY KEY (id)
);

CREATE SEQUENCE itemId;
CREATE TABLE item (
  id integer DEFAULT NEXTVAL('itemId'),
  name text,
  description text,
  add_hp integer,
  PRIMARY KEY (id)
);

CREATE SEQUENCE inventoryId;
CREATE TABLE inventory (
  id integer DEFAULT NEXTVAL('inventoryId'),
  mud_character_id integer,
  item_id integer,
  amount integer,
  PRIMARY KEY (id)
);

CREATE SEQUENCE npcTextId;
CREATE TABLE npctext (
  id integer DEFAULT NEXTVAL('npcTextId'),
  npc_id integer,
  text text,
  level integer,
  PRIMARY KEY (id)
  );

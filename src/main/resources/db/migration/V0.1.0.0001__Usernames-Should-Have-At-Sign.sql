UPDATE mudtwente.mudcharacter
SET username = concat('@', username)
WHERE id IN (
            SELECT id
            FROM (
                 SELECT id, username
                 FROM mudtwente.mudcharacter
                 WHERE substr(username, 1, 1) != '@'
                 ) wrongusername
            );

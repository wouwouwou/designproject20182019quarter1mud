ALTER TABLE mudcharacter
ADD COLUMN mastodon_account_id integer;

ALTER TABLE mudcharacter DROP CONSTRAINT mudcharacter_pkey;

ALTER TABLE mudcharacter ADD PRIMARY KEY (id);

CREATE SEQUENCE mudcharacterId;
ALTER TABLE mudcharacter
ALTER COLUMN id SET DEFAULT nextval('mudcharacterId');

SELECT setval('mudcharacterId', (SELECT MAX(id) FROM mudcharacter)+1);
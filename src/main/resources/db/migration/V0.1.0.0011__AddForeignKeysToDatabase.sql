

ALTER TABLE mudtwente.inventory
ADD FOREIGN KEY (item_id) REFERENCES item(id);
ALTER TABLE mudtwente.inventory
ADD FOREIGN KEY (mud_character_id) REFERENCES mudcharacter(id);


ALTER TABLE enemy_type RENAME COLUMN location TO location_id;
ALTER TABLE enemy_type RENAME COLUMN reward to reward_id;
ALTER TABLE mudcharacter RENAME COLUMN location TO location_id;
ALTER TABLE npc RENAME COLUMN location TO location_id;
ALTER TABLE npctext RENAME COLUMN reward_item TO reward_item_id;
ALTER TABLE npctext RENAME COLUMN required_item TO required_item_id;
ALTER TABLE shop RENAME COLUMN location TO location_id;
ALTER TABLE song RENAME COLUMN reward TO reward_item_id;
ALTER TABLE song RENAME COLUMN amount TO reward_amount;

DROP TABLE player_song;
DROP SEQUENCE playersongid;
CREATE SEQUENCE playersongid;
CREATE TABLE player_song (
  id integer default NEXTVAL('playersongid'),
  song_id integer,
  mud_character_id integer,
  PRIMARY KEY (id),
  FOREIGN KEY (song_id) REFERENCES song(id),
  FOREIGN KEY (mud_character_id) REFERENCES mudcharacter(id)
);

UPDATE mudtwente.enemy_type set location_id = null WHERE location_id = 0;
UPDATE mudtwente.enemy_type set reward_id = null WHERE reward_id = 0;
UPDATE mudtwente.mudcharacter set location_id = null WHERE location_id = 0;
UPDATE mudtwente.npc set location_id = null WHERE location_id = 0;
UPDATE mudtwente.npctext SET npc_id = null WHERE npc_id = 0;
UPDATE mudtwente.npctext SET required_item_id = null WHERE required_item_id = 0;
UPDATE mudtwente.npctext SET reward_item_id = null WHERE reward_item_id = 0;
UPDATE mudtwente.quest SET mud_character_id = null WHERE mud_character_id = 0;
UPDATE mudtwente.quest SET npc_id = null WHERE npc_id = 0;
UPDATE mudtwente.shop SET location_id = null WHERE location_id = 0;
UPDATE mudtwente.shopitem SET shop_id = null WHERE shop_id = 0;
UPDATE mudtwente.shopitem SET item_id = null WHERE item_id = 0;
UPDATE mudtwente.song SET reward_item_id = null WHERE reward_item_id = 0;

ALTER TABLE enemy_type ADD FOREIGN KEY (location_id) REFERENCES location(id);
ALTER TABLE enemy_type ADD FOREIGN KEY (reward_id) REFERENCES item(id);
ALTER TABLE mudcharacter ADD FOREIGN KEY (location_id) REFERENCES location(id);
ALTER TABLE npc ADD FOREIGN KEY (location_id) REFERENCES location(id);
ALTER TABLE npctext ADD FOREIGN KEY (npc_id) REFERENCES npc(id);
ALTER TABLE npctext ADD FOREIGN KEY (required_item_id) REFERENCES item(id);
ALTER TABLE npctext ADD FOREIGN KEY (reward_item_id) REFERENCES item(id);
ALTER TABLE quest ADD FOREIGN KEY (mud_character_id) REFERENCES mudtwente.mudcharacter(id);
ALTER TABLE quest ADD FOREIGN KEY (npc_id) REFERENCES npc(id);
ALTER TABLE shop ADD FOREIGN KEY (location_id) REFERENCES location(id);
ALTER TABLE shopitem ADD FOREIGN KEY (shop_id) REFERENCES shop(id);
ALTER TABLE shopitem ADD FOREIGN KEY (item_id) REFERENCES item(id);
ALTER TABLE song ADD FOREIGN KEY (reward_item_id) REFERENCES item(id);

ALTER TABLE shopitem RENAME TO shop_item;

CREATE SEQUENCE songId;
CREATE TABLE song (
  id integer DEFAULT NEXTVAL('songId'),
  song_text text,
  answer text,
  reward integer,
  amount integer,
  PRIMARY KEY (id)
);

CREATE SEQUENCE playersongId;
CREATE TABLE player_song (
  id integer DEFAULT NEXTVAL('playersongId'),
  character_id integer,
  song_text text,
  answer text,
  reward integer,
  amount integer,
  PRIMARY KEY (id, character_id)
);

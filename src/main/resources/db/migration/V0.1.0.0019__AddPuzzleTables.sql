CREATE SEQUENCE scaleId;
CREATE TABLE scale (
  id integer DEFAULT NEXTVAL('scaleId'),
  PRIMARY KEY (id)
);

CREATE TABLE scale_left_side (
  scale_id integer,
  left_side integer,
  FOREIGN KEY (scale_id) references mudtwente.scale(id)
);

CREATE TABLE scale_right_side (
  scale_id integer,
  right_side integer,
  FOREIGN KEY (scale_id) references mudtwente.scale(id)
);

CREATE TABLE scale_not_on_scale (
  scale_id integer,
  not_on_scale integer,
  FOREIGN KEY (scale_id) references mudtwente.scale(id)
);

CREATE SEQUENCE puzzleId;
CREATE TABLE puzzle (
  id integer DEFAULT NEXTVAL('puzzleId'),
  dtype varchar,
  description varchar NOT NULL,
  hint_after_completion varchar NOT NULL,
  binary_puzzle boolean,
  amount_of_weights integer,
  heaviest_weight_id integer,
  max_weighings integer,
  PRIMARY KEY (id)
);

CREATE SEQUENCE puzzleInstanceId;
CREATE TABLE puzzle_instance (
  id integer DEFAULT NEXTVAL('puzzleInstanceId'),
  dtype varchar,
  mud_character_id integer NOT NULL,
  puzzle_id integer NOT NULL,
  deleted_at timestamp,
  progress integer,
  scale_id integer,
  past_weighings integer,
  PRIMARY KEY (id),
  FOREIGN KEY (mud_character_id) REFERENCES mudtwente.mudcharacter(id),
  FOREIGN KEY (puzzle_id) REFERENCES mudtwente.puzzle(id),
  FOREIGN KEY (scale_id) REFERENCES mudtwente.scale(id),
  UNIQUE (mud_character_id, puzzle_id)
);

CREATE SEQUENCE lightswitchPatternId;
CREATE TABLE lightswitch_pattern (
  id integer DEFAULT NEXTVAL('lightswitchPatternId'),
  PRIMARY KEY (id)
);

CREATE TABLE lightswitch_pattern_switch_list (
  lightswitch_pattern_id integer,
  switch_list_order integer,
  switch_list boolean,
  FOREIGN KEY (lightswitch_pattern_id) REFERENCES mudtwente.lightswitch_pattern(id)
);

CREATE TABLE puzzle_lightswitch_patterns (
  lights_puzzle_id integer,
  lightswitch_patterns_id integer,
  lightswitch_patterns_order integer,
  FOREIGN KEY (lights_puzzle_id) references mudtwente.puzzle(id),
  FOREIGN KEY (lightswitch_patterns_id) references lightswitch_pattern(id)
);

CREATE TABLE puzzle_answers (
  puzzle_id integer,
  answers varchar,
  answers_order integer,
  FOREIGN KEY (puzzle_id) references mudtwente.puzzle(id)
);

CREATE TABLE puzzle_questions (
  puzzle_id integer,
  questions varchar,
  questions_order integer,
  FOREIGN KEY (puzzle_id) references mudtwente.puzzle(id)
);

CREATE TABLE puzzle_instance_lights (
  puzzleInstance_id integer,
  lights boolean,
  lights_order integer,
  FOREIGN KEY (puzzleInstance_id) references mudtwente.puzzle_instance(id)
);

CREATE SEQUENCE safeId;
CREATE TABLE safe (
  id integer DEFAULT NEXTVAL('safeId'),
  item_id integer NOT NULL,
  pass_code integer,
  info varchar NOT NULL ,
  PRIMARY KEY (id),
  FOREIGN KEY (item_id) REFERENCES mudtwente.item(id)
);

ALTER TABLE mudtwente.location
  ADD puzzle_id integer,
  ADD safe_id integer,
  ADD FOREIGN KEY (puzzle_id) REFERENCES puzzle(id),
  ADD FOREIGN KEY (safe_id) REFERENCES safe(id);

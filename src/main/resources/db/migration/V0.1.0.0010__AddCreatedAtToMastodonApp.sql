ALTER TABLE mudtwente.mastodon_app
ADD created_at timestamp;
ALTER TABLE mudtwente.mastodon_app ALTER COLUMN created_at SET DEFAULT now();
UPDATE mudtwente.mastodon_app
SET created_at = now();

ALTER TABLE monster RENAME TO enemy_type;
ALTER TABLE enemy_type RENAME COLUMN hp TO maxhp;
ALTER SEQUENCE attackmonsterid RENAME TO enemyinstanceid;
CREATE TABLE enemy_instance (
  id integer default nextVal('enemyinstanceid'),
  mud_character_id integer,
  enemy_type_id integer,
  hp integer,
  PRIMARY KEY (id),
  FOREIGN KEY (mud_character_id) REFERENCES mudtwente.mudcharacter(id),
  FOREIGN KEY (enemy_type_id) REFERENCES mudtwente.enemy_type(id)
);

INSERT INTO enemy_instance (id, mud_character_id, enemy_type_id, hp)
SELECT ma.id, mc.id, et.id, ma.hp
FROM mudtwente.monster_attack ma
LEFT JOIN mudtwente.mudcharacter mc ON character_id = mc.id
LEFT JOIN mudtwente.enemy_type et ON ma.location = et.location AND ma.level = et.level;
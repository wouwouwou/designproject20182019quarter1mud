package nl.utwente.mastodon.mudtwente.utils;

public class StringUtils {
    private StringUtils() {
        throw new IllegalStateException("Utility class");
    }

    public static String setFirstLetterUpperCase(String input) {
        if (input.equals("")) {
            return "";
        }
        return input.substring(0, 1).toUpperCase() + input.substring(1);
    }
}

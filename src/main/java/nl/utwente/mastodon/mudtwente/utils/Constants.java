package nl.utwente.mastodon.mudtwente.utils;

import nl.utwente.mastodon.mudtwente.entities.fightsystem.EnemyType;
import nl.utwente.mastodon.mudtwente.entities.itemsystem.Item;
import nl.utwente.mastodon.mudtwente.entities.itemsystem.ShopItem;
import nl.utwente.mastodon.mudtwente.entities.locationsystem.Location;
import nl.utwente.mastodon.mudtwente.entities.locationsystem.Shop;
import nl.utwente.mastodon.mudtwente.entities.npcsystem.NPC;
import nl.utwente.mastodon.mudtwente.entities.npcsystem.NPCText;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.Safe;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.BalancePuzzle;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.RiddlePuzzle;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.lightspuzzle.LightswitchPattern;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.lightspuzzle.LightsPuzzle;
import nl.utwente.mastodon.mudtwente.entities.songsystem.Song;

import java.util.*;

public class Constants {
    //Just Constants
    private static final String DRUNKMATE_TEXT = "drunk mate";
    private static final String BARTENDER_TEXT = "bartender";
    private static final String VESTINGBAR_TEXT = "vestingbar";
    private static final String WAIT_TEXT = "***\n";
    public static final int SLEEPCOSTS = 5;
    private static final String CUBICUS_TEXT = "cubicus";

    //Items
    public static final Item HAIR = new Item(1, "hair",
            "Hair of an infected student", 0);
    public static final Item PIE = new Item(2, "pie",
            "Pie to eat  and restore 5hp or to give to the examination board", 5);
    public static final Item WEAPON_DESIGN_1 = new Item(3, "weapon design 1",
            "Design of your first weapon", 0);
    public static final Item PLASTIC = new Item(4, "plastic",
            "Plastic to create your first weapon", 0);
    public static final Item PEN = new Item(5, "pen",
            "This pen is your first weapon created by create students", 0);
    public static final Item RUGBY_CLOTHES = new Item(6, "rugby clothes",
            "Your first set of protective armor from the rugby team", 0);
    public static final Item POTION = new Item(7, "potion",
            "A normal potion that restores 10hp", 10);
    public static final Item SUPER_POTION = new Item(8, "super potion",
            "A super potion that restores 20hp", 20);
    public static final Item KLAAS_SIKKEL = new Item(9, "klaas sikkel",
            "Professor of Computer Science", 0);
    //Perhaps change this to be field in MUDCharacter Entity? Would be easier for Inventory management.
    public static final Item COIN = new Item(10, "coins",
            "Coins to buy something", 0);
    public static final Item ROM_LANGERAK_ITEM = new Item(11, "rom langerak",
            "Professor at the University of Twente", 0);
    public static final Item DRUNK_MATE_ITEM = new Item(12, DRUNKMATE_TEXT,
            "A drunk mate who you have met during the Kick-In", 0);
    public static final Item BARTENDER_ITEM = new Item(13, BARTENDER_TEXT,
            "An irritated bartender", 0);
    public static final Item MICROPHONE = new Item(14, "microphone",
            "A microphone to sing with", 0);
    public static final Item THOMAS_CALCULUS = new Item(15, "thomas' calculus",
            "A Thomas' Calculus book to attack with", 0);
    public static final Item ENEMY_HOUSE = new Item(16, "enemy in the house",
            "An enemy in the house", 0);
    public static final Item ITALIAN_MAFIA_ITEM = new Item(17, "italian mafia",
            "A member of the Italian mafia", 0);
    public static final Item CHINESE_TEXT = new Item(18, "chinese letter",
            "A letter in Chinese received from the Italian mafia", 0);
    public static final Item ROSE = new Item(19, "rose",
            "A rose that smells nice", 0);
    public static final Item RAM = new Item(20, "ram",
            "An 8GB of RAM stick", 0);
    public static final Item CUP = new Item(21, "cup",
            "A cup to celebrate you finished the main story line", 0);

    //Locations
    public static final Location ATHLETICS_FIELD = new Location(1, "athletics field",
            -1, -1, 2, -1, "", false, null, null);
    public static final Location SPORT_CENTER = new Location(2, "sport center",
            1, -1, -1, 3, "", false, null, null);
    public static final Location PROMENADE_WEST = new Location(3, "promenade west",
            -1, 2, 4, 5, "", false, null, null);
    public static final Location COOP = new Location(4, "coop",
            3, -1, -1, -1, "", true, null, null);
    public static final Location PROMENADE_MIDDLE_WEST = new Location(5, "promenade middle west",
            -1, 3, 6, 8, "", false, null, null);
    public static final Location BASTILLE = new Location(6, "bastille",
            5, -1, -1, 7, "", false, null, null);
    public static final Location VESTINGBAR = new Location(7, VESTINGBAR_TEXT,
            -1, 6, 13, -1, "", true, null, null);
    public static final Location PROMENADE_MIDDLE_EAST = new Location(8, "promenade middle east",
            -1, 5, -1, 9, "", false, null, null);
    public static final Location PROMENADE_EAST = new Location(9, "promenade east",
            10, 8, 11, 22, "", false, null, null);
    public static final Location VRIJHOF = new Location(10, "vrijhof",
            -1, -1, 9, -1, "", true, null, null);
    public static final Location DRIENERLOLAAN_NORTH = new Location(11, "drienerlolaan 2",
            9, -1, 12, 20, "", false, null, null);
    public static final Location DRIENERLOLAAN_SOUTH = new Location(12, "drienerlolaan south",
            11, 13, 17, -1, "", false, null, null);
    public static final Location CARILLONVELD = new Location(13, "carillon field",
            7, 14, 16, 12, "", false, null, null);
    public static final Location STUDENT_HOUSING = new Location(14, "student housing",
            -1, -1, 15, 13, "", false, null, null);
    public static final Location DRIENERBEEKLAAN = new Location(15, "drienerbeeklaan",
            14, -1, -1, 16, "", false, null, null);
    public static final Location SPIEGEL = new Location(16, "spiegel",
            13, 15, -1, 17, "", true, null, null);
    public static final Location MAIN_ENTRANCE = new Location(17, "main entrance",
            12, 16, -1, 18, "", false, null, null);
    public static final Location HALLENWEG_WEST = new Location(18, "hallenweg west",
            -1, 17, -1, 19, "", false, null, null);
    public static final Location HALLENWEG_EAST = new Location(19, "hallenweg east",
            25, 18, -1, 26, "", false, null, null);
    public static final Location GANZENVELD = new Location(20, "ganzenveld",
            22, 11, -1, 21, "", false, null, null);
    public static final Location ZUL_NORTH = new Location(21, "de zul north",
            23, 20, 25, 44, "", true, null, null);
    public static final Location BOERDERIJLAAN_WEST = new Location(22, "boerderijlaan west",
            -1, 9, 20, 23, "", false, null, null);
    public static final Location BOERDERIJLAAN_EAST = new Location(23, "boerderijlaan east",
            -1, 22, 21, -1, "", false, null, null);
    public static final Location ZUL_SOUTH = new Location(25, "de zul south",
            21, -1, 19, -1, "", false, null, null);
    public static final Location OO_PLAZA = new Location(26, "o&o plaza",
            27, 19, 28, 29, "", false, null, null);
    public static final Location ZILVERLING = new Location(27, "zilverling",
            -1, -1, 26, -1, "", false, null, null);
    public static final Location DESIGN_LAB = new Location(28, "design lab",
            26, -1, -1, -1, "", false, null, null);
    public static final Location WAAIER = new Location(29, "waaier",
            -1, 26, 31, 30, "", true, null, null);
    public static final Location HORSTTOREN = new Location(30, "horsttoren",
            -1, 29, -1, -1, "", false, null, null);
    public static final Location NANOLAB = new Location(31, "nanolab",
            29, -1, -1, -1, "", false, null, null);

    //Tutorial Locations
    public static final Location REGISTRATION_TENT = new Location(32, "registration tent",
            -1, -1, 33, -1, "", false, null, null);
    public static final Location CARILLON_TUTORIAL = new Location(33, "carillon field",
            32, -1, -1, 34, "", false, null, null);
    public static final Location VESTINGBAR_TUTORIAL = new Location(34, VESTINGBAR_TEXT,
            -1, 33, 35, -1, "", false, null, null);
    public static final Location MEDICAL_TENT = new Location(35, "medical tent.",
            34, -1, -1, 36, "", false, null, null);
    public static final Location CULTURE = new Location(36, "culture day",
            37, 35, -1, -1, "", false, null, null);
    public static final Location CAMP = new Location(37, "camp",
            -1, -1, 36, 38, "", false, null, null);
    public static final Location PUB = new Location(38, "pub",
            -1, 37, 39, -1, "", false, null, null);
    public static final Location EXIT = new Location(39, "exit to normal life",
            38, -1, 17, -1, "", false, null, null);

    //Cubicus Safe Creation
    private static final Safe CUBICUS_SAFE = new Safe(1, PLASTIC, 2741,
            "There is a 4-digit code to unlock this safe, but you do not know it!\n " +
                    "Maybe someone left a couple of hints laying around somewhere in the Cubicus...\n" +
                    "(Use \"trycode <code>\" to try opening the safe with a code.)");

    //LightsPuzzle Creation
    private static final LightswitchPattern SWITCHES1 = new LightswitchPattern(1, new ArrayList<>(Arrays.asList(true, false, false, false, true, false)));
    private static final LightswitchPattern SWITCHES2 = new LightswitchPattern(2, new ArrayList<>(Arrays.asList(false, true, false, true, false, true)));
    private static final LightswitchPattern SWITCHES3 = new LightswitchPattern(3, new ArrayList<>(Arrays.asList(true, false, true, false, false, false)));
    private static final LightswitchPattern SWITCHES4 = new LightswitchPattern(4, new ArrayList<>(Arrays.asList(false, true, false, true, true, false)));
    private static final LightswitchPattern SWITCHES5 = new LightswitchPattern(5, new ArrayList<>(Arrays.asList(false, true, false, false, true, true)));
    private static final LightswitchPattern SWITCHES6 = new LightswitchPattern(6, new ArrayList<>(Arrays.asList(true, false, true, false, false, true)));

    private static final List<LightswitchPattern> SWITCHESLIST = createSwitchesList();
    private static List<LightswitchPattern> createSwitchesList()
    {
        List<LightswitchPattern> myList = new ArrayList<>();
        myList.add(SWITCHES1);
        myList.add(SWITCHES2);
        myList.add(SWITCHES3);
        myList.add(SWITCHES4);
        myList.add(SWITCHES5);
        myList.add(SWITCHES6);
        return myList;
    }

    private static final LightsPuzzle LIGHTS_PUZZLE = new LightsPuzzle(4,
            "You see six lights and six levers. A note on the wall reads:\n" +
                    "\"Your goal is to switch all six lights on. Each lever changes the lights in its own way.\n" +
                    "Use 'pull' <leverNumber> in order to pull a lever.\n" +
                    "Use 'puzzlestatus' to check the status of this puzzle.\"",
            "The first digit of the passcode is 2!",
            SWITCHESLIST);

    //BalancePuzzle Creation
    private static final BalancePuzzle BALANCE_PUZZLE = new BalancePuzzle(1,
            "In this room, you see a balance scale, and 9 weights on the table labeled 0 through 8. \n" +
                    "A magical voice whispers in your ear: \"I need you to find the heaviest weight.\n" +
                    "Use the scales to identify the heavier weight. Be warned! You only get to weigh the\n" +
                    "scales 3 times before the puzzle is reset! If you think you have identified the\n" +
                    "heaviest weight, pick it.\" There are six command you can use: \n\n" +
                    "- placeleft <weightNumber>: places the weight with the given number on the\n" +
                    "left side of the scale.\n" +
                    "- placeright <weightNumber>: places the weight with the given number on the\n" +
                    "right side of the scale.\n" +
                    "- placedown <weightNumber>: places the weight with the given number\n" +
                    "down from the scale.\n" +
                    "- pick: <weightNumber>: used to identify the weight after you have used the\n" +
                    "weigh command for 3 times. If incorrect, all weights are magically placed down from\n" +
                    "the scale and you have to start over again. \n" +
                    "- weigh: used to weigh the scale. Only shows if the scale is in balance or if one of" +
                    "the sides is heavier.\n" +
                    "- puzzlestatus: shows the current status of this puzzle.",
            "The second digit of the passcode is 7!",
            9, 1, 2);

    //RiddlePuzzle creation
    private static final String RIDDLE_QUESTION1 = "I have a mouth, yet do not eat. I have a bed, yet do not sleep. " +
            "I always run, but never walk. I always murmur, but never talk. What am I?";
    private static final String RIDDLE_QUESTION2 = "In the morning, I walk on 4 legs. In the afternoon, " +
            "I walk on 2 legs. In the evening, I walk on 3 legs. What am I?";
    private static final String RIDDLE_QUESTION3 = "If you are my brother, but I am not your brother, what am I?";
    private static final String RIDDLE_ANSWER1 = "river";
    private static final String RIDDLE_ANSWER2 = "human";
    private static final String RIDDLE_ANSWER3 = "sister";
    private static final List<String> RIDDLE_QUESTIONS = createStringList(
            RIDDLE_QUESTION1,
            RIDDLE_QUESTION2,
            RIDDLE_QUESTION3
    );
    private static final List<String> RIDDLE_ANSWERS = createStringList(
            RIDDLE_ANSWER1,
            RIDDLE_ANSWER2,
            RIDDLE_ANSWER3
    );

    private static final RiddlePuzzle RIDDLE_PUZZLE = new RiddlePuzzle(3,
            "There is a statue in this room. It says: \"You get three riddles.\n" +
                    "If you answer them all correct, you will receive your next hint.\"\n" +
                    "Use 'puzzlestatus' to get the current riddle to solve. Use 'answer'\n" +
                    "<answer> to answer the riddle. Answers only consist of one word!\n",
            "The third digit of the passcode is 4!",
            RIDDLE_QUESTIONS,
            RIDDLE_ANSWERS,
            false
    );

    //BinaryPuzzle creation
    private static final String BINARY_QUESTION1 = "What is 3 to the power of 2?";
    private static final String BINARY_QUESTION2 = "How many dots are on a single dice?";
    private static final String BINARY_QUESTION3 = "How many legs do 2 chickens, 3 cows, 1 ant and 1 spider have?";
    private static final String BINARY_ANWER1 = "9";
    private static final String BINARY_ANWER2 = "21";
    private static final String BINARY_ANWER3 = "30";
    private static final List<String> BINARY_QUESTIONS = createStringList(
            BINARY_QUESTION1,
            BINARY_QUESTION2,
            BINARY_QUESTION3
    );
    private static final List<String> BINARY_ANSWERS = createStringList(
            BINARY_ANWER1,
            BINARY_ANWER2,
            BINARY_ANWER3
    );

    private static final RiddlePuzzle BINARY_PUZZLE = new RiddlePuzzle(2,
            "You see five lights and five levers. On the one side of each lever you see \"ON\" and on the \n" +
                    "other side \"OFF\". Use the 'pull' <number> command to use them. The lights form your answer \n" +
                    "to the riddle. Tip: how do you represent a bigger number with only lights?\n" +
                    "Use the 'puzzlestatus' command to get the current riddle to solve!",
            "The fourth digit of the passcode is 1!",
            BINARY_QUESTIONS,
            BINARY_ANSWERS,
            true
    );

    //Cubicus maps
    public static final Location CUBICUS1 = new Location(40, CUBICUS_TEXT,
            46, 42, 43, 41, "", false, null, BALANCE_PUZZLE);
    public static final Location CUBICUS2 = new Location(41, CUBICUS_TEXT,
            47, 40, 44, 42, "", false, null, BINARY_PUZZLE);
    public static final Location CUBICUS3 = new Location(42, CUBICUS_TEXT,
            48, 41, 45, 40, "", false, null, null);
    public static final Location CUBICUS4 = new Location(43, CUBICUS_TEXT,
            40, 45, 46, -1, "", false, null, null);
    public static final Location CUBICUS5 = new Location(44, CUBICUS_TEXT,
            41, 21, 47, 45, "", false, CUBICUS_SAFE, null);
    public static final Location CUBICUS6 = new Location(45, CUBICUS_TEXT,
            42, 44, 48, 43, "", false, null, RIDDLE_PUZZLE);
    public static final Location CUBICUS7 = new Location(46, CUBICUS_TEXT,
            43, 48, 40, 47, "", false, null, LIGHTS_PUZZLE);
    public static final Location CUBICUS8 = new Location(47, CUBICUS_TEXT,
            44, 46, 41, 48, "", false, null, null);
    public static final Location CUBICUS9 = new Location(48, CUBICUS_TEXT,
            45, 47, 42, 46, "", false, null, null);

    //Songs
    public static final Song INTER_ACTIEF = new Song(1, "‘t Is Inter-Actief uit het oosten,\n" +
            "Groots en geweldig zijn wij\n" +
            "In dat wat we doen zijn we vaardig",
            "haal je pc er maar bij", MICROPHONE, 1);
    public static final Song SINTERKLAAS = new Song(2, "Sinterklaas kapoentje,\n" +
            "Gooi wat in mijn schoentje,\n" +
            "Gooi wat in mijn laarsje,",
            "dank je sinterklaasje!", MICROPHONE, 1);
    public static final Song MADONNA = new Song(3, "Life is a mystery\n" +
            "Everyone must stand alone\n" +
            "I hear you call my name",
            "and it feels like home", MICROPHONE, 1);
    public static final Song HOUSTON = new Song(4, "Don'tcha wanna dance with me baby\n" +
            "Dontcha wanna dance with me boy\n" +
            "Hey Don'tcha wanna dance with me baby",
            "with somebody who loves me", MICROPHONE, 1);



    //NPCs
    public static final NPC BIOLOGY_STUDENT = new NPC(1, "biology student", NANOLAB, 1, true);
    public static final NPC EXAMINATION_BOARD = new NPC(2, "examination board", ZILVERLING, 2, true);
    public static final NPC UNCLE_DUO = new NPC(3, "uncle duo", VRIJHOF, 3, true);
    public static final NPC NANOLAB_STUDENT = new NPC(4, "nanolab student", NANOLAB, 4, true);
    public static final NPC CREATIVE_TECHNOLOGY_STUDENT = new NPC(5, "creative technology student", DESIGN_LAB, 5, true);
    public static final NPC RUGBY_PLAYER = new NPC(6, "rugby player", ATHLETICS_FIELD, 7, true);
    public static final NPC DREAM = new NPC(7, "dream", STUDENT_HOUSING, 6, true);
    public static final NPC HOUSE_MATE_NPC = new NPC(16, "house mate", STUDENT_HOUSING, 10, true);
    public static final NPC ITALIAN_MAFIA_MEMBER = new NPC(17, "italian mafia member", GANZENVELD, 11, true);
    public static final NPC SMART_STUDENT = new NPC(18, "smart student", VRIJHOF, 13, true);
    public static final NPC BLAKE = new NPC(19, "blake", SPORT_CENTER, 14, true);
    public static final NPC ICTS_NPC = new NPC(20, "icts", OO_PLAZA, 17, true);
    public static final NPC JAIR = new NPC(21, "jair", ZILVERLING, 18, true);
    public static final NPC ACCOUNTANT = new NPC(22, "accountant", SPIEGEL, 20, true);
    public static final NPC NOBODY = new NPC(23, "nobody", ZILVERLING, 23, true);
    public static final NPC DJMADPROFESSOR = new NPC(24, "djmadprofessor", HORSTTOREN, 25, true);

    // Tutorial NPCs
    public static final NPC INFO_NPC = new NPC(8, "info", REGISTRATION_TENT, 1, true);
    public static final NPC COMMANDS_NPC = new NPC(9, "commands", CARILLON_TUTORIAL, 1, true);
    public static final NPC DRUNK_MATE = new NPC(10, DRUNKMATE_TEXT, VESTINGBAR_TUTORIAL, 1, true);
    public static final NPC DOCTOR_NPC = new NPC(11, "doctor", MEDICAL_TENT, 1, true);
    public static final NPC CULTURE_NPC = new NPC(12, "culture", CULTURE, 1, true);
    public static final NPC BARTENDER = new NPC(13, BARTENDER_TEXT, CAMP, 1, true);
    public static final NPC BARTENDERS_BOSS = new NPC(14, "boss", PUB, 1, true);
    public static final NPC EXIT_NPC = new NPC(15, "exit", EXIT, 1, true);

    //Normal NPCTexts
    public static final NPCText BIOLOGY_STUDENT_TEXT_1 = new NPCText(1, BIOLOGY_STUDENT,
            "Hello there! I need your help! " +
                    "It seems like some kind of virus has infected some students around the campus. " +
                    "I'd like to figure out what it is exactly, but they will not let me come close. " +
                    "Can you knock one of them out and grab some hairs so that I can research them. " +
                    "There is a student at the current location. " +
                    "Use the {attack} command to attack him!",
            1, null,0, null, 0, false, 1);
    public static final NPCText BIOLOGY_STUDENT_TEXT_2 = new NPCText(2, BIOLOGY_STUDENT,
            "Great! You found a piece of hair! " +
                    "I will research it now.\n...\n...\n" +
                    "Hmm... This appears to be a new kind of virus, and quite contagious, too!" +
                    "Since it has appeared just after the Kick-In, we will call it:: " +
                    "The Kick-In Virus!\n" +
                    "You should go to the Examination board to figure out what to do with these students! " +
                    "The Examination board is located at the Zilverling. " +
                    "The Zilverling is located to the north-west of your current location.",
            2, HAIR, 1, null, 0, true, 1);
    public static final NPCText EXAMINATION_BOARD_TEXT_1 = new NPCText(3, EXAMINATION_BOARD,
            "The examination board seem to be very busy talking amongst themselves. " +
                    "You try to speak up multiple times, but you're not getting through to them. " +
                    "Maybe if you had some delicious pies with you, they will be more willing to listen to you...\n" +
                    "However, you don't have any money to buy things with! " +
                    "You should go to Uncle Duo at the Vrijhof and see if he won't give you some money!. " +
                    "The Vrijhof is located to the north-west of your current location.",
            1, null, 0, null, 0, true, 2);
    public static final NPCText UNCLE_DUO_TEXT_1 = new NPCText(4, UNCLE_DUO,
            "You explain the current situation to Uncle Duo, but he is not convinced yet. " +
                    "You decide to threaten him with some recursive haskell code!\n" +
                    "First he gives you one coin. Then another. Then another. " +
                    "You decide to leave once you have 15 coins, enough to buy 3 pies at the Coop, one for each member of the Examination Board!" +
                    "The Coop is located south-west of your current location.",
            1, null, 0, COIN, 15, true, 3);
    public static final NPCText EXAMINATION_BOARD_TEXT_2 = new NPCText(5, EXAMINATION_BOARD,
            "The examination board is more than willing to listen to you after you bring them some pies!\n" +
                    "You quickly explain the situation to them, and they decide the best solution for now is to just subdue all the infected. " +
                    "However, you will need a weapon to subdue them with." +
                    "Maybe one of the students in the Nanolab might be able to help you with that! " +
                    "The Nanolab is located south-east of your current location.",
            2, PIE, 3, null, 0, true, 4);
    public static final NPCText NANOLAB_STUDENT_TEXT_1 = new NPCText(6, NANOLAB_STUDENT,
            "You explain to the Nanolab student that you need a weapon to subdue other students with," +
                    "and yes, you do have approval from the examination board to get it.\n" +
                    "It sounds like he does not really believe you, but luckily he decides to help you anyway." +
                    "He says: \"" +
                    "I can create a weapon for you, but I need a design and some materials. " +
                    "Go to the design lab and find a creative technology student, he can help you further.\"\n" +
                    "The Design lab is located to your west.",
            1, null, 0, null, 0, true, 5);
    public static final NPCText CREATE_STUDENT_TEXT_1 = new NPCText(7, CREATIVE_TECHNOLOGY_STUDENT,
            "Luckily, the CreaTe student accepts your proposal immediately!\n" +
                    "He says: \"I can create some designs for you, but in the meantime, you will have to gather the materials yourself!" +
                    "They are located in the Cubicus. " +
                    "Watch out! The Cubicus is a maze!" +
                    "Once you have found the materials, return to me.",
            1, null, 0, null, 0, false, 6);
    public static final NPCText CREATE_STUDENT_TEXT_2 = new NPCText(8, CREATIVE_TECHNOLOGY_STUDENT,
            "\"Great! You have found the plastic materials! " +
                    "Just in time too, I have just finished the design!\"" +
                    "He hands you the design. " +
                    "Go back to the Nanolab Student to create your very first weapon!",
            2, PLASTIC, 1, WEAPON_DESIGN_1, 1, true, 6);
    public static final NPCText NANOLAB_STUDENT_TEXT_2 = new NPCText(9, NANOLAB_STUDENT,
            "\"Cool! You have found both the plastic materials and the design. Give them to me and I will make your weapon!\"" +
                    "The student tinkers a bit with the plastic materials, and in no time at all hands you a ...pen? " +
                    "\"There you go! Here is your first weapon: The Pen 1000!\"" +
                    "Just as you take the weapon from him, someone busts the door down! " +
                    "Rom Langerak has appeared and he is infected. " +
                    "Knock him out and report back to the Examination board!",
            2, WEAPON_DESIGN_1, 1, PEN, 1, false, 7);
    public static final NPCText EXAMINATION_BOARD_TEXT_3 = new NPCText(10, EXAMINATION_BOARD,
            "The examination board looks quite impressed with your progress! " +
                    "\"You have already subdued a lot of infected students and even one professor. " +
                    "We have decided to promote you to level 2\"" +
                    "You have unlocked new weapons and armors!\n" +
                    "It is getting late. You should go to your student house to get some sleep and {dream} about your future. " +
                    "Your Student house is located west of your current location.",
            3, ROM_LANGERAK_ITEM, 1, null, 0, true, 7);
    public static final NPCText DREAM_TEXT_1 = new NPCText(13, DREAM,
            "You dream of a bad scenario. A lot more students have become infected as well, " +
                    "and to top it off there is a massive monster on the highest floor of the Horsttoren. " +
                    "The monster is so hideous and scary that you awaken immediately. " +
                    "You look out of the window and see that it was not a dream at all! " +
                    "At the highest floor of the Horsttoren there is a massive monster and on the road towards him are a lot of infected people. " +
                    "Too many to take on without at least some protective equipment... " +
                    "Maybe some of the rugby players on the athletics field can help you out with that!\n" +
                    "The Athletics field is located to the north-west of your current location.",
            1, null, 0, null, 0, true, 8);
    public static final NPCText RUGBY_PLAYER_TEXT_1 = new NPCText(11, RUGBY_PLAYER,
            "The rugby equipment is very expensive, and the rugby team won't just give it to you for free!\n" +
                    "They say: \"We need a new tackling pad, but we want to use a real human for it!" +
                    "We have had our eyes on Klaas Sikkel for a while now, and he seems like the perfect person for this! Go and get him! \"" +
                    "Go and get Klaas Sikkel! " +
                    "Klaas Sikkel is located in the Zilverling. " +
                    "The Zilverling is located south-east of your current location.",
            1, null, 0, null, 0, false, 9);
    public static final NPCText RUGBY_PLAYER_TEXT_2 = new NPCText(12, RUGBY_PLAYER,
            "Thanks for bringing us Klaas Sikkel. " +
                    "We are going to use him as our training pad. " +
                    "Here is your rugby equipment!\n" +
                    "Just wearing armor is not enough to help you, though. " +
                    "There are also items called potions. " +
                    "You can buy them at shops. " +
                    "Talk to me again to gain information about how to use a potion.",
            2, KLAAS_SIKKEL, 1, RUGBY_CLOTHES, 1, false, 9);
    public static final NPCText RUGBY_PLAYER_TEXT_3 = new NPCText(22, RUGBY_PLAYER,
            "This is a potion. " +
                    "The command for using this potion is:\n" +
                    "usepotion potion\n" +
                    "**POOF** you received 10 more hp. " +
                    "Here is a new potion for you. " +
                    "Use it in your next fight.\n" +
                    "I've heard that there are monsters attacking the student housing. " +
                    "Go and speak to your house mate to help defend your house.\n" +
                    "Student housing is located south-east of your current location.",
            3, null, 0, POTION, 1, true, 9);
    public static final NPCText HOUSE_MATE_TEXT_1 = new NPCText(23, HOUSE_MATE_NPC,
            "\"There you finally are! There are infected students attacking our houses! " +
                    "Maybe this will help defeating them...\"\n" +
                    "You have received a Thomas' Calculus book. " +
                    "Try to kill the enemy that has invaded your house. " +
                    "Do not forget to make use of your potion(s) when you need it. " +
                    "After you've killed the enemy, report back to the Examination board.",
            1, null, 0, THOMAS_CALCULUS, 1, true, 10);
    public static final NPCText EXAMINATION_BOARD_TEXT_4 = new NPCText(24, EXAMINATION_BOARD,
            "You tell the examination board about what happened at your house and the examination board " +
                    "is happy to hear that no one is injured.\n" +
                    "They say: \"We have heard from a trusted source that the Italian mafia could be behind this virus. " +
                    "We think that they are located at the Ganzenveld, " +
                    "since they like to eat geese. " +
                    "Try to defeat one member of the Italian mafia and interrogate him.\"\n" +
                    "The Ganzenveld is located north-west of your current location.",
            4, ENEMY_HOUSE, 1, null, 0, true, 11);

    public static final NPCText ITALIAN_MAFIA_MEMBER_TEXT_1 = new NPCText(25, ITALIAN_MAFIA_MEMBER,
            "\"You think we have created the Kick-In virus? You are wrong my friend, " +
                    "we also want the virus to be gone, because it infects also our geese. " +
                    "We received a letter written in Chinese from our rivals, the Chinese mafia. " +
                    "Try to reveal the translation of the text at the Vrijhof. " +
                    "There should be at least one smart student there that can translate the letter for you.\n" +
                    "The Vrijhof is located north-west of you.",
            1, ITALIAN_MAFIA_ITEM, 1, CHINESE_TEXT, 1, true, 12);
    public static final NPCText SMART_STUDENT_TEXT_1 = new NPCText(26, SMART_STUDENT,
            "\"You want me to translate this letter for you? " +
                    "I could do that, but it is not going to happen for free. " +
                    "You see, I have fallen in love with this guy called Blake. " +
                    "He is often at the sport center. " +
                    "However, I'm afraid of going up to him and speaking with him. " +
                    "Could you please try to find him and give these 10 roses to him?\"" +
                    "Sport center is located south-east of you.",
            1, CHINESE_TEXT, 1, ROSE, 10, true, 13);
    public static final NPCText BLAKE_TEXT_1 = new NPCText(27, BLAKE,
            "Wow, thanks for the roses. " +
                    "Ohh, you had to give them to me from that smart student at the Vrijhof. " +
                    "That is really nice, I will give her a kiss at the next party. ",
            1, ROSE, 10, null, 0, true, 14);
    public static final NPCText SMART_STUDENT_TEXT_2 = new NPCText(28, SMART_STUDENT,
            "\"Thanks for bringing the roses to Blake. " +
                    "I hope he likes them and gives me a kiss at the next party. " +
                    "Alright, let me take a look at that letter.\n" +
                    WAIT_TEXT +
                    WAIT_TEXT +
                    "The letter says that a mad professor that has created this virus, " +
                    "but even the Chinese mafia does not know who this professor is!\"\n" +
                    "Interesting... You should report this to the examination board at the Zilverling. " +
                    "Zilverling is located south-east of you. ",
            2, null, 0, null, 0, true, 15);
    public static final NPCText EXAMINATION_BOARD_TEXT_5 = new NPCText(29, EXAMINATION_BOARD,
            "You explain your findings to the examination board." +
                    "They say: \"This is indeed an interesting situation. " +
                    "We do not know who this mad professor could be either. " +
                    "Let us discuss for a few minutes privately.\n" +
                    WAIT_TEXT +
                    WAIT_TEXT +
                    "We have checked the Mastodon server of the University of Twente " +
                    "and it seems that there is an account there called djmadprofessor. " +
                    "Go to the o&o plaza and talk to one of the ICTS service people. " +
                    "Surely they can retrieve the information about the djmadprofessor account.\"",
            5, null, 0, null, 0, true, 16);
    public static final NPCText ICTS_TEXT_1 = new NPCText(30, ICTS_NPC,
            "\"Oooh, you have a hacking task for me! " +
                    "That seems very interesting, but I need more RAM to finish this task... " +
                    "There is a bus at Hallenweg west." +
                    "Try to attack that bus to retrieve the ram of the ECU of the bus!\"\n" +
                    "Hallenweg west is located west of you.",
            1, null, 0, null, 0, false, 17);
    public static final NPCText ICTS_TEXT_2 = new NPCText(31, ICTS_NPC,
            "\"Thanks for bringing me the RAM. " +
                    "With this new RAM stick I can do the hacking task.\n" +
                    WAIT_TEXT +
                    WAIT_TEXT +
                    WAIT_TEXT +
                    "**POOF** His computer has exploded.\n" +
                    "\"It seems like that I was not the only person that was hacking, " +
                    "djmadprofessor has hacked me and let my computer explode. " +
                    "You have to find another way to retrieve his identity. " +
                    "I think the best idea right now is to hire a booter and fix my computer. " +
                    "Then, we can DDoS djmadprofessor and then perform the hacking task. " +
                    "Go and find Jair at the Zilverling.\"\n" +
                    "Zilverling is located north of you.",
            2, RAM, 1, null, 0, true, 17);
    public static final NPCText JAIR_TEXT_1 = new NPCText(32, JAIR,
            "\"Unfortunately, I do not perform DDoS attacks, I only research them. " +
                    "But I could tell you how to create a DDoS attack: " +
                    "Go to: http://donothireabooter.com and pay a small fee " +
                    "and paste this url in the website to attack form: " +
                    "http://djmadprofessor.com\"\n" +
                    "Unfortunately, you don't have any money. " +
                    "Talk to the examination board to gain some coins such that you" +
                    "can pay the booter to 'stress test' the server of djmadprofessor. ",
            1, null, 0, null, 0, true, 18);
    public static final NPCText EXAMINATION_BOARD_TEXT_6 = new NPCText(33, EXAMINATION_BOARD,
            "\"Let me get this straight: you want to ask for funding to DDoS the server of djmadprofessor. " +
                    "We would love to give you this fund, but the accountant of the University " +
                    "also has to agree with this decision. And since such an action is illegal " +
                    "he will refuse it. But we are sure you'll come up with some way to get the funds anyway.\"" +
                    "The account can be found " +
                    "at the Spiegel.\"\n" +
                    "The Spiegel is located south-west of you. ",
            6, null, 0, null, 0, true, 19);
    public static final NPCText ACCOUNTANT_TEXT_1 = new NPCText(34, ACCOUNTANT,
            "\"You want funding for what now? " +
                    "A DDOS attack on the server of djmadprofessor?" +
                    "How in the world did you think I would agree to that?\"\n " +
                    WAIT_TEXT +
                    "You scream some COBOL programming language syntax at him " +
                    "and he gets confused. In his confusion, he drops 20 coins on the ground!\n" +
                    "That should be enough funding for the DDoS attack!" +
                    "Get back to Jair at the Zilverling. " +
                    "Zilverling is located north-east of you.",
            1, null, 0, COIN, 20, true, 20);
    public static final NPCText JAIR_TEXT_2 = new NPCText(35, JAIR,
            "\"Thanks for bringing me the 20 coins. " +
                    "Now we can start to DDoS the server of djmadprofessor. " +
                    "Quickly, go to the ICTS service desk " +
                    "and ask them to hack his server for you.\" " +
                    "ICTS is located at O&O plaza. " +
                    "O&O plaza is located south of you.",
            2, COIN, 20, null, 0, true, 21);
    public static final NPCText ICTS_TEXT_3 = new NPCText(36, ICTS_NPC,
            "\"Alright, I'm going to hack djmadprofessor's server now. " +
                    "Hopefully everything goes flawlessly today!\n" +
                    WAIT_TEXT +
                    WAIT_TEXT +
                    WAIT_TEXT +
                    WAIT_TEXT +
                    "Alright, I'm in! This is the personal data of djmadprofessor:\n" +
                    "NAME: Djoerd Hiemstra\n" +
                    "ADDRESS: Drienerlolaan 5, 7522 NB Enschede\n" +
                    "BUILDING: Zilverling\n" +
                    "ROOM: ZI-4096\n" +
                    "Take a look if he is still at his room at the Zilverling.\"" +
                    "Zilverling is located north of you.",
            3, null, 0, null, 0, true, 22);
    public static final NPCText NOBODY_TEXT_1 = new NPCText(37, NOBODY,
            "There is nobody here. " +
                    "Report back to the examination board.",
            1, null, 0, null, 0, true, 23);
    public static final NPCText EXAMINATION_BOARD_TEXT_7 = new NPCText(38, EXAMINATION_BOARD,
            "\"It seems like Djoerd has escaped from his room. " +
                    "You'll need to find him!" +
                    "We heard that he has been hidden in the Horsttoren. " +
                    "Go there, find him, and talk to him.\"\n" +
                    "Hosttoren is located south-east of you.",
            7, null, 0, null, 0, true, 24);
    public static final NPCText DJMADPROFESSOR_TEXT_1 = new NPCText(39, DJMADPROFESSOR,
            "\"MUAHAHAHAHAHA!\n" +
                    "So you have finally found me! " +
                    "Don't even bother trying anymore, it is already too late! " +
                    "Soon everyone at the University of Twente will be infected with my virus!\n" +
                    "But if you insist, I will kill YOU first!\"\n" +
                    "Oh no! You need to put an end to this right now! " +
                    "Defeat Djoerd and end the Virus once and for all!",
            1, null, 0, null, 0, true, 25);

    //Tutorial NPCTexts
    public static final NPCText TUTORIAL_INFO_TEXT_1 = new NPCText(14, INFO_NPC,
            "Welcome to the world of the University of Twente. " +
                    "You are currently located at the Registration tent. " +
                    "Type {go south} to go to the Carrilon field." +
                    "When you're there, {talk} to the Commands NPC.",
            1, null, 0, null, 0, false, 1);
    public static final NPCText TUTORIAL_COMMANDS_TEXT_1 = new NPCText(15, COMMANDS_NPC,
            "As you might have guessed, you play this game by inputting commands into Mastodon!" +
                    "Use the {help} command to figure out all commands. " +
                    "The basic commands are: help, go {direction}, location, locationinfo, whattobuy, buy, usepotion {potion}. " +
                    "Use the {location} command to find out where to go to talk to your " + DRUNKMATE_TEXT + ".",
            1, null, 0, null, 0, false, 1);
    public static final NPCText TUTORIAL_DRUNK_MATE_TEXT_1 = new NPCText(16, DRUNK_MATE,
            "Your 'friend' has had a bit too much to drink." +
                    "Your actions somehow annoy him and he starts a fight with you! " +
                    "Use the {attack} command to attack him. " +
                    "When you defeat him, go to the medical tent to see if they can patch him up.",
            1, null, 0, null, 0, false, 1);
    public static final NPCText TUTORIAL_DOCTOR_TEXT_1 = new NPCText(17, DOCTOR_NPC,
            "Ah, yes, I already see what the problem is here!" +
                    "A classic case of drunkeritis. " +
                    "Just leave your friend with us and we will make sure he will be alright." +
                    "Oh, but look at the time! " +
                    "It is culture day already!" +
                    "Quickly, head to the next location and talk to the Culture NPC.",
            1, DRUNK_MATE_ITEM, 1, null, 0, false, 1);
    public static final NPCText TUTORIAL_CULTURE_TEXT_1 = new NPCText(18, CULTURE_NPC,
            "The University of Twente is the first university that has its own Mastodon server, which you are currently using. " +
                    "Go to camp to talk to the " + BARTENDER_TEXT + ".",
            1, null, 0, null, 0, false, 1);
    public static final NPCText TUTORIAL_BARTENDER_TEXT_1 = new NPCText(19, BARTENDER,
            "The bartender looks a little angry." +
                    "His face and clothes are wet; it looks like someone has thrown a drink at him! " +
                    "You try to order something, but he won't help you. " +
                    "You remind him that it is his job to serve you, but this only makes him angrier! " +
                    "He attacks you!" +
                    "Watch out! The bartender looks quite a bit stronger than your drunk mate!" +
                    "When you have beaten him, go to the pub and talk to his boss.",
            1, null, 0, null, 0, false, 1);
    public static final NPCText TUTORIAL_BOSS_TEXT_1 = new NPCText(20, BARTENDERS_BOSS,
            "You have knocked out my bartender! " +
                    "Now I have to serve the customers myself! " +
                    "As a punishment, you will have to provide entertainment for a while!\n" +
                    "Use the command {newsong} to obtain a new song." +
                    "You have to complete this song to obtain a microphone. " +
                    "Bring the microphone to the EXIT NPC in the next location to complete the tutorial and start your main quest.",
            1, BARTENDER_ITEM, 1, null, 0, false, 1);
    public static final NPCText TUTORIAL_EXIT_TEXT_1 = new NPCText(21, EXIT_NPC,
            "Thanks for bringing me the microphone. " +
                    "It seems like you are ready to leave the tutorial and start in the real location. " +
                    "Good luck! Go south to enter the real world. " +
                    "You should meet the Biology student at the Nanolab to start your adventure! " +
                    "The Nanolab is located to the east of your current location in the main story.",
            1, MICROPHONE, 1, null, 0, false, 1);



    //Shops
    public static final Shop COOP_SHOP = new Shop(1, "coop", COOP);
    public static final Shop VESTINGBAR_SHOP = new Shop(2, VESTINGBAR_TEXT, VESTINGBAR);
    public static final Shop VRIJHOF_SHOP = new Shop(3, "vrijhof", VRIJHOF);
    public static final Shop SPIEGEL_SHOP = new Shop(4, "spiegel", SPIEGEL);
    public static final Shop ZUL_SHOP = new Shop(5, "de zul 2", ZUL_NORTH);
    public static final Shop WAAIER_SHOP = new Shop(6, "waaier", WAAIER);

    //Shopitems
    public static final ShopItem SHOP_ITEM_PIE_COOP = new ShopItem(1, COOP_SHOP, PIE, 5);
    public static final ShopItem SHOP_ITEM_POTION_COOP = new ShopItem(2, COOP_SHOP, POTION, 10);
    public static final ShopItem SHOP_ITEM_POTION_VESTINGBAR = new ShopItem(3, VESTINGBAR_SHOP, POTION, 10);
    public static final ShopItem SHOP_ITEM_POTION_VRIJHOF = new ShopItem(4, VRIJHOF_SHOP, POTION, 10);
    public static final ShopItem SHOP_ITEM_POTION_SPIEGEL = new ShopItem(5, SPIEGEL_SHOP, POTION, 10);
    public static final ShopItem SHOP_ITEM_POTION_ZUL = new ShopItem(6, ZUL_SHOP, POTION, 10);
    public static final ShopItem SHOP_ITEM_POTION_WAAIER = new ShopItem(7, WAAIER_SHOP, POTION, 10);
    public static final ShopItem SHOP_ITEM_SUPER_PUTION_COOP = new ShopItem(8, COOP_SHOP, SUPER_POTION, 20);
    public static final ShopItem SHOP_ITEM_SUPER_POTION_VESTINGBAR = new ShopItem(9, VESTINGBAR_SHOP, SUPER_POTION, 20);
    public static final ShopItem SHOP_ITEM_SUPER_POTION_VRIJHOF = new ShopItem(10, VRIJHOF_SHOP, SUPER_POTION, 20);
    public static final ShopItem SHOP_ITEM_SUPER_POTION_SPIEGEL = new ShopItem(11, SPIEGEL_SHOP, SUPER_POTION, 20);
    public static final ShopItem SHOP_ITEM_SUPER_POTION_ZUL = new ShopItem(12, ZUL_SHOP, SUPER_POTION, 20);
    public static final ShopItem SHOP_ITEM_SUPER_POTION_WAAIER = new ShopItem(13, WAAIER_SHOP, SUPER_POTION, 20);

    //Enemies
    public static final EnemyType ENEMY_BIOLOGY = new EnemyType(1, "first enemy", 10, 1, NANOLAB, HAIR, 1, 1);
    public static final EnemyType ENEMY_ROM_LANGERAK = new EnemyType(2, "rom langerak (mini boss)", 100, 7, NANOLAB, ROM_LANGERAK_ITEM, 1, 10);
    public static final EnemyType ENEMY_KLAAS_SIKKEL = new EnemyType(3, "klaas sikkel (mini boss)", 200, 9, ZILVERLING, KLAAS_SIKKEL, 1, 50);
    public static final EnemyType ENEMY_DRUNK_MATE = new EnemyType(4, DRUNKMATE_TEXT, 10, 1, VESTINGBAR_TUTORIAL, DRUNK_MATE_ITEM, 1, 1);
    public static final EnemyType ENEMY_BARTENDER = new EnemyType(5, BARTENDER_TEXT, 20, 1, CAMP, BARTENDER_ITEM, 1, 5);
    public static final EnemyType ENEMY_IN_HOUSE = new EnemyType(6, "enemy in the house", 150, 11, STUDENT_HOUSING, ENEMY_HOUSE, 1, 75);
    public static final EnemyType ENEMY_ITALIAN_MAFIA = new EnemyType(7, "member of the italian mafia", 200, 12, GANZENVELD, ITALIAN_MAFIA_ITEM, 1, 100);
    public static final EnemyType ENEMY_BUS = new EnemyType(8, "bus which contains ram", 250, 17, HALLENWEG_WEST, RAM, 1, 125);
    public static final EnemyType ENEMY_DJMADPROFESSOR = new EnemyType(9, "djmadprofessor (Djoerd Hiemstra)", 1000, 26, HORSTTOREN, CUP, 1, 1000);

    private Constants() {
        throw new IllegalStateException("Utility class");
    }

    private static List<String> createStringList(String string1,
                                                 String string2,
                                                 String string3) {
        List<String> myList = new ArrayList<>();
        myList.add(string1);
        myList.add(string2);
        myList.add(string3);
        return myList;
    }
}

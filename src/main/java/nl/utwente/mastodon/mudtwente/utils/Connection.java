package nl.utwente.mastodon.mudtwente.utils;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.sys1yagi.mastodon4j.MastodonClient;
import com.sys1yagi.mastodon4j.api.Scope;
import com.sys1yagi.mastodon4j.api.entity.Status;
import com.sys1yagi.mastodon4j.api.entity.auth.AccessToken;
import com.sys1yagi.mastodon4j.api.entity.auth.AppRegistration;
import com.sys1yagi.mastodon4j.api.exception.Mastodon4jRequestException;
import com.sys1yagi.mastodon4j.api.method.Apps;
import com.sys1yagi.mastodon4j.api.method.Statuses;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import nl.utwente.mastodon.mudtwente.converters.MastodonAppConverter;
import nl.utwente.mastodon.mudtwente.entities.MastodonApp;
import nl.utwente.mastodon.mudtwente.repositories.MastodonAppRepository;
import okhttp3.OkHttpClient;

import java.util.List;

/**
 * Connection makes a connection with the MUDTwente Bot on the Mastodon of the University of Twente.
 */
@Data
@Slf4j
public class Connection {

    private MastodonClient client;
    private Statuses statuses;

    public Connection(String psswrd,
                      MastodonAppRepository mastodonAppRepository,
                      MastodonAppConverter mastodonAppConverter) {
        MastodonClient temporaryClient = new MastodonClient.Builder("mastodon.utwente.nl", new OkHttpClient.Builder(), new Gson()).build();
        //Creates a MastodonClient to talk with a Mastodon instance.

        //Creates an Apps from the client
        Apps apps = new Apps(temporaryClient);

        //Get all known MastodonApps from our Database.
        List<MastodonApp> mastodonApps = Lists.newArrayList(mastodonAppRepository.findAll());

        //Instantiate an accessToken
        AccessToken accessToken = null;

        //If we have exactly remembered one MastodonApp
        if (mastodonApps.size() == 1) {
            //Get the MastodonApp
            MastodonApp mastodonApp = mastodonApps.get(0);

            //Try to get an Accesstoken by logging in.
            try {
                accessToken = apps.postUserNameAndPassword(
                        mastodonApp.getClientId(),
                        mastodonApp.getClientSecret(),
                        new Scope(),
                        "mudtwente@gmail.com",
                        psswrd
                ).execute();
            } catch (Mastodon4jRequestException e) {
                log.info("The app is not registered yet: ", e);
                accessToken = registerNewApp(mastodonAppRepository, mastodonAppConverter, psswrd, apps, accessToken);
            }
        } else {
            //Otherwise, try to start fresh.
            mastodonAppRepository.deleteAll();
            accessToken = registerNewApp(mastodonAppRepository, mastodonAppConverter, psswrd, apps, accessToken);
        }

        // If accessToken != null
        if (accessToken != null) {
            client = new MastodonClient.Builder("mastodon.utwente.nl", new OkHttpClient.Builder(), new Gson())
                    .accessToken(accessToken.getAccessToken())
                    .useStreamingApi()
                    .build();
        } else {
            throw new NullPointerException("Unable to retrieve accessToken!");
        }
        statuses = new Statuses(client);
    }

    public void postMessages(boolean debug, List<Message> replyMessages) {
        for (Message message : replyMessages) {
            if (!message.getContent().equals("")) {
                if (message.getContent().length() > 480) {
                    String oriMessage = message.getContent();
                    String username = message.getUsername();
                    int charactersLeftForMessage = 480 - username.length() - 1;
                    int amountOfMessages = message.getContent().length() / charactersLeftForMessage;
                    int modulo = message.getContent().length() % charactersLeftForMessage;
                    for (int i = 0; i <= amountOfMessages; i++) {
                        createMessageForTooLong(debug, message, oriMessage,charactersLeftForMessage, modulo, i);
                    }
                } else {
                    postDirectStatus(debug, message);
                }
            }
        }
    }

    private void createMessageForTooLong(boolean debug, Message message, String oriMessage, int charactersLeftForMessage, int modulo, int i) {
        if ((charactersLeftForMessage * (i + 1)) < oriMessage.length() && i > 0) {
            message.setContent(oriMessage.substring(charactersLeftForMessage * i, charactersLeftForMessage * (i + 1)));
        } else if ((charactersLeftForMessage * (i + 1)) < oriMessage.length()) {
            message.setContent(oriMessage.substring(charactersLeftForMessage * i, charactersLeftForMessage * (i + 1)));
        } else {
            message.setContent(oriMessage.substring(oriMessage.length() - modulo));
        }
        postDirectStatus(debug, message);
    }

    private AccessToken registerNewApp(MastodonAppRepository mastodonAppRepository,
                                       MastodonAppConverter mastodonAppConverter,
                                       String psswrd,
                                       Apps apps,
                                       AccessToken accessToken) {
        try {
            //Delete all known MastodonApps
            mastodonAppRepository.deleteAll();

            //Register the MUDTwente Bot to the Mastodon of the UT
            AppRegistration registration = apps.createApp(
                    "MUDTwenteBot",
                    "urn:ietf:wg:oauth:2.0:oob",
                    new Scope(Scope.Name.ALL),
                    "https://farm15.ewi.utwente.nl:8080"
            ).execute();

            //Convert the registration to a MastodonApp and save it in our Database
            mastodonAppRepository.save(mastodonAppConverter.fromAppRegistration(registration));

            //Get an accesstoken with the newly registered app.
            accessToken = apps.postUserNameAndPassword(
                    registration.getClientId(),
                    registration.getClientSecret(),
                    new Scope(),
                    "mudtwente@gmail.com",
                    psswrd
            ).execute();
        } catch (Mastodon4jRequestException e) {
            log.error("Context: ", e);
        }
        return accessToken;
    }

    public void postDirectStatus(boolean debug,
                                 Message message) {
        postDirectStatus(debug,
                message.getUsername() + " " + message.getContent(),
                message.getInReplyToId(),
                message.getMediaIds(),
                message.isSensitive(),
                message.getSpoilerText(),
                Status.Visibility.Direct);
    }

    public void postDirectStatus(boolean debug,
                                 String message) {
        postDirectStatus(debug, message, null);
    }

    public void postDirectStatus(boolean debug,
                                 String message,
                                 Long inReplyToId) {
        postDirectStatus(debug, message, inReplyToId, null);
    }

    public void postDirectStatus(boolean debug,
                                 String message,
                                 Long inReplyToId,
                                 List<Long> mediaIds) {
        postDirectStatus(debug, message, inReplyToId, mediaIds, false);
    }

    public void postDirectStatus(boolean debug,
                                 String message,
                                 Long inReplyToId,
                                 List<Long> mediaIds,
                                 boolean sensitive) {
        postDirectStatus(debug, message, inReplyToId, mediaIds, sensitive, null);
    }

    public void postDirectStatus(boolean debug,
                                 String message,
                                 Long inReplyToId,
                                 List<Long> mediaIds,
                                 boolean sensitive,
                                 String spoilerText) {
        postDirectStatus(debug, message, inReplyToId, mediaIds, sensitive, spoilerText, Status.Visibility.Direct);
    }

    public void postDirectStatus(boolean debug,
                                 String message,
                                 Long inReplyToId,
                                 List<Long> mediaIds,
                                 boolean sensitive,
                                 String spoilerText,
                                 Status.Visibility visibility) {
        if (debug) {
            log.info("Response: " + message);
        } else {
            try {
                statuses.postStatus(message, inReplyToId, mediaIds, sensitive, spoilerText, visibility).execute();
            } catch (Mastodon4jRequestException e) {
                log.error("Context: ", e);
            }
        }
    }

}

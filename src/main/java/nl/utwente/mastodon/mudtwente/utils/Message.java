package nl.utwente.mastodon.mudtwente.utils;

import com.sys1yagi.mastodon4j.api.entity.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.Nullable;

import java.util.List;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class Message {

    @NonNull
    private String username;

    @NonNull
    private String content;

    @Nullable
    private Long inReplyToId;

    @Nullable
    private List<Long> mediaIds;

    private boolean sensitive = false;

    @Nullable
    private String spoilerText;

    @NonNull
    private Status.Visibility visibility = Status.Visibility.Direct;

    public Message(String username, String content, Long inReplyToId) {
        this.username = username;
        this.content = content;
        this.inReplyToId = inReplyToId;
    }
}

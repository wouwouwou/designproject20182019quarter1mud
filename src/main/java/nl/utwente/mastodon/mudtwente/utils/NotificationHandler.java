package nl.utwente.mastodon.mudtwente.utils;

import com.sys1yagi.mastodon4j.api.entity.Account;
import com.sys1yagi.mastodon4j.api.entity.Application;
import com.sys1yagi.mastodon4j.api.entity.Notification;
import com.sys1yagi.mastodon4j.api.entity.Status;
import lombok.extern.slf4j.Slf4j;
import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.repositories.MUDCharacterRepository;
import nl.utwente.mastodon.mudtwente.repositories.RepositoryWrapper;
import nl.utwente.mastodon.mudtwente.repositories.fightsystem.EnemyInstanceRepository;
import nl.utwente.mastodon.mudtwente.repositories.itemsystem.InventoryRepository;
import nl.utwente.mastodon.mudtwente.repositories.itemsystem.ItemRepository;
import nl.utwente.mastodon.mudtwente.repositories.itemsystem.ShopitemRepository;
import nl.utwente.mastodon.mudtwente.repositories.locationsystem.LocationRepository;
import nl.utwente.mastodon.mudtwente.repositories.locationsystem.ShopRepository;
import nl.utwente.mastodon.mudtwente.repositories.npcsystem.NPCRepository;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.PuzzleSystemRepositoryWrapper;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.puzzle_instances.PuzzleInstancesRepositoryWrapper;
import nl.utwente.mastodon.mudtwente.repositories.songsystem.PlayerSongRepository;
import nl.utwente.mastodon.mudtwente.services.MUDCharacterService;
import nl.utwente.mastodon.mudtwente.services.fightsystem.EnemyInstanceService;
import nl.utwente.mastodon.mudtwente.services.itemsystem.InventoryService;
import nl.utwente.mastodon.mudtwente.services.locationsystem.LocationService;
import nl.utwente.mastodon.mudtwente.services.locationsystem.ShopService;
import nl.utwente.mastodon.mudtwente.services.npcsystem.NPCService;
import nl.utwente.mastodon.mudtwente.services.puzzlesystem.PuzzleInstanceService;
import nl.utwente.mastodon.mudtwente.services.puzzlesystem.PuzzleService;
import nl.utwente.mastodon.mudtwente.services.puzzlesystem.SafeService;
import nl.utwente.mastodon.mudtwente.services.puzzlesystem.puzzle_instances.BinaryPuzzleInstanceService;
import nl.utwente.mastodon.mudtwente.services.puzzlesystem.puzzle_instances.LightsPuzzleInstanceService;
import nl.utwente.mastodon.mudtwente.services.puzzlesystem.puzzle_instances.RiddlePuzzleInstanceService;
import nl.utwente.mastodon.mudtwente.services.puzzlesystem.puzzle_instances.balancepuzzle_instance.BalancePuzzleInstanceService;
import nl.utwente.mastodon.mudtwente.services.puzzlesystem.puzzle_instances.balancepuzzle_instance.ScaleService;
import nl.utwente.mastodon.mudtwente.services.songsystem.PlayerSongService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@Slf4j
public class NotificationHandler {
    private static MUDCharacter mudCharacter;
    private static List<Message> replyMessages;
    private static NPCRepository npcRepository;
    private static ItemRepository itemRepository;
    private static ShopitemRepository shopitemRepository;
    private static InventoryRepository inventoryRepository;

    private static LocationService locationService;
    private static MUDCharacterService mudCharacterService;
    private static NPCService npcService;
    private static ShopService shopService;
    private static InventoryService inventoryService;
    private static EnemyInstanceService enemyInstanceService;
    private static PlayerSongService playerSongService;

    private static SafeService safeService;
    private static PuzzleService puzzleService;
    private static ScaleService scaleService;
    private static PuzzleInstanceService puzzleInstanceService;
    private static RiddlePuzzleInstanceService riddlePuzzleInstanceService;
    private static BalancePuzzleInstanceService balancePuzzleInstanceService;
    private static BinaryPuzzleInstanceService binaryPuzzleInstanceService;
    private static LightsPuzzleInstanceService lightsPuzzleInstanceService;

    private static RepositoryWrapper repositoryWrapper2;
    private static MUDCharacterRepository mudCharacterRepository2;

    private static boolean usedAdminCommand;
    private static Account account;

    private NotificationHandler() {

    }

    /**
     * Handles an incoming notification.
     *
     * @param notification           The notification which has to be handled.
     * @param debug                  The debug flag of the program, which must be passed on to the messagesender.
     * @param connection             The connection with the Mastodon API. Must be passed on to the messagesender.
     * @param repositoryWrapper      A repositorywrapper to use.
     * @param mudCharacterRepository A mudCharacterRepository to use.
     */
    public static void handleNotification(Notification notification,
                                          boolean debug,
                                          Connection connection,
                                          RepositoryWrapper repositoryWrapper,
                                          MUDCharacterRepository mudCharacterRepository) {

        logNotification(notification);

        account = notification.getAccount();
        Status status = notification.getStatus();

        if (accountOrStatusNull(account, status)) {
            log.info("Received Notification's Account or Status is null!" +
                    "I cannot send an answer to this notification!");
            return;
        }

        LocationRepository locationRepository = repositoryWrapper.getLocationSystemRepositoryWrapper().getLocationRepository();
        npcRepository = repositoryWrapper.getNpcSystemRepositoryWrapper().getNpcRepository();
        itemRepository = repositoryWrapper.getItemSystemRepositoryWrapper().getItemRepository();
        ShopRepository shopRepository = repositoryWrapper.getLocationSystemRepositoryWrapper().getShopRepository();
        shopitemRepository = repositoryWrapper.getItemSystemRepositoryWrapper().getShopitemRepository();
        inventoryRepository = repositoryWrapper.getItemSystemRepositoryWrapper().getInventoryRepository();
        EnemyInstanceRepository enemyInstanceRepository = repositoryWrapper.getFightSystemRepositoryWrapper().getEnemyInstanceRepository();
        PlayerSongRepository playerSongRepository = repositoryWrapper.getSongSystemRepositoryWrapper().getPlayerSongRepository();

        locationService = new LocationService(locationRepository);
        mudCharacterService = new MUDCharacterService(mudCharacterRepository);
        npcService = new NPCService(npcRepository);
        shopService = new ShopService(shopRepository);
        inventoryService = new InventoryService(inventoryRepository);
        enemyInstanceService = new EnemyInstanceService(enemyInstanceRepository);
        playerSongService = new PlayerSongService(playerSongRepository);

        mudCharacterRepository2 = mudCharacterRepository;
        repositoryWrapper2 = repositoryWrapper;

        PuzzleSystemRepositoryWrapper puzzleSystemRepositoryWrapper = repositoryWrapper.getPuzzleSystemRepositoryWrapper();
        PuzzleInstancesRepositoryWrapper puzzleInstancesRepositoryWrapper = puzzleSystemRepositoryWrapper.getPuzzleInstancesRepositoryWrapper();
        puzzleService = new PuzzleService();
        safeService = new SafeService();
        scaleService = new ScaleService(puzzleSystemRepositoryWrapper.getPuzzleInstancesRepositoryWrapper().getScaleRepository());
        puzzleInstanceService = new PuzzleInstanceService(puzzleSystemRepositoryWrapper.getPuzzleInstanceRepository());
        riddlePuzzleInstanceService = new RiddlePuzzleInstanceService(puzzleInstancesRepositoryWrapper.getRiddlePuzzleInstanceRepository());
        balancePuzzleInstanceService = new BalancePuzzleInstanceService(puzzleInstancesRepositoryWrapper.getBalancePuzzleInstanceRepository());
        binaryPuzzleInstanceService = new BinaryPuzzleInstanceService(puzzleInstancesRepositoryWrapper.getBinaryPuzzleInstanceRepository());
        lightsPuzzleInstanceService = new LightsPuzzleInstanceService(puzzleInstancesRepositoryWrapper.getLightsPuzzleInstanceRepository());

        String username = "@" + account.getAcct();
        long replyToId = status.getId();
        log.info("Username: " + account.getUserName());
        mudCharacter = mudCharacterService
                .findMUDCharacterByUsernameAndMastodonAccountId(username,
                        (int) account.getId())
                .orElse(mudCharacterService
                        .findMUDCharacterByUsernameAndMastodonAccountId("@" + account.getUserName(),
                                (int) account.getId())
                        .orElse(null));
        String message = filterHtmlTagsFromStatusContent(status.getContent()).toLowerCase().trim();
        replyMessages = new ArrayList<>();

        if (message.equals("")) {
            replyMessages.add(new Message(username, "Please send me an actual message. This one is empty!", replyToId));
        } else if(!status.getVisibility().equals("direct")) {
            replyMessages.add(new Message(username, "Please send me a direct message!", replyToId));
        } else {
            log.info(message);

            List<String> words = new LinkedList<>(Arrays.asList(message.split("\\s+")));
            String command = words.remove(0);

            //If a MUDCharacter exists and command is not equal to register
            if (mudCharacter != null) {
                mudCharacter.setLastMessageAt(LocalDateTime.now());
                mudCharacterRepository.save(mudCharacter);
                if (!mudCharacter.isBanned()) {
                    usedAdminCommand = false;
                    handleAdminCommands(words, command, username, replyToId);
                    handlePlayerCommands(words, command, username, replyToId);
                } else {
                    replyMessages.add(new Message(username, "You are banned from the game.", replyToId));
                }
            } else if (command.equals("register")){
                replyMessages.add(new Message(username, mudCharacterService.handleRegisterCommand(words, account.getId(), username, inventoryRepository), replyToId));
            } else {
                replyMessages.add(new Message(username, "You do not yet have a character to play! Please use " +
                        "'register' <characterName>  to register your character!", replyToId));
            }
        }
        connection.postMessages(debug, replyMessages);
    }

    /**
     * Checks if the account or the status is null.
     *
     * @param account The Account to check.
     * @param status  The Status to check.
     * @return Account == null || Status == null;
     */
    private static boolean accountOrStatusNull(Account account, Status status) {
        return account == null || status == null;
    }

    /**
     * Filters HTML tags from the content of a notification. Also makes it lowercase and trims it.
     *
     * @param content The content of a notification with HTML tags. May be uppercase and surrounded with WS.
     * @return The content without HTML tags, lowercase and without surrounding WS.
     */
    private static String filterHtmlTagsFromStatusContent(String content) {
        if (content.contains("@MUDTwente")) {
            //First filter
            content = content.replaceAll("<span>|</span>|<p>|</p>|<br />|</a>", "");
            //Splits string so that everything in the actual message is contained in parts[1]
            List<String> parts = Arrays.asList(content.split("@MUDTwente"));
            if (!parts.isEmpty()) {
                return parts.get(parts.size() - 1).replaceAll("<span>|</span>|<p>|</p>|<br />|</a>", " ");
            }
        }
        return "";
    }

    /**
     * Logs much info of the notification
     *
     * @param notification The notification to log.
     */
    private static void logNotification(Notification notification) {
        String createdAt = notification.getCreatedAt();
        String type = notification.getType();
        long id = notification.getId();
        Account account = notification.getAccount();
        Status status = notification.getStatus();
        Application application = null;

        log.info("Notification ID: " + id);
        log.info("Notification created at: " + createdAt);
        log.info("Notification type: " + type);

        if (account != null) {
            String acct = account.getAcct();
            String avatar = account.getAvatar();
            String accountCreatedAt = account.getCreatedAt();
            String displayName = account.getDisplayName();
            String accountHeader = account.getHeader();
            String accountUsername = account.getUserName();

            log.info("Account Acct: " + acct);
            log.info("Account Avatar: " + avatar);
            log.info("Account created at: " + accountCreatedAt);
            log.info("Account Displayname: " + displayName);
            log.info("Account Header: " + accountHeader);
            log.info("Account Username: " + accountUsername);
        }

        if (status != null) {
            String content = status.getContent();
            String statusCreatedAt = status.getCreatedAt();
            long inReplyToId = -1;
            if (status.getInReplyToId() != null) {
                inReplyToId = status.getInReplyToId();
            }
            String visibility = status.getVisibility();
            String language = status.getLanguage();
            application = status.getApplication();

            log.info("Status Content: " + content);
            log.info("Status created at: " + statusCreatedAt);
            log.info("Status in reply to ID: " + inReplyToId);
            log.info("Status Visibility: " + visibility);
            log.info("Status Language: " + language);
        }

        if (application != null) {
            String applicationName = application.getName();
            String website = application.getWebsite();

            log.info("Application Name: " + applicationName);
            log.info("Application Website: " + website);
        }
    }

    /**
     * Handles admin commands.
     *
     * @param words Any text used by the user after the command.
     * @param command Command used by the user.
     * @param username Username of the Mastodon Account.
     * @param replyToId ID of the directMessage to reply to.
     */
    private static void handleAdminCommands(List<String> words, String command, String username, long replyToId) {
        if (mudCharacter.isAdmin()) {
            switch (command) {
                case "teleport":
                    replyMessages.add(new Message(username, locationService.handleTeleportCommand(words, mudCharacter, mudCharacterRepository2), replyToId));
                    usedAdminCommand = true;
                    break;
                case "ban":
                    replyMessages.add(new Message(username, mudCharacterService.handleBanCommand(words, mudCharacter)));
                    usedAdminCommand = true;
                    break;
                case "unban":
                    replyMessages.add(new Message(username, mudCharacterService.handleUnbanCommand(words, mudCharacter)));
                    usedAdminCommand = true;
                    break;
                case "warp":
                    replyMessages.add(new Message(username, mudCharacterService.handleWarpCommand(words, locationService, mudCharacter)));
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Handles player commands.
     *
     * @param words Any text used by the user after the command.
     * @param command Command used by the user.
     * @param username Username of the Mastodon Account.
     * @param replyToId ID of the directMessage to reply to.
     */
    private static void handlePlayerCommands(List<String> words, String command, String username, long replyToId) {
        switch (command) {
            case "go":
                replyMessages.add(new Message(username, locationService.handleGoCommand(words, mudCharacterService, mudCharacter)));
                mudCharacter = mudCharacterService.findMUDCharacterByUsernameAndMastodonAccountId
                        ("@" + account.getAcct(), (int) account.getId()).orElse(null);
                Message secondmessage;
                if (mudCharacter != null) {
                    secondmessage = new Message(username, locationService.handleLocationCommand(mudCharacter), replyToId);
                } else {
                    secondmessage = new Message(username, "The developers of this MUD made a mistake!", replyToId);
                }
                replyMessages.add(secondmessage);
                break;
            case "help":
                replyMessages.add(new Message(username, mudCharacterService.handleHelpCommand(), replyToId));
                break;
            case "location":
                replyMessages.add(new Message(username, locationService.handleLocationCommand(mudCharacter), replyToId));
                break;
            case "locationinfo":
                replyMessages.add(new Message(username, locationService.handleLocationInfoCommand(mudCharacter), replyToId));
                break;
            case "whoishere":
                replyMessages.add(new Message(username, locationService.handleWhoIsHereCommand(mudCharacterService, mudCharacter), replyToId));
                break;
            case "online":
                replyMessages.add(new Message(username, mudCharacterService.handleOnlineCommand(), replyToId));
                break;
            case "toallatlocation":
                replyMessages.addAll(
                        mudCharacterService.handleToAllAtLocationCommand(
                                words,
                                username,
                                replyToId,
                                mudCharacter));
                break;
            case "sleep":
                replyMessages.add(new Message(username, mudCharacterService.handleSleepCommand(mudCharacter, inventoryRepository), replyToId));
                break;
            case "whichnpcs":
                replyMessages.add(new Message(username, npcService.handleWhichNPCsCommand(npcRepository, mudCharacter), replyToId));
                break;
            case "whattobuy":
                replyMessages.add(new Message(username, shopService.handleWhatToBuyCommand(mudCharacter, shopitemRepository), replyToId));
                break;
            case "buy":
                replyMessages.add(new Message(username, shopService.handleBuyCommand(repositoryWrapper2, mudCharacter, words), replyToId));
                break;
            case "talk":
                replyMessages.add(new Message(username, npcService.handleTalkCommand(words, mudCharacter, repositoryWrapper2, mudCharacterRepository2), replyToId));
                break;
            case "inventory":
                replyMessages.add(new Message(username, inventoryService.handleInventoryCommand(mudCharacter), replyToId));
                break;
            case "usepotion":
                replyMessages.add(new Message(username, mudCharacterService.handleUsePotionCommand(words, itemRepository, mudCharacter, inventoryRepository, mudCharacterRepository2), replyToId));
                break;
            case "attack":
                replyMessages.add(new Message(username, enemyInstanceService.handleAttackCommand(mudCharacter, repositoryWrapper2, mudCharacterRepository2), replyToId));
                break;
            case "newsong":
                replyMessages.add(new Message(username, playerSongService.handleNewSongCommand(repositoryWrapper2, mudCharacter), replyToId));
                break;
            case "sing":
                replyMessages.add(new Message(username, playerSongService.handleSongCommand(words, repositoryWrapper2, mudCharacter), replyToId));
                break;
            case "answer":
                replyMessages.add(new Message(username, puzzleInstanceService.handleAnswerCommand(mudCharacter, words, riddlePuzzleInstanceService), replyToId));
                break;
            case "trycode":
                replyMessages.add(new Message(username, safeService.handleTryCodeCommand(mudCharacter, words, inventoryService), replyToId));
                break;
            case "pull":
                replyMessages.add(new Message(username, puzzleInstanceService.handlePullCommand(mudCharacter, words, lightsPuzzleInstanceService, binaryPuzzleInstanceService), replyToId));
                break;
            case "placeleft" :
                replyMessages.add(new Message(username, puzzleInstanceService.handlePlaceLeftCommand(mudCharacter, words, scaleService), replyToId));
                break;
            case "placeright" :
                replyMessages.add(new Message(username, puzzleInstanceService.handlePlaceRightCommand(mudCharacter, words, scaleService), replyToId));
                break;
            case "placedown" :
                replyMessages.add(new Message(username, puzzleInstanceService.handlePlaceDownCommand(mudCharacter, words, scaleService), replyToId));
                break;
            case "weigh":
                replyMessages.add(new Message(username, puzzleInstanceService.handleWeighCommand(mudCharacter, balancePuzzleInstanceService), replyToId));
                break;
            case "pick":
                replyMessages.add(new Message(username, puzzleInstanceService.handlePickCommand(mudCharacter, words, balancePuzzleInstanceService), replyToId));
                break;
            case "checksafe":
                replyMessages.add(new Message(username, safeService.handleCheckSafeCommand(mudCharacter), replyToId));
                break;
            case "checkpuzzle":
                replyMessages.add(new Message(username, puzzleService.handleCheckPuzzleCommand(mudCharacter), replyToId));
                break;
            case "puzzlestatus":
                replyMessages.add(new Message(username, puzzleInstanceService.handlePuzzleStatusCommand(mudCharacter), replyToId));
                break;
            default:
                if (!usedAdminCommand) {
                    replyMessages.add(new Message(username, "That command is unknown!", replyToId));
                }
        }
    }
}

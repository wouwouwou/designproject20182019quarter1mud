package nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.lightspuzzle;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Entity class for LightswitchPattern.
 */
@Entity
@Data
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
public class LightswitchPattern {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NonNull
    @OrderColumn
    @Size(min = 6, max = 6)
    @ElementCollection(fetch = FetchType.EAGER)
    private List<Boolean> switchList;

}

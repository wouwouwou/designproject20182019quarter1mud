package nl.utwente.mastodon.mudtwente.entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import nl.utwente.mastodon.mudtwente.entities.locationsystem.Location;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDateTime;

/**
 * Entity class for a MUDCharacter.
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MUDCharacter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NonNull
    private String name = "";

    @NonNull
    @ManyToOne
    private Location location;

    @NonNull
    private String username = "";

    private int health;

    private int storyLevel;

    @NonNull
    @PastOrPresent
    @DateTimeFormat(pattern = "yyyy.MM.dd.HH.mm.ss")
    private LocalDateTime createdAt = LocalDateTime.now();

    private int mastodonAccountId;

    @NonNull
    @PastOrPresent
    @DateTimeFormat(pattern = "yyyy.MM.dd.HH.mm.ss")
    private LocalDateTime lastMessageAt = LocalDateTime.now();

    private boolean admin;

    private boolean banned;
}

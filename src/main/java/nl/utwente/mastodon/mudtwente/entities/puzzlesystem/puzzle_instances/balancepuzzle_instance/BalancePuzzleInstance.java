package nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.balancepuzzle_instance;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.PuzzleInstance;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.BalancePuzzle;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Entity class for a BalancePuzzleInstance.
 */
@Entity
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
public class BalancePuzzleInstance extends PuzzleInstance {

    @NonNull
    @OneToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private Scale scale = new Scale();

    private int pastWeighings;

    /**
     * Constructor overriding @RequiredArgsConstructor annotation because of
     * super() call.
     * @param mudCharacter MUDCharacter of the PuzzleInstance
     * @param balancePuzzle BalancePuzzle of the PuzzleInstance
     * @param scale Scale of the BalancePuzzleInstance
     */
    public BalancePuzzleInstance(MUDCharacter mudCharacter,
                                 BalancePuzzle balancePuzzle,
                                 Scale scale) {
        super(mudCharacter, balancePuzzle);
        this.scale = scale;
    }

    /**
     * Constructor replacing @AllArgsConstructor annotation because of
     * super() call.
     * @param id ID of the PuzzleInstance
     * @param mudCharacter MUDCharacter of the PuzzleInstance
     * @param balancePuzzle BalancePuzzle of the PuzzleInstance
     * @param deletedAt DeletedAt of the PuzzleInstance
     * @param scale Scale of the BalancePuzzleInstance
     * @param pastWeighings pastWeighings of the BalancePuzzleInstance
     */
    public BalancePuzzleInstance(int id,
                                 MUDCharacter mudCharacter,
                                 BalancePuzzle balancePuzzle,
                                 LocalDateTime deletedAt,
                                 Scale scale,
                                 int pastWeighings) {
        super(id, mudCharacter, balancePuzzle, deletedAt);
        this.scale = scale;
        this.pastWeighings = pastWeighings;
    }

    /**
     * Returns a String which shows the status of the BalancePuzzleInstance.
     * Beautify function or alternative toString function.
     * @return A String which shows the status of the BalancePuzzleInstance.
     */
    public String getStatus() {
        if (getDeletedAt() != null) {
            return alreadySolved();
        }
        BalancePuzzle balancePuzzle = (BalancePuzzle) getPuzzle();
        int maxWeighings = balancePuzzle.getMaxWeighings();
        return "You have weighed " + pastWeighings + " out of " + maxWeighings + " attempts.\n" +
                "The goal is to find the one weight out of " + balancePuzzle.getAmountOfWeights() +
                " which is heavier than the others. You see a scale which you can use. \n\n" +
                scale.getStatus() +
                "\n\n" + getInstructions();
    }

    /**
     * Returns a string with instructions on how to solve the puzzle.
     * @return A string with instructions on how to solve the puzzle.
     */
    public String getInstructions() {
        return "You can use 'placeleft' to place weights on the left side of the scale.\n" +
                "'placeright' does the same thing for the right side.\n" +
                "'placedown' places weights off the scale.";
    }

    /**
     * Gets the BalancePuzzle of this instance, instead of the Puzzle.
     * @return BalancePuzzle of this instance.
     */
    public BalancePuzzle getBalancePuzzle() {
        return (BalancePuzzle) getPuzzle();
    }

    /**
     * Increases the pastWeigings by one.
     */
    public void increasePastWeighings() {
        pastWeighings++;
    }
}

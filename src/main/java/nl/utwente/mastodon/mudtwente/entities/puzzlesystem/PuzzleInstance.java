package nl.utwente.mastodon.mudtwente.entities.puzzlesystem;

import lombok.*;
import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Past;
import java.time.LocalDateTime;

/**
 * Abstract class for all PuzzleInstances.
 * PuzzleInstances should at least have a MUDCharacter and a Puzzle.
 */
@Entity
@Data
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(uniqueConstraints={
        @UniqueConstraint(columnNames = {"mud_character_id", "puzzle_id"})
})
public abstract class PuzzleInstance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NonNull
    @ManyToOne
    private MUDCharacter mudCharacter;

    @NonNull
    @ManyToOne
    private Puzzle puzzle;

    @Past
    @DateTimeFormat(pattern = "yyyy.MM.dd.HH.mm.ss")
    private LocalDateTime deletedAt;

    /**
     * Returns the status of a PuzzleInstance as a String.
     *
     * @return Status of a PuzzleInstance.
     */
    public abstract String getStatus();

    /**
     * Returns true if the PuzzleInstance (and therefore the Puzzle) is solved. If deletedAt not null, returns true.
     *
     * @return deletedAt != null
     */
    public boolean isSolved() {
        return deletedAt != null;
    }

    /**
     * Returns a String which indicates that the Puzzle already has been solved.
     *
     * @return A String which indicates that the Puzzle already has been solved.
     */
    public String alreadySolved() {
        return "You have already solved this! The hint that you got was: " +
                getPuzzle().getHintAfterCompletion();
    }
}

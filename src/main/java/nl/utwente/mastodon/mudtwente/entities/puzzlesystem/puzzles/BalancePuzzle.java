package nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.Puzzle;

import javax.persistence.*;

/**
 * Entity class for a BalancePuzzle.
 */
@Entity
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
public class BalancePuzzle extends Puzzle {

    private int amountOfWeights;

    private int heaviestWeightId;

    private int maxWeighings;

    /**
     * Constructor overriding @RequiredArgsConstructor annotation because of
     * super() call.
     * @param amountOfWeights Amount of weights for this puzzle.
     * @param heaviestWeightId ID of the heaviest weight of this puzzle.
     * @param maxWeighings Max amount of Weighings at this BalancePuzzle
     */
    public BalancePuzzle(int amountOfWeights,
                        int heaviestWeightId,
                        int maxWeighings) {
        super();
        this.amountOfWeights = amountOfWeights;
        this.heaviestWeightId = heaviestWeightId;
        this.maxWeighings = maxWeighings;
    }

    /**
     * Constructor replacing @AllArgsConstructor annotation because of
     * super() call.
     * @param id ID of the Puzzle
     * @param description Description of the Puzzle
     * @param hintAfterCompletion hintAfterCompletion of the Puzzle
     * @param amountOfWeights Amount of weights for this puzzle.
     * @param heaviestWeightId ID of the heaviest weight of this puzzle.
     * @param maxWeighings Max amount of Weighings at this BalancePuzzle
     */
    public BalancePuzzle(int id,
                         String description,
                         String hintAfterCompletion,
                         int amountOfWeights,
                         int heaviestWeightId,
                         int maxWeighings) {
        super(id, description, hintAfterCompletion);
        this.amountOfWeights = amountOfWeights;
        this.heaviestWeightId = heaviestWeightId;
        this.maxWeighings = maxWeighings;
    }
}

package nl.utwente.mastodon.mudtwente.entities.puzzlesystem;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import nl.utwente.mastodon.mudtwente.entities.itemsystem.Item;

import javax.persistence.*;

/**
 * Entity class for a MUDCharacter.
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Safe {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NonNull
    @OneToOne
    private Item item = new Item();

    private int passCode;

    @NonNull
    private String info = "";
}

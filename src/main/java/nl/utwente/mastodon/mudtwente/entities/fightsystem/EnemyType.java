package nl.utwente.mastodon.mudtwente.entities.fightsystem;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import nl.utwente.mastodon.mudtwente.entities.itemsystem.Item;
import nl.utwente.mastodon.mudtwente.entities.locationsystem.Location;
import org.springframework.lang.Nullable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.ManyToOne;

/**
 * Entity class for a EnemyType.
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EnemyType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NonNull
    private String name = "";

    private int maxHP;

    private int level;

    @ManyToOne
    @NonNull
    private Location location;

    @ManyToOne
    @Nullable
    private Item reward;

    private int rewardAmount;

    private int power;
}

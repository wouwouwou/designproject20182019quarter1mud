package nl.utwente.mastodon.mudtwente.entities.npcsystem;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import nl.utwente.mastodon.mudtwente.entities.locationsystem.Location;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.ManyToOne;

/**
 * Entity class for a NPC.
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NPC {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NonNull
    private String name = "";

    @ManyToOne
    @NonNull
    private Location location;

    private int minimalStoryLevel;

    private boolean isStoryLine;

}

package nl.utwente.mastodon.mudtwente.entities.puzzlesystem;

import lombok.*;

import javax.persistence.*;

/**
 * Abstract class for all Puzzles.
 * Puzzles should have a Location and may give out a hintAfterCompletion.
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Puzzle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NonNull
    private String description = "";

    @NonNull
    private String hintAfterCompletion = "";
}

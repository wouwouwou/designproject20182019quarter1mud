package nl.utwente.mastodon.mudtwente.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import nl.utwente.mastodon.mudtwente.entities.npcsystem.NPC;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.ManyToOne;

/**
 * Entity class for a Quest.
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Quest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @NonNull
    private MUDCharacter mudCharacter;

    @ManyToOne
    @NonNull
    private NPC npc;

    private int level;
}

package nl.utwente.mastodon.mudtwente.entities.npcsystem;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import nl.utwente.mastodon.mudtwente.entities.itemsystem.Item;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.ManyToOne;

/**
 * Entity class for a NPCText.
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NPCText {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @NonNull
    private NPC npc;

    @NonNull
    private String text = "";

    private int level;

    @ManyToOne
    private Item requiredItem;

    private int requiredAmount;

    @ManyToOne
    private Item rewardItem;

    private int rewardAmount;

    private boolean increaseStoryLevel;

    private int minimalStoryLevel;

}

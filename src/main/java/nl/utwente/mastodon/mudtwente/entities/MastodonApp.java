package nl.utwente.mastodon.mudtwente.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.springframework.format.annotation.DateTimeFormat;

import javax.annotation.Nonnull;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

/**
 * Entity class for a MastodonApp.
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MastodonApp {
	@Id
	private int id;

	@NonNull
	private String clientId = "";

	@NonNull
	private String clientSecret = "";

	@Nonnull
    @DateTimeFormat(pattern = "yyyy.MM.dd.HH.mm.ss")
    private LocalDateTime createdAt = LocalDateTime.now();
}

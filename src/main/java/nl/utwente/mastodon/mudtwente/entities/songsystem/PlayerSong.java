package nl.utwente.mastodon.mudtwente.entities.songsystem;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.ManyToOne;

/**
 * Entity class for a PlayerSong.
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlayerSong {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @NonNull
    private MUDCharacter mudCharacter;

    @ManyToOne
    @NonNull
    private Song song;
}

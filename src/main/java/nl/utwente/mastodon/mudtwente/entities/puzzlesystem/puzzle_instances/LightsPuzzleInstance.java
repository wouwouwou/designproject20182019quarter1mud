package nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances;

import lombok.*;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.PuzzleInstance;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.lightspuzzle.LightsPuzzle;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Entity class for a LightsPuzzleInstance.
 */
@Entity
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
public class LightsPuzzleInstance extends PuzzleInstance {

    @NonNull
    @OrderColumn
    @Size(min = 6, max = 6)
    @ElementCollection(fetch = FetchType.EAGER)
    @JoinTable(name = "puzzle_instance_lights",
            joinColumns = @JoinColumn(name = "puzzleinstance_id"))
    private List<Boolean> lights = new ArrayList<>(Arrays.asList(false, false, false, false, false, false));

    /**
     * Constructor overriding @RequiredArgsConstructor annotation because of
     * super() call.
     * @param mudCharacter MUDCharacter of the PuzzleInstance
     * @param lightsPuzzle LightsPuzzle of the PuzzleInstance
     * @param lights Lights of the LightsPuzzleInstance
     */
    public LightsPuzzleInstance(MUDCharacter mudCharacter,
                                LightsPuzzle lightsPuzzle,
                                List<Boolean> lights) {
        super(mudCharacter, lightsPuzzle);
        this.lights = lights;
    }

    /**
     * Constructor replacing @AllArgsConstructor annotation because of
     * super() call.
     * @param id ID of the PuzzleInstance
     * @param mudCharacter MUDCharacter of the PuzzleInstance
     * @param lightsPuzzle LightsPuzzle of the PuzzleInstance
     * @param deletedAt DeletedAt of the PuzzleInstance
     * @param lights Lights of the LightsPuzzleInstance
     */
    public LightsPuzzleInstance(int id,
                                MUDCharacter mudCharacter,
                                LightsPuzzle lightsPuzzle,
                                LocalDateTime deletedAt,
                                List<Boolean> lights) {
        super(id, mudCharacter, lightsPuzzle, deletedAt);
        this.lights = lights;
    }

    /**
     * Returns a String which shows which lights are on or off.
     * Beautify function or alternative toString function.
     * @return A String which shows which lights are on or off.
     */
    public String statusOfLights() {
        return IntStream.
                range(0, lights.size()).
                mapToObj(this::statusOfLight)
                .collect(Collectors.joining("\n"));
    }

    /**
     * Returns a String which shows the status of a light at index i.
     * Beautify function or alternative toString function
     * @param i The light index of which the status must be shown.
     * @return A string which shows if a light is on or off.
     */
    public String statusOfLight(int i) {
        return "-Light " + (i+1) + " is " + (lights.get(i) ? "ON.": "OFF.");
    }

    /**
     * Checks if all lights are on.
     * @return True if all lights are on.
     */
    public boolean allLightsOn() {
        return lights.stream().allMatch(e -> e);
    }

    /**
     * Returns a String which shows the status of the LightsPuzzleInstance.
     * Beautify function or alternative toString function.
     * @return A String which shows the status of the LightsPuzzleInstance.
     */
    public String getStatus() {
        if (getDeletedAt() != null) {
            return alreadySolved();
        }
        return "You see six lights: \n\n" + statusOfLights() +
                "\n\n" + getInstructions();
    }

    /**
     * Returns a string with instructions on how to solve the puzzle.
     * @return A string with instructions on how to solve the puzzle.
     */
    public String getInstructions() {
        return "Use the 'pull' <number> command to change the status of the lights.\n" +
                "There are " + lights.size() + " levers you can pull here. Each of them " +
                "changes the lights in a different way.\n" +
                "Tip: look for a pattern!";
    }

    /**
     * Gets the LightsPuzzle of this instance, instead of the Puzzle.
     * @return LightsPuzzle of this instance.
     */
    public LightsPuzzle getLightsPuzzle() {
        return (LightsPuzzle) getPuzzle();
    }

    /**
     * Switches the lights according to a switchPattern. Uses an XOR to do this.
     * @param switchPattern SwitchPattern to change the lights with (XOR).
     */
    public void switchLights(List<Boolean> switchPattern) {
        for (int i = 0; i < switchPattern.size(); i++) {
            boolean light = getLights().get(i);
            boolean switchy = switchPattern.get(i);
            getLights().set(i, switchy ^ light);
        }
    }


}

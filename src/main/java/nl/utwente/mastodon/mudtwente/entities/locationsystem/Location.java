package nl.utwente.mastodon.mudtwente.entities.locationsystem;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.Puzzle;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.Safe;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Location {

    @Id
    private int id;

    @NonNull
    private String name = "";

    private int north;

    private int west;

    private int south;

    private int east;

    @NonNull
    private String info = "";

    private boolean shop;

    @OneToOne(cascade = CascadeType.ALL)
    private Safe safe;

    @OneToOne(cascade = CascadeType.ALL)
    private Puzzle puzzle;
}

package nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.lightspuzzle;

import lombok.*;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.Puzzle;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Entity class for a LightsPuzzle.
 */
@Entity
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
public class LightsPuzzle extends Puzzle {

    @NonNull
    @OrderColumn
    @Size(min = 6, max = 6)
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<LightswitchPattern> lightswitchPatterns;

    /**
     * Constructor overriding @RequiredArgsConstructor annotation because of
     * super() call.
     * @param lightswitchPatterns Switch Lists of the LightsPuzzle
     */
    public LightsPuzzle(List<LightswitchPattern> lightswitchPatterns) {
        super();
        this.lightswitchPatterns = lightswitchPatterns;
    }

    /**
     * Constructor replacing @AllArgsConstructor annotation because of
     * super() call.
     * @param id ID of the Puzzle
     * @param description Description of the Puzzle
     * @param hintAfterCompletion hintAfterCompletion of the Puzzle
     * @param lightswitchPatterns Switch Lists of the LightsPuzzle
     */
    public LightsPuzzle(int id,
                        String description,
                        String hintAfterCompletion,
                        List<LightswitchPattern> lightswitchPatterns) {
        super(id, description, hintAfterCompletion);
        this.lightswitchPatterns = lightswitchPatterns;
    }
}

package nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles;

import lombok.*;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.Puzzle;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Entity class for a RiddlePuzzle.
 */
@Entity
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
public class RiddlePuzzle extends Puzzle {

    @NonNull
    @OrderColumn
    @ElementCollection(fetch = FetchType.EAGER)
    @JoinTable(name = "puzzle_questions",
            joinColumns = @JoinColumn(name = "puzzle_id"))
    private List<String> questions = new ArrayList<>();

    @NonNull
    @OrderColumn
    @ElementCollection(fetch = FetchType.EAGER)
    @JoinTable(name = "puzzle_answers",
            joinColumns = @JoinColumn(name = "puzzle_id"))
    private List<String> answers = new ArrayList<>();

    private boolean binaryPuzzle;

    /**
     * Constructor overriding @RequiredArgsConstructor annotation because of
     * super() call.
     * @param questions List of the RiddlePuzzle questions
     * @param answers List of the RiddlePuzzle answers
     */
    public RiddlePuzzle(List<String> questions,
                        List<String> answers,
                        boolean binaryPuzzle) {
        super();
        this.questions = questions;
        this.answers = answers;
        this.binaryPuzzle = binaryPuzzle;
    }

    /**
     * Constructor replacing @AllArgsConstructor annotation because of
     * super() call.
     * @param id ID of the Puzzle
     * @param description Description of the Puzzle
     * @param hintAfterCompletion hintAfterCompletion of the Puzzle
     * @param questions List of the RiddlePuzzle questions
     * @param answers List of the RiddlePuzzle answers
     */
    public RiddlePuzzle(int id,
                        String description,
                        String hintAfterCompletion,
                        List<String> questions,
                        List<String> answers,
                        boolean binaryPuzzle) {
        super(id, description, hintAfterCompletion);
        this.questions = questions;
        this.answers = answers;
        this.binaryPuzzle = binaryPuzzle;
    }
}

package nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.PuzzleInstance;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.RiddlePuzzle;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Entity class for a BinaryPuzzleInstance.
 */
@Entity
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
public class BinaryPuzzleInstance extends PuzzleInstance {

    @NonNull
    @OrderColumn
    @Size(min = 5, max = 5)
    @ElementCollection(fetch = FetchType.EAGER)
    @JoinTable(name = "puzzle_instance_lights",
            joinColumns = @JoinColumn(name = "puzzleinstance_id"))
    private List<Boolean> lights = new ArrayList<>(Arrays.asList(false, false, false, false, false));

    private int progress;

    /**
     * Constructor overriding @RequiredArgsConstructor annotation because of
     * super() call.
     * @param mudCharacter MUDCharacter of the PuzzleInstance
     * @param riddlePuzzle RiddlePuzzle of the BinaryPuzzleInstance
     * @param lights Lights of the BinaryPuzzleInstance
     */
    public BinaryPuzzleInstance(MUDCharacter mudCharacter,
                                RiddlePuzzle riddlePuzzle,
                                List<Boolean> lights) {
        super(mudCharacter, riddlePuzzle);
        this.lights = lights;
    }

    /**
     * Constructor replacing @AllArgsConstructor annotation because of
     * super() call.
     * @param id ID of the PuzzleInstance
     * @param mudCharacter MUDCharacter of the PuzzleInstance
     * @param riddlePuzzle RiddlePuzzle of the PuzzleInstance
     * @param deletedAt DeletedAt of the PuzzleInstance
     * @param lights Lights of the BinaryPuzzleInstance
     */
    public BinaryPuzzleInstance(int id,
                                MUDCharacter mudCharacter,
                                RiddlePuzzle riddlePuzzle,
                                LocalDateTime deletedAt,
                                List<Boolean> lights) {
        super(id, mudCharacter, riddlePuzzle, deletedAt);
        this.lights = lights;
    }

    /**
     * Returns a String which shows which lights are on or off.
     * Beautify function or alternative toString function.
     * @return A String which shows which lights are on or off.
     */
    public String statusOfLights() {
        return IntStream.
                range(0, lights.size()).
                mapToObj(this::statusOfLight)
                .collect(Collectors.joining("\n"));
    }

    /**
     * Returns a String which shows the status of a light at index i.
     * Beautify function or alternative toString function
     * @param i The light index of which the status must be shown.
     * @return A string which shows if a light is on or off.
     */
    public String statusOfLight(int i) {
        return "-Light " + (i+1) + " is " + (lights.get(i) ? "ON.": "OFF.");
    }

    /**
     * Calculates the decimal number (int) which is displayed in binary by the lights.
     * @return Decimal number (int) which is displayed in binary by the lights.
     */
    public int calculateDecimal() {
        int result = 0;
        if (lights.get(0)) {
            result += 1;
        }
        if (lights.get(1)) {
            result += 2;
        }
        if (lights.get(2)) {
            result += 4;
        }
        if (lights.get(3)) {
            result += 8;
        }
        if (lights.get(4)) {
            result += 16;
        }
        return result;
    }

    /**
     * Returns a String which shows the status of the BinaryPuzzleInstance.
     * Beautify function or alternative toString function.
     * @return A String which shows the status of the BinaryPuzzleInstance.
     */
    public String getStatus() {
        if (getDeletedAt() != null) {
            return alreadySolved();
        }
        RiddlePuzzle riddlePuzzle = getRiddlePuzzle();
        int maxprogress = riddlePuzzle.getQuestions().size();
        return "You have answered " + progress + " out of " + maxprogress + " riddles.\nYour next riddle is: \n" +
                riddlePuzzle.getQuestions().get(progress) +
                "\nYou also see five lights: \n\n" + statusOfLights() +
                "\n\n" + getInstructions();
    }

    /**
     * Returns a string with instructions on how to solve the puzzle.
     * @return A string with instructions on how to solve the puzzle.
     */
    public String getInstructions() {
        return "There are five levers with which you can turn the lights on or off. Use the 'pull' <number>\n" +
                "command to use them. The lights form your answer to the riddle. Tip: how do you represent a bigger\n" +
                "number with only lights?";
    }

    /**
     * Gets the RiddlePuzzle of this instance, instead of the Puzzle.
     * @return RiddlePuzzle of this instance.
     */
    public RiddlePuzzle getRiddlePuzzle() {
        return (RiddlePuzzle) getPuzzle();
    }

    /**
     * Sets the RiddlePuzzleInstance to use the next Riddle. If it was the last one,
     * soft-deletes the RiddlePuzzleInstance and considers it solved.
     */
    public void correctLightStatus() {
        progress++;
        if (progress == getRiddlePuzzle().getQuestions().size()) {
            setDeletedAt(LocalDateTime.now());
        }
    }

    /**
     * Checks if the current Light Status corresponds to the answer of the RiddlePuzzle.
     * @return True if current Light Status corresponds to the answer of the RiddlePuzzle.
     */
    public boolean isCorrectLightStatus() {
        return getRiddlePuzzle().getAnswers().get(progress).equals(String.valueOf(calculateDecimal()));
    }
}

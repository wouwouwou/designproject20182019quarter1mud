package nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.PuzzleInstance;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.RiddlePuzzle;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Entity class for a RiddlePuzzleInstance.
 */
@Entity
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
public class RiddlePuzzleInstance extends PuzzleInstance {

    private int progress;

    /**
     * Constructor overriding @RequiredArgsConstructor annotation because of
     * super() call.
     * @param mudCharacter MUDCharacter of the PuzzleInstance
     * @param riddlePuzzle RiddlePuzzle of the PuzzleInstance
     */
    public RiddlePuzzleInstance(MUDCharacter mudCharacter,
                                RiddlePuzzle riddlePuzzle) {
        super(mudCharacter, riddlePuzzle);
    }

    /**
     * Constructor replacing @AllArgsConstructor annotation because of
     * super() call.
     * @param id ID of the PuzzleInstance
     * @param mudCharacter MUDCharacter of the PuzzleInstance
     * @param riddlePuzzle RiddlePuzzle of the PuzzleInstance
     * @param deletedAt DeletedAt of the PuzzleInstance
     */
    public RiddlePuzzleInstance(int id,
                                MUDCharacter mudCharacter,
                                RiddlePuzzle riddlePuzzle,
                                LocalDateTime deletedAt) {
        super(id, mudCharacter, riddlePuzzle, deletedAt);
    }

    /**
     * Returns a String which shows the status of the RiddlePuzzleInstance.
     * Beautify function or alternative toString function.
     * @return A String which shows the status of the RiddlePuzzleInstance.
     */
    public String getStatus() {
        if (isSolved()) {
            return alreadySolved();
        }
        RiddlePuzzle riddlePuzzle = getRiddlePuzzle();
        int maxprogress = riddlePuzzle.getQuestions().size();
        return "You have answered " + progress + " out of " + maxprogress + " riddles. Your next riddle is: \n" +
                riddlePuzzle.getQuestions().get(progress) +
                "\n\nUse the 'answer' command to try and solve this riddle.";
    }

    /**
     * Checks if a MUDCharacter's answer is correct.
     * @param answer MUDCharacter's answer.
     * @return True if the MUDCharacter's answer is correct for the current Riddle.
     */
    public boolean isCorrectAnswer(String answer) {
        return getRiddlePuzzle().getAnswers().get(progress).equals(answer);
    }

    /**
     * Sets the RiddlePuzzleInstance to use the next Riddle. If it was the last one,
     * soft-deletes the RiddlePuzzleInstance and considers it solved.
     */
    public void correctAnswerGiven() {
        progress++;
        if (progress == getRiddlePuzzle().getQuestions().size()) {
            setDeletedAt(LocalDateTime.now());
        }
    }

    /**
     * Gets the RiddlePuzzle of this instance, instead of the Puzzle.
     * @return RiddlePuzzle of this instance.
     */
    public RiddlePuzzle getRiddlePuzzle() {
        return (RiddlePuzzle) getPuzzle();
    }
}

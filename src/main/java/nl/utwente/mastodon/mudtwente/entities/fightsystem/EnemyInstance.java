package nl.utwente.mastodon.mudtwente.entities.fightsystem;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.ManyToOne;

/**
 * Entity class for a EnemyInstance.
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EnemyInstance {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @NonNull
    private MUDCharacter mudCharacter;

    @ManyToOne
    @NonNull
    private EnemyType enemyType;

    private int hp;
}

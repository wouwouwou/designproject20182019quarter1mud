package nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.balancepuzzle_instance;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Entity class for a Scale.
 */
@Entity
@NoArgsConstructor
@Data
@AllArgsConstructor
public class Scale {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NonNull
    @ElementCollection(fetch = FetchType.EAGER)
    private Set<Integer> leftSide = new HashSet<>();

    @NonNull
    @ElementCollection(fetch = FetchType.EAGER)
    private Set<Integer> rightSide = new HashSet<>();

    @NonNull
    @ElementCollection(fetch = FetchType.EAGER)
    private Set<Integer> notOnScale = new HashSet<>();

    /**
     * Shows the status of this Scale.
     * Beautify function or alternative toString function.
     *
     * @return String containing the status of this Scale.
     */
    public String getStatus() {
        return "On the left side of the scale are weights: " + leftSideStatus() +
                "\nOn the right side of the scale are weights: " + rightSideStatus() +
                "\nOn the table are weights: " + notOnScaleStatus();
    }

    /**
     * Returns the status of the leftSide Set.
     *
     * @return Status of the leftSide Set.
     */
    private String leftSideStatus() {
        return statusOfScaleSet(leftSide);
    }

    /**
     * Returns the status of the rightSide Set.
     *
     * @return Status of the rightSide Set.
     */
    private String rightSideStatus() {
        return statusOfScaleSet(rightSide);
    }

    /**
     * Returns the status of the notOnScale Set.
     *
     * @return Status of the notOnScale Set.
     */
    private String notOnScaleStatus() {
        return statusOfScaleSet(notOnScale);
    }

    /**
     * Returns the status of the Scale Set as String. Each integer is separated by a ', '
     *
     * @param set The Scale Set to return the status of.
     * @return One String containing all integers of the set, sparated by a ', '
     */
    private String statusOfScaleSet(Set<Integer> set) {
        return set.stream().map(Objects::toString).collect(Collectors.joining(", "));
    }

    /**
     * Returns true if the amount of weights on both sides are equal.
     *
     * @return True if the amount of weights on both sides are equal.
     */
    public boolean isBalanced() {
        return leftSide.size() == rightSide.size();
    }

    /**
     * Returns true if the left side contains more weights than the right side.
     *
     * @return True if the left side contains more weights than the right side.
     */
    public boolean isLeftSideHeavier() {
        return leftSide.size() > rightSide.size();
    }

    /**
     * Returns true if the right side contains more weights than the left side.
     *
     * @return True if the right side contains more weights than the left side.
     */
    public boolean isRightSideHeavier() {
        return rightSide.size() > leftSide.size();
    }

    /**
     * Removes a weightId from the right and notOnScale set, and moves it to leftSide.
     *
     * @param weightId weightId to move.
     */
    public void placeLeft(int weightId) {
        rightSide.remove(weightId);
        notOnScale.remove(weightId);
        leftSide.add(weightId);
    }

    /**
     * Removes a weightId from the leftSide and notOnScale and moves it to rightSide.
     *
     * @param weightId weightId to move.
     */
    public void placeRight(int weightId) {
        rightSide.add(weightId);
        notOnScale.remove(weightId);
        leftSide.remove(weightId);
    }

    /**
     * Removes a weightId from the leftSide and rightSide and moves it to notOnScale.
     *
     * @param weightId weightId to move.
     */
    public void placeDown(int weightId) {
        rightSide.remove(weightId);
        notOnScale.add(weightId);
        leftSide.remove(weightId);
    }

    /**
     * Weighs the scale and returns a String with the weigh status of the scale.
     * Compares amount of weights on either side of the scale and if balanced, looks if
     * the heavier weight is on one of the sides.
     *
     * @param heavierWeightId Weight id to compare the sides of the scale with if balanced in amount of weights.
     * @return String with the weigh status of the scale.
     */
    public String weigh(int heavierWeightId) {
        if (isLeftSideHeavier() || isBalanced() && leftSide.contains(heavierWeightId)) {
            return "The left side of the scale drops. It is heavier than the right side!";
        } else if (isRightSideHeavier() || isBalanced() && rightSide.contains(heavierWeightId)) {
            return "The right side of the scale drops. It is heavier than the left side!";
        }
        return "The scales are perfectly balanced!";
    }

    /**
     * Resets the scale. Moves everything from right and left side down off the scale.
     */
    public void reset() {
        notOnScale.addAll(rightSide);
        notOnScale.addAll(leftSide);
        rightSide.clear();
        leftSide.clear();
    }
}

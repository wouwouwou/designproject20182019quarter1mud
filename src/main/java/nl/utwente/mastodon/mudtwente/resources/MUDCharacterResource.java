package nl.utwente.mastodon.mudtwente.resources;

import com.google.common.collect.Lists;
import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.repositories.MUDCharacterRepository;
import nl.utwente.mastodon.mudtwente.repositories.QuestRepository;
import nl.utwente.mastodon.mudtwente.repositories.fightsystem.EnemyInstanceRepository;
import nl.utwente.mastodon.mudtwente.repositories.itemsystem.InventoryRepository;
import nl.utwente.mastodon.mudtwente.repositories.songsystem.PlayerSongRepository;
import nl.utwente.mastodon.mudtwente.services.MUDCharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDateTime;

@Controller
@RequestMapping("/api")
public class MUDCharacterResource {

    private final MUDCharacterRepository mudCharacterRepository;
    private final EnemyInstanceRepository enemyInstanceRepository;
    private final QuestRepository questRepository;
    private final InventoryRepository inventoryRepository;
    private final PlayerSongRepository playerSongRepository;
    private final MUDCharacterService mudCharacterService;

    @Autowired
    public MUDCharacterResource(MUDCharacterRepository mudCharacterRepository,
                                EnemyInstanceRepository enemyInstanceRepository,
                                QuestRepository questRepository,
                                InventoryRepository inventoryRepository,
                                PlayerSongRepository playerSongRepository,
                                MUDCharacterService mudCharacterService) {
        this.mudCharacterRepository = mudCharacterRepository;
        this.enemyInstanceRepository = enemyInstanceRepository;
        this.questRepository = questRepository;
        this.inventoryRepository = inventoryRepository;
        this.playerSongRepository = playerSongRepository;
        this.mudCharacterService = mudCharacterService;
    }

    @GetMapping("/mudcharacters")
    public @ResponseBody
    Iterable<MUDCharacter> getAllUsers() {
        return mudCharacterRepository.findAll();
    }

    @GetMapping("/mudcharacters/{id}")
    public @ResponseBody
    MUDCharacter findMudcharacterById(@PathVariable int id) {
        return mudCharacterRepository.findById(id).orElse(null);
    }

    @GetMapping("/mudcharacters/registeredlastweek")
    public @ResponseBody
    int getAllUsersLastSevenDays() {
        //TODO: Make method in MUDCharacterService for this.
        int counter = 0;
        for (MUDCharacter mudCharacter : mudCharacterRepository.findAll()) {
            LocalDateTime localDateTime = mudCharacter.getCreatedAt();
            if (localDateTime.isAfter(LocalDateTime.now().minusDays(7))) {
                counter++;
            }
        }
        return counter;
    }

    @GetMapping("/mudcharacters/averagelevel")
    public @ResponseBody
    Double getAverageLevel() {
        //TODO: Make method in MUDCharacterService for this.
        int amountOfCharacters = 0;
        int sumLevels = 0;
        for (MUDCharacter mudCharacter : mudCharacterRepository.findAll()) {
            amountOfCharacters++;
            sumLevels = sumLevels + mudCharacter.getStoryLevel();
        }
        if (amountOfCharacters == 0) {
            return (double) 0;
        }
        return (double) sumLevels / (double) amountOfCharacters;
    }

    @GetMapping("/mudcharacters/playersonline")
    public @ResponseBody
    int getPlayersOnline() {
        return mudCharacterService.getAllOnlinePlayers().size();
    }

    @GetMapping("/mudcharacters/monsterattacks")
    public @ResponseBody
    int getMonsterAttacks() {
        return Lists.newArrayList(enemyInstanceRepository.findAll()).size();
    }

    @GetMapping("/mudcharacters/amountquests")
    public @ResponseBody
    int getAmountOfQuests() {
        return Lists.newArrayList(questRepository.findAll()).size();
    }

    @GetMapping("/mudcharacters/amountinventories")
    public @ResponseBody
    int getAmountOfInventories() {
        return Lists.newArrayList(inventoryRepository.findAll()).size();
    }

    @GetMapping("/mudcharacters/amountsongs")
    public @ResponseBody
    int getAmountOfSongs() {
        return Lists.newArrayList(playerSongRepository.findAll()).size();
    }
}

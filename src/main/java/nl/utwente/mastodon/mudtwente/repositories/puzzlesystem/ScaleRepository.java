package nl.utwente.mastodon.mudtwente.repositories.puzzlesystem;

import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.balancepuzzle_instance.Scale;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository class for a Scale
 */
@Repository
public interface ScaleRepository extends CrudRepository<Scale, Integer> {
}

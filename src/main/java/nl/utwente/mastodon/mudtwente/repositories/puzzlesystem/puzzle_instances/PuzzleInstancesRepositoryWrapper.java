package nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.puzzle_instances;

import lombok.Data;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.ScaleRepository;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.puzzle_instances.balancepuzzle_instance.BalancePuzzleInstanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Service
public class PuzzleInstancesRepositoryWrapper {

    private final RiddlePuzzleInstanceRepository riddlePuzzleInstanceRepository;
    private final BalancePuzzleInstanceRepository balancePuzzleInstanceRepository;
    private final BinaryPuzzleInstanceRepository binaryPuzzleInstanceRepository;
    private final LightsPuzzleInstanceRepository lightsPuzzleInstanceRepository;
    private final ScaleRepository scaleRepository;

    @Autowired
    public PuzzleInstancesRepositoryWrapper(RiddlePuzzleInstanceRepository riddlePuzzleInstanceRepository,
                                            BalancePuzzleInstanceRepository balancePuzzleInstanceRepository,
                                            BinaryPuzzleInstanceRepository binaryPuzzleInstanceRepository,
                                            LightsPuzzleInstanceRepository lightsPuzzleInstanceRepository,
                                            ScaleRepository scaleRepository) {
        this.riddlePuzzleInstanceRepository = riddlePuzzleInstanceRepository;
        this.balancePuzzleInstanceRepository = balancePuzzleInstanceRepository;
        this.binaryPuzzleInstanceRepository = binaryPuzzleInstanceRepository;
        this.lightsPuzzleInstanceRepository = lightsPuzzleInstanceRepository;
        this.scaleRepository = scaleRepository;
    }
}

package nl.utwente.mastodon.mudtwente.repositories.songsystem;

import nl.utwente.mastodon.mudtwente.entities.songsystem.Song;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository class for a Song.
 */
@Repository
public interface SongRepository extends CrudRepository<Song, Integer> {

}

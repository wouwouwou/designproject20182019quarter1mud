package nl.utwente.mastodon.mudtwente.repositories;

import nl.utwente.mastodon.mudtwente.entities.MastodonApp;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository class for a MastodonApp.
 */
@Repository
public interface MastodonAppRepository extends CrudRepository<MastodonApp, Integer> {

}

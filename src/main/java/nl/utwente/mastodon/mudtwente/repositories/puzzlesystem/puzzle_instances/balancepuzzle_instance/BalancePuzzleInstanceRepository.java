package nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.puzzle_instances.balancepuzzle_instance;

import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.balancepuzzle_instance.BalancePuzzleInstance;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository class for a BalancePuzzleInstance
 */
@Repository
public interface BalancePuzzleInstanceRepository extends CrudRepository<BalancePuzzleInstance, Integer> {

}

package nl.utwente.mastodon.mudtwente.repositories.fightsystem;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.fightsystem.EnemyInstance;
import nl.utwente.mastodon.mudtwente.entities.fightsystem.EnemyType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Repository class for a EnemyInstance.
 */
@Repository
public interface EnemyInstanceRepository extends CrudRepository<EnemyInstance, Integer> {
    Optional<EnemyInstance> findByMudCharacterAndEnemyType(MUDCharacter mudCharacter, EnemyType enemyType);
}

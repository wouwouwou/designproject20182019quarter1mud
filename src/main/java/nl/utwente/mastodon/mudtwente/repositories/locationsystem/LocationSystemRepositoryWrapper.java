package nl.utwente.mastodon.mudtwente.repositories.locationsystem;


import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Service
public class LocationSystemRepositoryWrapper {

    private LocationRepository locationRepository;
    private ShopRepository shopRepository;

    @Autowired
    public LocationSystemRepositoryWrapper(LocationRepository locationRepository, ShopRepository shopRepository) {
        this.locationRepository = locationRepository;
        this.shopRepository = shopRepository;
    }
}

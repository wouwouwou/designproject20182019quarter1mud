package nl.utwente.mastodon.mudtwente.repositories.fightsystem;

import nl.utwente.mastodon.mudtwente.entities.fightsystem.EnemyType;
import nl.utwente.mastodon.mudtwente.entities.locationsystem.Location;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Repository class for a EnemyType.
 */
@Repository
public interface EnemyTypeRepository extends CrudRepository<EnemyType, Integer> {
    Optional<EnemyType> findByLocationAndLevel(Location location, int level);
}

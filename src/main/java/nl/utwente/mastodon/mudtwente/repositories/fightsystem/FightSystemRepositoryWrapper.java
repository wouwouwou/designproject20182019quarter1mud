package nl.utwente.mastodon.mudtwente.repositories.fightsystem;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Service
public class FightSystemRepositoryWrapper {
    private final EnemyInstanceRepository enemyInstanceRepository;
    private final EnemyTypeRepository enemyTypeRepository;

    @Autowired
    public FightSystemRepositoryWrapper(EnemyInstanceRepository enemyInstanceRepository, EnemyTypeRepository enemyTypeRepository) {
        this.enemyInstanceRepository = enemyInstanceRepository;
        this.enemyTypeRepository = enemyTypeRepository;
    }

}

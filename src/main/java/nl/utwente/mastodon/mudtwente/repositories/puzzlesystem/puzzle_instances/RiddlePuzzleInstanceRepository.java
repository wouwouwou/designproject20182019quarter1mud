package nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.puzzle_instances;

import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.RiddlePuzzleInstance;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository class for a RiddlePuzzleInstance
 */
@Repository
public interface RiddlePuzzleInstanceRepository extends CrudRepository<RiddlePuzzleInstance, Integer> {

}

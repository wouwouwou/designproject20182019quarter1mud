package nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.puzzle_instances;

import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.BinaryPuzzleInstance;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository class for a BinaryPuzzleInstance
 */
@Repository
public interface BinaryPuzzleInstanceRepository extends CrudRepository<BinaryPuzzleInstance, Integer> {

}

package nl.utwente.mastodon.mudtwente.repositories.songsystem;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.songsystem.PlayerSong;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Repository class for a PlayerSong.
 */
@Repository
public interface PlayerSongRepository extends CrudRepository<PlayerSong, Integer> {
    Optional<PlayerSong> findByMudCharacter(MUDCharacter mudCharacter);
}

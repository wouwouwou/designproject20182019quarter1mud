package nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.puzzle_instances;

import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.LightsPuzzleInstance;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository class for a LightsPuzzleInstance
 */
@Repository
public interface LightsPuzzleInstanceRepository extends CrudRepository<LightsPuzzleInstance, Integer> {

}

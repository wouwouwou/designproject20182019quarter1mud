package nl.utwente.mastodon.mudtwente.repositories.itemsystem;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Service
public class ItemSystemRepositoryWrapper {

    private final InventoryRepository inventoryRepository;
    private final ItemRepository itemRepository;
    private final ShopitemRepository shopitemRepository;

    @Autowired
    public ItemSystemRepositoryWrapper(InventoryRepository inventoryRepository, ItemRepository itemRepository, ShopitemRepository shopitemRepository) {
        this.inventoryRepository = inventoryRepository;
        this.itemRepository = itemRepository;
        this.shopitemRepository = shopitemRepository;
    }

}

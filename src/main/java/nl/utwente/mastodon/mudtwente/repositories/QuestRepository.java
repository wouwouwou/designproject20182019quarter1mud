package nl.utwente.mastodon.mudtwente.repositories;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.Quest;
import nl.utwente.mastodon.mudtwente.entities.npcsystem.NPC;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Repository class for a Quest.
 */
@Repository
public interface QuestRepository extends CrudRepository<Quest, Integer> {
    Optional<Quest> findByMudCharacterAndNpc(MUDCharacter mudCharacter, NPC npc);
}

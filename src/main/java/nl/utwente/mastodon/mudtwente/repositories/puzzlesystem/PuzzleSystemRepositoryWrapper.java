package nl.utwente.mastodon.mudtwente.repositories.puzzlesystem;

import lombok.Data;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.puzzle_instances.PuzzleInstancesRepositoryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Service
public class PuzzleSystemRepositoryWrapper {

    private final PuzzleInstanceRepository puzzleInstanceRepository;
    private final PuzzleInstancesRepositoryWrapper puzzleInstancesRepositoryWrapper;

    @Autowired
    public PuzzleSystemRepositoryWrapper(PuzzleInstanceRepository puzzleInstanceRepository,
                                         PuzzleInstancesRepositoryWrapper puzzleInstancesRepositoryWrapper) {
        this.puzzleInstanceRepository = puzzleInstanceRepository;
        this.puzzleInstancesRepositoryWrapper = puzzleInstancesRepositoryWrapper;
    }
}

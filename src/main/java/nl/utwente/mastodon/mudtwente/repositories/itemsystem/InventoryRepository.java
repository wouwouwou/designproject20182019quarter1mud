package nl.utwente.mastodon.mudtwente.repositories.itemsystem;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.itemsystem.Inventory;
import nl.utwente.mastodon.mudtwente.entities.itemsystem.Item;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Repository class for an Inventory.
 */
@Repository
public interface InventoryRepository extends CrudRepository<Inventory, Integer> {
    Optional<Inventory> findByMudCharacterAndItem(MUDCharacter mudCharacter, Item item);
    List<Inventory> findByMudCharacter(MUDCharacter mudCharacter);
}

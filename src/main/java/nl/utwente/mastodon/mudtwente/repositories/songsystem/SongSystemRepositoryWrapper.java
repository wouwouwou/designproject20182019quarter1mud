package nl.utwente.mastodon.mudtwente.repositories.songsystem;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Service
public class SongSystemRepositoryWrapper {

    private final SongRepository songRepository;
    private final PlayerSongRepository playerSongRepository;

    @Autowired
    public SongSystemRepositoryWrapper(SongRepository songRepository, PlayerSongRepository playerSongRepository) {
        this.songRepository = songRepository;
        this.playerSongRepository = playerSongRepository;
    }
}

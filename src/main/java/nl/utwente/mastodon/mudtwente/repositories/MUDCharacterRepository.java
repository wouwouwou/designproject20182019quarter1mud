package nl.utwente.mastodon.mudtwente.repositories;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Repository class for a MUDCharacter
 */
@Repository
public interface MUDCharacterRepository extends CrudRepository<MUDCharacter, Integer> {
    List<MUDCharacter> findByName(String name);
    Optional<MUDCharacter> findMUDCharacterByUsername(String username);
    Optional<MUDCharacter> findMUDCharacterByUsernameAndMastodonAccountId(String username, int mastodonAccountId);
}

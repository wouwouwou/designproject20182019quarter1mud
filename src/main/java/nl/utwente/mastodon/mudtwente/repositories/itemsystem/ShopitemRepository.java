package nl.utwente.mastodon.mudtwente.repositories.itemsystem;

import nl.utwente.mastodon.mudtwente.entities.itemsystem.ShopItem;
import nl.utwente.mastodon.mudtwente.entities.locationsystem.Shop;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository class for a ShopItem.
 */
@Repository
public interface ShopitemRepository extends CrudRepository<ShopItem, Integer> {
    List<ShopItem> findByShop(Shop shop);
}

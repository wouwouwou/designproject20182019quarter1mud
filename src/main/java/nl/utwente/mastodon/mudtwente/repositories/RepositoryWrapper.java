package nl.utwente.mastodon.mudtwente.repositories;

import lombok.Data;
import nl.utwente.mastodon.mudtwente.repositories.fightsystem.FightSystemRepositoryWrapper;
import nl.utwente.mastodon.mudtwente.repositories.itemsystem.ItemSystemRepositoryWrapper;
import nl.utwente.mastodon.mudtwente.repositories.locationsystem.LocationSystemRepositoryWrapper;
import nl.utwente.mastodon.mudtwente.repositories.npcsystem.NPCSystemRepositoryWrapper;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.PuzzleSystemRepositoryWrapper;
import nl.utwente.mastodon.mudtwente.repositories.songsystem.SongSystemRepositoryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Service
public class RepositoryWrapper {

    private final LocationSystemRepositoryWrapper locationSystemRepositoryWrapper;
    private final NPCSystemRepositoryWrapper npcSystemRepositoryWrapper;
    private final ItemSystemRepositoryWrapper itemSystemRepositoryWrapper;
    private final QuestRepository questRepository;
    private final FightSystemRepositoryWrapper fightSystemRepositoryWrapper;
    private final SongSystemRepositoryWrapper songSystemRepositoryWrapper;
    private final PuzzleSystemRepositoryWrapper puzzleSystemRepositoryWrapper;

    @Autowired
    public RepositoryWrapper(LocationSystemRepositoryWrapper locationSystemRepositoryWrapper,
                             NPCSystemRepositoryWrapper npcSystemRepositoryWrapper,
                             ItemSystemRepositoryWrapper itemSystemRepositoryWrapper,
                             QuestRepository questRepository,
                             FightSystemRepositoryWrapper fightSystemRepositoryWrapper,
                             SongSystemRepositoryWrapper songSystemRepositoryWrapper,
                             PuzzleSystemRepositoryWrapper puzzleSystemRepositoryWrapper
    ) {
        this.locationSystemRepositoryWrapper = locationSystemRepositoryWrapper;
        this.npcSystemRepositoryWrapper = npcSystemRepositoryWrapper;
        this.itemSystemRepositoryWrapper = itemSystemRepositoryWrapper;
        this.questRepository = questRepository;
        this.fightSystemRepositoryWrapper = fightSystemRepositoryWrapper;
        this.songSystemRepositoryWrapper = songSystemRepositoryWrapper;
        this.puzzleSystemRepositoryWrapper = puzzleSystemRepositoryWrapper;
    }
}

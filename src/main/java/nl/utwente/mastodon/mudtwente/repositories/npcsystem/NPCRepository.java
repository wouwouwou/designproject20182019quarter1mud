package nl.utwente.mastodon.mudtwente.repositories.npcsystem;

import nl.utwente.mastodon.mudtwente.entities.locationsystem.Location;
import nl.utwente.mastodon.mudtwente.entities.npcsystem.NPC;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Repository class for an NPC.
 */
@Repository
public interface NPCRepository extends CrudRepository<NPC, Integer> {
    Optional<NPC> findByName(String name);
    List<NPC> findByLocation(Location location);
}

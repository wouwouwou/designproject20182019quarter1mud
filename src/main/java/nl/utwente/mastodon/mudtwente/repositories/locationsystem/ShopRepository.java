package nl.utwente.mastodon.mudtwente.repositories.locationsystem;

import nl.utwente.mastodon.mudtwente.entities.locationsystem.Location;
import nl.utwente.mastodon.mudtwente.entities.locationsystem.Shop;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * Repository class for a Shop
 */
public interface ShopRepository extends CrudRepository<Shop, Integer> {
    Optional<Shop> findByLocation(Location location);
}

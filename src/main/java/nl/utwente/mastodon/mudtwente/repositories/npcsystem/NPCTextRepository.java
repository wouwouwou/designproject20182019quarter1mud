package nl.utwente.mastodon.mudtwente.repositories.npcsystem;

import nl.utwente.mastodon.mudtwente.entities.npcsystem.NPC;
import nl.utwente.mastodon.mudtwente.entities.npcsystem.NPCText;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository class for an NPCText.
 */
@Repository
public interface NPCTextRepository extends CrudRepository<NPCText, Integer> {
    List<NPCText> findByNpc(NPC npc);
}

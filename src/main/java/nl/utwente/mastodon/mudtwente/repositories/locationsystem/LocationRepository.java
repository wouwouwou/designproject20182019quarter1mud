package nl.utwente.mastodon.mudtwente.repositories.locationsystem;

import nl.utwente.mastodon.mudtwente.entities.locationsystem.Location;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LocationRepository extends CrudRepository<Location, Integer> {
    Optional<Location> findById(int id);
    List<Location> findByName(String name);
}

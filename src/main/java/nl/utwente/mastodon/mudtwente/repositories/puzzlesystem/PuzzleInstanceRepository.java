package nl.utwente.mastodon.mudtwente.repositories.puzzlesystem;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.Puzzle;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.PuzzleInstance;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Repository class for a PuzzleInstance
 */
@Repository
public interface PuzzleInstanceRepository extends CrudRepository<PuzzleInstance, Integer> {
    Optional<PuzzleInstance> findByMudCharacterAndPuzzle(MUDCharacter mudCharacter, Puzzle puzzle);
}

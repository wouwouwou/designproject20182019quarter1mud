package nl.utwente.mastodon.mudtwente.repositories.itemsystem;

import nl.utwente.mastodon.mudtwente.entities.itemsystem.Item;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Repository class for an Item.
 */
@Repository
public interface ItemRepository extends CrudRepository<Item, Integer> {
    Optional<Item> findByName(String name);
}

package nl.utwente.mastodon.mudtwente.repositories.npcsystem;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Service
public class NPCSystemRepositoryWrapper {

    private final NPCRepository npcRepository;
    private final NPCTextRepository npcTextRepository;

    @Autowired
    public NPCSystemRepositoryWrapper(NPCRepository npcRepository, NPCTextRepository npcTextRepository) {
        this.npcRepository = npcRepository;
        this.npcTextRepository = npcTextRepository;
    }
}

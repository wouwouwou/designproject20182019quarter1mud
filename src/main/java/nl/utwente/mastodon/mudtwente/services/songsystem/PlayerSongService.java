package nl.utwente.mastodon.mudtwente.services.songsystem;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.songsystem.PlayerSong;
import nl.utwente.mastodon.mudtwente.entities.songsystem.Song;
import nl.utwente.mastodon.mudtwente.repositories.RepositoryWrapper;
import nl.utwente.mastodon.mudtwente.repositories.songsystem.PlayerSongRepository;
import nl.utwente.mastodon.mudtwente.repositories.songsystem.SongRepository;
import nl.utwente.mastodon.mudtwente.services.itemsystem.InventoryService;
import nl.utwente.mastodon.mudtwente.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Random;

/**
 * Service class for a Quest.
 */
@Service
public class PlayerSongService {

    private PlayerSongRepository playerSongRepository;

    @Autowired
    public PlayerSongService(PlayerSongRepository playerSongRepository) {
        this.playerSongRepository = playerSongRepository;
    }

    /**
     * May find a PlayerSong by characterId. Returns an Optional<PlayerSong> which may contain a valid PlayerSong whose
     * characterId equals the parameter.
     * @param mudCharacter The character to search for.
     * @return An Optional<PlayerSong> which may contain a valid PlayerSong whose characterId equals the parameter.
     */
    public Optional<PlayerSong> findByMudCharacter(MUDCharacter mudCharacter) {
        return playerSongRepository.findByMudCharacter(mudCharacter);
    }

    public String handleNewSongCommand(RepositoryWrapper repositoryWrapper,
                                        MUDCharacter mudCharacter) {

        SongRepository songRepository = repositoryWrapper.getSongSystemRepositoryWrapper().getSongRepository();
        playerSongRepository = repositoryWrapper.getSongSystemRepositoryWrapper().getPlayerSongRepository();

        Optional<PlayerSong> playerSongOptional = playerSongRepository.findByMudCharacter(mudCharacter);

        //If song exists
        if (playerSongOptional.isPresent()) {
            PlayerSong playerSong = playerSongOptional.get();
            return "You already got a song. Finish this couplet: " + playerSong.getSong().getSongText();
        }

        int amountSong = (int) songRepository.count();
        Random random = new Random();
        int randomId = random.nextInt(amountSong) + 1;

        Optional<Song> songOptional = songRepository.findById(randomId);
        if (songOptional.isPresent()) {
            Song song = songOptional.get();
            PlayerSong playerSong = new PlayerSong();
            playerSong.setMudCharacter(mudCharacter);
            playerSong.setSong(song);
            save(playerSong);
            return "You have to finish this verse: " + song.getSongText();
        }
        return "It is not possible to create a song right now. Sorry!";
    }

    private void save(PlayerSong playerSong) {
        playerSongRepository.save(playerSong);
    }

    public String handleSongCommand(List<String> words,
                                     RepositoryWrapper repositoryWrapper,
                                     MUDCharacter mudCharacter) {

        playerSongRepository = repositoryWrapper.getSongSystemRepositoryWrapper().getPlayerSongRepository();


        String playerAnswer = String.join(" ", words);

        Optional<PlayerSong> playerSongOptional = findByMudCharacter(mudCharacter);

        // If not correct answer or no song
        if (!playerSongOptional.isPresent()) {
            return "You do not have a song quest yet! Try" +
                    " the command {newsong} to get a new song. ";
        }

        PlayerSong playerSong = playerSongOptional.get();
        Song song = playerSong.getSong();
        if (playerAnswer.equals(song.getAnswer())) {

            playerSongRepository.delete(playerSong);

            // If there is not a rewardItem
            if (song.getRewardItem() == null) {
                return "You have successfully finished the verse!.";
            }

            InventoryService inventoryService = new InventoryService(repositoryWrapper.getItemSystemRepositoryWrapper().getInventoryRepository());
            inventoryService.addItemToInventory(mudCharacter, song.getRewardItem(), song.getRewardAmount());
            String itemName = song.getRewardItem().getName();

            return "You have successfully finished the verse! Your reward is: " +
                    song.getRewardAmount() + " " + StringUtils.setFirstLetterUpperCase(itemName);
        }
        return "Unfortunately that is not the correct answer. Feel free to try again!";
    }
}

package nl.utwente.mastodon.mudtwente.services;

import com.google.api.client.util.Lists;
import nl.utwente.mastodon.mudtwente.entities.MastodonApp;
import nl.utwente.mastodon.mudtwente.repositories.MastodonAppRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service class for a MastodonApp.
 */
@Service
public class MastodonAppService {

    private MastodonAppRepository mastodonAppRepository;

    @Autowired
    public MastodonAppService(MastodonAppRepository mastodonAppRepository) {
        this.mastodonAppRepository = mastodonAppRepository;
    }

    /**
     * Finds all MastodonApps out of the database.
     * @return A list of all mastodonApps.
     */
    List<MastodonApp> findAll() {
        return Lists.newArrayList(mastodonAppRepository.findAll());
    }
}

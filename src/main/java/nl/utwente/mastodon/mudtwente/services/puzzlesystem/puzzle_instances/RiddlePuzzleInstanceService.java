package nl.utwente.mastodon.mudtwente.services.puzzlesystem.puzzle_instances;

import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.RiddlePuzzleInstance;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.puzzle_instances.RiddlePuzzleInstanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service class for a RiddlePuzzleInstance.
 */
@Service
public class RiddlePuzzleInstanceService {

    private RiddlePuzzleInstanceRepository riddlePuzzleInstanceRepository;

    @Autowired
    public RiddlePuzzleInstanceService(RiddlePuzzleInstanceRepository riddlePuzzleInstanceRepository) {
        this.riddlePuzzleInstanceRepository = riddlePuzzleInstanceRepository;
    }

    /**
     * Saves the RiddlePuzzleInstance in the database.
     *
     * @param riddlePuzzleInstance RiddlePuzzleInstance to save.
     */
    public void save(RiddlePuzzleInstance riddlePuzzleInstance) {
        riddlePuzzleInstanceRepository.save(riddlePuzzleInstance);
    }

    /**
     * Attempts to answer a riddle. Gets the answer from words. Checks the correct usage of the
     * 'answer' command. Checks if the answer is correct and handles it accordingly.
     * @param words List of Strings, containing the answer.
     * @param riddlePuzzleInstance RiddlePuzzleInstance needed for the attempt.
     * @return A string with the result of the attempt.
     */
    public String tryAnswer(List<String> words,
                            RiddlePuzzleInstance riddlePuzzleInstance) {
        String wrongUsage = "Please use 'answer' <numericAnswer> in order to answer the riddle!";
        if (words.size() != 1) {
            return wrongUsage;
        }

        if (!riddlePuzzleInstance.isCorrectAnswer(words.get(0))) {
            return "You have not given the right answer to the riddle!";
        }

        riddlePuzzleInstance.correctAnswerGiven();
        save(riddlePuzzleInstance);
        String correctAnswer = "That answer is correct!\n";

        if (riddlePuzzleInstance.isSolved()) {
            return correctAnswer +
                    "You have solved all the riddles of this puzzle! The statue says: \"" +
                    riddlePuzzleInstance.getRiddlePuzzle().getHintAfterCompletion() + "\"";
        }
        return correctAnswer +
                "The next riddle of this puzzle is:\n" +
                riddlePuzzleInstance.getRiddlePuzzle().getQuestions().get(
                        riddlePuzzleInstance.getProgress()
                );
    }
}

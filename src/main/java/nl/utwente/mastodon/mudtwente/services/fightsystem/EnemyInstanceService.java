package nl.utwente.mastodon.mudtwente.services.fightsystem;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.fightsystem.EnemyInstance;
import nl.utwente.mastodon.mudtwente.entities.fightsystem.EnemyType;
import nl.utwente.mastodon.mudtwente.entities.locationsystem.Location;
import nl.utwente.mastodon.mudtwente.repositories.MUDCharacterRepository;
import nl.utwente.mastodon.mudtwente.repositories.RepositoryWrapper;
import nl.utwente.mastodon.mudtwente.repositories.fightsystem.EnemyInstanceRepository;
import nl.utwente.mastodon.mudtwente.repositories.fightsystem.EnemyTypeRepository;
import nl.utwente.mastodon.mudtwente.services.itemsystem.InventoryService;
import nl.utwente.mastodon.mudtwente.utils.Constants;
import nl.utwente.mastodon.mudtwente.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Random;

/**
 * Service class for a MUDCharacter.
 */
@Service
public class EnemyInstanceService {

    private EnemyInstanceRepository enemyInstanceRepository;

    @Autowired
    public EnemyInstanceService(EnemyInstanceRepository enemyInstanceRepository) {
        this.enemyInstanceRepository = enemyInstanceRepository;
    }

    /**
     * May find a EnemyInstance by characterId and location. Returns an Optional<EnemyInstance> which may contain a valid
     * EnemyInstance whose characterId and location equals the parameters.
     * @param mudCharacter The character to search for.
     * @param enemyType The enemy to search for.
     * @return An Optional<EnemyInstance> which may contain a valid EnemyInstance whose characterId and location equals the
     * parameters
     */
    public Optional<EnemyInstance> findByMudCharacterAndEnemyType(MUDCharacter mudCharacter, EnemyType enemyType) {
        return enemyInstanceRepository.findByMudCharacterAndEnemyType(mudCharacter, enemyType);
    }

    //TODO add documentation
    public String handleAttackCommand(MUDCharacter mudCharacter,
                                       RepositoryWrapper repositoryWrapper,
                                       MUDCharacterRepository mudCharacterRepository) {

        Location location = mudCharacter.getLocation();
        int level = mudCharacter.getStoryLevel();
        int characterHp = mudCharacter.getHealth();
        InventoryService inventoryService = new InventoryService(repositoryWrapper.getItemSystemRepositoryWrapper().getInventoryRepository());
        EnemyTypeRepository enemyTypeRepository = repositoryWrapper.getFightSystemRepositoryWrapper().getEnemyTypeRepository();
        enemyInstanceRepository = repositoryWrapper.getFightSystemRepositoryWrapper().getEnemyInstanceRepository();

        Optional<EnemyType> enemyTypeOptional = enemyTypeRepository.findByLocationAndLevel(location, level);
        if (!enemyTypeOptional.isPresent()) {
            return "There is no enemy at this location!";
        }
        EnemyType enemyType = enemyTypeOptional.get();
        Optional<EnemyInstance> enemyInstanceOptional = enemyInstanceRepository.findByMudCharacterAndEnemyType(mudCharacter, enemyType);
        EnemyInstance enemyInstance;
        // Check if there is already a fight ongoing
        if (enemyInstanceOptional.isPresent()) {
            enemyInstance = enemyInstanceOptional.get();
        } else {
            // If this is a new fight
            enemyInstance = new EnemyInstance();
            enemyInstance.setMudCharacter(mudCharacter);
            enemyInstance.setEnemyType(enemyType);
            enemyInstance.setHp(enemyType.getMaxHP());
        }

        Random random = new Random();
        int attackFromPlayer = random.nextInt(10 + mudCharacter.getStoryLevel()) + mudCharacter.getStoryLevel();
        int attackFromEnemy = random.nextInt(enemyType.getPower()) + 1;
        // Player kills the enemyType
        if (attackFromPlayer >= enemyInstance.getHp()) {
            String result = "You successfully killed the enemy " + StringUtils.setFirstLetterUpperCase(enemyType.getName()) + "!";
            enemyInstanceRepository.delete(enemyInstance);
            // There is a rewardItem
            if (enemyType.getReward() != null) {
                inventoryService.addItemToInventory(mudCharacter, enemyType.getReward(), enemyType.getRewardAmount());
                return result + "\nYou got a reward: " + enemyType.getRewardAmount() + " " + StringUtils.setFirstLetterUpperCase(enemyType.getReward().getName());
            }
            // Player does NOT kill the enemyType
        }
        enemyInstance.setHp(enemyInstance.getHp() - attackFromPlayer);
        enemyInstanceRepository.save(enemyInstance);
        // If player is dead
        if (attackFromEnemy >= characterHp) {
            //He has to sleep now.
            mudCharacter.setHealth(1);
            // Set location to student housing.
            mudCharacter.setLocation(Constants.STUDENT_HOUSING);   // Student housing
            mudCharacterRepository.save(mudCharacter);
            delete(enemyInstance);
            return "The enemy has killed you. You are warped to your Student house" +
                    "and you should take some sleep, since you have 1 hp left.";
        }
        // Player is not dead
        mudCharacter.setHealth(characterHp - attackFromEnemy);
        mudCharacterRepository.save(mudCharacter);
        return "You attacked the enemy. The enemy lost " + attackFromPlayer + " hp and his health is: " + enemyInstance.getHp()
                + ". The enemy also attacked you. You lost " + attackFromEnemy + " hp and your health is: " + mudCharacter.getHealth();
    }

    //TODO add documentation
    public void delete(EnemyInstance enemyInstance) {
        enemyInstanceRepository.delete(enemyInstance);
    }
}

package nl.utwente.mastodon.mudtwente.services.locationsystem;

import nl.utwente.mastodon.mudtwente.entities.locationsystem.Location;
import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.locationsystem.Shop;
import nl.utwente.mastodon.mudtwente.entities.itemsystem.Inventory;
import nl.utwente.mastodon.mudtwente.entities.itemsystem.Item;
import nl.utwente.mastodon.mudtwente.entities.itemsystem.ShopItem;
import nl.utwente.mastodon.mudtwente.repositories.RepositoryWrapper;
import nl.utwente.mastodon.mudtwente.repositories.locationsystem.ShopRepository;
import nl.utwente.mastodon.mudtwente.repositories.itemsystem.InventoryRepository;
import nl.utwente.mastodon.mudtwente.repositories.itemsystem.ItemRepository;
import nl.utwente.mastodon.mudtwente.repositories.itemsystem.ShopitemRepository;
import nl.utwente.mastodon.mudtwente.services.itemsystem.InventoryService;
import nl.utwente.mastodon.mudtwente.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static nl.utwente.mastodon.mudtwente.utils.Constants.*;

/**
 * Service class for a Shop.
 */
@Service
public class ShopService {

    private ShopRepository shopRepository;

    @Autowired
    public ShopService(ShopRepository shopRepository) {
        this.shopRepository = shopRepository;
    }

    /**
     * May find a Shop by location. Returns an Optional<Shop> which may contain a valid Shop whose
     * location equals the parameter.
     * @param location The location to search for.
     * @return An Optional<Shop> which may contain a valid Quest whose location equals the parameter.
     */
    public Optional<Shop> findByLocation(Location location) {
        return shopRepository.findByLocation(location);
    }

    /**
     * May find a Shop by id. Returns an Optional<Shop> which may contain a valid Shop whose
     * id equals the parameter.
     * @param id The id to search for.
     * @return An Optional<Shop> which may contain a valid Quest whose location equals the parameter.
     */
    public Optional<Shop> findById(int id) {
        return shopRepository.findById(id);
    }

    /**
     * Handles the 'buy' command and returns a string which contains the message which the bot has to send back.
     * @param repositoryWrapper RepositoryWrapper needed for handling the 'buy' command
     * @param mudCharacter MUDCharacter needed for handling the 'buy' command
     * @param words The rest of the notification's content (without the command)
     * @return A string which contains the message which the bot has to send back.
     */
    public String handleBuyCommand(RepositoryWrapper repositoryWrapper,
                                    MUDCharacter mudCharacter,
                                    List<String> words) {

        ItemRepository itemRepository = repositoryWrapper.getItemSystemRepositoryWrapper().getItemRepository();
        shopRepository = repositoryWrapper.getLocationSystemRepositoryWrapper().getShopRepository();
        ShopitemRepository shopitemRepository = repositoryWrapper.getItemSystemRepositoryWrapper().getShopitemRepository();
        InventoryRepository inventoryRepository = repositoryWrapper.getItemSystemRepositoryWrapper().getInventoryRepository();
        InventoryService inventoryService = new InventoryService(inventoryRepository);

        Optional<Shop> shopOptional = shopRepository.findByLocation(mudCharacter.getLocation());

        if (!shopOptional.isPresent()) {
            return " This location does not have a shop!";
        }

        if (words.isEmpty()) {
            return " What do you want to buy?";
        }

        Optional<Item> optionalItem = itemRepository.findByName(words.get(0));

        if (!optionalItem.isPresent()) {
            return " Item does not exist!";
        }

        Item itemtoBuy = optionalItem.get();
        ShopItem correctItem = null;
        for (ShopItem shopitem : shopitemRepository.findByShop(shopOptional.get())) {
            if (shopitem.getItem().equals(itemtoBuy)) {
                correctItem = shopitem;
                break;
            }
        }

        if (correctItem == null) {
            return " Shop does not have this item!";
        }

        Optional<Inventory> inventoryOptional = inventoryRepository.findByMudCharacterAndItem(mudCharacter, COIN);
        if (inventoryOptional.isPresent() && inventoryOptional.get().getAmount() < correctItem.getCost()) {
            return " You do not own enough coins to buy this item!";
        }

        inventoryService.addItemToInventory(mudCharacter, COIN, -correctItem.getCost());
        inventoryService.addItemToInventory(mudCharacter, correctItem.getItem(), 1);

        return " You have successfully bought your item!";
    }

    /**
     * Handles the 'whattobuy' command and returns a string which contains the message which the bot has to send back.
     * @param mudCharacter MUDCharacter needed for handling the 'whattobuy' command
     * @param shopitemRepository ShopitemRepository needed for handling the 'whattobuy' command
     * @return A string which contains the message which the bot has to send back.
     */
    public String handleWhatToBuyCommand(MUDCharacter mudCharacter,
                                         ShopitemRepository shopitemRepository) {
        if (mudCharacter.getLocation().isShop()) {
            Optional<Shop> shopOptional = shopRepository.findByLocation(mudCharacter.getLocation());
            if (shopOptional.isPresent()) {
                Shop shop = shopOptional.get();
                StringBuilder result = new StringBuilder();
                for (ShopItem shopitem : shopitemRepository.findByShop(shop)) {
                        Item item = shopitem.getItem();
                        result.append("\n");
                        result.append(StringUtils.setFirstLetterUpperCase(item.getName()));
                        result.append(": ");
                        result.append(item.getDescription());
                        result.append(" - ");
                        result.append(shopitem.getCost());
                        result.append(" coins.");
                }
                return "This shop sells: " + result.toString();
            }
        }
        return "This location does not have a shop!";
    }

    /**
     * Saves all Shop Constants in the database.
     */
    public void addToDatabase() {
        testIfCorrectInDatabase(COOP_SHOP.getId(), COOP_SHOP);
        testIfCorrectInDatabase(VESTINGBAR_SHOP.getId(), VESTINGBAR_SHOP);
        testIfCorrectInDatabase(VRIJHOF_SHOP.getId(), VRIJHOF_SHOP);
        testIfCorrectInDatabase(SPIEGEL_SHOP.getId(), SPIEGEL_SHOP);
        testIfCorrectInDatabase(ZUL_SHOP.getId(), ZUL_SHOP);
        testIfCorrectInDatabase(WAAIER_SHOP.getId(), WAAIER_SHOP);
    }

    /**
     * Test if the constant is set correctly in the database. If not save it again.
     * 
     * @param shopId to search for
     * @param constantShop to search for
     */
    public void testIfCorrectInDatabase(int shopId, Shop constantShop) {
        Optional<Shop> shopOptional = findById(shopId);
        if (!(shopOptional.isPresent() && shopOptional.get().equals(constantShop))) {
            shopRepository.save(constantShop);
        }
    }
}

package nl.utwente.mastodon.mudtwente.services.itemsystem;

import nl.utwente.mastodon.mudtwente.entities.itemsystem.Item;
import nl.utwente.mastodon.mudtwente.repositories.itemsystem.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static nl.utwente.mastodon.mudtwente.utils.Constants.*;

/**
 * Service class for an Item.
 */
@Service
public class ItemService {

    private ItemRepository itemRepository;

    @Autowired
    public ItemService(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    /**
     * May find an Item by name. Returns an Optional<Item> which may contain a valid Item whose
     * name equals the name.
     * @param name The name to search for.
     * @return An Optional<Item> which may contain a valid Item whose name equals the parameter.
     */
    public Optional<Item> findByName(String name) {
        return itemRepository.findByName(name);
    }

    public Optional<Item> findById(int id) {
        return itemRepository.findById(id);
    }

    /**
     * Saves all Item Constants in the database.
     */
    public void addToDatabase() {
        testIfCorrectInDatabase(HAIR.getId(), HAIR);
        testIfCorrectInDatabase(PIE.getId(), PIE);
        testIfCorrectInDatabase(WEAPON_DESIGN_1.getId(), WEAPON_DESIGN_1);
        testIfCorrectInDatabase(PLASTIC.getId(), PLASTIC);
        testIfCorrectInDatabase(PEN.getId(), PEN);
        testIfCorrectInDatabase(RUGBY_CLOTHES.getId(), RUGBY_CLOTHES);
        testIfCorrectInDatabase(POTION.getId(), POTION);
        testIfCorrectInDatabase(SUPER_POTION.getId(), SUPER_POTION);
        testIfCorrectInDatabase(KLAAS_SIKKEL.getId(), KLAAS_SIKKEL);
        testIfCorrectInDatabase(COIN.getId(), COIN);
        testIfCorrectInDatabase(ROM_LANGERAK_ITEM.getId(), ROM_LANGERAK_ITEM);
        testIfCorrectInDatabase(DRUNK_MATE_ITEM.getId(), DRUNK_MATE_ITEM);
        testIfCorrectInDatabase(BARTENDER_ITEM.getId(), BARTENDER_ITEM);
        testIfCorrectInDatabase(MICROPHONE.getId(), MICROPHONE);
        testIfCorrectInDatabase(THOMAS_CALCULUS.getId(), THOMAS_CALCULUS);
        testIfCorrectInDatabase(ENEMY_HOUSE.getId(), ENEMY_HOUSE);
        testIfCorrectInDatabase(ITALIAN_MAFIA_ITEM.getId(), ITALIAN_MAFIA_ITEM);
        testIfCorrectInDatabase(CHINESE_TEXT.getId(), CHINESE_TEXT);
        testIfCorrectInDatabase(ROSE.getId(), ROSE);
        testIfCorrectInDatabase(RAM.getId(), RAM);
        testIfCorrectInDatabase(CUP.getId(), CUP);
    }

    /**
     * Test if the constant is set correctly in the database. If not save it again.
     *
     * @param itemId to search for
     * @param constantItem to search for
     */
    public void testIfCorrectInDatabase(int itemId, Item constantItem) {
        Optional<Item> itemOptional = findById(itemId);
        if (!(itemOptional.isPresent() && itemOptional.get().equals(constantItem))) {
            itemRepository.save(constantItem);
        }
    }
}

package nl.utwente.mastodon.mudtwente.services.puzzlesystem;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.Puzzle;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.PuzzleInstance;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.balancepuzzle_instance.BalancePuzzleInstance;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.BinaryPuzzleInstance;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.LightsPuzzleInstance;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.RiddlePuzzleInstance;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.RiddlePuzzle;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.BalancePuzzle;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.balancepuzzle_instance.Scale;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.lightspuzzle.LightsPuzzle;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.PuzzleInstanceRepository;
import nl.utwente.mastodon.mudtwente.services.puzzlesystem.puzzle_instances.balancepuzzle_instance.BalancePuzzleInstanceService;
import nl.utwente.mastodon.mudtwente.services.puzzlesystem.puzzle_instances.BinaryPuzzleInstanceService;
import nl.utwente.mastodon.mudtwente.services.puzzlesystem.puzzle_instances.LightsPuzzleInstanceService;
import nl.utwente.mastodon.mudtwente.services.puzzlesystem.puzzle_instances.RiddlePuzzleInstanceService;
import nl.utwente.mastodon.mudtwente.services.puzzlesystem.puzzle_instances.balancepuzzle_instance.ScaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Service class for a PuzzleInstance.
 */
@Service
public class PuzzleInstanceService {

    private PuzzleInstanceRepository puzzleInstanceRepository;

    private static final String NO_PUZZLE = "There is no puzzle at this location. " +
            "Therefore you cannot use this command here!";

    @Autowired
    public PuzzleInstanceService(PuzzleInstanceRepository puzzleInstanceRepository) {
        this.puzzleInstanceRepository = puzzleInstanceRepository;
    }

    /**
     * May find a PuzzleInstance by MUDCharacter and Puzzle. Returns an Optional<PuzzleInstance> which may contain a
     * valid PuzzleInstance whose MUDCharacter and Puzzle equals the parameters.
     *
     * @param mudCharacter MUDCharacter to search for.
     * @param puzzle Puzzle to search for.
     * @return An Optional<PuzzleInstance> which may contain a valid PuzzleInstance whose MUDCharacter and Puzzle
     * equals the parameters.
     */
    public Optional<PuzzleInstance> findByMudCharacterAndPuzzle(MUDCharacter mudCharacter,
                                                         Puzzle puzzle) {
        return puzzleInstanceRepository.findByMudCharacterAndPuzzle(mudCharacter, puzzle);
    }

    /**
     * Saves the PuzzleInstance in the database.
     *
     * @param puzzleInstance PuzzleInstance to save.
     */
    public void save(PuzzleInstance puzzleInstance) {
        puzzleInstanceRepository.save(puzzleInstance);
    }

    /**
     * Handles the 'puzzlestatus' command and returns a string which
     * contains the message which the bot has to send back.
     *
     * @param mudCharacter MUDCharacter needed for handling the 'puzzlestatus' command
     * @return A string which contains the message which the bot has to send back.
     */
    public String handlePuzzleStatusCommand(MUDCharacter mudCharacter) {
        return Optional.ofNullable(mudCharacter.getLocation().getPuzzle())
                .map(puzzle -> findByMudCharacterAndPuzzle(mudCharacter, puzzle)
                        .map(PuzzleInstance::getStatus)
                        .orElseGet(() -> createPuzzleInstance(mudCharacter, puzzle)))
                .orElse("There is no puzzle at this location to check the status of!");
    }

    /**
     * Handles the 'answer' command and returns a string which contains the message which the bot has to send back.
     *
     *
     * @param mudCharacter MUDCharacter needed for handling the 'answer' command
     * @param words The rest of the notification's content (without the command)
     * @param riddlePuzzleInstanceService RiddlePuzzleInstanceService needed for handling the 'answer' command
     * @return A string which contains the message which the bot has to send back.
     */
    public String handleAnswerCommand(MUDCharacter mudCharacter,
                                      List<String> words,
                                      RiddlePuzzleInstanceService riddlePuzzleInstanceService) {
        return Optional.ofNullable(mudCharacter.getLocation().getPuzzle())
                .map(puzzle -> findByMudCharacterAndPuzzle(mudCharacter, puzzle)
                        .map(puzzleInstance -> riddlePuzzleInstanceService.tryAnswer(words,
                                (RiddlePuzzleInstance) puzzleInstance))
                        .orElseGet(() -> riddlePuzzleInstanceService.tryAnswer(words, createRiddlePuzzleInstance(mudCharacter,
                                (RiddlePuzzle) puzzle))))
                .orElse(NO_PUZZLE);
    }

    /**
     * Handles the 'weigh' command and returns a string which contains the message which the bot has to send back.
     *
     * @param mudCharacter MUDCharacter needed for handling the 'weigh' command
     * @param balancePuzzleInstanceService BalancePuzzleInstanceService needed for handling the 'weigh' command
     * @return A string which contains the message which the bot has to send back.
     */
    public String handleWeighCommand(MUDCharacter mudCharacter,
                                     BalancePuzzleInstanceService balancePuzzleInstanceService) {
        return Optional.ofNullable(mudCharacter.getLocation().getPuzzle())
                .map(puzzle -> findByMudCharacterAndPuzzle(mudCharacter, puzzle)
                        .map(puzzleInstance -> balancePuzzleInstanceService
                                .attemptWeighing((BalancePuzzleInstance) puzzleInstance))
                        .orElseGet(() -> "You have not moved any weights yet! \n\n" +
                                createBalancePuzzleInstance(mudCharacter,
                                (BalancePuzzle) puzzle).getStatus()))
                .orElse(NO_PUZZLE);
    }

    /**
     * Handles the 'placeleft' command and returns a string which contains the message which the bot has to send back.
     *
     * @param mudCharacter MUDCharacter needed for handling the 'placeleft' command
     * @param words The rest of the notification's content (without the command)
     * @param scaleService ScaleServic needed for handling the 'placeleft' command
     * @return A string which contains the message which the bot has to send back.
     */
    public String handlePlaceLeftCommand(MUDCharacter mudCharacter,
                                         List<String> words,
                                         ScaleService scaleService) {
        return Optional.ofNullable(mudCharacter.getLocation().getPuzzle())
                .map(puzzle -> findByMudCharacterAndPuzzle(mudCharacter, puzzle)
                        .map(puzzleInstance -> scaleService
                                .attemptPlaceLeft((BalancePuzzleInstance) puzzleInstance,
                                        words))
                        .orElseGet(() -> scaleService
                                .attemptPlaceLeft(createBalancePuzzleInstance(mudCharacter,
                                (BalancePuzzle) puzzle), words)))
                .orElse(NO_PUZZLE);
    }

    /**
     * Handles the 'placeright' command and returns a string which contains the message which the bot has to send back.
     *
     * @param mudCharacter MUDCharacter needed for handling the 'placeright' command
     * @param words The rest of the notification's content (without the command)
     * @param scaleService ScaleService needed for handling the 'placeright' command
     * @return A string which contains the message which the bot has to send back.
     */
    public String handlePlaceRightCommand(MUDCharacter mudCharacter,
                                          List<String> words,
                                          ScaleService scaleService) {
        return Optional.ofNullable(mudCharacter.getLocation().getPuzzle())
                .map(puzzle -> findByMudCharacterAndPuzzle(mudCharacter, puzzle)
                        .map(puzzleInstance -> scaleService
                                .attemptPlaceRight((BalancePuzzleInstance) puzzleInstance,
                                        words))
                        .orElseGet(() -> scaleService
                                .attemptPlaceRight(createBalancePuzzleInstance(mudCharacter,
                                        (BalancePuzzle) puzzle), words)))
                .orElse(NO_PUZZLE);
    }

    /**
     * Handles the 'placedown' command and returns a string which contains the message which the bot has to send back.
     *
     * @param mudCharacter MUDCharacter needed for handling the 'placedown' command
     * @param words The rest of the notification's content (without the command)
     * @param scaleService ScaleService needed for handling the 'placedown' command
     * @return A string which contains the message which the bot has to send back.
     */
    public String handlePlaceDownCommand(MUDCharacter mudCharacter,
                                         List<String> words,
                                         ScaleService scaleService) {
        return Optional.ofNullable(mudCharacter.getLocation().getPuzzle())
                .map(puzzle -> findByMudCharacterAndPuzzle(mudCharacter, puzzle)
                        .map(puzzleInstance -> scaleService
                                .attemptPlaceDown((BalancePuzzleInstance) puzzleInstance,
                                        words))
                        .orElseGet(() -> scaleService
                                .attemptPlaceDown(createBalancePuzzleInstance(mudCharacter,
                                        (BalancePuzzle) puzzle), words)))
                .orElse(NO_PUZZLE);
    }

    /**
     * Handles the 'pick' command and returns a string which contains the message which the bot has to send back.
     *
     * @param mudCharacter MUDCharacter needed for handling the 'pick' command
     * @param words The rest of the notification's content (without the command)
     * @param balancePuzzleInstanceService BalancePuzzleInstanceService needed for handling the 'pick' command
     * @return A string which contains the message which the bot has to send back.
     */
    public String handlePickCommand(MUDCharacter mudCharacter,
                                    List<String> words,
                                    BalancePuzzleInstanceService balancePuzzleInstanceService) {
        return Optional.ofNullable(mudCharacter.getLocation().getPuzzle())
                .map(puzzle -> findByMudCharacterAndPuzzle(mudCharacter, puzzle)
                        .map(puzzleInstance -> balancePuzzleInstanceService
                                .pick((BalancePuzzleInstance) puzzleInstance,
                                        words))
                        .orElseGet(() -> "You have not moved any weights yet! \n\n" +
                                createBalancePuzzleInstance(mudCharacter,
                                        (BalancePuzzle) puzzle).getStatus()))
                .orElse(NO_PUZZLE);
    }

    /**
     * Handles the 'pull' command and returns a string which contains the message which the bot has to send back.
     *
     * @param mudCharacter MUDCharacter needed for handling the 'pull' command
     * @param words The rest of the notification's content (without the command)
     * @param lightsPuzzleInstanceService LightsPuzzleInstanceService needed for handling the 'pull' command
     * @param binaryPuzzleInstanceService BinaryPuzzleInstanceService needed for handling the 'pull' command
     * @return A string which contains the message which the bot has to send back.
     */
    public String handlePullCommand(MUDCharacter mudCharacter,
                                    List<String> words,
                                    LightsPuzzleInstanceService lightsPuzzleInstanceService,
                                    BinaryPuzzleInstanceService binaryPuzzleInstanceService) {
        return Optional.ofNullable(mudCharacter.getLocation().getPuzzle())
                .map(puzzle -> findByMudCharacterAndPuzzle(mudCharacter, puzzle)
                        .map(puzzleInstance -> puzzleInstance instanceof LightsPuzzleInstance ?
                                lightsPuzzleInstanceService.pull((LightsPuzzleInstance) puzzleInstance, words) :
                                binaryPuzzleInstanceService.pull((BinaryPuzzleInstance) puzzleInstance, words))
                        .orElseGet(() -> puzzle instanceof LightsPuzzle ?
                                lightsPuzzleInstanceService.pull(
                                        createLightsPuzzleInstance(mudCharacter, (LightsPuzzle) puzzle), words) :
                                binaryPuzzleInstanceService.pull(
                                        createBinaryPuzzleInstance(mudCharacter, (RiddlePuzzle) puzzle), words)))
                .orElse(NO_PUZZLE);
    }

    /**
     * Creates a PuzzleInstance and saves it. Returns the status of the newly created
     * PuzzleInstance. Returns a friendl message if a type of puzzle is still in development.
     * Determines which PuzzleInstance to make by looking at the Puzzle's dtype.
     *
     * @param mudCharacter MUDCharacter needed to make a PuzzleInstance.
     * @param puzzle Puzzle needed to make a PuzzleInstance.
     * @return Newly created PuzzleInstance's Status. Friendly message if puzzle type is
     *         still in development.
     */
    private String createPuzzleInstance(MUDCharacter mudCharacter,
                                        Puzzle puzzle) {
        if (puzzle instanceof LightsPuzzle) {
            return createLightsPuzzleInstance(mudCharacter, (LightsPuzzle) puzzle).getStatus();
        } else if (puzzle instanceof  BalancePuzzle) {
            return createBalancePuzzleInstance(mudCharacter, (BalancePuzzle) puzzle).getStatus();
        }
        return createPuzzleInstance(mudCharacter, (RiddlePuzzle) puzzle).getStatus();
    }

    /**
     * Creates a PuzzleInstance from a RiddlePuzzle and saves it. Determines which
     * PuzzleInstance to make from the RiddlePuzzle's isBinaryPuzzle method.
     * Returns the status of the newly created PuzzleInstance.
     *
     * @param mudCharacter MUDCharacter needed to make a PuzzleInstance.
     * @param riddlePuzzle RiddlePuzzle needed to make a PuzzleInstance.
     * @return Newly created PuzzleInstance's Status.
     */
    private PuzzleInstance createPuzzleInstance(MUDCharacter mudCharacter,
                                                RiddlePuzzle riddlePuzzle) {
        if (riddlePuzzle.isBinaryPuzzle()) {
            return createBinaryPuzzleInstance(mudCharacter, riddlePuzzle);
        }

        return createRiddlePuzzleInstance(mudCharacter, riddlePuzzle);
    }

    /**
     * Creates a LighstPuzzleInstance and saves it. Returns the newly created LightsPuzzleInstance.
     *
     * @param mudCharacter MUDCharacter needed to make a LighstPuzzleInstance.
     * @param lightsPuzzle LightsPuzzle needed to make a LighstPuzzleInstance.
     * @return Newly created LighstPuzzleInstance.
     */
    private LightsPuzzleInstance createLightsPuzzleInstance(MUDCharacter mudCharacter,
                                                            LightsPuzzle lightsPuzzle) {
        LightsPuzzleInstance lightsPuzzleInstance = new LightsPuzzleInstance(
                mudCharacter,
                lightsPuzzle,
                Arrays.asList(false, false, false, false, false, false));
        save(lightsPuzzleInstance);
        return lightsPuzzleInstance;
    }

    /**
     * Creates a BalancePuzzleInstance and saves it. Returns a string mentioning that the MUDCharacter
     * didn't move any weights yet. Also includes the status of the newly created BalancePuzzleInstance.
     *
     * @param mudCharacter MUDCharacter needed to make a BalancePuzleInstance.
     * @param balancePuzzle BalancePuzzle needed to make a BalancePuzleInstance.
     * @return Newly created BalancePuzzleInstance's status including that no weights have been moved.
     */
    private BalancePuzzleInstance createBalancePuzzleInstance(MUDCharacter mudCharacter,
                                               BalancePuzzle balancePuzzle) {
        Set<Integer> notOnScale = IntStream
                .range(0, balancePuzzle.getAmountOfWeights())
                .boxed()
                .collect(Collectors.toSet());
        Scale scale = new Scale();
        scale.setNotOnScale(notOnScale);
        BalancePuzzleInstance balancePuzzleInstance = new BalancePuzzleInstance(mudCharacter,
                balancePuzzle,
                scale);
        save(balancePuzzleInstance);
        return balancePuzzleInstance;
    }

    /**
     * Creates a RiddlePuzzleInstance and saves it. Returns the status of the newly created RiddlePuzzleInstance.
     *
     * @param mudCharacter MUDCharacter needed to make a RiddlePuzzleInstance.
     * @param riddlePuzzle RiddlePuzzle needed to make a RiddlePuzzleInstance.
     * @return Newly created RiddlePuzzleInstance's Status.
     */
    private RiddlePuzzleInstance createRiddlePuzzleInstance(MUDCharacter mudCharacter,
                                              RiddlePuzzle riddlePuzzle) {
        RiddlePuzzleInstance riddlePuzzleInstance = new RiddlePuzzleInstance(
                mudCharacter,
                riddlePuzzle
        );
        save(riddlePuzzleInstance);
        return riddlePuzzleInstance;
    }

    /**
     * Creates a BinaryPuzzleInstance and saves it. Returns the status of the newly created BinaryPuzzleInstance.
     *
     * @param mudCharacter MUDCharacter needed to make a BinaryPuzzleInstance.
     * @param riddlePuzzle RiddlePuzzle needed to make a BinaryPuzzleInstance.
     * @return Newly created BinaryPuzzleInstance's Status.
     */
    private BinaryPuzzleInstance createBinaryPuzzleInstance(MUDCharacter mudCharacter,
                                              RiddlePuzzle riddlePuzzle) {
        BinaryPuzzleInstance binaryPuzzleInstance = new BinaryPuzzleInstance(
                mudCharacter,
                riddlePuzzle,
                Arrays.asList(false, false, false, false, false)
        );
        save(binaryPuzzleInstance);
        return binaryPuzzleInstance;
    }
}

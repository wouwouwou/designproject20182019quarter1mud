package nl.utwente.mastodon.mudtwente.services.puzzlesystem.puzzle_instances.balancepuzzle_instance;

import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.balancepuzzle_instance.BalancePuzzleInstance;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.balancepuzzle_instance.Scale;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.ScaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service class for a Scale.
 */
@Service
public class ScaleService {

    private ScaleRepository scaleRepository;

    private static final String WEIGHT_NOT_EXISTS = "This weight does not exist!\n\n";
    private static final String SUCCESFUL_PLACE = "Succesfully placed weight ";

    @Autowired
    public ScaleService(ScaleRepository scaleRepository) {
        this.scaleRepository = scaleRepository;
    }

    /**
     * Saves the Scale in the database.
     *
     * @param scale Scale to save.
     */
    public void save(Scale scale) {
        scaleRepository.save(scale);
    }

    /**
     * Attempts to place a weight to the left. Gets the weightId from words. Checks if
     * the weight is not already there and if it exists. Checks if exactly one weight
     * is given with the command. Checks if the given weightId is a number.
     *
     * @param balancePuzzleInstance BalancePuzzleInstance needed for the attempt.
     * @param words List of Strings, containing the weightId of the attempt.
     * @return A string with the result of the attempt.
     */
    public String attemptPlaceLeft(BalancePuzzleInstance balancePuzzleInstance,
                                   List<String> words) {
        String wrongUsage = "Please use 'placeleft' <weightNumber> in order to " +
                "put a weight on the left side of the scale!";
        if (words.size() != 1) {
            return wrongUsage;
        }

        int weightId;
        try {
            weightId = Integer.parseInt(words.get(0));
        } catch (NumberFormatException e) {
            return wrongUsage;
        }

        Scale scale = balancePuzzleInstance.getScale();

        if (scale.getLeftSide().contains(weightId)) {
            return "This weight is already on the left side!\n\n" + balancePuzzleInstance.getStatus();
        }

        if (!scale.getRightSide().contains(weightId) && !scale.getNotOnScale().contains(weightId)) {
            return WEIGHT_NOT_EXISTS + balancePuzzleInstance.getStatus();
        }

        scale.placeLeft(weightId);
        save(scale);
        return SUCCESFUL_PLACE + weightId + " on the left side of the scale!\n\n"
                + balancePuzzleInstance.getStatus();
    }

    /**
     * Attempts to place a weight to the right. Gets the weightId from words. Checks if
     * the weight is not already there and if it exists. Checks if exactly one weight
     * is given with the command. Checks if the given weightId is a number.
     *
     * @param balancePuzzleInstance BalancePuzzleInstance needed for the attempt.
     * @param words List of Strings, containing the weightId of the attempt.
     * @return A string with the result of the attempt.
     */
    public String attemptPlaceRight(BalancePuzzleInstance balancePuzzleInstance,
                                    List<String> words) {
        String wrongUsage = "Please use 'placeright' <weightNumber> in order to " +
                "put a weight on the right side of the scale!";
        if (words.size() != 1) {
            return wrongUsage;
        }

        int weightId;
        try {
            weightId = Integer.parseInt(words.get(0));
        } catch (NumberFormatException e) {
            return wrongUsage;
        }

        Scale scale = balancePuzzleInstance.getScale();

        if (scale.getRightSide().contains(weightId)) {
            return "This weight is already on the right side!\n\n" + balancePuzzleInstance.getStatus();
        }

        if (!scale.getLeftSide().contains(weightId) && !scale.getNotOnScale().contains(weightId)) {
            return WEIGHT_NOT_EXISTS + balancePuzzleInstance.getStatus();
        }

        scale.placeRight(weightId);
        save(scale);
        return SUCCESFUL_PLACE + weightId + " on the right side of the scale!\n\n"
                + balancePuzzleInstance.getStatus();
    }

    /**
     * Attempts to place a weight to down. Gets the weightId from words. Checks if
     * the weight is not already there and if it exists. Checks if exactly one weight
     * is given with the command. Checks if the given weightId is a number.
     *
     * @param balancePuzzleInstance BalancePuzzleInstance needed for the attempt.
     * @param words List of Strings, containing the weightId of the attempt.
     * @return A string with the result of the attempt.
     */
    public String attemptPlaceDown(BalancePuzzleInstance balancePuzzleInstance,
                                   List<String> words) {
        String wrongUsage = "Please use 'placedown' <weightNumber> in order to " +
                "put a weight down from the scale!";
        if (words.size() != 1) {
            return wrongUsage;
        }

        int weightId;
        try {
            weightId = Integer.parseInt(words.get(0));
        } catch (NumberFormatException e) {
            return wrongUsage;
        }

        Scale scale = balancePuzzleInstance.getScale();

        if (scale.getNotOnScale().contains(weightId)) {
            return "This weight is already down from the scale!\n\n" + balancePuzzleInstance.getStatus();
        }

        if (!scale.getRightSide().contains(weightId) && !scale.getLeftSide().contains(weightId)) {
            return WEIGHT_NOT_EXISTS + balancePuzzleInstance.getStatus();
        }

        scale.placeDown(weightId);
        save(scale);
        return SUCCESFUL_PLACE + weightId + " down from the scale!\n\n"
                + balancePuzzleInstance.getStatus();
    }
}

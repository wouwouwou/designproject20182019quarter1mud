package nl.utwente.mastodon.mudtwente.services.itemsystem;

import lombok.extern.slf4j.Slf4j;
import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.itemsystem.Inventory;
import nl.utwente.mastodon.mudtwente.entities.itemsystem.Item;
import nl.utwente.mastodon.mudtwente.repositories.itemsystem.InventoryRepository;
import nl.utwente.mastodon.mudtwente.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Service class for an Inventory.
 */
@Service
@Slf4j
public class InventoryService {

    private InventoryRepository inventoryRepository;

    @Autowired
    public InventoryService(InventoryRepository inventoryRepository) {
        this.inventoryRepository = inventoryRepository;
    }

    public void save(Inventory inventory) {
        inventoryRepository.save(inventory);
    }

    /**
     * Adds an rewardAmount of Items to an Inventory of a MUDCharacter by mudCharacterId.
     * @param mudCharacter The mudCharacter in question.
     * @param item The Item in question.
     * @param amount The rewardAmount with which the item should be added.
     */
    public Inventory addItemToInventory(MUDCharacter mudCharacter, Item item, int amount) {
        Optional<Inventory> inventoryOptional = inventoryRepository.findByMudCharacterAndItem(mudCharacter, item);
        Inventory inventory;
        int resultAmount = 0;

        if (inventoryOptional.isPresent()) {
            inventory = inventoryOptional.get();
            if (amount > 0 || inventory.getAmount() + amount > 0) {
                resultAmount = inventory.getAmount() + amount;
            }
        } else {
            inventory = new Inventory();
            inventory.setMudCharacter(mudCharacter);
            inventory.setItem(item);
            resultAmount = amount;
        }

        inventory.setAmount(resultAmount);
        inventoryRepository.save(inventory);
        return inventory;
    }

    /**
     * May find an Inventory by mudCharacterId and itemId. Returns an Optional<Inventory> which may contain a valid
     * Inventory whose mudCharacterId and itemId equals the parameters.
     * @param mudCharacter The mudCharacter to search for.
     * @param item The itemId to search for.
     * @return An Optional<Inventory> which may contain a valid Inventory whose mudCharacterId and itemId equals the
     * parameters.
     */
    public Optional<Inventory> findByMudCharacterAndItem(MUDCharacter mudCharacter, Item item) {
        return inventoryRepository.findByMudCharacterAndItem(mudCharacter, item);
    }

    /**
     * Finds Inventories by mudCharacterId. Returns a list of Inventories whose mudCharacterId equals the parameter.
     * @param mudCharacter The mudCharacter to search for.
     * @return A list of Inventories whose mudCharacterId equals the parameter.
     */
    public List<Inventory> findByMudCharacter(MUDCharacter mudCharacter) {
        return inventoryRepository.findByMudCharacter(mudCharacter);
    }

    public String handleInventoryCommand(MUDCharacter mudCharacter) {
        List<Inventory> inventories = findByMudCharacter(mudCharacter);
        StringBuilder result = new StringBuilder();
        for (Inventory inventory : inventories) {
            if (inventory.getAmount() > 0) {
                result.append("\n");
                result.append(inventory.getAmount());
                result.append(" ");
                result.append(StringUtils.setFirstLetterUpperCase(inventory.getItem().getName()));
            }
        }
        if (!result.toString().equals("")) {
            return result.toString();
        }
        return "You don't have any items in your inventory!";
    }
}

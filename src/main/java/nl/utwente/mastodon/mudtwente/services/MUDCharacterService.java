package nl.utwente.mastodon.mudtwente.services;

import lombok.extern.slf4j.Slf4j;
import nl.utwente.mastodon.mudtwente.entities.locationsystem.Location;
import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.itemsystem.Inventory;
import nl.utwente.mastodon.mudtwente.entities.itemsystem.Item;
import nl.utwente.mastodon.mudtwente.repositories.locationsystem.LocationRepository;
import nl.utwente.mastodon.mudtwente.repositories.MUDCharacterRepository;
import nl.utwente.mastodon.mudtwente.repositories.itemsystem.InventoryRepository;
import nl.utwente.mastodon.mudtwente.repositories.itemsystem.ItemRepository;
import nl.utwente.mastodon.mudtwente.services.itemsystem.InventoryService;
import nl.utwente.mastodon.mudtwente.services.locationsystem.LocationService;
import nl.utwente.mastodon.mudtwente.utils.Constants;
import nl.utwente.mastodon.mudtwente.utils.Message;
import nl.utwente.mastodon.mudtwente.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service class for a MUDCharacter.
 */
@Slf4j
@Service
public class MUDCharacterService {

    private MUDCharacterRepository mudCharacterRepository;

    @Autowired
    public MUDCharacterService(MUDCharacterRepository mudCharacterRepository) {
        this.mudCharacterRepository = mudCharacterRepository;
    }

    /**
     * May find a MUDCharacter by id. Returns an Optional<MUDCharacter> which may contain a valid MUDCharacter whose
     * id equals the parameter.
     *
     * @param id The id to search for.
     * @return An Optional<MUDCharacter> which may contain a valid MUDCharacter whose id equals the parameter.
     */
    public Optional<MUDCharacter> findById(int id) {
        return mudCharacterRepository.findById(id);
    }

    /**
     * Finds MUDCharacters by name. Returns a list of MUDCharacters whose name equals the name parameter.
     *
     * @param name The name to search for.
     * @return A list of MUDCharacters whose name equals the name parameter.
     */
    public List<MUDCharacter> findByName(String name) {
        return mudCharacterRepository.findByName(name);
    }

    /**
     * Saves the MUDCharacter in the database.
     *
     * @param mudCharacter the character to save.
     */
    public void save(MUDCharacter mudCharacter) {
        mudCharacterRepository.save(mudCharacter);
    }


    /**
     * Finds MUDCharacters by username and mastodonAccountId. Returns a Optional of MUDCharacter whose username and
     * mastodonAccountId status equals the parameters.
     *
     * @param username          The username to search for.
     * @param mastodonAccountId The mastodonAccountId to search for.
     * @return A list of MUDCharacters whose location and online status equals the parameters.
     */
    public Optional<MUDCharacter> findMUDCharacterByUsernameAndMastodonAccountId(String username,
                                                                                 int mastodonAccountId) {
        return mudCharacterRepository.findMUDCharacterByUsernameAndMastodonAccountId(username, mastodonAccountId);
    }

    /**
     * Handles the 'online' command and returns a string which contains the message which the bot has to send back.
     *
     * @return A string which contains the message which the bot has to send back.
     */
    public String handleOnlineCommand() {
        return "The users that are online: " + getAllOnlinePlayers()
                .stream()
                .map(MUDCharacter::getName)
                .collect(Collectors.joining(", "));
    }

    /**
     * Handles the 'help' command and returns a string which contains the message which the bot has to send back.
     *
     * @return A string which contains the message which the bot has to send back.
     */
    public String handleHelpCommand() {
        return "go north/west/south/east - Go the given direction\n" +
                "whichnpcs - Shows the NPCs\n" +
                "talk {npcname} - Talks to the NPC\n" +
                "location - Shows current and neighbour locations\n" +
                "locationinfo - Shows info about the current location\n" +
                "whattobuy - Shows what is for sale\n" +
                "buy {itemname} - Buys the item\n" +
                "whoishere - Shows which players are at the current location\n" +
                "online - Shows who is online\n" +
                "toallatlocation {message} - Sends a message to all players at this location.\n" +
                "sleep - Go sleep for a while to recover health. This might cost you money!\n" +
                "inventory - Shows what and how many items are in your inventory\n" +
                "usepotion {potionname} - Uses the potion to recover your health.\n" +
                "attack - Attacks the current enemy.\n" +
                "newsong - Gets you a new song if you do not have one already.\n" +
                "sing {answer} - Try an answer to your current song.\n" +
                "checksafe - Checks if there is a safe at this location.\n" +
                "checkpuzzle - Checks if there is a puzzle at this location.\n" +
                "puzzlestatus - Shows the status of the puzzle at this location.\n";
    }

    /**
     * Handles the 'register' command and returns a string which contains the message which the bot has to send back.
     *
     * @param words               The rest of the notification's content (without the command)
     * @param accountId           accountId needed for handling the 'register' command
     * @param username            username (including @) needed for handling the 'register' command
     * @param inventoryRepository InventoryRepository needed for handling the 'register' command
     * @return A string which contains the message which the bot has to send back.
     */
    public String handleRegisterCommand(List<String> words,
                                        long accountId,
                                        String username,
                                        InventoryRepository inventoryRepository) {
        InventoryService inventoryService = new InventoryService(inventoryRepository);

        //If message contains a username
        if (words.size() != 1) {
            return "You did not enter 1 character name. Please enter only one name!";
        }

        //Get username
        String charName = words.get(0);

        //If username already exists
        if (!mudCharacterRepository.findByName(charName).isEmpty()) {
            return "That username is not available anymore.";
        }

        if (mudCharacterRepository.findMUDCharacterByUsername(username).isPresent()) {
            return "You already got an account. Use the {help} command to get instructions.";
        }

        MUDCharacter mudCharacter = new MUDCharacter();
        mudCharacter.setName(charName);
        mudCharacter.setLocation(Constants.REGISTRATION_TENT);
        mudCharacter.setUsername(username);
        mudCharacter.setHealth(100);
        mudCharacter.setStoryLevel(1);
        mudCharacter.setCreatedAt(LocalDateTime.now());
        mudCharacter.setMastodonAccountId((int) accountId);
        mudCharacter.setLastMessageAt(LocalDateTime.now());
        mudCharacter.setAdmin(false);
        mudCharacter.setBanned(false);
        save(mudCharacter);
        inventoryService.addItemToInventory(mudCharacter, Constants.COIN, 0);

        return "You have been succesfully registered. It is now possible to play the game!\n" +
                "Use the {location} command to get your current location and use {go north/west/south/east} to " +
                "walk into a direction.\n" +
                "Your first quest is to talk to the NPC Info via using the {talk Info} command.\n" +
                "You start location is the Registration tent at the start of the Kick-In!";
    }

    /**
     * Handles the 'usepotion' command and returns a string which contains the message which the bot has to send back.
     *
     * @param words                  The rest of the notification's content (without the command)
     * @param itemRepository         ItemRepository needed for handling the 'usepotion' command
     * @param mudCharacter           MUDCharacter needed for handling the 'usepotion' command
     * @param inventoryRepository    InventoryRepository needed for handling the 'usepotion' command
     * @param mudCharacterRepository MUDCharacterRepository needed for handling the 'usepotion' command
     * @return A string which contains the message which the bot has to send back.
     */
    public String handleUsePotionCommand(List<String> words,
                                         ItemRepository itemRepository,
                                         MUDCharacter mudCharacter,
                                         InventoryRepository inventoryRepository,
                                         MUDCharacterRepository mudCharacterRepository) {
        InventoryService inventoryService = new InventoryService(inventoryRepository);

        if (words.isEmpty()) {
            return "Which potion do you want to use?";
        }

        Optional<Item> optionalItem = itemRepository.findByName(words.get(0));

        if (!optionalItem.isPresent()) {
            return "That item is unknown!";
        }

        Optional<Inventory> optionalInventory = inventoryRepository.findByMudCharacterAndItem(mudCharacter,
                optionalItem.get());

        if (!optionalInventory.isPresent()) {
            return "You do not own that item!";
        }

        Inventory inventory = optionalInventory.get();

        int potionHp = inventory.getItem().getAddHp();
        mudCharacter.setHealth(mudCharacter.getHealth() + potionHp);
        if (mudCharacter.getHealth() > 100) {
            mudCharacter.setHealth(100);
        }
        mudCharacterRepository.save(mudCharacter);

        inventoryService.addItemToInventory(mudCharacter, inventory.getItem(), -1);
        return "You have successfully used the potion!";
    }

    /**
     * Handles the 'sleep' command and returns a string which contains the message which the bot has to send back.
     *
     * @param mudCharacter        MUDCharacter needed for handling the 'sleep' command
     * @param inventoryRepository InventoryRepository needed for handling the 'sleep' command
     * @return A string which contains the message which the bot has to send back.
     */
    public String handleSleepCommand(MUDCharacter mudCharacter,
                                     InventoryRepository inventoryRepository) {
        InventoryService inventoryService = new InventoryService(inventoryRepository);

        if (mudCharacter.getLocation().equals(Constants.STUDENT_HOUSING)) {
            mudCharacter.setHealth(100);
            save(mudCharacter);
            return "You slept for free and your health is now maximal (100).";
        }

        Optional<Inventory> inventoryOptional = inventoryRepository
                .findByMudCharacterAndItem(mudCharacter, Constants.COIN);
        if (inventoryOptional.isPresent() && inventoryOptional.get().getAmount() >= Constants.SLEEPCOSTS) {
            inventoryService.addItemToInventory(mudCharacter, Constants.COIN, -Constants.SLEEPCOSTS);
        }
        return "You slept for " + Constants.SLEEPCOSTS + " coins and your health is now maximal (100).";
    }

    /**
     * Handles the 'toallatlocation' command and returns a string which contains the message
     * which the bot has to send back.
     *
     * @param words        The rest of the notification's content (without the command)
     * @param username     username (including @) needed for handling the 'register' command
     * @param userId       userId needed for the inReplyTo field of a message wich the bot has to send back.
     * @param mudCharacter MUDCharacter needed for handling the 'sleep' command
     * @return A string which contains the message which the bot has to send back.
     */
    public List<Message> handleToAllAtLocationCommand(List<String> words,
                                                      String username,
                                                      Long userId,
                                                      MUDCharacter mudCharacter) {
        List<Message> resultList = new ArrayList<>();
        if (words.isEmpty()) {
            String messageContent = "You used the command wrong! The command is: toallatlocation {message}";
            resultList.add(new Message(username, messageContent, userId));
            return resultList;
        }

        String formatString = "You received a message from: %s.%nThe character name is: %s.%nThe message is: %s";
        for (MUDCharacter user : getAllOnlinePlayersInLocation(mudCharacter.getLocation())) {
            String messageContent = String.format(
                    formatString,
                    username,
                    StringUtils.setFirstLetterUpperCase(mudCharacter.getName()),
                    String.join(" ", words));
            resultList.add(new Message(user.getUsername(), messageContent));
        }
        return resultList;
    }

    /**
     * Lets the MUDCharacter walk in a direction.
     *
     * @param mudCharacter       MUDCharacter which has to walk.
     * @param direction          The direction in which the MUDCharacter should walk.
     * @param locationRepository A LocationRepository to actually walk.
     * @return The updated MUDCharacter after a possible walk.
     */
    public MUDCharacter walkTo(MUDCharacter mudCharacter,
                               String direction,
                               LocationRepository locationRepository) {
        //TODO refactor this
        LocationService locationService = new LocationService(locationRepository);
        Location location = mudCharacter.getLocation();
        Optional<Location> north = locationService.findById(location.getNorth());
        Optional<Location> east = locationService.findById(location.getEast());
        Optional<Location> south = locationService.findById(location.getSouth());
        Optional<Location> west = locationService.findById(location.getWest());
        if (direction.equals("north") && north.isPresent()) {
            mudCharacter.setLocation(north.get());
            save(mudCharacter);
            return mudCharacter;
        } else if (direction.equals("east") && east.isPresent()) {
            mudCharacter.setLocation(east.get());
            save(mudCharacter);
            return mudCharacter;
        } else if (direction.equals("south") && south.isPresent()) {
            mudCharacter.setLocation(south.get());
            save(mudCharacter);
            return mudCharacter;
        } else if (direction.equals("west") && west.isPresent()) {
            mudCharacter.setLocation(west.get());
            save(mudCharacter);
            return mudCharacter;
        }
        log.info("ERROR: Cannot go in that direction!");
        return mudCharacter;
    }

    /**
     * Retrieves a List of all Online MUDCharacters.
     *
     * @return A List<MUDCharacter> which contains all online MUDCharacters.
     */
    public List<MUDCharacter> getAllOnlinePlayers() {
        List<MUDCharacter> result = new ArrayList<>();
        for (MUDCharacter mudCharacter : mudCharacterRepository.findAll()) {
            if (mudCharacter.getLastMessageAt().isAfter(LocalDateTime.now().minusMinutes(15))) {
                result.add(mudCharacter);
            }
        }
        return result;
    }

    /**
     * Retrieves a List of all Online MUDCharacters at a given Location.
     *
     * @param location Location at which the MUDCharacter should be.
     * @return A List<MUDCharacter> which contains all online MUDCharacters at the given Location.
     */
    public List<MUDCharacter> getAllOnlinePlayersInLocation(Location location) {
        List<MUDCharacter> result = new ArrayList<>();
        for (MUDCharacter mudCharacter : getAllOnlinePlayers()) {
            if (mudCharacter.getLocation().equals(location)) {
                result.add(mudCharacter);
            }
        }
        return result;
    }

    /**
     * Handles the 'ban' command and returns a string which contains the message which the bot has to send back.
     *
     * @param words The rest of the notification's content (without the command)
     * @return A string which contains the message which the bot has to send back.
     */
    public String handleBanCommand(List<String> words, MUDCharacter adminCharacter) {
        return banAndUnbanPlayer(words, true, adminCharacter);
    }

    /**
     * Handles the 'unban' command and returns a string which contains the message which the bot has to send back.
     *
     * @param words The rest of the notification's content (without the command)
     * @return A string which contains the message which the bot has to send back.
     */
    public String handleUnbanCommand(List<String> words, MUDCharacter adminCharacter) {
        return banAndUnbanPlayer(words, false, adminCharacter);
    }

    /**
     * Handles both the 'ban' and 'unban' command and returns a string which contains the message which the
     * bot has to send back.
     *
     * @param words The rest of the notification's content (without the command)
     * @param banPlayer Boolean stating if the player has to be banned or unbanned.
     * @return A string which contains the message which the bot has to send back.
     */
    public String banAndUnbanPlayer(List<String> words, boolean banPlayer, MUDCharacter adminCharacter) {
        if (!adminCharacter.isAdmin()) {
            return "You are not allowed to use this command";
        }
        if (words.size() != 1) {
            return "You have incorrectly used the ban or unban <playername> command.\n" +
                    "Please give only one player name!";
        }

        List<MUDCharacter> mudCharactersList = findByName(words.get(0));

        if (mudCharactersList.size() != 1) {
            return "There is no character found by that name OR there are multiple characters with the same name.";
        }

        MUDCharacter mudCharacter = mudCharactersList.get(0);
        if (banPlayer && mudCharacter.isBanned()) {
            return "The player is already banned!";
        } else if (banPlayer && !mudCharacter.isBanned()) {
            mudCharacter.setBanned(true);
            save(mudCharacter);
            return "You have succesfully banned: " + words.get(0);
        } else if (!banPlayer && mudCharacter.isBanned()){
            mudCharacter.setBanned(false);
            save(mudCharacter);
            return "You have succesfully unbanned: " + words.get(0);
        }
        return "The player is already unbanned!";
    }

    /**
     * Admins can use the warp command to instantly warp a character to any location on the map.
     * @param words Should contain 2 strings: The MUDCharacter to warp and the location ID to warp to.
     * @param locationService Is needed for finding the correct location.
     * @param adminCharacter The mudCharacter that tried to use the command; if it's not an admin, the command is not executed.
     * @return A String the bot should send back to the adminCharacter.
     */
    public String handleWarpCommand(List<String> words, LocationService locationService, MUDCharacter adminCharacter) {
        if (!adminCharacter.isAdmin()) {
            return "You are not allowed to use this command";
        }
        if (words.size() == 2) {
            String characterName = words.get(0);
            List<MUDCharacter> mudCharacterList = findByName(characterName);
            if (mudCharacterList.size() == 1) {
                MUDCharacter characterToWarp = mudCharacterList.get(0);
                try {
                    int locationId = Integer.parseInt(words.get(1));
                    Optional<Location> location = locationService.findById(locationId);
                    if (location.isPresent()) {
                        characterToWarp.setLocation(location.get());
                        save(characterToWarp);
                        return "You warped character: " + characterName +
                                " successfully to location: " + location.get().getName();
                    } else {
                        return "Location is unknown";
                    }
                } catch (NumberFormatException e) {
                    return "Can only use the warp command with a number!";
                }
            } else {
                return "There is no or multiple characters with that name.";
            }
        } else {
            return "You entered wrong parameters. To use the command use warp {charactername} {locationId}.";
        }
    }
}

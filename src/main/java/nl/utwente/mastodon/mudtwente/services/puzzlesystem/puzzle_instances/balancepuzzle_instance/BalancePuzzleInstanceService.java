package nl.utwente.mastodon.mudtwente.services.puzzlesystem.puzzle_instances.balancepuzzle_instance;

import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.balancepuzzle_instance.BalancePuzzleInstance;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzles.BalancePuzzle;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.puzzle_instances.balancepuzzle_instance.BalancePuzzleInstanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Service class for a BalancePuzzleInstance.
 */
@Service
public class BalancePuzzleInstanceService {

    private BalancePuzzleInstanceRepository balancePuzzleInstanceRepository;

    @Autowired
    public BalancePuzzleInstanceService(BalancePuzzleInstanceRepository balancePuzzleInstanceRepository) {
        this.balancePuzzleInstanceRepository = balancePuzzleInstanceRepository;
    }

    /**
     * Saves the BalancePuzzleInstance in the database.
     *
     * @param balancePuzzleInstance BalancePuzzleInstance to save.
     */
    public void save(BalancePuzzleInstance balancePuzzleInstance) {
        balancePuzzleInstanceRepository.save(balancePuzzleInstance);
    }

    /**
     * Weighs the current scale and gives the result. Also increases weighing amount by one.
     * @param balancePuzzleInstance BalancePuzzleInstance needed to attempt a weighing
     * @return A string with information about the weighing attempt.
     */
    public String attemptWeighing(BalancePuzzleInstance balancePuzzleInstance) {
        BalancePuzzle balancePuzzle = balancePuzzleInstance.getBalancePuzzle();
        String maxWeighingAttemps = "You have reached the maximum amount of weighing attempts! " +
                "Please pick a weight of the scale!";
        if (balancePuzzleInstance.getPastWeighings() >= balancePuzzle.getMaxWeighings()) {
            return maxWeighingAttemps;
        }

        balancePuzzleInstance.increasePastWeighings();
        save(balancePuzzleInstance);

        String weighing = balancePuzzleInstance
                .getScale()
                .weigh(balancePuzzle.getHeaviestWeightId());
        String weighingAttempts = balancePuzzleInstance.getPastWeighings() >= balancePuzzle.getMaxWeighings() ?
                "\n\n" + maxWeighingAttemps :
                "\n\nYou have now weighted this puzzle " +
                balancePuzzleInstance.getPastWeighings() +
                " out of " + balancePuzzle.getMaxWeighings() + " times.";
        return weighing + weighingAttempts;
    }

    /**
     * Attempts to pick a weight. Gets the weightId from words. Checks if the given weightId is a number.
     * Checks if exactly one weight is given with the command. Checks if the MUDCharacter used the weigh command
     * for the maximum amount of times. Resets the puzzle if it is the wrong weight. Returns the
     * hintAfterCompletion if it is the right weight.
     * @param balancePuzzleInstance BalancePuzzleInstance needed for the attempt.
     * @param words List of Strings, containing the weightId of the attempt.
     * @return A string with the result of the attempt.
     */
    public String pick(BalancePuzzleInstance balancePuzzleInstance,
                       List<String> words) {
        String wrongUsage = "Please use 'pick' <weightNumber> in order to " +
                "pick out a weight from the scale you think is heaviest!";
        if (words.size() != 1) {
            return wrongUsage;
        }

        int pastWeighings = balancePuzzleInstance.getPastWeighings();
        int maxWeighings = balancePuzzleInstance.getBalancePuzzle().getMaxWeighings();
        if (pastWeighings != maxWeighings) {
            return "You must first weigh a total of " + maxWeighings + " times in order to pick out a weight from " +
                    "the scale you think is heaviest.\n\n" + balancePuzzleInstance.getStatus();
        }

        int heaviestWeightId = balancePuzzleInstance.getBalancePuzzle().getHeaviestWeightId();
        int weightId;
        try {
            weightId = Integer.parseInt(words.get(0));
        } catch (NumberFormatException e) {
            return wrongUsage;
        }

        if (heaviestWeightId != weightId) {
            balancePuzzleInstance.getScale().reset();
            balancePuzzleInstance.setPastWeighings(0);
            save(balancePuzzleInstance);
            return "You have picked the wrong weight! The puzzle has been reset!\n\n" +
                    balancePuzzleInstance.getStatus();
        }

        balancePuzzleInstance.setDeletedAt(LocalDateTime.now());
        save(balancePuzzleInstance);
        return "You have picked the correct weight! It says: \"" +
                balancePuzzleInstance.getBalancePuzzle().getHintAfterCompletion() + "\"";
    }
}

package nl.utwente.mastodon.mudtwente.services.npcsystem;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.Quest;
import nl.utwente.mastodon.mudtwente.entities.itemsystem.Item;
import nl.utwente.mastodon.mudtwente.entities.locationsystem.Location;
import nl.utwente.mastodon.mudtwente.entities.npcsystem.NPC;
import nl.utwente.mastodon.mudtwente.entities.npcsystem.NPCText;
import nl.utwente.mastodon.mudtwente.repositories.MUDCharacterRepository;
import nl.utwente.mastodon.mudtwente.repositories.QuestRepository;
import nl.utwente.mastodon.mudtwente.repositories.RepositoryWrapper;
import nl.utwente.mastodon.mudtwente.repositories.itemsystem.InventoryRepository;
import nl.utwente.mastodon.mudtwente.repositories.npcsystem.NPCRepository;
import nl.utwente.mastodon.mudtwente.repositories.npcsystem.NPCTextRepository;
import nl.utwente.mastodon.mudtwente.services.QuestService;
import nl.utwente.mastodon.mudtwente.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static nl.utwente.mastodon.mudtwente.utils.Constants.*;

/**
 * Service class for an NPC.
 */
@Service
public class NPCService {

    private NPCRepository npcRepository;

    @Autowired
    public NPCService(NPCRepository npcRepository) {
        this.npcRepository = npcRepository;
    }

    /**
     * May find a NPC by name. Returns an Optional<NPC> which may contain a valid NPC whose
     * name equals the parameter.
     * @param name The name to search for.
     * @return An Optional<NPC> which may contain a valid NPC whose name equals the parameter.
     */
    public Optional<NPC> findByName(String name) {
        return npcRepository.findByName(name);
    }

    /**
     * Finds NPCs by online status. Returns a list of NPCs whose int location equals the parameter.
     * @param location The int location to search for.
     * @return A list of NPCs whose int location equals the parameter.
     */
    public List<NPC> findByLocation(Location location) {
        return npcRepository.findByLocation(location);
    }

    /**
     * May find a NPC by id. Returns an Optional<NPC> which may contain a valid NPC whose
     * id equals the parameter.
     * @param id The id to search for.
     * @return An Optional<NPC> which may contain a valid NPC whose id equals the parameter.
     */
    public Optional<NPC> findById(int id) {
        return npcRepository.findById(id);
    }


    public String handleTalkCommand(List<String> words,
                                     MUDCharacter mudCharacter,
                                     RepositoryWrapper repositoryWrapper,
                                     MUDCharacterRepository mudCharacterRepository) {

        npcRepository = repositoryWrapper.getNpcSystemRepositoryWrapper().getNpcRepository();
        NPCTextRepository npcTextRepository = repositoryWrapper.getNpcSystemRepositoryWrapper().getNpcTextRepository();
        QuestRepository questRepository = repositoryWrapper.getQuestRepository();
        InventoryRepository inventoryRepository = repositoryWrapper.getItemSystemRepositoryWrapper().getInventoryRepository();
        QuestService questService = new QuestService(questRepository);

        if (words.isEmpty()) {
            return "To who do you want to talk?";
        }

        Optional<NPC> npcOptional = npcRepository.findByName(String.join(" ", words));

        if (!npcOptional.isPresent()) {
            return "Unknown NPC!";
        }

        NPC npc = npcOptional.get();

        if (!npc.isStoryLine() ||
                mudCharacter.getStoryLevel() < npc.getMinimalStoryLevel() ||
                !npc.getLocation().equals(mudCharacter.getLocation())) {
            return "It is currently not possible to talk with this NPC!";
        }

        List<NPCText> npcText = npcTextRepository.findByNpc(npc);
        Optional<Quest> questOptional = questRepository.findByMudCharacterAndNpc(mudCharacter, npc);
        Quest quest;

        // Character has not started the quest yet
        if (!questOptional.isPresent()) {
            quest = new Quest();
            quest.setMudCharacter(mudCharacter);
            quest.setNpc(npc);
            quest.setLevel(1);
            questRepository.save(quest);
        } else {
            quest = questOptional.get();
        }

        int questLevel = quest.getLevel();
        String npcTextResult = "";
        Item requiredItem = null;
        int requiredAmount = 0;
        Item rewardItem = null;
        int rewardAmount = 0;
        boolean increaseStoryLevel = false;
        int minimalStoryLevel = 0;
        for (NPCText NPCText : npcText) {
            if (NPCText.getLevel() == questLevel) {
                npcTextResult = NPCText.getText();
                requiredItem = NPCText.getRequiredItem();
                requiredAmount = NPCText.getRequiredAmount();
                rewardItem = NPCText.getRewardItem();
                rewardAmount = NPCText.getRewardAmount();
                increaseStoryLevel = NPCText.isIncreaseStoryLevel();
                minimalStoryLevel = NPCText.getMinimalStoryLevel();
                break;
            }
        }

        // There is no quest with the correct level
        if (npcTextResult.equals("") || minimalStoryLevel != mudCharacter.getStoryLevel()) {
            return "You do not got access yet to this npc/quest!";
        }

        String result = questService.handleRequiredItem(requiredItem, inventoryRepository, mudCharacter, requiredAmount);

        if (!result.isEmpty()) {
            return result;
        }

        questService.handleRewardItem(inventoryRepository, mudCharacter, rewardItem, rewardAmount, increaseStoryLevel, mudCharacterRepository);

        quest.setLevel(quest.getLevel() + 1);
        questRepository.save(quest);
        return " " + npcTextResult;
    }

    public String handleWhichNPCsCommand(NPCRepository npcRepository,
                                          MUDCharacter mudCharacter) {
        String npcString = npcRepository.findByLocation(mudCharacter.getLocation())
                .stream()
                .map(NPC::getName)
                .collect(Collectors.joining(", "));
        if (!npcString.isEmpty()) {
            return "These are the NPCs in this location: " + StringUtils.setFirstLetterUpperCase(npcString);
        }
        return "There are no NPCs in this location!";
    }

    /**
     * Saves all NPC Constants in the database.
     */
    public void addToDatabase() {
        testIfCorrectInDatabase(BIOLOGY_STUDENT.getId(), BIOLOGY_STUDENT);
        testIfCorrectInDatabase(EXAMINATION_BOARD.getId(), EXAMINATION_BOARD);
        testIfCorrectInDatabase(UNCLE_DUO.getId(), UNCLE_DUO);
        testIfCorrectInDatabase(NANOLAB_STUDENT.getId(), NANOLAB_STUDENT);
        testIfCorrectInDatabase(CREATIVE_TECHNOLOGY_STUDENT.getId(), CREATIVE_TECHNOLOGY_STUDENT);
        testIfCorrectInDatabase(RUGBY_PLAYER.getId(), RUGBY_PLAYER);
        testIfCorrectInDatabase(DREAM.getId(), DREAM);
        //Tutorial NPCs
        testIfCorrectInDatabase(INFO_NPC.getId(), INFO_NPC);
        testIfCorrectInDatabase(COMMANDS_NPC.getId(), COMMANDS_NPC);
        testIfCorrectInDatabase(DRUNK_MATE.getId(), DRUNK_MATE);
        testIfCorrectInDatabase(DOCTOR_NPC.getId(), DOCTOR_NPC);
        testIfCorrectInDatabase(CULTURE_NPC.getId(), CULTURE_NPC);
        testIfCorrectInDatabase(BARTENDER.getId(), BARTENDER);
        testIfCorrectInDatabase(BARTENDERS_BOSS.getId(), BARTENDERS_BOSS);
        testIfCorrectInDatabase(EXIT_NPC.getId(), EXIT_NPC);

        testIfCorrectInDatabase(HOUSE_MATE_NPC.getId(), HOUSE_MATE_NPC);
        testIfCorrectInDatabase(ITALIAN_MAFIA_MEMBER.getId(), ITALIAN_MAFIA_MEMBER);
        testIfCorrectInDatabase(SMART_STUDENT.getId(), SMART_STUDENT);
        testIfCorrectInDatabase(BLAKE.getId(), BLAKE);
        testIfCorrectInDatabase(ICTS_NPC.getId(), ICTS_NPC);
        testIfCorrectInDatabase(JAIR.getId(), JAIR);
        testIfCorrectInDatabase(ACCOUNTANT.getId(), ACCOUNTANT);
        testIfCorrectInDatabase(NOBODY.getId(), NOBODY);
        testIfCorrectInDatabase(DJMADPROFESSOR.getId(), DJMADPROFESSOR);
    }

    /**
     * Test if the constant is set correctly in the database. If not save it again.
     *
     * @param npcId to search for
     * @param constantNpc to search for
     */
    public void testIfCorrectInDatabase(int npcId, NPC constantNpc) {
        Optional<NPC> npcOptional = findById(npcId);
        if (!(npcOptional.isPresent() && npcOptional.get().equals(constantNpc))) {
            npcRepository.save(constantNpc);
        }
    }
}

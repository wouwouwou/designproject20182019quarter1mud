package nl.utwente.mastodon.mudtwente.services.puzzlesystem.puzzle_instances;

import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.LightsPuzzleInstance;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.puzzle_instances.LightsPuzzleInstanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Service class for a LightsPuzzleInstance.
 */
@Service
public class LightsPuzzleInstanceService {

    private LightsPuzzleInstanceRepository lightsPuzzleInstanceRepository;

    @Autowired
    public LightsPuzzleInstanceService(LightsPuzzleInstanceRepository lightsPuzzleInstanceRepository) {
        this.lightsPuzzleInstanceRepository = lightsPuzzleInstanceRepository;
    }

    /**
     * Saves the LightsPuzzleInstance in the database.
     *
     * @param lightsPuzzleInstance LightsPuzzleInstance to save.
     */
    public void save(LightsPuzzleInstance lightsPuzzleInstance) {
        lightsPuzzleInstanceRepository.save(lightsPuzzleInstance);
    }

    /**
     * Attempts to pull a lever. Gets the leverId / lightId from words. Checks the correct usage
     * of the 'pull' command. Checks if lever exists. After checks, attempts to pull the lever.
     * After pulling, checks if all lights are on.
     *
     * @param lightsPuzzleInstance LightsPuzzleInstance needed for the attempt.
     * @param words List of Strings, containing the leverId of the attempt.
     * @return A string with the result of the attempt.
     */
    public String pull(LightsPuzzleInstance lightsPuzzleInstance,
                       List<String> words) {
        String wrongUsage = "Please use 'pull' <leverNumber> in order to pull a lever!";
        if (words.size() != 1) {
            return wrongUsage;
        }

        int leverId;
        try {
            leverId = Integer.parseInt(words.get(0)) - 1;
        } catch (NumberFormatException e) {
            return wrongUsage;
        }

        if (leverId >= lightsPuzzleInstance.getLights().size() || leverId < 0) {
            int lightAmount = lightsPuzzleInstance.getLights().size();
            return "There are " + lightAmount +
                    " levers labeled 1 to " + lightAmount +
                    ". Please pick an existing lever to pull!";
        }

        List<Boolean> switchPattern = lightsPuzzleInstance
                .getLightsPuzzle()
                .getLightswitchPatterns()
                .get(leverId)
                .getSwitchList();
        lightsPuzzleInstance.switchLights(switchPattern);

        if (!lightsPuzzleInstance.allLightsOn()) {
            save(lightsPuzzleInstance);
            return "You have pulled lever " + (leverId + 1) + ".\n\n" +
                    lightsPuzzleInstance.getStatus();
        }

        lightsPuzzleInstance.setDeletedAt(LocalDateTime.now());
        save(lightsPuzzleInstance);
        return "The lights are all on! Good job! A message magically appears: \"" +
                lightsPuzzleInstance.getLightsPuzzle().getHintAfterCompletion() + "\"";
    }
}

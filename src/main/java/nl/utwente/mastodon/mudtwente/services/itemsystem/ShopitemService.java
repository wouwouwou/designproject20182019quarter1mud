package nl.utwente.mastodon.mudtwente.services.itemsystem;

import nl.utwente.mastodon.mudtwente.entities.itemsystem.ShopItem;
import nl.utwente.mastodon.mudtwente.entities.locationsystem.Shop;
import nl.utwente.mastodon.mudtwente.repositories.itemsystem.ShopitemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static nl.utwente.mastodon.mudtwente.utils.Constants.*;

/**
 * Service class for a ShopItem.
 */
@Service
public class ShopitemService {

    private ShopitemRepository shopitemRepository;

    @Autowired
    public ShopitemService(ShopitemRepository shopitemRepository) {
        this.shopitemRepository = shopitemRepository;
    }

    /**
     * Finds Shopitems by shopId. Returns a list of Shopitems whose shopId equals the parameter.
     * @param shop The shop to search for.
     * @return A list of Shopitems whose shopId equals the parameter.
     */
    public List<ShopItem> findByShop(Shop shop) {
        return shopitemRepository.findByShop(shop);
    }

    /**
     * Finds ShopItem by id. Returns an Optional of ShopItem whose id equals the parameter.
     * @param id The id to search for.
     * @return An Optional of ShopItem whose id equals the parameter.
     */
    public Optional<ShopItem> findById(int id) {
        return shopitemRepository.findById(id);
    }

    /**
     * Saves all ShopItem Constants in the database.
     */
    public void addToDatabase() {
        testIfCorrectInDatabase(SHOP_ITEM_PIE_COOP.getId(), SHOP_ITEM_PIE_COOP);
        testIfCorrectInDatabase(SHOP_ITEM_POTION_COOP.getId(), SHOP_ITEM_POTION_COOP);
        testIfCorrectInDatabase(SHOP_ITEM_POTION_VESTINGBAR.getId(), SHOP_ITEM_POTION_VESTINGBAR);
        testIfCorrectInDatabase(SHOP_ITEM_POTION_VRIJHOF.getId(), SHOP_ITEM_POTION_VRIJHOF);
        testIfCorrectInDatabase(SHOP_ITEM_POTION_SPIEGEL.getId(), SHOP_ITEM_POTION_SPIEGEL);
        testIfCorrectInDatabase(SHOP_ITEM_POTION_ZUL.getId(), SHOP_ITEM_POTION_ZUL);
        testIfCorrectInDatabase(SHOP_ITEM_POTION_WAAIER.getId(), SHOP_ITEM_POTION_WAAIER);
        testIfCorrectInDatabase(SHOP_ITEM_SUPER_PUTION_COOP.getId(), SHOP_ITEM_SUPER_PUTION_COOP);
        testIfCorrectInDatabase(SHOP_ITEM_SUPER_POTION_VESTINGBAR.getId(), SHOP_ITEM_SUPER_POTION_VESTINGBAR);
        testIfCorrectInDatabase(SHOP_ITEM_SUPER_POTION_VRIJHOF.getId(), SHOP_ITEM_SUPER_POTION_VRIJHOF);
        testIfCorrectInDatabase(SHOP_ITEM_SUPER_POTION_SPIEGEL.getId(), SHOP_ITEM_SUPER_POTION_SPIEGEL);
        testIfCorrectInDatabase(SHOP_ITEM_SUPER_POTION_ZUL.getId(), SHOP_ITEM_SUPER_POTION_ZUL);
        testIfCorrectInDatabase(SHOP_ITEM_SUPER_POTION_WAAIER.getId(), SHOP_ITEM_SUPER_POTION_WAAIER);
    }

    /**
     * Test if the constant is set correctly in the database. If not save it again.
     *
     * @param shopItemId to search for
     * @param constantShopItem to search for
     */
    public void testIfCorrectInDatabase(int shopItemId, ShopItem constantShopItem) {
        Optional<ShopItem> shopItemOptional = findById(shopItemId);
        if (!(shopItemOptional.isPresent() && shopItemOptional.get().equals(constantShopItem))) {
            shopitemRepository.save(constantShopItem);
        }
    }
}

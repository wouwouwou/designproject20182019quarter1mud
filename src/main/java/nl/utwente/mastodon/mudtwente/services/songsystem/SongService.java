package nl.utwente.mastodon.mudtwente.services.songsystem;

import nl.utwente.mastodon.mudtwente.entities.songsystem.Song;
import nl.utwente.mastodon.mudtwente.repositories.songsystem.SongRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static nl.utwente.mastodon.mudtwente.utils.Constants.*;

/**
 * Service class for a Song.
 */
@Service
public class SongService {

    private SongRepository songRepository;

    @Autowired
    public SongService(SongRepository songRepository) {
        this.songRepository = songRepository;
    }

    /**
     * May find a Song by songId. Returns an Optional<Song> which may contain a valid Song whose
     * songId equals the parameter.
     * @param songId The songId to search for.
     * @return An Optional<Song> which may contain a valid Song whose songId equals the parameter.
     */
    public Optional<Song> findById(int songId) {
        return songRepository.findById(songId);
    }

    /**
     * Saves all Song Constants in the database.
     */
    public void addToDatabase() {
        testIfCorrectInDatabase(INTER_ACTIEF.getId(), INTER_ACTIEF);
        testIfCorrectInDatabase(SINTERKLAAS.getId(), SINTERKLAAS);
        testIfCorrectInDatabase(MADONNA.getId(), MADONNA);
        testIfCorrectInDatabase(HOUSTON.getId(), HOUSTON);
    }

    /**
     * Test if the constant is set correctly in the database. If not save it again.
     * @param songId to search for
     * @param constantSong to search for
     */
    public void testIfCorrectInDatabase(int songId, Song constantSong) {
        Optional<Song> songOptional = findById(songId);
        if (!(songOptional.isPresent() && songOptional.get().equals(constantSong))) {
            songRepository.save(constantSong);
        }
    }
}

package nl.utwente.mastodon.mudtwente.services.fightsystem;

import nl.utwente.mastodon.mudtwente.entities.fightsystem.EnemyType;
import nl.utwente.mastodon.mudtwente.entities.locationsystem.Location;
import nl.utwente.mastodon.mudtwente.repositories.fightsystem.EnemyTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static nl.utwente.mastodon.mudtwente.utils.Constants.*;

/**
 * Service class for a EnemyType.
 */
@Service
public class EnemyTypeService {

    private EnemyTypeRepository enemyTypeRepository;

    @Autowired
    public EnemyTypeService(EnemyTypeRepository enemyTypeRepository) {
        this.enemyTypeRepository = enemyTypeRepository;
    }

    /**
     * May find a EnemyType by location and level. Returns an Optional<EnemyType> which may contain a valid EnemyType whose
     * location and level equals the parameters.
     * @param location The location to search for.
     * @param level The level to search for.
     * @return An Optional<EnemyType> which may contain a valid EnemyType whose location and level equals the parameters.
     */
    public Optional<EnemyType> findByLocationAndLevel(Location location, int level) {
        return enemyTypeRepository.findByLocationAndLevel(location, level);
    }

    public Optional<EnemyType> findById(int id) {
        return enemyTypeRepository.findById(id);
    }

    /**
     * Saves all EnemyType Constants in the database.
     */
    public void addToDatabase() {
        testIfCorrectInDatabase(ENEMY_BIOLOGY.getId(), ENEMY_BIOLOGY);
        testIfCorrectInDatabase(ENEMY_ROM_LANGERAK.getId(), ENEMY_ROM_LANGERAK);
        testIfCorrectInDatabase(ENEMY_KLAAS_SIKKEL.getId(), ENEMY_KLAAS_SIKKEL);
        testIfCorrectInDatabase(ENEMY_DRUNK_MATE.getId(), ENEMY_DRUNK_MATE);
        testIfCorrectInDatabase(ENEMY_BARTENDER.getId(), ENEMY_BARTENDER);
        testIfCorrectInDatabase(ENEMY_IN_HOUSE.getId(), ENEMY_IN_HOUSE);
        testIfCorrectInDatabase(ENEMY_ITALIAN_MAFIA.getId(), ENEMY_ITALIAN_MAFIA);
        testIfCorrectInDatabase(ENEMY_BUS.getId(), ENEMY_BUS);
        testIfCorrectInDatabase(ENEMY_DJMADPROFESSOR.getId(), ENEMY_DJMADPROFESSOR);
    }

    /**
     * Test if the constant is set correctly in the database. If not save it again.
     *
     * @param enemyTypeId to search for
     * @param enemyType to search for
     */
    public void testIfCorrectInDatabase(int enemyTypeId, EnemyType enemyType) {
        Optional<EnemyType> enemyTypeOptional = findById(enemyTypeId);
        if (!(enemyTypeOptional.isPresent() && enemyTypeOptional.get().equals(enemyType))) {
            enemyTypeRepository.save(enemyType);
        }
    }
}

package nl.utwente.mastodon.mudtwente.services.puzzlesystem;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Service class for a Puzzle.
 */
@Service
public class PuzzleService {

    /**
     * Handles the 'checkpuzzle' command and returns a string which contains the message which the bot has to send back.
     * @param mudCharacter MUDCharacter needed for handling the 'checkpuzzle' command.
     * @return A string which contains the message which the bot has to send back.
     */
    public String handleCheckPuzzleCommand(MUDCharacter mudCharacter) {
        return Optional.ofNullable(mudCharacter.getLocation().getPuzzle())
                .map(puzzle -> "More information about the puzzle at this location:\n" + puzzle.getDescription())
                .orElse("There is no puzzle at this location to give more information about!");
    }
}

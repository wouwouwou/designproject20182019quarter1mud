package nl.utwente.mastodon.mudtwente.services.puzzlesystem.puzzle_instances;

import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.puzzle_instances.BinaryPuzzleInstance;
import nl.utwente.mastodon.mudtwente.repositories.puzzlesystem.puzzle_instances.BinaryPuzzleInstanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service class for a BinaryPuzzleInstance.
 */
@Service
public class BinaryPuzzleInstanceService {

    private BinaryPuzzleInstanceRepository binaryPuzzleInstanceRepository;

    @Autowired
    public BinaryPuzzleInstanceService(BinaryPuzzleInstanceRepository binaryPuzzleInstanceRepository) {
        this.binaryPuzzleInstanceRepository = binaryPuzzleInstanceRepository;
    }

    /**
     * Saves the BinaryPuzzleInstance in the database.
     *
     * @param binaryPuzzleInstance BinaryPuzzleInstance to save.
     */
    private void save(BinaryPuzzleInstance binaryPuzzleInstance) {
        binaryPuzzleInstanceRepository.save(binaryPuzzleInstance);
    }

    /**
     * Attempts to pull a lever. Gets the leverId / lightId from words. Checks the correct usage
     * of the 'pull' command. Checks if lever exists. After checks, attempts to pull the lever.
     * After pulling, checks if the status of the Lights give a correct answer. Also
     * checks if puzzle is completed.
     *
     * @param binaryPuzzleInstance BinaryPuzzleInstance needed for the attempt.
     * @param words List of Strings, containing the leverId of the attempt.
     * @return A string with the result of the attempt.
     */
    public String pull(BinaryPuzzleInstance binaryPuzzleInstance,
                       List<String> words) {
        String wrongUsage = "Please use 'pull' <leverNumber> in order to pull a lever!";
        if (words.size() != 1) {
            return wrongUsage;
        }

        int leverId;
        try {
            leverId = Integer.parseInt(words.get(0)) - 1;
        } catch (NumberFormatException e) {
            return wrongUsage;
        }

        if (leverId >= binaryPuzzleInstance.getLights().size() || leverId < 0) {
            return "There are no more than " + binaryPuzzleInstance.getLights().size() +
                    " levers. Please pick an existing lever to pull!";
        }

        binaryPuzzleInstance.getLights().set(leverId,
                !binaryPuzzleInstance.getLights().get(leverId));

        if (!binaryPuzzleInstance.isCorrectLightStatus()) {
            save(binaryPuzzleInstance);
            return "You have pulled lever " + leverId + ".\n\n" +
            binaryPuzzleInstance.getStatus();
        }

        binaryPuzzleInstance.correctLightStatus();
        save(binaryPuzzleInstance);
        String correctLightStatus = "The lights show the correct answer!\n";

        if (binaryPuzzleInstance.isSolved()) {
            return correctLightStatus +
                    "You have solved all the riddles of this puzzle! The most right light throws " +
                    "out a paper that reads: \"" +
                    binaryPuzzleInstance.getRiddlePuzzle().getHintAfterCompletion() + "\"";

        }
        return correctLightStatus +
                binaryPuzzleInstance.getStatus() +
                "\nThe next riddle of this puzzle is: " +
                binaryPuzzleInstance.getRiddlePuzzle().getQuestions().get(
                        binaryPuzzleInstance.getProgress()
                );
    }
}

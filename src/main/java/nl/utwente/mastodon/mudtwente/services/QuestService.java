package nl.utwente.mastodon.mudtwente.services;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.Quest;
import nl.utwente.mastodon.mudtwente.entities.itemsystem.Inventory;
import nl.utwente.mastodon.mudtwente.entities.itemsystem.Item;
import nl.utwente.mastodon.mudtwente.entities.npcsystem.NPC;
import nl.utwente.mastodon.mudtwente.repositories.MUDCharacterRepository;
import nl.utwente.mastodon.mudtwente.repositories.QuestRepository;
import nl.utwente.mastodon.mudtwente.repositories.itemsystem.InventoryRepository;
import nl.utwente.mastodon.mudtwente.services.itemsystem.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Service class for a Quest.
 */
@Service
public class QuestService {

    private QuestRepository questRepository;

    @Autowired
    public QuestService(QuestRepository questRepository) {
        this.questRepository = questRepository;
    }

    /**
     * May find a Quest by mudCharacterId and npcId. Returns an Optional<Quest> which may contain a valid Quest whose
     * mudCharacterId and npcId equals the parameters.
     * @param mudCharacter The mudCharacter to search for.
     * @param npc The npc to search for.
     * @return An Optional<Quest> which may contain a valid Quest whose mudCharacterId and npcId equals the parameters.
     */
    public Optional<Quest> findByMudCharacterAndNpc(MUDCharacter mudCharacter, NPC npc) {
        return questRepository.findByMudCharacterAndNpc(mudCharacter, npc);
    }

    public String handleRequiredItem(Item requiredItem,
                                     InventoryRepository inventoryRepository,
                                     MUDCharacter mudCharacter,
                                     int requiredAmount) {
        InventoryService inventoryService = new InventoryService(inventoryRepository);

        // There is an item required
        if (requiredItem != null) {
            Optional<Inventory> inventoryOptional = inventoryRepository.findByMudCharacterAndItem(mudCharacter, requiredItem);
            if (!inventoryOptional.isPresent() || inventoryOptional.get().getAmount() < requiredAmount) {
                return "You do not have the required items in your inventory!";
            }
            inventoryService.addItemToInventory(mudCharacter, requiredItem, -requiredAmount);
        }
        return "";
    }

    public void handleRewardItem(InventoryRepository inventoryRepository,
                                  MUDCharacter mudCharacter,
                                  Item rewardItem,
                                  int rewardAmount,
                                  boolean increaseStoryLevel,
                                  MUDCharacterRepository mudCharacterRepository) {

        InventoryService inventoryService = new InventoryService(inventoryRepository);

        if (rewardItem != null) {
            inventoryService.addItemToInventory(mudCharacter, rewardItem, rewardAmount);
        }

        if (increaseStoryLevel) {
            mudCharacter.setStoryLevel(mudCharacter.getStoryLevel() + 1);
            mudCharacterRepository.save(mudCharacter);
        }
    }

}

package nl.utwente.mastodon.mudtwente.services.puzzlesystem;

import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.itemsystem.Item;
import nl.utwente.mastodon.mudtwente.entities.puzzlesystem.Safe;
import nl.utwente.mastodon.mudtwente.services.itemsystem.InventoryService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Service class for a Safe.
 */
@Service
public class SafeService {
    
    /**
     * Handles the 'checksafe' command and returns a string which contains the message which the bot has to send back.
     *
     * @param mudCharacter MUDCharacter needed for handling the 'checksafe' command.
     * @return A string which contains the message which the bot has to send back.
     */
    public String handleCheckSafeCommand(MUDCharacter mudCharacter) {
        return Optional.ofNullable(mudCharacter.getLocation().getSafe())
                .map(safe -> "More information about the safe at this location:\n" + safe.getInfo())
                .orElse("There is no safe at this location to give more information about!");
    }

    /**
     * Handles the 'trycode' command and returns a string which contains the message which the bot has to send back.
     *
     * @param mudCharacter MUDCharacter needed for handling the 'trycode' command.
     * @param words The rest of the notification's content (without the command)
     * @param inventoryService InventoryService needed for handling the 'trycode' command.
     * @return A string which contains the message which the bot has to send back.
     */
    public String handleTryCodeCommand(MUDCharacter mudCharacter,
                                       List<String> words,
                                       InventoryService inventoryService) {
        if (words.isEmpty() || words.size() > 1) {
            return "Please follow the 'trycode' command with the code you want to try on the safe. " +
                    "You have not given exactly one code to try.";
        }

        Safe safe = mudCharacter.getLocation().getSafe();
        if (safe == null) {
            return "There is no safe to try your code on at this location!";
        }

        int codeAttempt;
        try {
            codeAttempt = Integer.parseInt(words.get(0));
        } catch (NumberFormatException e) {
            return "Please use a numeric code on the safe!";
        }
        return tryOpen(mudCharacter, safe, codeAttempt, inventoryService);
    }

    /**
     * Method for an attempt to open the Safe. Returns a string containing information about the result of the attempt.
     *
     * @param mudCharacter MUDCharacter to which a possible reward must be given.
     * @param safe The Safe to which the attempt applies.
     * @param codeAttempt The actual attempt.
     * @param inventoryService The service to add the reward item to a MUDCharacter's inventory.
     * @return A string containing information about the result of the attempt.
     */
    public String tryOpen(MUDCharacter mudCharacter,
                          Safe safe,
                          int codeAttempt,
                          InventoryService inventoryService) {
        if (codeAttempt != safe.getPassCode()) {
            return "You input the code... and the safe stays locked! That was the wrong code!";
        }

        Item reward = safe.getItem();
        inventoryService.addItemToInventory(mudCharacter, reward, 1);
        return "The safe opens! Inside, you find " + reward.getName() + ". You add it to your inventory.";
    }
}

package nl.utwente.mastodon.mudtwente.services.npcsystem;

import nl.utwente.mastodon.mudtwente.entities.npcsystem.NPC;
import nl.utwente.mastodon.mudtwente.entities.npcsystem.NPCText;
import nl.utwente.mastodon.mudtwente.repositories.npcsystem.NPCTextRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static nl.utwente.mastodon.mudtwente.utils.Constants.*;

/**
 * Service class for a NPCText.
 */
@Service
public class NPCTextService {

    private NPCTextRepository npcTextRepository;

    @Autowired
    public NPCTextService(NPCTextRepository npcTextRepository) {
        this.npcTextRepository = npcTextRepository;
    }

    /**
     * Finds NPCTexts by npcId. Returns a list of NPCTexts whose npcId equals the npcId parameter.
     * @param npc The npc to search for.
     * @return A list of NPCTexts whose npcId equals the npcId parameter.
     */
    public List<NPCText> findByNpc(NPC npc) {
        return npcTextRepository.findByNpc(npc);
    }

    /**
     *
     * @param id
     * @return
     */
    public Optional<NPCText> findById(int id) {
        return npcTextRepository.findById(id);
    }

    /**
     * Saves all NPCText Constants in the database.
     */
    public void addToDatabase() {
        testIfCorrectInDatabase(BIOLOGY_STUDENT_TEXT_1.getId(), BIOLOGY_STUDENT_TEXT_1);
        testIfCorrectInDatabase(BIOLOGY_STUDENT_TEXT_2.getId(), BIOLOGY_STUDENT_TEXT_2);
        testIfCorrectInDatabase(EXAMINATION_BOARD_TEXT_1.getId(), EXAMINATION_BOARD_TEXT_1);
        testIfCorrectInDatabase(UNCLE_DUO_TEXT_1.getId(), UNCLE_DUO_TEXT_1);
        testIfCorrectInDatabase(EXAMINATION_BOARD_TEXT_2.getId(), EXAMINATION_BOARD_TEXT_2);
        testIfCorrectInDatabase(NANOLAB_STUDENT_TEXT_1.getId(), NANOLAB_STUDENT_TEXT_1);
        testIfCorrectInDatabase(CREATE_STUDENT_TEXT_1.getId(), CREATE_STUDENT_TEXT_1);
        testIfCorrectInDatabase(CREATE_STUDENT_TEXT_2.getId(), CREATE_STUDENT_TEXT_2);
        testIfCorrectInDatabase(NANOLAB_STUDENT_TEXT_2.getId(), NANOLAB_STUDENT_TEXT_2);
        testIfCorrectInDatabase(EXAMINATION_BOARD_TEXT_3.getId(), EXAMINATION_BOARD_TEXT_3);
        testIfCorrectInDatabase(RUGBY_PLAYER_TEXT_1.getId(), RUGBY_PLAYER_TEXT_1);
        testIfCorrectInDatabase(RUGBY_PLAYER_TEXT_2.getId(), RUGBY_PLAYER_TEXT_2);
        testIfCorrectInDatabase(RUGBY_PLAYER_TEXT_3.getId(), RUGBY_PLAYER_TEXT_3);
        testIfCorrectInDatabase(HOUSE_MATE_TEXT_1.getId(), HOUSE_MATE_TEXT_1);
        testIfCorrectInDatabase(DREAM_TEXT_1.getId(), DREAM_TEXT_1);
        testIfCorrectInDatabase(TUTORIAL_INFO_TEXT_1.getId(), TUTORIAL_INFO_TEXT_1);
        testIfCorrectInDatabase(TUTORIAL_COMMANDS_TEXT_1.getId(), TUTORIAL_COMMANDS_TEXT_1);
        testIfCorrectInDatabase(TUTORIAL_DRUNK_MATE_TEXT_1.getId(), TUTORIAL_DRUNK_MATE_TEXT_1);
        testIfCorrectInDatabase(TUTORIAL_DOCTOR_TEXT_1.getId(), TUTORIAL_DOCTOR_TEXT_1);
        testIfCorrectInDatabase(TUTORIAL_CULTURE_TEXT_1.getId(), TUTORIAL_CULTURE_TEXT_1);
        testIfCorrectInDatabase(TUTORIAL_BARTENDER_TEXT_1.getId(), TUTORIAL_BARTENDER_TEXT_1);
        testIfCorrectInDatabase(TUTORIAL_BOSS_TEXT_1.getId(), TUTORIAL_BOSS_TEXT_1);
        testIfCorrectInDatabase(TUTORIAL_EXIT_TEXT_1.getId(), TUTORIAL_EXIT_TEXT_1);
        testIfCorrectInDatabase(EXAMINATION_BOARD_TEXT_4.getId(), EXAMINATION_BOARD_TEXT_4);
        testIfCorrectInDatabase(ITALIAN_MAFIA_MEMBER_TEXT_1.getId(), ITALIAN_MAFIA_MEMBER_TEXT_1);
        testIfCorrectInDatabase(SMART_STUDENT_TEXT_1.getId(), SMART_STUDENT_TEXT_1);
        testIfCorrectInDatabase(BLAKE_TEXT_1.getId(), BLAKE_TEXT_1);
        testIfCorrectInDatabase(SMART_STUDENT_TEXT_2.getId(), SMART_STUDENT_TEXT_2);
        testIfCorrectInDatabase(EXAMINATION_BOARD_TEXT_5.getId(), EXAMINATION_BOARD_TEXT_5);
        testIfCorrectInDatabase(ICTS_TEXT_1.getId(), ICTS_TEXT_1);
        testIfCorrectInDatabase(ICTS_TEXT_2.getId(), ICTS_TEXT_2);
        testIfCorrectInDatabase(JAIR_TEXT_1.getId(), JAIR_TEXT_1);
        testIfCorrectInDatabase(EXAMINATION_BOARD_TEXT_6.getId(), EXAMINATION_BOARD_TEXT_6);
        testIfCorrectInDatabase(ACCOUNTANT_TEXT_1.getId(), ACCOUNTANT_TEXT_1);
        testIfCorrectInDatabase(JAIR_TEXT_2.getId(), JAIR_TEXT_2);
        testIfCorrectInDatabase(ICTS_TEXT_3.getId(), ICTS_TEXT_3);
        testIfCorrectInDatabase(NOBODY_TEXT_1.getId(), NOBODY_TEXT_1);
        testIfCorrectInDatabase(EXAMINATION_BOARD_TEXT_7.getId(), EXAMINATION_BOARD_TEXT_7);
        testIfCorrectInDatabase(DJMADPROFESSOR_TEXT_1.getId(), DJMADPROFESSOR_TEXT_1);
    }

    /**
     * Test if the constant is set correctly in the database. If not save it again.
     *
     * @param npcTextId to search for
     * @param constantNpcText to search for
     */
    public void testIfCorrectInDatabase(int npcTextId, NPCText constantNpcText) {
        Optional<NPCText> npcTextOptional = findById(npcTextId);
        if (!(npcTextOptional.isPresent() && npcTextOptional.get().equals(constantNpcText))) {
            npcTextRepository.save(constantNpcText);
        }
    }
}

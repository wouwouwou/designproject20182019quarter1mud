package nl.utwente.mastodon.mudtwente.services.locationsystem;


import nl.utwente.mastodon.mudtwente.entities.MUDCharacter;
import nl.utwente.mastodon.mudtwente.entities.locationsystem.Location;
import nl.utwente.mastodon.mudtwente.repositories.MUDCharacterRepository;
import nl.utwente.mastodon.mudtwente.repositories.locationsystem.LocationRepository;
import nl.utwente.mastodon.mudtwente.services.MUDCharacterService;
import nl.utwente.mastodon.mudtwente.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static nl.utwente.mastodon.mudtwente.utils.Constants.*;

/**
 * Service class for a Location.
 */
@Service
public class LocationService {

    private LocationRepository locationRepository;

    @Autowired
    public LocationService(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    /**
     * May find a Location by id. Returns an Optional<Location> which may contain a valid Location whose
     * id equals the parameter.
     *
     * @param id The id to search for.
     * @return An Optional<Location> which may contain a valid Location whose id equals the parameter.
     */
    public Optional<Location> findById(int id) {
        return locationRepository.findById(id);
    }

    /**
     * Finds Locations by name. Returns a list of Locations whose name
     * equals the parameter.
     *
     * @param name The name to search for.
     * @return A list of Locations whose name equals the parameter.
     */
    public List<Location> findByName(String name) {
        return locationRepository.findByName(name);
    }

    /**
     * Handles the 'go' command and returns a string which contains the message which the bot has to send back.
     *
     * @param words                 The rest of the notification's content (without the command).
     * @param mudCharacterService   MUDCharacterService needed for handling the 'go' command.
     * @param mudCharacter          MUDCharacter needed for handling the 'go' command.
     * @return                      A string which contains the message which the bot has to send back.
     */
    public String handleGoCommand(List<String> words,
                                  MUDCharacterService mudCharacterService,
                                  MUDCharacter mudCharacter) {
        if (words.isEmpty()) {
            return "Which direction do you want to go?";
        }

        if (words.size() != 1) {
            return "You have incorrectly used the 'go' <direction> command.\n" +
                    "Possible directions are: north, west, south and east.";
        }

        //All directions
        if (words.contains("north") &&
                mudCharacterService.walkTo(mudCharacter, "north", locationRepository) != null) {
            return "Lets go north!";
        } else if (words.contains("west") &&
                mudCharacterService.walkTo(mudCharacter, "west", locationRepository) != null) {
            return "Lets go west!";
        } else if (words.contains("south") &&
                mudCharacterService.walkTo(mudCharacter, "south", locationRepository) != null) {
            return "Lets go south!";
        } else if (words.contains("east") &&
                mudCharacterService.walkTo(mudCharacter, "east", locationRepository) != null) {
            return "Lets go east!";
        }
        return "That direction is not valid!";
    }

    /**
     * Handles the 'location' command and returns a string which contains the message which the bot has to send back.
     *
     * @param mudCharacter MUDCharacter needed for handling the 'location' command.
     * @return A string which contains the message which the bot has to send back.
     */
    public String handleLocationCommand(MUDCharacter mudCharacter) {
        Location currentLocation = mudCharacter.getLocation();
        int north = currentLocation.getNorth();
        int east = currentLocation.getEast();
        int south = currentLocation.getSouth();
        int west = currentLocation.getWest();
        String nothing = "nothing";
        String currentLocationName = StringUtils.setFirstLetterUpperCase(currentLocation.getName());
        String northLocationName = StringUtils.setFirstLetterUpperCase(findById(north).map(Location::getName).orElse(nothing));
        String eastLocationName = StringUtils.setFirstLetterUpperCase(findById(east).map(Location::getName).orElse(nothing));
        String southLocationName = StringUtils.setFirstLetterUpperCase(findById(south).map(Location::getName).orElse(nothing));
        String westLocationName = StringUtils.setFirstLetterUpperCase(findById(west).map(Location::getName).orElse(nothing));
        String puzzleInfo = "";
        if (currentLocation.getPuzzle() != null) {
            puzzleInfo = "\n\nThere is a puzzle at this location: " +
                    currentLocation.getPuzzle().getDescription() +
                    "\nUse the 'checkpuzzle' command to see the information of the puzzle again!";
        }
        String safeInfo = "";
        if (currentLocation.getSafe() != null) {
            safeInfo = "\n\nThere is a safe at this location: \n" +
                    currentLocation.getSafe().getInfo() +
                    "\nUse the 'checkSafe' command to see the information of the safe again!";
        }
        return "Your current location is: " +
                currentLocationName +
                "\nNorth of you is: " +
                northLocationName +
                "\nWest of you is: " +
                westLocationName +
                "\nSouth of you is: " +
                southLocationName +
                "\nEast of you is: " +
                eastLocationName +
                puzzleInfo +
                safeInfo;
    }

    /**
     * Handles the 'teleport' command and returns a string which contains the message which the bot has to send back.
     *
     * @param words The rest of the notification's content (without the command).
     * @param mudCharacter MUDCharacter needed for handling the 'teleport' command.
     * @param mudCharacterRepository MUDCharacterRepository needed for handling the 'teleport' command.
     * @return A string which contains the message which the bot has to send back.
     */
    public String handleTeleportCommand(List<String> words,
                                        MUDCharacter mudCharacter,
                                        MUDCharacterRepository mudCharacterRepository) {
        if (words.size() != 1) {
            return "You have incorrectly used the 'teleport' <locationId> command.";
        }

        int locationId;
        try {
            locationId = Integer.parseInt(words.get(0));
        } catch (NumberFormatException e) {
            return "Please put in a number for the 'teleport' <locationId>  command!";
        }

        if (locationId < 1) {
            return "Please put in a location ID bigger than 0!";
        }

        Optional<Location> locationOptional = findById(locationId);
        if (locationOptional.isPresent()) {
            mudCharacter.setLocation(locationOptional.get());
            mudCharacterRepository.save(mudCharacter);
            return "You teleported correctly to: " +
                    StringUtils.setFirstLetterUpperCase(locationRepository.findById(Integer.valueOf(words.get(0))).map(Location::getName).orElse(""));
        }

        return "A location with this ID does not exist!";
    }

    /**
     * Handles the 'locationinfo' command and returns a string which contains the message which the bot
     * has to send back.
     *
     * @param mudCharacter MUDCharacter needed for handling the 'locationinfo' command.
     * @return A string which contains the message which the bot has to send back.
     */
    public String handleLocationInfoCommand(MUDCharacter mudCharacter) {
        String info = mudCharacter.getLocation().getInfo();
        if (info.equals("")) {
            return "This map has no further information.";
        }
        return info;
    }

    /**
     * Handles the 'whoishere' command and returns a string which contains the message which the bot has to send back.
     *
     * @param mudCharacterService MUDCharacterService needed for handling the 'whoishere' command.
     * @param mudCharacter MUDCharacter needed for handling the 'whoishere' command.
     * @return A string which contains the message which the bot has to send back.
     */
    public String handleWhoIsHereCommand(MUDCharacterService mudCharacterService,
                                         MUDCharacter mudCharacter) {
        return "The users that are online and in your location: " +
                StringUtils.setFirstLetterUpperCase(mudCharacterService
                                    .getAllOnlinePlayersInLocation(
                                            mudCharacter.getLocation())
                        .stream()
                        .map(MUDCharacter::getName)
                        .collect(Collectors.joining(", ")));
    }

    /**
     * Saves all Location Constants in the database.
     */
    public void addToDatabase() {
        testIfCorrectInDatabase(ATHLETICS_FIELD.getId(), ATHLETICS_FIELD);
        testIfCorrectInDatabase(SPORT_CENTER.getId(), SPORT_CENTER);
        testIfCorrectInDatabase(PROMENADE_WEST.getId(), PROMENADE_WEST);
        testIfCorrectInDatabase(COOP.getId(), COOP);
        testIfCorrectInDatabase(PROMENADE_MIDDLE_WEST.getId(), PROMENADE_MIDDLE_WEST);
        testIfCorrectInDatabase(BASTILLE.getId(), BASTILLE);
        testIfCorrectInDatabase(VESTINGBAR.getId(), VESTINGBAR);
        testIfCorrectInDatabase(PROMENADE_MIDDLE_EAST.getId(), PROMENADE_MIDDLE_EAST);
        testIfCorrectInDatabase(PROMENADE_EAST.getId(), PROMENADE_EAST);
        testIfCorrectInDatabase(VRIJHOF.getId(), VRIJHOF);
        testIfCorrectInDatabase(DRIENERLOLAAN_NORTH.getId(), DRIENERLOLAAN_NORTH);
        testIfCorrectInDatabase(DRIENERLOLAAN_SOUTH.getId(), DRIENERLOLAAN_SOUTH);
        testIfCorrectInDatabase(CARILLONVELD.getId(), CARILLONVELD);
        testIfCorrectInDatabase(STUDENT_HOUSING.getId(), STUDENT_HOUSING);
        testIfCorrectInDatabase(DRIENERBEEKLAAN.getId(), DRIENERBEEKLAAN);
        testIfCorrectInDatabase(SPIEGEL.getId(), SPIEGEL);
        testIfCorrectInDatabase(MAIN_ENTRANCE.getId(), MAIN_ENTRANCE);
        testIfCorrectInDatabase(HALLENWEG_WEST.getId(), HALLENWEG_WEST);
        testIfCorrectInDatabase(HALLENWEG_EAST.getId(), HALLENWEG_EAST);
        testIfCorrectInDatabase(GANZENVELD.getId(), GANZENVELD);
        testIfCorrectInDatabase(ZUL_NORTH.getId(), ZUL_NORTH);
        testIfCorrectInDatabase(BOERDERIJLAAN_WEST.getId(), BOERDERIJLAAN_WEST);
        testIfCorrectInDatabase(BOERDERIJLAAN_EAST.getId(), BOERDERIJLAAN_EAST);
        testIfCorrectInDatabase(ZUL_SOUTH.getId(), ZUL_SOUTH);
        testIfCorrectInDatabase(OO_PLAZA.getId(), OO_PLAZA);
        testIfCorrectInDatabase(ZILVERLING.getId(), ZILVERLING);
        testIfCorrectInDatabase(DESIGN_LAB.getId(), DESIGN_LAB);
        testIfCorrectInDatabase(WAAIER.getId(), WAAIER);
        testIfCorrectInDatabase(HORSTTOREN.getId(), HORSTTOREN);
        testIfCorrectInDatabase(NANOLAB.getId(), NANOLAB);

        //Tutorial Locations
        testIfCorrectInDatabase(REGISTRATION_TENT.getId(), REGISTRATION_TENT);
        testIfCorrectInDatabase(CARILLON_TUTORIAL.getId(), CARILLON_TUTORIAL);
        testIfCorrectInDatabase(VESTINGBAR_TUTORIAL.getId(), VESTINGBAR_TUTORIAL);
        testIfCorrectInDatabase(MEDICAL_TENT.getId(), MEDICAL_TENT);
        testIfCorrectInDatabase(CULTURE.getId(), CULTURE);
        testIfCorrectInDatabase(CAMP.getId(), CAMP);
        testIfCorrectInDatabase(PUB.getId(), PUB);
        testIfCorrectInDatabase(EXIT.getId(), EXIT);

        testIfCorrectInDatabase(CUBICUS1.getId(), CUBICUS1);
        testIfCorrectInDatabase(CUBICUS2.getId(), CUBICUS2);
        testIfCorrectInDatabase(CUBICUS3.getId(), CUBICUS3);
        testIfCorrectInDatabase(CUBICUS4.getId(), CUBICUS4);
        testIfCorrectInDatabase(CUBICUS5.getId(), CUBICUS5);
        testIfCorrectInDatabase(CUBICUS6.getId(), CUBICUS6);
        testIfCorrectInDatabase(CUBICUS7.getId(), CUBICUS7);
        testIfCorrectInDatabase(CUBICUS8.getId(), CUBICUS8);
        testIfCorrectInDatabase(CUBICUS9.getId(), CUBICUS9);
    }

    /**
     * Test if the constants is set correctly in the database. If not save it again.
     * @param locationId locationId to search for
     * @param locationConstant location to compare to
     */
    public void testIfCorrectInDatabase(int locationId, Location locationConstant) {
        Optional<Location> locationOptional = findById(locationId);
        if (!(locationOptional.isPresent() && locationOptional.get().equals(locationConstant))) {
            save(locationConstant);
        }
    }

    private void save(Location location) {
        locationRepository.save(location);
    }
}

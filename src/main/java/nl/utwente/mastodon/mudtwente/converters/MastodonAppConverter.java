package nl.utwente.mastodon.mudtwente.converters;

import com.sys1yagi.mastodon4j.api.entity.auth.AppRegistration;
import nl.utwente.mastodon.mudtwente.entities.MastodonApp;
import org.springframework.stereotype.Component;

@Component
public class MastodonAppConverter {

    public MastodonApp fromAppRegistration(AppRegistration appRegistration) {
        MastodonApp result = new MastodonApp();
        result.setId((int) appRegistration.getId());
        result.setClientId(appRegistration.getClientId());
        result.setClientSecret(appRegistration.getClientSecret());
        return result;
    }

}

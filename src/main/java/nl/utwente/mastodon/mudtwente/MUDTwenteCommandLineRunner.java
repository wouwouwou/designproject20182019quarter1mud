package nl.utwente.mastodon.mudtwente;


import com.sys1yagi.mastodon4j.api.Handler;
import com.sys1yagi.mastodon4j.api.entity.Notification;
import com.sys1yagi.mastodon4j.api.entity.Status;
import com.sys1yagi.mastodon4j.api.method.Streaming;
import lombok.extern.slf4j.Slf4j;
import nl.utwente.mastodon.mudtwente.converters.MastodonAppConverter;
import nl.utwente.mastodon.mudtwente.repositories.MUDCharacterRepository;
import nl.utwente.mastodon.mudtwente.repositories.MastodonAppRepository;
import nl.utwente.mastodon.mudtwente.repositories.RepositoryWrapper;
import nl.utwente.mastodon.mudtwente.repositories.fightsystem.EnemyTypeRepository;
import nl.utwente.mastodon.mudtwente.repositories.itemsystem.ItemRepository;
import nl.utwente.mastodon.mudtwente.repositories.itemsystem.ShopitemRepository;
import nl.utwente.mastodon.mudtwente.repositories.locationsystem.LocationRepository;
import nl.utwente.mastodon.mudtwente.repositories.locationsystem.ShopRepository;
import nl.utwente.mastodon.mudtwente.repositories.npcsystem.NPCRepository;
import nl.utwente.mastodon.mudtwente.repositories.npcsystem.NPCTextRepository;
import nl.utwente.mastodon.mudtwente.repositories.songsystem.SongRepository;
import nl.utwente.mastodon.mudtwente.services.fightsystem.EnemyTypeService;
import nl.utwente.mastodon.mudtwente.services.itemsystem.ItemService;
import nl.utwente.mastodon.mudtwente.services.itemsystem.ShopitemService;
import nl.utwente.mastodon.mudtwente.services.locationsystem.LocationService;
import nl.utwente.mastodon.mudtwente.services.locationsystem.ShopService;
import nl.utwente.mastodon.mudtwente.services.npcsystem.NPCService;
import nl.utwente.mastodon.mudtwente.services.npcsystem.NPCTextService;
import nl.utwente.mastodon.mudtwente.services.songsystem.SongService;
import nl.utwente.mastodon.mudtwente.utils.Connection;
import nl.utwente.mastodon.mudtwente.utils.NotificationHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
@Slf4j
public class MUDTwenteCommandLineRunner implements CommandLineRunner {

    private final RepositoryWrapper repositoryWrapper;
    private final MastodonAppRepository mastodonAppRepository;
    private final MUDCharacterRepository mudCharacterRepository;
    private final MastodonAppConverter mastodonAppConverter;

    private List<String> args = new ArrayList<>();
    private Connection connection;

    @Autowired
    public MUDTwenteCommandLineRunner(RepositoryWrapper repositoryWrapper,
                                      MastodonAppRepository mastodonAppRepository,
                                      MUDCharacterRepository mudCharacterRepository,
                                      MastodonAppConverter mastodonAppConverter) {
        this.repositoryWrapper = repositoryWrapper;
        this.mastodonAppRepository = mastodonAppRepository;
        this.mudCharacterRepository = mudCharacterRepository;
        this.mastodonAppConverter = mastodonAppConverter;
    }

    @Override
    public void run(String... args) {
        Collections.addAll(this.args, args);

        if (this.args.size() < 2) {
            throw new IllegalArgumentException("You have entered too little program arguments! \n" +
                    "Please give the following arguments: \n" +
                    "- MUDTwente Mastodon password \n" +
                    "- debug boolean parameter (true = debug, false = live \n" +
                    "- Database url (--DATABASE_URL=<database-url>) \n" +
                    "- Database username (--DATABASE_USERNAME=<database_username>) \n" +
                    "- Database pwd;");
        }

        connection = new Connection(args[0], mastodonAppRepository, mastodonAppConverter);

        LocationRepository locationRepository = repositoryWrapper.getLocationSystemRepositoryWrapper().getLocationRepository();
        NPCRepository npcRepository = repositoryWrapper.getNpcSystemRepositoryWrapper().getNpcRepository();
        ItemRepository itemRepository = repositoryWrapper.getItemSystemRepositoryWrapper().getItemRepository();
        ShopRepository shopRepository = repositoryWrapper.getLocationSystemRepositoryWrapper().getShopRepository();
        ShopitemRepository shopitemRepository = repositoryWrapper.getItemSystemRepositoryWrapper().getShopitemRepository();
        NPCTextRepository npcTextRepository = repositoryWrapper.getNpcSystemRepositoryWrapper().getNpcTextRepository();
        EnemyTypeRepository enemyTypeRepository = repositoryWrapper.getFightSystemRepositoryWrapper().getEnemyTypeRepository();
        SongRepository songRepository = repositoryWrapper.getSongSystemRepositoryWrapper().getSongRepository();

        LocationService locationService = new LocationService(locationRepository);
        NPCService npcService = new NPCService(npcRepository);
        ItemService itemService = new ItemService(itemRepository);
        ShopService shopService = new ShopService(shopRepository);
        ShopitemService shopitemService = new ShopitemService(shopitemRepository);
        NPCTextService npcTextService = new NPCTextService(npcTextRepository);
        EnemyTypeService enemyTypeService = new EnemyTypeService(enemyTypeRepository);
        SongService songService = new SongService(songRepository);

        itemService.addToDatabase();
        locationService.addToDatabase();
        npcService.addToDatabase();
        shopService.addToDatabase();
        shopitemService.addToDatabase();
        npcTextService.addToDatabase();
        enemyTypeService.addToDatabase();
        songService.addToDatabase();

        Handler notificationHandler = new Handler() {
            @Override
            public void onStatus(Status status) {
                log.info("The notificationhandler received an onStatus call." +
                        "Currently we do nothing with this.");
            }

            @Override
            public void onNotification(Notification notification) {
                NotificationHandler.handleNotification(notification,
                        Boolean.parseBoolean(args[1]),
                        connection,
                        repositoryWrapper,
                        mudCharacterRepository);
            }

            @Override
            public void onDelete(long l) {
                log.info("The notificationhandler received an onDelete call." +
                        "Currently we do nothing with this.");
            }
        };

        Streaming streaming = new Streaming(connection.getClient());
        try {
            streaming.user(notificationHandler);
        } catch (Exception e) {
            log.info("Context: ", e);
        }
    }
}

[![pipeline status](https://git.snt.utwente.nl/designproject20182019quarter1mud/designproject20182019quarter1mud/badges/master/pipeline.svg)](https://git.snt.utwente.nl/designproject20182019quarter1mud/designproject20182019quarter1mud/commits/master)
[![coverage report](https://git.snt.utwente.nl/designproject20182019quarter1mud/designproject20182019quarter1mud/badges/master/coverage.svg)](https://git.snt.utwente.nl/designproject20182019quarter1mud/designproject20182019quarter1mud/commits/master)

#MUDTwente
This repository contains the source code behind the bot account @MUDTwente on the Mastodon 
of the University of Twente.

Mastodon is a social network based on open web protocols and free, 
open-source software. The University of Twente is the first Dutch university 
that runs it own Mastodon instance. Currently, there are not a lot of users using 
the Mastodon of the University of Twente. One of the best ways to lure users to a social 
network is to let users tell acquaintances that they use Mastodon. But if there are 
not a lot of active users, how can we increase that number? An option is to lure 
them with an online multiplayer game. We created a Multi-User Dungeon (MUD) game. 
This is how we tried increasing the amount of active users on the Mastodon server 
of the University of Twente. When playing the MUD a player can follow the storyline 
or perform side quests. Since we will use a Mastodon-bot, players can interact with 
other players via Mastodon. We hope that players gain more social contacts and lure 
even more people to the Mastodon network and to the MUD.

## Requirements
* Java 8. Higher java versions reported some issues when compiling with Maven.
* PostgreSQL database
* Maven

## Installation and Getting Started
Option 1: download one of the releases from the pipeline.

Option 2: download the source code and compile the .jar file using maven.

Usage of the jar file:
```
java -jar <filename>.jar <password_bot_account> <debug_flag> --DATABASE_URL=<database_url> 
--DATABASE_USERNAME=<database_username> --DATABASE_PASSWORD=<database_password>
```
You will need a PostgreSQL 10.5 database for the application to connect with. The application will
need port 8080 to be free since it shows some basic statistics. If you are connected via
ssh we recommend using the ```nohup``` command.

When the ```<debug_flag>``` is ```true``` then the application won't post anything on Mastodon.

## Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).


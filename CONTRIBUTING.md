##Database setup:

- Install PostreSQL 10.5
- You might want to install something like DataGrip / pgadmin3 / pgadmin4
- Setup the PostgreSQL user (just use a simple password)
- Create new schema mudtwente
- Run MUDTwenteApplication (it should crash)

Use following Environment Variables in your run configuration:

| Name | Value |
| ------ | ------ |
| DATABASE_URL | jdbc:postgresql://localhost:5432/postgres |
| DATABASE_USERNAME | postgres |
| DATABASE_PASSWORD | [your database password] |

- Run again, it should work now.

##SonarQube setup:
This is not mandatory! We do, however, use the SonarLint plugin of IntelliJ to ensure code quality.
- Download [SonarQube](https://www.sonarqube.org/downloads/) 
  Community Edition
- Extract somewhere (remember where!)
- Put this in your ~/.m2/settings.xml
```xml
<settings>
    <pluginGroups>
        <pluginGroup>org.sonarsource.scanner.maven</pluginGroup>
    </pluginGroups>
    <profiles>
        <profile>
            <id>sonar</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <properties>
                <!-- Optional URL to server. Default value is http://localhost:9000 -->
                <sonar.host.url>
                  http://myserver:9000
                </sonar.host.url>
            </properties>
        </profile>
     </profiles>
</settings>
```
- Run ```[sonarqube_location]/bin/[OS]/sonar.sh console```
- Run ```mvn clean verify sonar:sonar``` in the root folder of the project
- Go to [http://localhost:9000](http://localhost:9000)

##Useful
- We use [Lombok](https://projectlombok.org/features/all).
  Now we don't have to write getters and setters. :D
- Flyway is used to track Database Migrations. Migration files
  have to follow the
  [naming convention](https://flywaydb.org/documentation/migrations#sql-based-migrations)
  and are located in resources/db/migration
